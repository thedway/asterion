-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: asterion_characters
-- ------------------------------------------------------
-- Server version	5.5.49-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_data`
--

DROP TABLE IF EXISTS `account_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_data` (
  `accountId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`accountId`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_data`
--

LOCK TABLES `account_data` WRITE;
/*!40000 ALTER TABLE `account_data` DISABLE KEYS */;
INSERT INTO `account_data` VALUES (1,0,1463090716,'SET flaggedTutorials \"v##M##:##%##&##$##+##Z##1##9##I##D##0##J##V##[##C##7##)##(##\\##K##X##3##Y##,##6##5##Q##-##A##B##=##]##*##^\"\nSET UnitNameOwn \"1\"\nSET fctCombatState \"1\"\nSET fctDodgeParryMiss \"1\"\nSET fctDamageReduction \"1\"\nSET fctRepChanges \"1\"\nSET fctReactives \"1\"\nSET fctFriendlyHealers \"1\"\nSET fctEnergyGains \"1\"\nSET fctPeriodicEnergyGains \"1\"\nSET fctHonorGains \"1\"\nSET fctAuras \"1\"\nSET fctSpellMechanics \"1\"\nSET fctSpellMechanicsOther \"1\"\nSET cameraDistanceMaxFactor \"1\"\nSET talentFrameShown \"1\"\nSET ShowAllSpellRanks \"0\"\n'),(1,4,1463070727,'MACRO 1 \"AutoLearn\" INV_Misc_QuestionMark\r\n/run LoadAddOn\"Blizzard_TrainerUI\" f=ClassTrainerTrainButton f.e = 0 if f:GetScript\"OnUpdate\" then f:SetScript(\"OnUpdate\", nil)else f:SetScript(\"OnUpdate\", function(f,e) f.e=f.e+e if f.e>.01 then f.e=0 f:Click() end end)end\r\nEND\r\n'),(5,0,1463088106,'SET showTargetOfTarget \"1\"\nSET flaggedTutorials \"v##M##%##&##$##[##:##(##K##Y##*##)##7##A##B##X##\\##6##4##+##;##Z##1##9##I##D##0##J##V##,##5##S##T##Q##C##3##^##]##-##?##_##<##O##E##8##.##/##W##>##G\"\nSET UnitNameOwn \"1\"\nSET UnitNameNPC \"1\"\nSET UnitNameNonCombatCreatureName \"1\"\nSET fctCombatState \"1\"\nSET fctDodgeParryMiss \"1\"\nSET fctDamageReduction \"1\"\nSET fctRepChanges \"1\"\nSET fctReactives \"1\"\nSET fctFriendlyHealers \"1\"\nSET playerStatusText \"1\"\nSET petStatusText \"1\"\nSET partyStatusText \"1\"\nSET targetStatusText \"1\"\nSET cameraDistanceMaxFactor \"2\"\nSET showItemLevel \"1\"\nSET showTutorials \"0\"\nSET serviceTypeFilter \"7\"\nSET talentFrameShown \"1\"\nSET ShowAllSpellRanks \"0\"\n'),(5,2,0,''),(5,4,1463061785,'MACRO 9 \"asd\" INV_Misc_QuestionMark\r\n.aura 67541\r\nEND\r\nMACRO 8 \"auto\" INV_Misc_QuestionMark\r\n/run LoadAddOn\"Blizzard_TrainerUI\" f=ClassTrainerTrainButton f.e = 0 if f:GetScript\"OnUpdate\" then f:SetScript(\"OnUpdate\", nil)else f:SetScript(\"OnUpdate\", function(f,e) f.e=f.e+e if f.e>.01 then f.e=0 f:Click() end end)end\r\nEND\r\nMACRO 1 \"bagitems\" INV_Misc_QuestionMark\r\n/run for i=0,4 do for j=1,GetContainerNumSlots(i)do n=(GetContainerItemLink(i,j)or\"\") if(not n:find(\"Junkbox\")) then PickupContainerItem(i,j) DeleteCursorItem()end end end\r\nEND\r\nMACRO 10 \"best\" INV_Misc_QuestionMark\r\n.cooldown\r\nEND\r\nMACRO 3 \"die\" INV_Misc_QuestionMark\r\n.die\r\nEND\r\nMACRO 4 \"dmg\" INV_Misc_QuestionMark\r\n.damage 20000000\r\nEND\r\nMACRO 7 \"mov\" INV_Misc_QuestionMark\r\n.np mov\r\nEND\r\nMACRO 5 \"RANDOM\" INV_Misc_QuestionMark\r\n/castrandom Swift Spectral Tiger, Invincible\r\nEND\r\nMACRO 2 \"restar\" Ability_Creature_Cursed_04\r\n.server shutdown 0\r\nEND\r\nMACRO 6 \"top\" INV_Misc_QuestionMark\r\n.aura 65126\r\n.aura 72525\r\n.aura 64112\r\n.aura 47008\r\n.aura 40733\r\n.aura 64238\r\n.aura 68378\r\n.aura 61715\r\n.mod sp 8\r\n.mod scale 1\r\nEND\r\nMACRO 11 \"untop\" INV_Misc_QuestionMark\r\n.unaura 65126\r\n.unaura 72525\r\n.unaura 64112\r\n.unaura 47008\r\n.unaura 40733\r\n.unaura 64238\r\n.unaura 68378\r\n.unaura 61715\r\n.mod sp 1\r\n.mod scale 1\r\nEND\r\n'),(10,0,1463337330,'SET showTargetOfTarget \"1\"\nSET flaggedTutorials \"v##;##O##:##6##Z##1##9##I##D##0##J##V##[##5##)##^##K##+##,##M##%##&##$##\\##F##X##7##(##S##T##Q##C##A##B##Y##*##4##3##E##8##?##@##.##/\"\nSET guildShowOffline \"0\"\nSET UnitNameOwn \"1\"\nSET UnitNameNPC \"1\"\nSET UnitNameEnemyGuardianName \"1\"\nSET UnitNameEnemyTotemName \"1\"\nSET UnitNameFriendlyGuardianName \"1\"\nSET UnitNameFriendlyTotemName \"1\"\nSET fctCombatState \"1\"\nSET fctDodgeParryMiss \"1\"\nSET fctDamageReduction \"1\"\nSET fctRepChanges \"1\"\nSET fctReactives \"1\"\nSET fctFriendlyHealers \"1\"\nSET fctEnergyGains \"1\"\nSET fctPeriodicEnergyGains \"1\"\nSET fctHonorGains \"1\"\nSET fctSpellMechanics \"1\"\nSET fctSpellMechanicsOther \"1\"\nSET playerStatusText \"1\"\nSET petStatusText \"1\"\nSET partyStatusText \"1\"\nSET targetStatusText \"1\"\nSET cameraView \"3\"\nSET cameraDistanceMaxFactor \"2\"\nSET showItemLevel \"1\"\nSET showTutorials \"0\"\nSET serviceTypeFilter \"1\"\nSET talentFrameShown \"1\"\nSET timeMgrUseLocalTime \"1\"\nSET threatShowNumeric \"1\"\nSET ShowAllSpellRanks \"0\"\n'),(10,4,1463340172,'MACRO 1 \"...\" INV_Misc_QuestionMark\r\n/run LoadAddOn\"Blizzard_TrainerUI\" f=ClassTrainerTrainButton f.e = 0 if f:GetScript\"OnUpdate\" then f:SetScript(\"OnUpdate\", nil)else f:SetScript(\"OnUpdate\", function(f,e) f.e=f.e+e if f.e>.01 then f.e=0 f:Click() end end)end ;\r\nEND\r\nMACRO 46 \"ASTERION\" INV_Misc_QuestionMark\r\n/castrandom Swift Spectral Tiger, Invincible\r\nEND\r\nMACRO 49 \"charge\" INV_Misc_QuestionMark\r\n#showtooltip Charge\r\n/cast charge\r\n/use Wrathful Gladiator\'s Plate Gauntlets\r\nEND\r\nMACRO 36 \"CL\" INV_Misc_QuestionMark\r\n/startattack\r\n/cast Chain Lightning(Rank 8)\r\nEND\r\nMACRO 40 \"cl1\" INV_Misc_QuestionMark\r\n/cast [target=party1] Cleanse Spirit\r\nEND\r\nMACRO 41 \"cl2\" INV_Misc_QuestionMark\r\n/cast [target=party2] Cleanse spirit\r\nEND\r\nMACRO 39 \"cleans\" INV_Misc_QuestionMark\r\n/cast [target=player] Cleanse spirit\r\nEND\r\nMACRO 38 \"CRTX\" INV_Misc_QuestionMark\r\n/cast [target=party1] Cure Toxins\r\nEND\r\nMACRO 47 \"FSWAP\" INV_Misc_QuestionMark\r\n/equip Glorenzelg, High-Blade of the Silver Hand\r\n/equip Glorenzelg, High-Blade of the Silver Hand\r\nEND\r\nMACRO 37 \"GOTN\" INV_Misc_QuestionMark\r\n/cast [target=party1] Gift of the Naaru(Racial)\r\nEND\r\nMACRO 34 \"GT\" INV_Misc_QuestionMark\r\n/stopcasting\r\n/cast Grounding Totem\r\nEND\r\nMACRO 20 \"GUILD\" Ability_Creature_Disease_02\r\n/4 gilda iz gonna snipe you nabera typkov s 2,7+ ratom, ziadnych egokokotov. mame erekciuzarucujuci gh v nagrande a mongoloidny kolektiv /whisp\r\nEND\r\nMACRO 26 \"heal1\" INV_Misc_QuestionMark\r\n/cast [target=party1]Healing Wave(Rank 14)\r\nEND\r\nMACRO 15 \"hero\" INV_Misc_QuestionMark\r\n/cast Heroism\r\n/y ALAH AKBAR <3\r\nEND\r\nMACRO 33 \"hex\" INV_Misc_QuestionMark\r\n/cast [target=focus] Hex\r\nEND\r\nMACRO 18 \"horror\" INV_Misc_QuestionMark\r\n/cast [target=focus] Psychic Horror\r\nEND\r\nMACRO 16 \"instant\" INV_Misc_QuestionMark\r\n/cast Elemental Mastery\r\n/use K1 - Scale of Fates\r\nEND\r\nMACRO 31 \"ja\" INV_Misc_QuestionMark\r\n/cast [target=player] Lesser Healing Wave(Rank 9)\r\nEND\r\nMACRO 35 \"LB\" INV_Misc_QuestionMark\r\n/startattack\r\n/cast Lava Burst(Rank 2)\r\n/use Wrathful Gladiator\'s Ringmail Gauntlets\r\nEND\r\nMACRO 30 \"less2\" INV_Misc_QuestionMark\r\n/cast [target=party2] Lesser Healing Wave(Rank 9)\r\nEND\r\nMACRO 25 \"lesser1\" INV_Misc_QuestionMark\r\n/cast [target=party1] Lesser Healing Wave(Rank 9)\r\nEND\r\nMACRO 45 \"MOUNT\" INV_Misc_QuestionMark\r\n/castrandom Swift Zulian Tiger, Mekgineer\'s Chopper\r\nEND\r\nMACRO 11 \"poly\" INV_Misc_QuestionMark\r\n/cast [target=focus] Silence\r\nEND\r\nMACRO 24 \"rip1\" INV_Misc_QuestionMark\r\n/cast [target=party1] Riptide(Rank 4)\r\nEND\r\nMACRO 43 \"RIP1\" INV_Misc_QuestionMark\r\n/cast [target=party1] Riptide(Rank 4)\r\nEND\r\nMACRO 8 \"sap\" INV_Misc_QuestionMark\r\n/targetenemy\r\n/cast Sap\r\nEND\r\nMACRO 42 \"tide\" INV_Misc_QuestionMark\r\n/cast Mana Tide Totem\r\n/p TIDE TIDE TIDE\r\nEND\r\nMACRO 17 \"ws\" INV_Misc_QuestionMark\r\n/stopcasting\r\n/cast Wind Shear\r\nEND\r\nMACRO 32 \"wsf\" INV_Misc_QuestionMark\r\n/stopcasting\r\n/cast [target=focus] Wind Shear\r\nEND\r\n'),(11,0,1463199921,'SET showTargetOfTarget \"1\"\nSET rotateMinimap \"1\"\nSET displayFreeBagSlots \"1\"\nSET flaggedTutorials \"v##E##P##M##%##&##$##:##(##Z##1##9##I##D##0##J##V##[##+##;##K##)##?##-##A##B##\\##7##6##5##X##C##Y##,##4##O##S##T##Q##*##<##H##G##3##F##^##]##8##.##/\"\nSET guildShowOffline \"0\"\nSET UnitNameOwn \"1\"\nSET fctCombatState \"1\"\nSET fctDodgeParryMiss \"1\"\nSET fctDamageReduction \"1\"\nSET fctRepChanges \"1\"\nSET fctReactives \"1\"\nSET fctFriendlyHealers \"1\"\nSET fctEnergyGains \"1\"\nSET fctPeriodicEnergyGains \"1\"\nSET fctHonorGains \"1\"\nSET fctAuras \"1\"\nSET fctSpellMechanics \"1\"\nSET fctSpellMechanicsOther \"1\"\nSET xpBarText \"1\"\nSET playerStatusText \"1\"\nSET petStatusText \"1\"\nSET partyStatusText \"1\"\nSET targetStatusText \"1\"\nSET cameraPitchMoveSpeed \"135\"\nSET cameraYawMoveSpeed \"270\"\nSET cameraView \"3\"\nSET cameraDistanceMaxFactor \"1\"\nSET showItemLevel \"1\"\nSET showTutorials \"0\"\nSET talentFrameShown \"1\"\nSET threatShowNumeric \"1\"\nSET ShowAllSpellRanks \"0\"\n'),(11,2,1463016054,'BINDINGMODE 0\r\nbind BUTTON3 ACTIONBUTTON11\r\nbind 6 NONE\r\nbind 7 NONE\r\nbind 8 NONE\r\nbind 9 NONE\r\nbind 0 NONE\r\nbind - NONE\r\nbind = NONE\r\nbind SHIFT-1 CLICK BT4Button14:LeftButton\r\nbind SHIFT-2 CLICK BT4Button15:LeftButton\r\nbind SHIFT-3 CLICK BT4Button16:LeftButton\r\nbind SHIFT-4 CLICK BT4Button17:LeftButton\r\nbind SHIFT-V ACTIONBUTTON12\r\nbind CTRL-W CLICK BT4Button19:LeftButton\r\nbind ; CLICK BT4Button13:LeftButton\r\nbind ALT-1 CLICK BT4Button18:LeftButton\r\nbind CTRL-BUTTON3 CLICK BT4Button20:LeftButton\r\nbind ALT-MOUSEWHEELDOWN CLICK BT4Button21:LeftButton\r\nbind ALT-3 CLICK BT4Button22:LeftButton\r\nbind ALT-E ACTIONBUTTON10\r\nbind SHIFT-Q ACTIONBUTTON6\r\nbind SHIFT-E ACTIONBUTTON7\r\nbind ALT-Q ACTIONBUTTON9\r\nbind SHIFT-BUTTON3 ACTIONBUTTON8\r\nbind ALT-W CLICK BT4Button49:LeftButton\r\nbind ALT-CTRL-W CLICK BT4Button37:LeftButton\r\n'),(11,4,1463016055,'MACRO 13 \"1HS\" INV_Misc_QuestionMark\r\n#show\r\n/equipslot 16 Wrathful Gladiator\'s Handaxe\r\n/equipslot 17 Wrathful Gladiator\'s Shield Wall\r\nEND\r\nMACRO 15 \"2HnS\" INV_Misc_QuestionMark\r\n#show\r\n/equipslot 16 Shadowmourne\r\nEND\r\nMACRO 18 \"@2hčka\" INV_Misc_QuestionMark\r\nEND\r\nMACRO 9 \"asd\" INV_Misc_QuestionMark\r\n/roll 100\r\nEND\r\nMACRO 16 \"Blood Elfka\" INV_Misc_QuestionMark\r\n/unequip blood elfka\r\n/equip blood elfka\r\nEND\r\nMACRO 5 \"CameraDistance\" INV_Misc_QuestionMark\r\n/console cameraDistanceMax 50\r\nEND\r\nMACRO 6 \"CancelHoP\" Ability_Creature_Cursed_05\r\n/cancelaura Hands of Protection\r\nEND\r\nMACRO 7 \"Duel\" Ability_Creature_Poison_05\r\n/duel\r\nEND\r\nMACRO 17 \"Learn\" INV_Misc_QuestionMark\r\n/run LoadAddOn\"Blizzard_TrainerUI\" f=ClassTrainerTrainButton f.e = 0 if f:GetScript\"OnUpdate\" then f:SetScript(\"OnUpdate\", nil)else f:SetScript(\"OnUpdate\", function(f,e) f.e=f.e+e if f.e>.01 then f.e=0 f:Click() end end)end ;\r\nEND\r\nMACRO 11 \"MSG\" INV_Misc_QuestionMark\r\n/in 1 /run if UnitExists\"target\"then SendChatMessage(\"YOUR TEXT MESSAGE GOES HERE\",\"WHISPER\",nil,UnitName\"target\")end\r\nEND\r\nMACRO 10 \"specza spect pal\" INV_Misc_QuestionMark\r\n.sp p niskala\r\nEND\r\nMACRO 12 \"test\" Ability_Ambush\r\n/run for i=1,GetNumAuctionItems(\"owner\") do saleStatus = select(13,GetAuctionItemInfo(\"owner\", i)) if saleStatus~=1 then CancelAuction(i) end end\r\nEND\r\nMACRO 8 \"Yield\" Ability_Creature_Poison_04\r\n/yield\r\nEND\r\n'),(315,0,1462710530,'SET flaggedTutorials \"v##M##:\"\nSET cameraDistanceMaxFactor \"1\"\n'),(10000,0,1462970499,'SET flaggedTutorials \"v##M##:\"\nSET CombatDamage \"0\"\nSET CombatHealing \"0\"\nSET enableCombatText \"0\"\nSET cameraDistanceMaxFactor \"1\"\n');
/*!40000 ALTER TABLE `account_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_instance_times`
--

DROP TABLE IF EXISTS `account_instance_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_instance_times` (
  `accountId` int(10) unsigned NOT NULL,
  `instanceId` int(10) unsigned NOT NULL DEFAULT '0',
  `releaseTime` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`,`instanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_instance_times`
--

LOCK TABLES `account_instance_times` WRITE;
/*!40000 ALTER TABLE `account_instance_times` DISABLE KEYS */;
INSERT INTO `account_instance_times` VALUES (1,3,1463092538),(5,5,1463419238),(10,4,1463342205),(11,4,1463342060);
/*!40000 ALTER TABLE `account_instance_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_tutorial`
--

DROP TABLE IF EXISTS `account_tutorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_tutorial` (
  `accountId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier',
  `tut0` int(10) unsigned NOT NULL DEFAULT '0',
  `tut1` int(10) unsigned NOT NULL DEFAULT '0',
  `tut2` int(10) unsigned NOT NULL DEFAULT '0',
  `tut3` int(10) unsigned NOT NULL DEFAULT '0',
  `tut4` int(10) unsigned NOT NULL DEFAULT '0',
  `tut5` int(10) unsigned NOT NULL DEFAULT '0',
  `tut6` int(10) unsigned NOT NULL DEFAULT '0',
  `tut7` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_tutorial`
--

LOCK TABLES `account_tutorial` WRITE;
/*!40000 ALTER TABLE `account_tutorial` DISABLE KEYS */;
INSERT INTO `account_tutorial` VALUES (1,3798905847,125051617,0,0,0,0,0,0),(5,1828634615,91499245,0,0,0,0,0,0),(10,4620503,32514688,0,0,0,0,0,0),(11,1626259447,32776929,0,0,0,0,0,0),(315,4194304,512,0,0,0,0,0,0),(10000,4194304,512,0,0,0,0,0,0);
/*!40000 ALTER TABLE `account_tutorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addons`
--

DROP TABLE IF EXISTS `addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addons` (
  `name` varchar(120) NOT NULL DEFAULT '',
  `crc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Addons';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addons`
--

LOCK TABLES `addons` WRITE;
/*!40000 ALTER TABLE `addons` DISABLE KEYS */;
INSERT INTO `addons` VALUES ('Blizzard_AchievementUI',1276933997),('Blizzard_ArenaUI',1276933997),('Blizzard_AuctionUI',1276933997),('Blizzard_BarbershopUI',1276933997),('Blizzard_BattlefieldMinimap',1276933997),('Blizzard_BindingUI',1276933997),('Blizzard_Calendar',1276933997),('Blizzard_CombatLog',1276933997),('Blizzard_CombatText',1276933997),('Blizzard_DebugTools',1276933997),('Blizzard_GlyphUI',1276933997),('Blizzard_GMChatUI',1276933997),('Blizzard_GMSurveyUI',1276933997),('Blizzard_GuildBankUI',1276933997),('Blizzard_InspectUI',1276933997),('Blizzard_ItemSocketingUI',1276933997),('Blizzard_MacroUI',1276933997),('Blizzard_RaidUI',1276933997),('Blizzard_TalentUI',1276933997),('Blizzard_TimeManager',1276933997),('Blizzard_TokenUI',1276933997),('Blizzard_TradeSkillUI',1276933997),('Blizzard_TrainerUI',1276933997);
/*!40000 ALTER TABLE `addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arena_team`
--

DROP TABLE IF EXISTS `arena_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arena_team` (
  `arenaTeamId` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(24) NOT NULL,
  `captainGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rating` smallint(5) unsigned NOT NULL DEFAULT '0',
  `seasonGames` smallint(5) unsigned NOT NULL DEFAULT '0',
  `seasonWins` smallint(5) unsigned NOT NULL DEFAULT '0',
  `weekGames` smallint(5) unsigned NOT NULL DEFAULT '0',
  `weekWins` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `backgroundColor` int(10) unsigned NOT NULL DEFAULT '0',
  `emblemStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emblemColor` int(10) unsigned NOT NULL DEFAULT '0',
  `borderStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `borderColor` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`arenaTeamId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arena_team`
--

LOCK TABLES `arena_team` WRITE;
/*!40000 ALTER TABLE `arena_team` DISABLE KEYS */;
/*!40000 ALTER TABLE `arena_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arena_team_member`
--

DROP TABLE IF EXISTS `arena_team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arena_team_member` (
  `arenaTeamId` int(10) unsigned NOT NULL DEFAULT '0',
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `weekGames` smallint(5) unsigned NOT NULL DEFAULT '0',
  `weekWins` smallint(5) unsigned NOT NULL DEFAULT '0',
  `seasonGames` smallint(5) unsigned NOT NULL DEFAULT '0',
  `seasonWins` smallint(5) unsigned NOT NULL DEFAULT '0',
  `personalRating` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`arenaTeamId`,`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arena_team_member`
--

LOCK TABLES `arena_team_member` WRITE;
/*!40000 ALTER TABLE `arena_team_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `arena_team_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auctionhouse`
--

DROP TABLE IF EXISTS `auctionhouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auctionhouse` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `houseid` tinyint(3) unsigned NOT NULL DEFAULT '7',
  `itemguid` int(10) unsigned NOT NULL DEFAULT '0',
  `itemowner` int(10) unsigned NOT NULL DEFAULT '0',
  `buyoutprice` int(10) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `buyguid` int(10) unsigned NOT NULL DEFAULT '0',
  `lastbid` int(10) unsigned NOT NULL DEFAULT '0',
  `startbid` int(10) unsigned NOT NULL DEFAULT '0',
  `deposit` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_guid` (`itemguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctionhouse`
--

LOCK TABLES `auctionhouse` WRITE;
/*!40000 ALTER TABLE `auctionhouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `auctionhouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_addons`
--

DROP TABLE IF EXISTS `banned_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_addons` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Version` varchar(255) NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `idx_name_ver` (`Name`,`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_addons`
--

LOCK TABLES `banned_addons` WRITE;
/*!40000 ALTER TABLE `banned_addons` DISABLE KEYS */;
/*!40000 ALTER TABLE `banned_addons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `battleground_deserters`
--

DROP TABLE IF EXISTS `battleground_deserters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `battleground_deserters` (
  `guid` int(10) unsigned NOT NULL COMMENT 'characters.guid',
  `type` tinyint(3) unsigned NOT NULL COMMENT 'type of the desertion',
  `datetime` datetime NOT NULL COMMENT 'datetime of the desertion'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `battleground_deserters`
--

LOCK TABLES `battleground_deserters` WRITE;
/*!40000 ALTER TABLE `battleground_deserters` DISABLE KEYS */;
/*!40000 ALTER TABLE `battleground_deserters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugreport`
--

DROP TABLE IF EXISTS `bugreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugreport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `type` longtext NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Debug System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugreport`
--

LOCK TABLES `bugreport` WRITE;
/*!40000 ALTER TABLE `bugreport` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_events`
--

DROP TABLE IF EXISTS `calendar_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_events` (
  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `creator` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '4',
  `dungeon` int(10) NOT NULL DEFAULT '-1',
  `eventtime` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `time2` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_events`
--

LOCK TABLES `calendar_events` WRITE;
/*!40000 ALTER TABLE `calendar_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_invites`
--

DROP TABLE IF EXISTS `calendar_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_invites` (
  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `event` bigint(20) unsigned NOT NULL DEFAULT '0',
  `invitee` int(10) unsigned NOT NULL DEFAULT '0',
  `sender` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `statustime` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `text` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_invites`
--

LOCK TABLES `calendar_invites` WRITE;
/*!40000 ALTER TABLE `calendar_invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `name` varchar(128) NOT NULL,
  `team` int(10) unsigned NOT NULL,
  `announce` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ownership` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `password` varchar(32) DEFAULT NULL,
  `bannedList` text,
  `lastUsed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`,`team`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Channel System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES ('world',469,1,1,'','',1465678913);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `char_proposal`
--

DROP TABLE IF EXISTS `char_proposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `char_proposal` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `p_char` int(10) NOT NULL DEFAULT '0',
  `s_char` int(10) NOT NULL DEFAULT '0',
  `p_id` int(10) NOT NULL DEFAULT '0',
  `s_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`s_char`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `char_proposal`
--

LOCK TABLES `char_proposal` WRITE;
/*!40000 ALTER TABLE `char_proposal` DISABLE KEYS */;
/*!40000 ALTER TABLE `char_proposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `char_trade_logs`
--

DROP TABLE IF EXISTS `char_trade_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `char_trade_logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Propose_char` int(10) DEFAULT NULL,
  `Seller_char` int(10) DEFAULT NULL,
  `Propose_acc` int(10) DEFAULT NULL,
  `Seller_acc` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `char_trade_logs`
--

LOCK TABLES `char_trade_logs` WRITE;
/*!40000 ALTER TABLE `char_trade_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `char_trade_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_account_data`
--

DROP TABLE IF EXISTS `character_account_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_account_data` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`guid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_account_data`
--

LOCK TABLES `character_account_data` WRITE;
/*!40000 ALTER TABLE `character_account_data` DISABLE KEYS */;
INSERT INTO `character_account_data` VALUES (1,1,1463200192,'SET minimapZoom \"1\"\nSET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"5.004004\"\nSET cameraSavedVehicleDistance \"50.000000\"\nSET cameraSavedPitch \"27.149982\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_MELEE_COMBAT\"\nSET showTokenFrame \"1\"\nSET miniWorldMap \"1\"\n'),(1,5,1463001155,'MACRO 16777218 \"Reload ELUNA\" INV_Misc_QuestionMark\r\n.reload eluna\r\nEND\r\nMACRO 16777217 \"restart 0\" INV_Misc_QuestionMark\r\n.server restart 3 Yolo:)\r\nEND\r\n'),(1,7,1463200193,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 2097155\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(2,1,1465678785,'SET equipmentManager \"1\"\nSET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"24.993624\"\nSET cameraSavedVehicleDistance \"49.999371\"\nSET cameraSavedPitch \"32.100011\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_DEFENSES\"\nSET showTokenFrame \"1\"\nSET previewTalents \"1\"\nSET miniWorldMap \"1\"\n'),(2,5,1462730190,'MACRO 16777223 \"allin\" INV_Misc_QuestionMark\r\n.gm i\r\n.server info\r\n.ticket list\r\nEND\r\nMACRO 16777224 \"asd\" INV_Misc_QuestionMark\r\n.unaura 69700\r\n.unaura 70157\r\n.unaura 71665\r\nEND\r\nMACRO 16777225 \"frostmourne\" INV_Misc_QuestionMark\r\n.aura 72405\r\nEND\r\nMACRO 16777222 \"gmi\" INV_Misc_QuestionMark\r\n.gm i\r\nEND\r\nMACRO 16777219 \"np\" INV_Misc_QuestionMark\r\n.np del\r\nEND\r\nMACRO 16777221 \"sinfo\" INV_Misc_QuestionMark\r\n.server info\r\nEND\r\nMACRO 16777220 \"TLIST\" INV_Misc_QuestionMark\r\n.ticket list\r\nEND\r\n'),(2,7,1462730191,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nworld 1 5\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 Y\nPARTY 170 170 255 Y\nRAID 255 127 0 Y\nGUILD 64 255 64 Y\nOFFICER 64 192 64 Y\nYELL 255 64 64 Y\nWHISPER 255 128 255 Y\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 Y\nEMOTE 255 128 64 Y\nTEXT_EMOTE 255 128 64 Y\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 Y\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 Y\nDND 255 128 255 Y\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 Y\nRAID_WARNING 255 72 0 Y\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 Y\nBATTLEGROUND_LEADER 255 219 183 Y\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 Y\nGUILD_ACHIEVEMENT 64 255 64 Y\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 Y\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 Y\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 Y\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 Y\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 12\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nworld\nEND\n\nZONECHANNELS 2097155\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nNAME world\nSIZE 12\nCOLOR 0 0 0 63\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 3\nSHOWN 0\nMESSAGES\nCHANNEL\nEND\n\nCHANNELS\nworld\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nNAME Party\nSIZE 12\nCOLOR 0 0 0 63\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 4\nSHOWN 0\nPOSITION BOTTOMLEFT 0.025622 0.143229\nDIMENSIONS 430.000031 120.000015\n\nMESSAGES\nPARTY\nPARTY_LEADER\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nNAME Guild\nSIZE 12\nCOLOR 0 0 0 63\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 5\nSHOWN 0\nMESSAGES\nGUILD\nGUILD_ACHIEVEMENT\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(4,1,1462779532,'SET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"10.548000\"\nSET cameraSavedPitch \"24.550005\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_SPELL_COMBAT\"\n'),(4,7,1462779531,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nEND\n\nZONECHANNELS 2097155\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(5,1,1463065980,'SET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"19.544403\"\nSET cameraSavedPitch \"30.149859\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_MELEE_COMBAT\"\n'),(5,7,1463065981,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 14\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nEND\n\nZONECHANNELS 2097155\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(6,1,1462970500,'SET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"5.550000\"\nSET cameraSavedPitch \"10.000000\"\nSET nameplateShowEnemies \"1\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_MELEE_COMBAT\"\nSET ShowClassColorInNameplate \"1\"\n'),(6,7,1462970501,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nEND\n\nZONECHANNELS 35651587\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(7,1,1463337469,'SET equipmentManager \"1\"\nSET minimapInsideZoom \"0\"\nSET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"8.996400\"\nSET cameraSavedPitch \"11.337431\"\nSET nameplateShowEnemies \"1\"\nSET playerStatLeftDropdown \"PLAYERSTAT_DEFENSES\"\nSET playerStatRightDropdown \"PLAYERSTAT_SPELL_COMBAT\"\nSET showTokenFrame \"1\"\nSET showTokenFrameHonor \"1\"\nSET previewTalents \"1\"\nSET miniWorldMap \"1\"\n'),(7,3,1463016069,'BINDINGMODE 0\r\nbind 6 NONE\r\nbind 7 NONE\r\nbind 8 NONE\r\nbind 9 NONE\r\nbind 0 NONE\r\nbind - NONE\r\nbind = NONE\r\nbind CTRL-F1 CLICK BT4Button45:LeftButton\r\nbind CTRL-1 CLICK BT4Button31:LeftButton\r\nbind CTRL-2 CLICK BT4Button32:LeftButton\r\nbind SHIFT-1 CLICK BT4Button26:LeftButton\r\nbind SHIFT-2 CLICK BT4Button27:LeftButton\r\nbind SHIFT-3 CLICK BT4Button28:LeftButton\r\nbind SHIFT-4 CLICK BT4Button29:LeftButton\r\nbind SHIFT-5 CLICK BT4Button30:LeftButton\r\nbind SHIFT-MOUSEWHEELUP CLICK BT4Button21:LeftButton\r\nbind SHIFT-MOUSEWHEELDOWN ACTIONBUTTON12\r\nbind SHIFT-V ACTIONBUTTON8\r\nbind SHIFT-T ACTIONBUTTON11\r\nbind SHIFT-C CLICK BT4Button33:LeftButton\r\nbind CTRL-R ACTIONBUTTON7\r\nbind CTRL-Q CLICK BT4Button19:LeftButton\r\nbind CTRL-E CLICK BT4Button20:LeftButton\r\nbind CTRL-BUTTON3 ACTIONBUTTON6\r\nbind SHIFT-Q ACTIONBUTTON9\r\nbind SHIFT-E ACTIONBUTTON10\r\nbind ALT-1 CLICK BT4Button15:LeftButton\r\nbind ALT-2 CLICK BT4Button16:LeftButton\r\nbind ; CLICK BT4Button13:LeftButton\r\nbind SHIFT-BUTTON3 CLICK BT4Button25:LeftButton\r\nbind ALT-S CLICK BT4Button34:LeftButton\r\nbind SHIFT-X CLICK BT4Button35:LeftButton\r\nbind ALT-CTRL-W CLICK BT4Button38:LeftButton\r\nbind CTRL-C CLICK BT4Button24:LeftButton\r\nbind ALT-E CLICK BT4Button18:LeftButton\r\nbind ALT-Q CLICK BT4Button17:LeftButton\r\nbind SHIFT-F CLICK BT4Button14:LeftButton\r\nbind ALT-3 CLICK BT4Button22:LeftButton\r\nbind ALT-4 CLICK BT4Button23:LeftButton\r\nbind ALT-W CLICK BT4Button49:LeftButton\r\nbind ALT-SHIFT-F1 CLICK BT4Button51:LeftButton\r\nbind ALT-SHIFT-F2 CLICK BT4Button39:LeftButton\r\nbind ALT-F1 CLICK BT4Button48:LeftButton\r\nbind ALT-F2 CLICK BT4Button47:LeftButton\r\nbind ALT-F3 CLICK BT4Button46:LeftButton\r\nbind ALT-SHIFT-1 CLICK BT4Button60:LeftButton\r\nbind ALT-SHIFT-2 CLICK BT4Button59:LeftButton\r\nbind ALT-SHIFT-3 CLICK BT4Button58:LeftButton\r\nbind ALT-SHIFT-W CLICK BT4Button37:LeftButton\r\n'),(7,5,1463016070,'MACRO 16777217 \"fckinnerdox\" INV_Misc_QuestionMark\r\n.aura 65126\r\n.aura 72525\r\n.aura 64112\r\n.aura 47008\r\n.aura 40733\r\n.aura 64238\r\n.aura 68378\r\n.aura 61715\r\n.mod sp 8\r\nEND\r\n'),(7,7,1463016071,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nworld 1 6\nEND\n\nZONECHANNELS 52428803\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 Y\nPARTY 170 170 255 Y\nRAID 255 127 0 Y\nGUILD 64 255 64 Y\nOFFICER 64 192 64 Y\nYELL 255 64 64 Y\nWHISPER 255 128 255 Y\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 Y\nEMOTE 255 128 64 Y\nTEXT_EMOTE 255 128 64 Y\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 Y\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 Y\nDND 255 128 255 Y\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 Y\nRAID_WARNING 255 72 0 Y\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 Y\nBATTLEGROUND_LEADER 255 219 183 Y\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 Y\nGUILD_ACHIEVEMENT 64 255 64 Y\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 Y\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 Y\nCHANNEL2 255 192 192 Y\nCHANNEL3 255 192 192 Y\nCHANNEL4 255 192 192 Y\nCHANNEL5 255 192 192 Y\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 12\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nworld\nEND\n\nZONECHANNELS 18874371\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(10,1,1463489925,'SET equipmentManager \"1\"\nSET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"19.544401\"\nSET cameraSavedPitch \"30.000001\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_SPELL_COMBAT\"\nSET showTokenFrame \"1\"\nSET previewTalents \"1\"\n'),(10,3,1463074489,'BINDINGMODE 0\r\nbind BUTTON3 MULTIACTIONBAR3BUTTON6\r\nbind Q MULTIACTIONBAR2BUTTON6\r\nbind E MULTIACTIONBAR2BUTTON7\r\nbind X ACTIONBUTTON9\r\nbind Z MULTIACTIONBAR1BUTTON8\r\nbind BUTTON4 MULTIACTIONBAR3BUTTON1\r\nbind PAGEUP CAMERAZOOMIN\r\nbind PAGEDOWN CAMERAZOOMOUT\r\nbind R MULTIACTIONBAR1BUTTON1\r\nbind SHIFT-R MULTIACTIONBAR1BUTTON11\r\nbind 6 NONE\r\nbind 7 ACTIONBUTTON12\r\nbind 9 NONE\r\nbind 0 NONE\r\nbind - NONE\r\nbind = NONE\r\nbind SHIFT-1 NONE\r\nbind SHIFT-2 NONE\r\nbind SHIFT-3 NONE\r\nbind SHIFT-4 NONE\r\nbind SHIFT-5 NONE\r\nbind SHIFT-6 NONE\r\nbind SHIFT-MOUSEWHEELDOWN MULTIACTIONBAR4BUTTON6\r\nbind G MULTIACTIONBAR1BUTTON10\r\nbind F ACTIONBUTTON6\r\nbind V ACTIONBUTTON11\r\nbind SHIFT-V ACTIONBUTTON10\r\nbind T MULTIACTIONBAR1BUTTON5\r\nbind SHIFT-T MULTIACTIONBAR3BUTTON9\r\nbind C FOCUSTARGET\r\nbind Y MULTIACTIONBAR4BUTTON5\r\nbind SHIFT-Y NONE\r\nbind SHIFT-C MULTIACTIONBAR1BUTTON12\r\nbind MOUSEWHEELUP MULTIACTIONBAR3BUTTON5\r\nbind MOUSEWHEELDOWN MULTIACTIONBAR4BUTTON8\r\nbind CTRL-Q MULTIACTIONBAR1BUTTON3\r\nbind CTRL-E MULTIACTIONBAR1BUTTON2\r\nbind SHIFT-G MULTIACTIONBAR1BUTTON7\r\nbind SHIFT-F MULTIACTIONBAR1BUTTON6\r\nbind ; MULTIACTIONBAR1BUTTON9\r\nbind SHIFT-E MULTIACTIONBAR2BUTTON2\r\nbind SHIFT-X MULTIACTIONBAR2BUTTON3\r\nbind BUTTON5 MULTIACTIONBAR3BUTTON6\r\nbind SHIFT-BUTTON4 NONE\r\nbind SHIFT-BUTTON5 MULTIACTIONBAR3BUTTON12\r\nbind ALT-R MULTIACTIONBAR3BUTTON4\r\nbind SHIFT-Z MULTIACTIONBAR3BUTTON8\r\nbind SHIFT-Q MULTIACTIONBAR3BUTTON10\r\nbind SHIFT-; MULTIACTIONBAR4BUTTON7\r\nbind ALT-2 MULTIACTIONBAR3BUTTON3\r\nbind ALT-1 MULTIACTIONBAR3BUTTON2\r\n'),(10,5,1463074495,'MACRO 16777227 \"bala\" INV_Misc_QuestionMark\r\n/equip Royal Scepter of Terenas II\r\n/equip Shadow Silk Spindle\r\nEND\r\nMACRO 16777219 \"Cyclon\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [target=focus] Cyclone\r\nEND\r\nMACRO 16777234 \"heal\" INV_Misc_QuestionMark\r\n#showtooltip Fel Healthstone\r\n/use Fel Healthstone\r\n/cast Lifeblood(Rank 6)\r\nEND\r\nMACRO 16777220 \"Hibernate\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [target=focus] Hibernate\r\nEND\r\nMACRO 16777232 \"Innc\" INV_Misc_QuestionMark\r\n#showtooltip innervate\r\n/cast [target=control] Innervate\r\nEND\r\nMACRO 16777221 \"Inner\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [target=player] Innervate\r\nEND\r\nMACRO 16777223 \"Innp1\" INV_Misc_QuestionMark\r\n#showtooltip Innervate\r\n/cast [target=party1] Innervate\r\nEND\r\nMACRO 16777225 \"Innp2\" INV_Misc_QuestionMark\r\n#showtooltip Innervate\r\n/cast [target=party2] Innervate\r\nEND\r\nMACRO 16777217 \"Instant\" INV_Misc_QuestionMark\r\n#showtooltip Nature\'s Swiftness\r\n/cast Nature\'s Swiftness\r\n/cast Healing Touch(Rank 15)\r\nEND\r\nMACRO 16777235 \"PM\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [stealth] Pounce\r\n/cast [nostealth] Mangle (Cat)(Rank 5)\r\nEND\r\nMACRO 16777233 \"pvp\" INV_Misc_QuestionMark\r\n/equip Wrathful Gladiator\'s Salvation\r\n/equip Wrathful Gladiator\'s Reprieve\r\nEND\r\nMACRO 16777224 \"RCP1\" INV_Misc_QuestionMark\r\n#showtooltip Remove Curse\r\n/cast [target=party1] Remove Curse\r\nEND\r\nMACRO 16777226 \"RCP2\" INV_Misc_QuestionMark\r\n#showtooltip Remove Curse\r\n/cast [target=party2] Remove Curse\r\nEND\r\nMACRO 16777218 \"Rooty\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [target=focus] Entangling Roots(Rank 8)\r\nEND\r\nMACRO 16777236 \"SR\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast [stealth] Ravage(Rank 7)\r\n/cast [nostealth] Shred(Rank 9)\r\nEND\r\nMACRO 16777228 \"staff\" INV_Misc_QuestionMark\r\n/equip Mag\'hari Chieftain\'s Staff\r\nEND\r\nMACRO 16777222 \"Travel\" INV_Misc_QuestionMark\r\n#showtooltip\r\n/cast Travel Form\r\nEND\r\n'),(10,7,1463074492,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 52428803\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nLookingForGroup\nEND\n\nZONECHANNELS 18874371\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(11,1,1463089904,'SET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"15.000000\"\nSET cameraSavedPitch \"27.749992\"\nSET nameplateShowEnemies \"1\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_MELEE_COMBAT\"\nSET showTokenFrame \"1\"\n'),(11,7,1463089903,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nEND\n\nZONECHANNELS 52428803\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 18874371\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n'),(13,1,1463338529,'SET equipmentManager \"1\"\nSET questLogCollapseFilter \"-1\"\nSET trackedQuests \"v\"\nSET trackedAchievements \"v\"\nSET cameraSavedDistance \"14.001398\"\nSET cameraSavedVehicleDistance \"49.999996\"\nSET cameraSavedPitch \"16.050002\"\nSET nameplateShowEnemies \"1\"\nSET playerStatLeftDropdown \"PLAYERSTAT_BASE_STATS\"\nSET playerStatRightDropdown \"PLAYERSTAT_DEFENSES\"\nSET showTokenFrame \"1\"\nSET previewTalents \"1\"\n'),(13,5,1463079057,'MACRO 16777217 \"burst\" INV_Misc_QuestionMark\r\n/cast Arcane Power\r\n/cast Icy Viens\r\n/cast Mirror Image\r\n/cast Presence of Mind\r\nEND\r\n'),(13,7,1463199930,'VERSION 5\n\nADDEDVERSION 13\n\nOPTION_GUILD_RECRUITMENT_CHANNEL AUTO\n\nCHANNELS\nworld 1 5\nEND\n\nZONECHANNELS 35651587\n\nCOLORS\nSYSTEM 255 255 0 N\nSAY 255 255 255 N\nPARTY 170 170 255 N\nRAID 255 127 0 N\nGUILD 64 255 64 N\nOFFICER 64 192 64 N\nYELL 255 64 64 N\nWHISPER 255 128 255 N\nWHISPER_FOREIGN 255 128 255 N\nWHISPER_INFORM 255 128 255 N\nEMOTE 255 128 64 N\nTEXT_EMOTE 255 128 64 N\nMONSTER_SAY 255 255 159 N\nMONSTER_PARTY 170 170 255 N\nMONSTER_YELL 255 64 64 N\nMONSTER_WHISPER 255 181 235 N\nMONSTER_EMOTE 255 128 64 N\nCHANNEL 255 192 192 N\nCHANNEL_JOIN 192 128 128 N\nCHANNEL_LEAVE 192 128 128 N\nCHANNEL_LIST 192 128 128 N\nCHANNEL_NOTICE 192 192 192 N\nCHANNEL_NOTICE_USER 192 192 192 N\nAFK 255 128 255 N\nDND 255 128 255 N\nIGNORED 255 0 0 N\nSKILL 85 85 255 N\nLOOT 0 170 0 N\nMONEY 255 255 0 N\nOPENING 128 128 255 N\nTRADESKILLS 255 255 255 N\nPET_INFO 128 128 255 N\nCOMBAT_MISC_INFO 128 128 255 N\nCOMBAT_XP_GAIN 111 111 255 N\nCOMBAT_HONOR_GAIN 224 202 10 N\nCOMBAT_FACTION_CHANGE 128 128 255 N\nBG_SYSTEM_NEUTRAL 255 120 10 N\nBG_SYSTEM_ALLIANCE 0 174 239 N\nBG_SYSTEM_HORDE 255 0 0 N\nRAID_LEADER 255 72 9 N\nRAID_WARNING 255 72 0 N\nRAID_BOSS_EMOTE 255 221 0 N\nRAID_BOSS_WHISPER 255 221 0 N\nFILTERED 255 0 0 N\nBATTLEGROUND 255 127 0 N\nBATTLEGROUND_LEADER 255 219 183 N\nRESTRICTED 255 0 0 N\nBATTLENET 255 255 255 N\nACHIEVEMENT 255 255 0 N\nGUILD_ACHIEVEMENT 64 255 64 N\nARENA_POINTS 255 255 255 N\nPARTY_LEADER 118 200 255 N\nTARGETICONS 255 255 0 N\nBN_WHISPER 0 255 246 N\nBN_WHISPER_INFORM 0 255 246 N\nBN_CONVERSATION 0 177 240 N\nBN_CONVERSATION_NOTICE 0 177 240 N\nBN_CONVERSATION_LIST 0 177 240 N\nBN_INLINE_TOAST_ALERT 130 197 255 N\nBN_INLINE_TOAST_BROADCAST 130 197 255 N\nBN_INLINE_TOAST_BROADCAST_INFORM 130 197 255 N\nBN_INLINE_TOAST_CONVERSATION 130 197 255 N\nCHANNEL1 255 192 192 N\nCHANNEL2 255 192 192 N\nCHANNEL3 255 192 192 N\nCHANNEL4 255 192 192 N\nCHANNEL5 255 192 192 N\nCHANNEL6 255 192 192 N\nCHANNEL7 255 192 192 N\nCHANNEL8 255 192 192 N\nCHANNEL9 255 192 192 N\nCHANNEL10 255 192 192 N\nEND\n\nWINDOW 1\nNAME General\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 1\nSHOWN 1\nMESSAGES\nSYSTEM\nSYSTEM_NOMENU\nSAY\nEMOTE\nYELL\nWHISPER\nPARTY\nPARTY_LEADER\nRAID\nRAID_LEADER\nRAID_WARNING\nBATTLEGROUND\nBATTLEGROUND_LEADER\nGUILD\nOFFICER\nMONSTER_SAY\nMONSTER_YELL\nMONSTER_EMOTE\nMONSTER_WHISPER\nMONSTER_BOSS_EMOTE\nMONSTER_BOSS_WHISPER\nERRORS\nAFK\nDND\nIGNORED\nBG_HORDE\nBG_ALLIANCE\nBG_NEUTRAL\nCOMBAT_FACTION_CHANGE\nSKILL\nLOOT\nMONEY\nCHANNEL\nACHIEVEMENT\nGUILD_ACHIEVEMENT\nTARGETICONS\nBN_WHISPER\nBN_WHISPER_INFORM\nBN_CONVERSATION\nBN_INLINE_TOAST_ALERT\nEND\n\nCHANNELS\nworld\nEND\n\nZONECHANNELS 2097155\n\nEND\n\nWINDOW 2\nNAME Combat Log\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 2\nSHOWN 0\nMESSAGES\nOPENING\nTRADESKILLS\nPET_INFO\nCOMBAT_XP_GAIN\nCOMBAT_HONOR_GAIN\nCOMBAT_MISC_INFO\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 3\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 4\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 5\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 6\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 7\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 8\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 9\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\nWINDOW 10\nSIZE 0\nCOLOR 0 0 0 40\nLOCKED 1\nUNINTERACTABLE 0\nDOCKED 0\nSHOWN 0\nMESSAGES\nEND\n\nCHANNELS\nEND\n\nZONECHANNELS 0\n\nEND\n\n');
/*!40000 ALTER TABLE `character_account_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_achievement`
--

DROP TABLE IF EXISTS `character_achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_achievement` (
  `guid` int(10) unsigned NOT NULL,
  `achievement` smallint(5) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`achievement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_achievement`
--

LOCK TABLES `character_achievement` WRITE;
/*!40000 ALTER TABLE `character_achievement` DISABLE KEYS */;
INSERT INTO `character_achievement` VALUES (1,6,1462851385),(1,7,1462851385),(1,8,1462851385),(1,9,1462851385),(1,10,1462851385),(1,11,1462851385),(1,12,1462851385),(1,13,1462851385),(1,457,1462851385),(1,465,1462851385),(1,522,1462851385),(1,889,1462825914),(1,890,1462825914),(1,891,1462825914),(1,892,1462825914),(1,1405,1462851385),(1,3838,1462976669),(1,4531,1462977312),(1,4534,1462976669),(1,4536,1462976926),(1,4537,1462977312),(1,4582,1462977444),(1,4598,1462851385),(1,4785,1462976669),(2,6,1463056902),(2,7,1463056902),(2,8,1463056902),(2,9,1463056902),(2,10,1463056902),(2,11,1463056902),(2,12,1463056902),(2,13,1463056902),(2,16,1463056902),(2,466,1463056902),(2,705,1463056902),(2,889,1463401135),(2,890,1463401135),(2,891,1463401135),(2,892,1463401135),(2,1409,1463056902),(2,3838,1462976669),(2,4531,1462977312),(2,4534,1462976669),(2,4536,1462976926),(2,4537,1462977312),(2,4582,1462977444),(2,4600,1463486369),(2,4784,1463063791),(2,4785,1462976669),(2,4815,1463063791),(2,4816,1463063791),(8,460,1463003956),(8,1408,1463003956),(10,6,1463069054),(10,7,1463069054),(10,8,1463069054),(10,9,1463069054),(10,10,1463069054),(10,11,1463069054),(10,12,1463069054),(10,13,1463069054),(10,16,1463337227),(10,466,1463069000),(10,522,1463069284),(10,556,1463071102),(10,557,1463071102),(10,621,1463088194),(10,695,1463088916),(10,701,1463068818),(10,705,1463337227),(10,889,1463069069),(10,890,1463069069),(10,891,1463069069),(10,892,1463069069),(10,1157,1463074789),(10,1409,1463069000),(10,2716,1463069215),(10,3838,1463090670),(10,4534,1463090670),(10,4536,1463340564),(10,4598,1463069284),(10,4784,1463090670),(10,4785,1463340304),(11,6,1463068590),(11,7,1463068590),(11,8,1463068590),(11,9,1463068590),(11,10,1463068590),(11,11,1463068942),(11,12,1463068942),(11,13,1463068942),(11,16,1463074321),(11,131,1463068590),(11,132,1463068590),(11,133,1463068590),(11,461,1463068942),(11,522,1463069288),(11,705,1463074321),(11,889,1463068947),(11,890,1463068947),(11,891,1463068947),(11,892,1463068947),(11,1157,1463074760),(11,1408,1463068947),(11,2716,1463069236),(11,3838,1463090670),(11,4534,1463090670),(11,4598,1463069288),(11,4784,1463090670),(13,6,1463078114),(13,7,1463078114),(13,8,1463078114),(13,9,1463078114),(13,10,1463078114),(13,11,1463078114),(13,12,1463078114),(13,13,1463078114),(13,16,1463197476),(13,116,1463198625),(13,121,1463198674),(13,122,1463198674),(13,123,1463198674),(13,124,1463198674),(13,125,1463198674),(13,131,1463198678),(13,132,1463198678),(13,133,1463198678),(13,134,1463198678),(13,135,1463198678),(13,522,1463078334),(13,546,1463200122),(13,556,1463078790),(13,557,1463078790),(13,621,1463080737),(13,705,1463197476),(13,731,1463198625),(13,732,1463198625),(13,733,1463198625),(13,734,1463198625),(13,735,1463198633),(13,883,1463200017),(13,885,1463200005),(13,889,1463078298),(13,890,1463078298),(13,891,1463078298),(13,892,1463078298),(13,1205,1463198667),(13,1415,1463198660),(13,1416,1463198674),(13,1419,1463198678),(13,1422,1463198633),(13,1423,1463198625),(13,1427,1463198667),(13,1795,1463198674),(13,1796,1463198674),(13,1797,1463198674),(13,1798,1463198674),(13,1799,1463198674),(13,2141,1463200023),(13,2716,1463081222),(13,3838,1463339971),(13,4534,1463339971),(13,4536,1463340564),(13,4598,1463078334),(13,4625,1463200032),(13,4627,1463200012),(13,4785,1463339971),(13,4788,1463198667),(13,4789,1463198667);
/*!40000 ALTER TABLE `character_achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_achievement_progress`
--

DROP TABLE IF EXISTS `character_achievement_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_achievement_progress` (
  `guid` int(10) unsigned NOT NULL,
  `criteria` smallint(5) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`criteria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_achievement_progress`
--

LOCK TABLES `character_achievement_progress` WRITE;
/*!40000 ALTER TABLE `character_achievement_progress` DISABLE KEYS */;
INSERT INTO `character_achievement_progress` VALUES (1,34,80,1462851385),(1,35,80,1462851385),(1,36,80,1462851385),(1,37,80,1462851385),(1,38,80,1462851385),(1,39,80,1462851385),(1,40,80,1462851385),(1,41,80,1462851385),(1,73,1,1462851385),(1,81,1,1462851385),(1,111,2,1462976988),(1,162,4294967295,1462976685),(1,164,1,1462821050),(1,167,1,1462710530),(1,170,2,1462976988),(1,230,1,1462851385),(1,231,1,1462851385),(1,232,1,1462851385),(1,233,1,1462851385),(1,234,1,1462851385),(1,236,1,1462851385),(1,616,75,1462821050),(1,641,1,1462851385),(1,655,1,1462710530),(1,656,1,1462851385),(1,753,1,1462710530),(1,754,5,1462976629),(1,755,1,1462710530),(1,756,1,1462710530),(1,834,1,1462851385),(1,848,75,1462821050),(1,859,75,1462821050),(1,870,75,1462821050),(1,881,75,1462821050),(1,892,75,1462821050),(1,977,1,1462851385),(1,978,1,1462851385),(1,979,1,1462851385),(1,980,1,1462851385),(1,981,1,1462851385),(1,982,1,1462851385),(1,984,1,1462851385),(1,985,1,1462851385),(1,992,500,1462710530),(1,993,500,1462710530),(1,994,3100,1462710530),(1,995,500,1462710530),(1,996,4000,1462710530),(1,1147,1,1463004738),(1,1149,1,1462851385),(1,1227,1,1462851385),(1,1870,300,1462825914),(1,1871,300,1462825914),(1,1872,300,1462825914),(1,1873,300,1462825914),(1,1884,3500,1462710530),(1,2020,200,1462710530),(1,2239,1,1462851385),(1,2356,1,1462821050),(1,2428,1,1462851385),(1,2429,1,1462851385),(1,3354,138202,1463004839),(1,3506,138202,1463004839),(1,3507,138202,1463004839),(1,3510,138202,1463004839),(1,3511,138202,1463004839),(1,3512,138202,1463004839),(1,3631,1,1462851385),(1,3737,1,1462821050),(1,4093,138202,1463004839),(1,4224,135808,1462976708),(1,4763,3500,1462710530),(1,4787,3,1462855169),(1,4944,71,1463004838),(1,4946,68,1463004838),(1,4948,4,1462977307),(1,4953,18,1463004838),(1,4955,46,1462977444),(1,4956,1,1462977444),(1,5212,80,1462851385),(1,5215,80,1462851385),(1,5230,80,1462851385),(1,5241,1,1462821050),(1,5299,1,1462851385),(1,5300,1,1462851385),(1,5301,8,1462851385),(1,5313,500,1462710530),(1,5314,500,1462710530),(1,5315,500,1462710530),(1,5316,3100,1462710530),(1,5317,4000,1462710530),(1,5371,13187,1462976770),(1,5372,48122,1463011295),(1,5373,1461251968,1462976731),(1,5512,1,1463004710),(1,5529,42,1463004838),(1,5532,1,1463004838),(1,5551,75,1462821050),(1,5565,1,1462821050),(1,5576,1,1462710530),(1,5577,1,1462710530),(1,5578,1,1462851385),(1,5579,5,1462976629),(1,5580,1,1462710530),(1,5581,1,1462851385),(1,5589,1,1462710530),(1,5590,1,1462851385),(1,5593,2,1462976988),(1,5711,1,1462821050),(1,5860,1,1462976777),(1,5868,1,1463004738),(1,6140,4,1462977444),(1,6141,6,1462977444),(1,6142,6,1462977444),(1,6386,4,1462851385),(1,6845,2,1462976988),(1,7223,4,1462851385),(1,7550,3,1462855169),(1,7551,3,1462855169),(1,7552,3,1462855169),(1,8819,500,1462710530),(1,8820,500,1462710530),(1,8821,500,1462710530),(1,8822,500,1462710530),(1,9224,3,1462855169),(1,9372,1,1462976685),(1,9598,80,1462851385),(1,9683,500,1462710530),(1,9684,500,1462710530),(1,9685,4000,1462710530),(1,9686,500,1462710530),(1,9687,3100,1462710530),(1,12498,2,1462976669),(1,12499,8,1462977444),(1,12500,8,1462977444),(1,12501,8,1462977444),(1,12502,8,1462977444),(1,12503,8,1462977444),(1,12504,8,1462977444),(1,12505,8,1462977444),(1,12506,8,1462977444),(1,12698,205,1462977444),(1,12762,1,1462977444),(1,12765,1,1462977312),(1,12770,1,1462976669),(1,12771,1,1462976926),(1,12772,1,1462977312),(1,12773,1,1462976745),(1,12775,1,1462976669),(1,12777,1,1462976926),(1,12778,1,1462977312),(1,12911,42000,1462851385),(1,12917,1,1462976669),(1,12919,1,1462976926),(1,12920,1,1462977312),(1,12924,1,1462977444),(1,13033,1,1462977444),(1,13089,1,1462976669),(1,13090,1,1462976669),(1,13091,1,1462976669),(1,13092,1,1462976669),(1,13093,1,1462976745),(1,13094,2,1462977124),(1,13095,1,1462977312),(1,13098,1,1462977444),(1,13104,1,1462976745),(1,13105,1,1462976745),(1,13106,1,1462976745),(1,13184,4,1462977444),(1,13307,2,1462976988),(1,13331,1,1462976669),(1,13332,1,1462976745),(1,13333,2,1462977124),(1,13334,1,1462977312),(1,13339,1,1462977444),(1,13388,1,1462976669),(1,13409,3500,1462710530),(2,34,81,1463056902),(2,35,81,1463056902),(2,36,81,1463056902),(2,37,81,1463056902),(2,38,81,1463056902),(2,39,81,1463056902),(2,40,81,1463056902),(2,41,81,1463056902),(2,111,3,1462976995),(2,162,4294967295,1462977116),(2,167,405,1463056902),(2,170,3,1462976995),(2,653,405,1463056902),(2,655,405,1463056902),(2,656,405,1463056902),(2,657,405,1463056902),(2,756,405,1463056902),(2,834,405,1463056902),(2,1147,1,1463486369),(2,1149,1,1462976621),(2,1227,1,1462976621),(2,1299,1,1462976621),(2,1870,300,1463401135),(2,1871,300,1463401135),(2,1872,300,1463401135),(2,1873,300,1463401135),(2,2020,200,1463056902),(2,2030,3100,1463056902),(2,2031,3100,1463056902),(2,2032,4000,1463056902),(2,2033,3100,1463056902),(2,2034,3000,1463056902),(2,2045,2000,1463056902),(2,2356,1,1462876261),(2,2357,1,1462879595),(2,3354,135808,1462976708),(2,3506,135808,1462976708),(2,3507,135808,1462976708),(2,3510,135808,1462976708),(2,3511,135808,1462976708),(2,3512,135808,1462976708),(2,3737,1,1462876261),(2,3738,1,1462879595),(2,4093,135808,1462976708),(2,4172,1,1463486369),(2,4224,99135807,1462976708),(2,4787,1,1463486369),(2,4943,4185,1462977344),(2,4944,71,1463063791),(2,4946,69,1463063791),(2,4948,4,1462977307),(2,4950,2,1463063791),(2,4951,2,1463063779),(2,4953,16,1462977118),(2,4955,44,1462977444),(2,4956,1,1462977444),(2,5212,81,1463056902),(2,5214,81,1463056902),(2,5234,81,1463056902),(2,5293,1,1463486369),(2,5301,9,1462976634),(2,5328,4000,1463056902),(2,5329,3100,1463056902),(2,5330,3100,1463056902),(2,5331,3100,1463056902),(2,5332,3000,1463056902),(2,5371,12164,1462976766),(2,5372,5069,1462976992),(2,5373,719269696,1462977362),(2,5374,15409,1463486134),(2,5375,4185,1462977344),(2,5376,1239,1462977342),(2,5529,40,1463063791),(2,5580,405,1463056902),(2,5581,405,1463056902),(2,5582,405,1462998402),(2,5585,405,1463056902),(2,5589,405,1463056902),(2,5590,405,1463056902),(2,5591,405,1463056902),(2,5593,3,1462976995),(2,5860,1,1462976621),(2,5868,1,1462976621),(2,5871,1,1462977003),(2,6140,5,1463063791),(2,6141,5,1463063791),(2,6142,5,1463063791),(2,6845,3,1462976995),(2,7550,1,1463486369),(2,7551,1,1463486369),(2,7552,1,1463486369),(2,8819,500,1463056902),(2,8820,500,1463056902),(2,8821,500,1463056902),(2,8822,500,1463056902),(2,9223,1,1463486369),(2,9372,1,1462976685),(2,9598,81,1463056902),(2,9678,4000,1463056902),(2,9679,3000,1463056902),(2,9680,3100,1463056902),(2,9681,3100,1463056902),(2,9682,3100,1463056902),(2,12498,2,1462976669),(2,12499,11,1463063791),(2,12500,11,1463063791),(2,12501,11,1463063791),(2,12502,11,1463063791),(2,12503,11,1463063791),(2,12504,11,1463063791),(2,12505,11,1463063791),(2,12506,11,1463063791),(2,12698,235,1463401135),(2,12762,1,1462977444),(2,12765,1,1462977312),(2,12770,1,1462976669),(2,12771,1,1462976926),(2,12772,1,1462977312),(2,12773,1,1462976745),(2,12775,1,1462976669),(2,12777,1,1462976926),(2,12778,1,1462977312),(2,12911,2680,1462977444),(2,12914,1,1463486369),(2,12917,1,1462976669),(2,12919,1,1462976926),(2,12920,1,1462977312),(2,12924,1,1462977444),(2,13033,1,1462977444),(2,13089,1,1462976669),(2,13090,1,1462976669),(2,13091,1,1462976669),(2,13092,1,1462976669),(2,13093,1,1462976745),(2,13094,2,1462977124),(2,13095,1,1462977312),(2,13098,1,1462977444),(2,13104,1,1462976745),(2,13105,1,1462976745),(2,13106,1,1462976745),(2,13184,5,1463063791),(2,13307,3,1462976995),(2,13331,1,1462976669),(2,13332,1,1462976745),(2,13333,2,1462977124),(2,13334,1,1462977312),(2,13339,1,1462977444),(2,13383,1,1463063791),(2,13388,1,1462976669),(2,13451,1,1463063791),(2,13452,1,1463063791),(2,13465,1,1463063791),(2,13467,1,1463063791),(4,34,81,1462779462),(4,35,81,1462779462),(4,36,81,1462779462),(4,37,81,1462779462),(4,38,81,1462779462),(4,39,81,1462779462),(4,40,81,1462779462),(4,41,81,1462779462),(4,167,1,1462779462),(4,653,1,1462779462),(4,655,1,1462779462),(4,657,1,1462779462),(4,756,1,1462779462),(4,1299,1,1462779462),(4,2020,200,1462779462),(4,2030,3100,1462779462),(4,2031,3100,1462779462),(4,2032,4000,1462779462),(4,2033,3100,1462779462),(4,2034,3000,1462779462),(4,2045,2000,1462779462),(4,5212,81,1462779462),(4,5214,81,1462779462),(4,5234,81,1462779462),(4,5301,6,1462779462),(4,5328,4000,1462779462),(4,5329,3100,1462779462),(4,5330,3100,1462779462),(4,5331,3100,1462779462),(4,5332,3000,1462779462),(4,5580,1,1462779462),(4,5585,1,1462779462),(4,5589,1,1462779462),(4,5591,1,1462779462),(4,8819,500,1462779462),(4,8820,500,1462779462),(4,8821,500,1462779462),(4,8822,500,1462779462),(4,9598,81,1462779462),(4,9678,4000,1462779462),(4,9679,3000,1462779462),(4,9680,3100,1462779462),(4,9681,3100,1462779462),(4,9682,3100,1462779462),(4,12698,80,1462779462),(5,34,2,1463065403),(5,35,2,1463065403),(5,36,2,1463065403),(5,37,2,1463065403),(5,38,2,1463065403),(5,39,2,1463065403),(5,40,2,1463065403),(5,41,2,1463065403),(5,162,511,1463066033),(5,167,1,1462891637),(5,653,7,1463066028),(5,756,1,1462891637),(5,1146,1,1462891646),(5,2020,200,1462891637),(5,2030,4000,1462891637),(5,2031,3100,1462891637),(5,2032,3100,1462891637),(5,2033,3100,1462891637),(5,2034,3000,1462891637),(5,4944,11,1463066033),(5,4946,11,1463066033),(5,4948,10,1463066033),(5,5212,2,1463065403),(5,5221,2,1463065403),(5,5233,2,1463065403),(5,5301,6,1462891637),(5,5328,3100,1462891637),(5,5329,3100,1462891637),(5,5330,3100,1462891637),(5,5331,4000,1462891637),(5,5332,3000,1462891637),(5,5371,2,1463064970),(5,5372,45,1463066032),(5,5373,30,1463065403),(5,5512,11,1463066033),(5,5529,11,1463066033),(5,5585,7,1463066028),(5,5587,1,1462891637),(5,5589,1,1462891637),(5,8819,500,1462891637),(5,8820,500,1462891637),(5,8821,500,1462891637),(5,8822,500,1462891637),(5,9378,1,1462891637),(5,9598,2,1463065403),(5,9678,3100,1462891637),(5,9679,3000,1462891637),(5,9680,3100,1462891637),(5,9681,3100,1462891637),(5,9682,4000,1462891637),(6,34,1,1462970499),(6,35,1,1462970499),(6,36,1,1462970499),(6,37,1,1462970499),(6,38,1,1462970499),(6,39,1,1462970499),(6,40,1,1462970499),(6,41,1,1462970499),(6,167,1,1462970499),(6,653,1,1462970499),(6,756,1,1462970499),(6,1146,1,1462970549),(6,2020,200,1462970499),(6,2030,4000,1462970499),(6,2031,3100,1462970499),(6,2032,3100,1462970499),(6,2033,3100,1462970499),(6,2034,3000,1462970499),(6,5212,1,1462970499),(6,5221,1,1462970499),(6,5233,1,1462970499),(6,5301,6,1462970499),(6,5328,3100,1462970499),(6,5329,3100,1462970499),(6,5330,3100,1462970499),(6,5331,4000,1462970499),(6,5332,3000,1462970499),(6,5585,1,1462970499),(6,5587,1,1462970499),(6,5589,1,1462970499),(6,8819,500,1462970499),(6,8820,500,1462970499),(6,8821,500,1462970499),(6,8822,500,1462970499),(6,9378,1,1462970499),(6,9598,1,1462970499),(6,9678,3100,1462970499),(6,9679,3000,1462970499),(6,9680,3100,1462970499),(6,9681,3100,1462970499),(6,9682,4000,1462970499),(7,34,3,1463338364),(7,35,3,1463338364),(7,36,3,1463338364),(7,37,3,1463338364),(7,38,3,1463338364),(7,39,3,1463338364),(7,40,3,1463338364),(7,41,3,1463338364),(7,111,1,1463338515),(7,162,272,1463338471),(7,167,1,1463016068),(7,170,1,1463338515),(7,653,7,1463336704),(7,756,1,1463016068),(7,822,1,1463016090),(7,832,1,1463336769),(7,992,3100,1463016068),(7,993,3100,1463016068),(7,994,500,1463016068),(7,995,4000,1463016068),(7,996,400,1463016068),(7,2020,200,1463016068),(7,4944,3,1463338471),(7,4946,2,1463336731),(7,4948,2,1463336731),(7,5212,3,1463338364),(7,5221,3,1463338364),(7,5237,3,1463338364),(7,5301,6,1463016068),(7,5313,4000,1463016068),(7,5314,3100,1463016068),(7,5315,3100,1463016068),(7,5316,500,1463016068),(7,5317,400,1463016068),(7,5371,4164,1463338515),(7,5372,14742,1463338515),(7,5373,82,1463338471),(7,5529,4,1463338471),(7,5530,3,1463336731),(7,5585,7,1463336704),(7,5587,3,1463337544),(7,5589,1,1463016068),(7,5593,1,1463338515),(7,5860,1,1463338364),(7,6845,1,1463338515),(7,8819,500,1463016068),(7,8820,500,1463016068),(7,8821,500,1463016068),(7,8822,500,1463016068),(7,9378,3,1463337544),(7,9598,3,1463338364),(7,9683,3100,1463016068),(7,9684,4000,1463016068),(7,9685,400,1463016068),(7,9686,3100,1463016068),(7,9687,500,1463016068),(7,13307,1,1463338515),(10,34,80,1463069054),(10,35,80,1463069054),(10,36,80,1463069054),(10,37,80,1463069054),(10,38,80,1463069054),(10,39,80,1463069054),(10,40,80,1463069054),(10,41,80,1463069054),(10,73,1,1463069305),(10,81,1,1463069305),(10,111,14,1463339579),(10,162,37730559,1463340627),(10,167,400,1463337227),(10,170,14,1463339579),(10,230,1,1463069305),(10,231,1,1463069305),(10,232,1,1463069305),(10,233,1,1463069305),(10,234,1,1463069305),(10,236,1,1463069305),(10,653,400,1463337227),(10,655,400,1463337227),(10,656,400,1463337227),(10,657,400,1463337227),(10,756,400,1463337227),(10,834,400,1463337227),(10,977,1,1463069284),(10,978,1,1463069284),(10,979,1,1463069284),(10,980,1,1463069284),(10,981,1,1463069284),(10,982,1,1463069284),(10,984,1,1463069284),(10,985,1,1463069284),(10,1149,1,1463080210),(10,1299,1,1463068218),(10,1870,150,1463069069),(10,1871,225,1463069069),(10,1872,150,1463069069),(10,1873,300,1463069069),(10,2020,200,1463068217),(10,2030,3100,1463068217),(10,2031,3100,1463068217),(10,2032,4000,1463068217),(10,2033,3100,1463068217),(10,2034,3000,1463068217),(10,2045,2000,1463068217),(10,2239,1,1463069305),(10,2342,1,1463069360),(10,2343,1,1463070913),(10,2344,1,1463069361),(10,2345,1,1463069361),(10,2346,1,1463071016),(10,2347,1,1463069361),(10,2348,1,1463071037),(10,2349,1,1463071044),(10,2350,1,1463069361),(10,2351,1,1463070889),(10,2353,1,1463070735),(10,2355,1,1463070958),(10,2356,1,1463069358),(10,2357,1,1463071102),(10,2428,1,1463069284),(10,2429,1,1463069284),(10,3354,267316,1463339972),(10,3506,267316,1463339972),(10,3507,267316,1463339972),(10,3510,267316,1463339972),(10,3511,267316,1463339972),(10,3512,267316,1463339972),(10,3629,3,1463075293),(10,3630,8,1463076086),(10,3631,1,1463069305),(10,3680,1,1463074789),(10,3723,1,1463069360),(10,3724,1,1463070913),(10,3725,1,1463069361),(10,3726,1,1463069361),(10,3727,1,1463071016),(10,3728,1,1463069361),(10,3729,1,1463071037),(10,3730,1,1463071044),(10,3731,1,1463069361),(10,3732,1,1463070889),(10,3733,1,1463071062),(10,3734,1,1463070735),(10,3735,1,1463071053),(10,3736,1,1463070958),(10,3737,1,1463069358),(10,3738,1,1463071102),(10,4019,1,1463088916),(10,4093,267316,1463339972),(10,4224,2140121746,1463339972),(10,4635,1,1463088916),(10,4787,2,1463069068),(10,4943,2578653,1463339546),(10,4944,118,1463340421),(10,4946,87,1463340421),(10,4953,13,1463340421),(10,4955,71,1463340402),(10,4956,4,1463337592),(10,5212,80,1463069054),(10,5214,80,1463069054),(10,5234,80,1463069054),(10,5299,1,1463069284),(10,5300,1,1463069284),(10,5301,8,1463338795),(10,5328,4000,1463068217),(10,5329,3100,1463068217),(10,5330,3100,1463068217),(10,5331,3100,1463068217),(10,5332,3000,1463068217),(10,5371,26427,1463090326),(10,5372,3235512,1463340121),(10,5373,650000,1463340536),(10,5374,48043,1463090001),(10,5375,2241585,1463339546),(10,5376,48043,1463090001),(10,5529,86,1463340421),(10,5530,5,1463337779),(10,5580,400,1463337227),(10,5581,400,1463337227),(10,5582,400,1463337227),(10,5585,400,1463337227),(10,5589,400,1463337227),(10,5590,400,1463337227),(10,5591,400,1463337227),(10,5593,14,1463339579),(10,5816,1,1463071062),(10,5817,1,1463071053),(10,5860,1,1463088908),(10,6140,4,1463340592),(10,6141,66,1463340592),(10,6142,66,1463340592),(10,6845,14,1463339579),(10,7550,2,1463069068),(10,7551,2,1463069068),(10,7552,2,1463069068),(10,8819,500,1463068217),(10,8820,500,1463068217),(10,8821,500,1463068217),(10,8822,500,1463068217),(10,9223,2,1463069068),(10,9369,1,1463089065),(10,9372,1,1463089068),(10,9598,80,1463069054),(10,9619,1,1463069215),(10,9678,4000,1463068217),(10,9679,3000,1463068217),(10,9680,3100,1463068217),(10,9681,3100,1463068217),(10,9682,3100,1463068217),(10,12498,2,1463090670),(10,12499,8,1463340592),(10,12500,8,1463340592),(10,12501,8,1463340592),(10,12502,8,1463340592),(10,12503,8,1463340592),(10,12504,8,1463340592),(10,12505,8,1463340592),(10,12506,8,1463340592),(10,12698,295,1463340564),(10,12770,1,1463090670),(10,12771,1,1463340564),(10,12773,1,1463340304),(10,12775,1,1463090670),(10,12777,1,1463340564),(10,12911,42999,1463069284),(10,12917,1,1463090670),(10,12919,1,1463340564),(10,13089,2,1463339971),(10,13090,2,1463339971),(10,13091,2,1463339971),(10,13092,2,1463339971),(10,13093,1,1463340304),(10,13094,1,1463340564),(10,13104,1,1463340304),(10,13105,1,1463340304),(10,13106,1,1463340304),(10,13184,4,1463340592),(10,13242,1,1463088194),(10,13282,8,1463090585),(10,13297,8,1463090585),(10,13307,14,1463339579),(10,13331,2,1463339971),(10,13332,1,1463340304),(10,13333,1,1463340564),(10,13371,1,1463068818),(10,13383,1,1463090670),(10,13388,1,1463340304),(11,34,55,1463068590),(11,35,55,1463068590),(11,36,55,1463068590),(11,37,55,1463068590),(11,38,55,1463068590),(11,39,80,1463068942),(11,40,80,1463068942),(11,41,80,1463068942),(11,111,7,1463090446),(11,162,7005281,1463090670),(11,167,400,1463074321),(11,170,7,1463090446),(11,641,400,1463074321),(11,655,400,1463074321),(11,656,400,1463074321),(11,752,1,1463075183),(11,753,400,1463074321),(11,754,400,1463074321),(11,755,400,1463074321),(11,756,400,1463074321),(11,834,400,1463074321),(11,840,300,1463068590),(11,841,300,1463068590),(11,842,300,1463068590),(11,843,300,1463068590),(11,844,300,1463068590),(11,977,1,1463069288),(11,978,1,1463069288),(11,979,1,1463069288),(11,980,1,1463069288),(11,981,1,1463069288),(11,982,1,1463069288),(11,984,1,1463069288),(11,985,1,1463069288),(11,1149,1,1463068774),(11,1870,225,1463068947),(11,1871,225,1463068947),(11,1872,225,1463068947),(11,1873,300,1463068947),(11,2002,1,1463075183),(11,2020,200,1463068590),(11,2030,4000,1463068590),(11,2031,3100,1463068590),(11,2032,3100,1463068590),(11,2033,3100,1463068590),(11,2034,3000,1463068590),(11,2342,1,1463074810),(11,2344,1,1463074811),(11,2345,1,1463074812),(11,2347,1,1463074808),(11,2350,1,1463074807),(11,2356,1,1463070820),(11,2417,3200,1463068590),(11,2428,1,1463069288),(11,2429,1,1463069288),(11,3629,3,1463075386),(11,3630,3,1463075293),(11,3680,1,1463074760),(11,3723,1,1463074810),(11,3725,1,1463074811),(11,3726,1,1463074812),(11,3728,1,1463074808),(11,3731,1,1463074807),(11,3737,1,1463070820),(11,4224,2140000000,1463069229),(11,4743,3200,1463068590),(11,4787,4,1463075173),(11,4943,50704,1463090477),(11,4944,18,1463090670),(11,4946,13,1463090670),(11,4953,1,1463074344),(11,4955,11,1463090670),(11,4956,2,1463075471),(11,5212,80,1463068942),(11,5219,80,1463068942),(11,5233,80,1463068942),(11,5249,270,1463068590),(11,5299,1,1463069288),(11,5300,1,1463069288),(11,5301,7,1463069288),(11,5328,3100,1463068590),(11,5329,3100,1463068590),(11,5330,3100,1463068590),(11,5331,4000,1463068590),(11,5332,3000,1463068590),(11,5371,24000,1463089975),(11,5372,415649,1463090616),(11,5373,16862,1463090281),(11,5374,10000000,1463075450),(11,5375,179496,1463090624),(11,5376,10000000,1463075450),(11,5529,8,1463090456),(11,5530,2,1463074344),(11,5562,270,1463068590),(11,5576,400,1463074321),(11,5577,400,1463074321),(11,5578,400,1463074321),(11,5579,400,1463074321),(11,5580,400,1463074321),(11,5581,400,1463074321),(11,5589,400,1463074321),(11,5590,400,1463074321),(11,5592,270,1463068590),(11,5593,7,1463090446),(11,6140,1,1463090670),(11,6141,25,1463090670),(11,6142,25,1463090670),(11,6394,26,1463068576),(11,6845,7,1463090446),(11,7222,26,1463068576),(11,7550,4,1463075173),(11,7551,4,1463075173),(11,7552,4,1463075173),(11,8819,500,1463068590),(11,8820,500,1463068590),(11,8821,500,1463068590),(11,8822,500,1463068590),(11,9223,4,1463075173),(11,9369,1,1463089943),(11,9598,55,1463068590),(11,9619,1,1463069236),(11,9678,3100,1463068590),(11,9679,3000,1463068590),(11,9680,3100,1463068590),(11,9681,3100,1463068590),(11,9682,4000,1463068590),(11,12498,2,1463090670),(11,12499,2,1463090670),(11,12500,2,1463090670),(11,12501,2,1463090670),(11,12502,2,1463090670),(11,12503,2,1463090670),(11,12504,2,1463090670),(11,12505,2,1463090670),(11,12506,2,1463090670),(11,12698,250,1463090670),(11,12770,1,1463090670),(11,12775,1,1463090670),(11,12911,42999,1463069288),(11,12917,1,1463090670),(11,13089,1,1463090670),(11,13090,1,1463090670),(11,13091,1,1463090670),(11,13092,1,1463090670),(11,13184,1,1463090670),(11,13282,5,1463090446),(11,13297,5,1463090446),(11,13307,7,1463090446),(11,13331,1,1463090670),(11,13383,1,1463090670),(13,34,80,1463078114),(13,35,80,1463078114),(13,36,80,1463078114),(13,37,80,1463078114),(13,38,80,1463078114),(13,39,80,1463078114),(13,40,80,1463078114),(13,41,80,1463078114),(13,73,1,1463078348),(13,81,1,1463078348),(13,111,8,1463339930),(13,162,12337022,1463340564),(13,164,450,1463198660),(13,167,400,1463197476),(13,168,150,1463198674),(13,170,8,1463339930),(13,230,1,1463078348),(13,231,1,1463078348),(13,232,1,1463078348),(13,233,1,1463078348),(13,234,1,1463078348),(13,236,1,1463078348),(13,612,225,1463198674),(13,613,300,1463198674),(13,614,375,1463198674),(13,615,450,1463198674),(13,616,150,1463198660),(13,621,150,1463198633),(13,622,150,1463198625),(13,626,150,1463198667),(13,653,400,1463197476),(13,657,400,1463197476),(13,752,7,1463200122),(13,753,400,1463197476),(13,756,400,1463197476),(13,757,400,1463197476),(13,840,150,1463198678),(13,841,225,1463198678),(13,842,300,1463198678),(13,843,375,1463198678),(13,844,450,1463198678),(13,846,1,1463198678),(13,847,1,1463198674),(13,848,225,1463198660),(13,853,225,1463198633),(13,854,225,1463198625),(13,858,225,1463198667),(13,859,300,1463198660),(13,864,300,1463198633),(13,865,300,1463198625),(13,869,300,1463198667),(13,870,375,1463198660),(13,875,375,1463198633),(13,876,375,1463198625),(13,880,375,1463198667),(13,881,450,1463198660),(13,886,450,1463198633),(13,887,450,1463198625),(13,891,450,1463198667),(13,892,450,1463198660),(13,897,450,1463198633),(13,898,450,1463198625),(13,902,450,1463198667),(13,977,1,1463078334),(13,978,1,1463078334),(13,979,1,1463078334),(13,980,1,1463078334),(13,981,1,1463078334),(13,982,1,1463078334),(13,984,1,1463078334),(13,985,1,1463078334),(13,1146,1,1463078057),(13,1147,1,1463336639),(13,1149,1,1463199431),(13,1870,150,1463078298),(13,1871,225,1463078298),(13,1872,150,1463078298),(13,1873,300,1463078298),(13,2002,7,1463200122),(13,2020,200,1463078016),(13,2030,4000,1463078016),(13,2031,3100,1463078016),(13,2032,3100,1463078016),(13,2033,3100,1463078016),(13,2034,3000,1463078016),(13,2072,1755,1463336480),(13,2239,1,1463078348),(13,2342,1,1463078119),(13,2343,1,1463078456),(13,2344,1,1463078120),(13,2345,1,1463078119),(13,2346,1,1463078411),(13,2347,1,1463078118),(13,2348,1,1463078790),(13,2349,1,1463078438),(13,2350,1,1463078118),(13,2351,1,1463078349),(13,2353,1,1463078295),(13,2355,1,1463078449),(13,2356,1,1463078301),(13,2357,1,1463078532),(13,2416,3000,1463078016),(13,2428,1,1463078334),(13,2429,1,1463078334),(13,3354,227364,1463339972),(13,3361,12,1463080642),(13,3506,227364,1463339972),(13,3507,227364,1463339972),(13,3510,227364,1463339972),(13,3511,227364,1463339972),(13,3512,227364,1463339972),(13,3631,1,1463078348),(13,3723,1,1463078119),(13,3724,1,1463078456),(13,3725,1,1463078120),(13,3726,1,1463078119),(13,3727,1,1463078411),(13,3728,1,1463078118),(13,3729,1,1463078790),(13,3730,1,1463078438),(13,3731,1,1463078118),(13,3732,1,1463078349),(13,3733,1,1463078394),(13,3734,1,1463078295),(13,3735,1,1463078314),(13,3736,1,1463078449),(13,3737,1,1463078301),(13,3738,1,1463078532),(13,4091,12,1463080642),(13,4093,227364,1463339972),(13,4224,451322284,1463235564),(13,4742,3000,1463078016),(13,4787,11,1463200032),(13,4944,77,1463340421),(13,4946,56,1463340421),(13,4948,3,1463338529),(13,4953,15,1463340421),(13,4955,40,1463340402),(13,5018,80,1463078114),(13,5212,80,1463078114),(13,5233,80,1463078114),(13,5241,450,1463198660),(13,5242,450,1463198674),(13,5249,450,1463198678),(13,5252,450,1463198633),(13,5253,450,1463198625),(13,5257,450,1463198667),(13,5299,1,1463078334),(13,5300,1,1463078334),(13,5301,8,1463199929),(13,5328,3100,1463078016),(13,5329,3100,1463078016),(13,5330,3100,1463078016),(13,5331,4000,1463078016),(13,5332,3000,1463078016),(13,5371,24786,1463339663),(13,5372,480541,1463339930),(13,5373,100000,1463340291),(13,5374,22288,1463338644),(13,5375,213475,1463339514),(13,5512,4,1463338529),(13,5529,30,1463340304),(13,5532,3,1463336314),(13,5551,450,1463198660),(13,5556,450,1463198633),(13,5557,450,1463198625),(13,5561,450,1463198667),(13,5562,450,1463198678),(13,5563,450,1463198674),(13,5565,450,1463198660),(13,5570,450,1463198633),(13,5571,450,1463198625),(13,5575,450,1463198667),(13,5576,400,1463197476),(13,5585,400,1463197476),(13,5586,400,1463197476),(13,5589,400,1463197476),(13,5591,400,1463197476),(13,5592,450,1463198678),(13,5593,8,1463339930),(13,5701,450,1463198674),(13,5711,450,1463198660),(13,5714,450,1463198667),(13,5716,450,1463198633),(13,5717,450,1463198625),(13,5763,1,1463198674),(13,5816,1,1463078394),(13,5817,1,1463078314),(13,5860,1,1463088898),(13,5868,1,1463235593),(13,6140,3,1463340592),(13,6141,101,1463340592),(13,6142,101,1463340592),(13,6164,1,1463200017),(13,6166,1,1463200005),(13,6386,264,1463198660),(13,6389,449,1463198633),(13,6390,566,1463198625),(13,6392,439,1463198667),(13,6393,181,1463198674),(13,6394,34,1463198678),(13,6534,1,1463199387),(13,6618,1,1463339971),(13,6751,25,1463198674),(13,6752,50,1463198674),(13,6753,75,1463198674),(13,6754,100,1463198674),(13,6755,160,1463198674),(13,6756,1,1463198674),(13,6757,1,1463339971),(13,6845,8,1463339930),(13,7221,181,1463198674),(13,7222,34,1463198678),(13,7223,264,1463198660),(13,7227,449,1463198633),(13,7228,566,1463198625),(13,7230,439,1463198667),(13,7550,10,1463200023),(13,7551,11,1463200032),(13,7552,11,1463200032),(13,8819,500,1463078016),(13,8820,500,1463078016),(13,8821,500,1463078016),(13,8822,500,1463078016),(13,9223,11,1463200032),(13,9369,1,1463089065),(13,9372,1,1463089068),(13,9598,80,1463078114),(13,9619,1,1463081222),(13,9678,3100,1463078016),(13,9679,3000,1463078016),(13,9680,3100,1463078016),(13,9681,3100,1463078016),(13,9682,4000,1463078016),(13,12498,2,1463339971),(13,12499,6,1463340592),(13,12500,6,1463340592),(13,12501,6,1463340592),(13,12502,6,1463340592),(13,12503,6,1463340592),(13,12504,6,1463340592),(13,12505,6,1463340592),(13,12506,6,1463340592),(13,12698,495,1463340564),(13,12770,1,1463339971),(13,12771,1,1463340564),(13,12773,1,1463340304),(13,12775,1,1463339971),(13,12777,1,1463340564),(13,12911,42999,1463078334),(13,12917,1,1463339971),(13,12919,1,1463340564),(13,13008,1,1463200032),(13,13010,1,1463200012),(13,13089,1,1463339971),(13,13090,1,1463339971),(13,13091,1,1463339971),(13,13092,1,1463339971),(13,13093,1,1463340304),(13,13094,1,1463340564),(13,13104,1,1463340304),(13,13105,1,1463340304),(13,13106,1,1463340304),(13,13184,3,1463340592),(13,13242,1,1463080737),(13,13257,1,1463199387),(13,13282,3,1463339699),(13,13297,3,1463339699),(13,13307,8,1463339930),(13,13331,1,1463339971),(13,13332,1,1463340304),(13,13333,1,1463340564),(13,13388,1,1463339971),(13,13399,1,1463198667),(13,13401,1,1463198667),(13,13413,1,1463198667),(13,13427,1,1463198667),(13,13428,1,1463198667);
/*!40000 ALTER TABLE `character_achievement_progress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_action`
--

DROP TABLE IF EXISTS `character_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_action` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `spec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `button` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `action` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spec`,`button`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_action`
--

LOCK TABLES `character_action` WRITE;
/*!40000 ALTER TABLE `character_action` DISABLE KEYS */;
INSERT INTO `character_action` VALUES (1,0,0,26540,0),(1,0,1,2,65),(1,0,2,5176,0),(1,0,11,1,65),(2,0,0,5176,0),(2,0,1,5185,0),(2,0,3,6,64),(2,0,4,11,64),(2,0,5,53201,0),(2,0,10,2,64),(2,0,11,58984,0),(4,0,0,5176,0),(4,0,1,5185,0),(4,0,11,58984,0),(5,0,0,6603,0),(5,0,1,1752,0),(5,0,2,2098,0),(5,0,3,2764,0),(5,0,11,59752,0),(6,0,0,6603,0),(6,0,1,1752,0),(6,0,2,2098,0),(6,0,3,2764,0),(6,0,11,59752,0),(7,0,0,6603,0),(7,0,1,1752,0),(7,0,2,2098,0),(7,0,3,2764,0),(7,0,4,26297,0),(10,0,0,48443,0),(10,0,1,18562,0),(10,0,2,2893,0),(10,0,4,50726,128),(10,0,5,50464,0),(10,0,7,48447,0),(10,0,8,5570,0),(10,0,9,48378,0),(10,0,10,1,65),(10,0,11,58984,0),(10,0,12,22812,0),(10,0,13,53312,0),(10,0,14,51377,128),(10,0,16,17116,0),(10,0,18,770,0),(10,0,19,48447,0),(10,0,23,5,65),(10,0,24,770,0),(10,0,25,17,65),(10,0,26,12,65),(10,0,27,39803,0),(10,0,28,33891,0),(10,0,29,9634,0),(10,0,30,7,65),(10,0,31,9,65),(10,0,32,8,65),(10,0,33,10,65),(10,0,36,50763,0),(10,0,40,6,65),(10,0,41,768,0),(10,0,42,33786,0),(10,0,43,5215,0),(10,0,49,53312,0),(10,0,50,2782,0),(10,0,51,8946,0),(10,0,53,48451,0),(10,0,54,48441,0),(10,0,56,53307,0),(10,0,57,48470,0),(10,0,58,48469,0),(10,0,60,48461,0),(10,0,61,48465,0),(10,0,62,48463,0),(10,0,64,22812,0),(10,0,65,2,65),(10,0,66,53308,0),(10,0,67,48467,0),(10,0,68,3,65),(10,0,69,5,65),(10,0,70,51377,128),(10,0,71,4,65),(10,0,72,33357,0),(10,0,73,49803,0),(10,0,74,5215,0),(10,0,75,48574,0),(10,0,76,50726,128),(10,0,77,49800,0),(10,0,83,58984,0),(10,0,84,48443,0),(10,0,85,18562,0),(10,0,86,2893,0),(10,0,88,50726,128),(10,0,89,50464,0),(10,0,91,48447,0),(10,0,92,5570,0),(10,0,93,48378,0),(10,0,94,1,65),(10,0,95,58984,0),(10,0,96,8983,0),(10,0,97,5229,0),(10,0,99,48480,0),(10,0,100,22842,0),(10,0,101,48568,0),(10,0,103,16857,0),(10,0,104,48560,0),(10,0,105,48562,0),(10,0,106,5209,0),(10,0,107,6795,0),(10,0,109,48463,0),(10,0,110,48461,0),(10,0,111,48465,0),(10,0,116,770,0),(10,0,117,26995,0),(10,0,119,58984,0),(10,1,0,48461,0),(10,1,1,48465,0),(10,1,2,48463,0),(10,1,3,53201,0),(10,1,4,50726,128),(10,1,5,50464,0),(10,1,7,48447,0),(10,1,8,48468,0),(10,1,9,33831,0),(10,1,10,2893,0),(10,1,11,58984,0),(10,1,12,22812,0),(10,1,13,53312,0),(10,1,14,51377,128),(10,1,18,770,0),(10,1,19,48447,0),(10,1,23,5,65),(10,1,24,770,0),(10,1,25,11,65),(10,1,26,12,65),(10,1,27,39803,0),(10,1,28,24858,0),(10,1,29,9634,0),(10,1,30,7,65),(10,1,31,9,65),(10,1,32,8,65),(10,1,33,10,65),(10,1,36,50763,0),(10,1,40,6,65),(10,1,41,768,0),(10,1,42,33786,0),(10,1,43,5215,0),(10,1,49,53312,0),(10,1,50,2782,0),(10,1,51,8946,0),(10,1,53,48451,0),(10,1,54,48441,0),(10,1,56,53307,0),(10,1,57,48470,0),(10,1,58,48469,0),(10,1,60,48443,0),(10,1,61,48378,0),(10,1,62,50464,0),(10,1,64,22812,0),(10,1,65,2,65),(10,1,66,53308,0),(10,1,67,48467,0),(10,1,68,3,65),(10,1,69,5,65),(10,1,70,51377,128),(10,1,71,4,65),(10,1,72,33357,0),(10,1,73,49803,0),(10,1,74,5215,0),(10,1,75,48574,0),(10,1,76,50726,128),(10,1,77,49800,0),(10,1,83,58984,0),(10,1,84,48443,0),(10,1,86,2893,0),(10,1,88,50726,128),(10,1,89,50464,0),(10,1,91,48447,0),(10,1,92,48468,0),(10,1,93,48378,0),(10,1,94,1,65),(10,1,95,58984,0),(10,1,96,8983,0),(10,1,97,5229,0),(10,1,99,48480,0),(10,1,100,22842,0),(10,1,101,48568,0),(10,1,103,16857,0),(10,1,104,48560,0),(10,1,105,48562,0),(10,1,106,5209,0),(10,1,107,6795,0),(10,1,108,48461,0),(10,1,109,48465,0),(10,1,110,48463,0),(10,1,111,53201,0),(10,1,112,50726,128),(10,1,113,50464,0),(10,1,115,48447,0),(10,1,116,48468,0),(10,1,117,33831,0),(10,1,118,2893,0),(10,1,119,58984,0),(11,0,0,6603,0),(11,0,1,49576,0),(11,0,2,49909,0),(11,0,3,49921,0),(11,0,4,49930,0),(11,0,5,49924,0),(11,0,6,49895,0),(11,0,7,49938,0),(11,0,10,1,64),(11,0,11,59752,0),(11,1,0,6603,0),(11,1,1,49576,0),(11,1,2,49909,0),(11,1,3,49921,0),(11,1,4,49930,0),(11,1,5,49895,0),(11,1,10,1,64),(11,1,11,59752,0),(13,0,0,42897,0),(13,0,1,42846,0),(13,0,2,1,65),(13,0,3,47610,0),(13,0,4,42931,0),(13,0,5,42917,0),(13,0,6,12472,0),(13,0,7,12826,0),(13,0,8,2139,0),(13,0,9,30449,0),(13,0,10,475,0),(13,0,11,1953,0),(13,0,27,40733,0),(13,0,30,43523,128),(13,0,31,33312,128),(13,0,32,43015,0),(13,0,33,43046,0),(13,0,34,7301,0),(13,0,35,42995,0),(13,0,41,58659,0),(13,0,42,42956,0),(13,0,43,42985,0),(13,0,45,43017,0),(13,0,46,43008,0),(13,0,47,43002,0),(13,0,48,42940,0),(13,0,49,42926,0),(13,0,51,12051,0),(13,0,52,42842,0),(13,0,53,42833,0),(13,0,55,12043,0),(13,0,56,130,0),(13,0,58,43010,0),(13,0,59,43012,0),(13,0,60,59752,0),(13,0,61,5019,0),(13,0,63,66,0),(13,0,64,55342,0),(13,0,65,43020,0),(13,0,66,45438,0),(13,0,68,42873,0),(13,0,69,42914,0),(13,0,71,42921,0),(13,1,0,42842,0),(13,1,1,42914,0),(13,1,2,47610,0),(13,1,3,42931,0),(13,1,4,42917,0),(13,1,5,44572,0),(13,1,6,12472,0),(13,1,7,12826,0),(13,1,8,2139,0),(13,1,9,30449,0),(13,1,10,475,0),(13,1,11,1953,0),(13,1,24,75614,0),(13,1,30,43523,128),(13,1,31,33312,128),(13,1,32,43015,0),(13,1,33,43046,0),(13,1,34,7301,0),(13,1,35,42995,0),(13,1,36,42777,0),(13,1,41,58659,0),(13,1,42,42956,0),(13,1,43,42985,0),(13,1,45,43017,0),(13,1,46,43008,0),(13,1,47,43002,0),(13,1,48,42940,0),(13,1,49,42926,0),(13,1,50,54646,0),(13,1,51,42897,0),(13,1,52,42846,0),(13,1,53,42833,0),(13,1,55,31687,0),(13,1,56,130,0),(13,1,58,43010,0),(13,1,59,43012,0),(13,1,60,59752,0),(13,1,61,5019,0),(13,1,62,66,0),(13,1,63,55342,0),(13,1,64,43039,0),(13,1,65,43020,0),(13,1,66,45438,0),(13,1,68,11958,0),(13,1,69,42873,0),(13,1,70,12051,0),(13,1,71,42921,0);
/*!40000 ALTER TABLE `character_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_arena_stats`
--

DROP TABLE IF EXISTS `character_arena_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_arena_stats` (
  `guid` int(10) NOT NULL,
  `slot` tinyint(3) NOT NULL,
  `matchMakerRating` smallint(5) NOT NULL,
  `maxReachedMatchMakerRating` smallint(5) NOT NULL,
  PRIMARY KEY (`guid`,`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_arena_stats`
--

LOCK TABLES `character_arena_stats` WRITE;
/*!40000 ALTER TABLE `character_arena_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_arena_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_aura`
--

DROP TABLE IF EXISTS `character_aura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_aura` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `casterGuid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Full Global Unique Identifier',
  `itemGuid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `effectMask` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `recalculateMask` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `stackCount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `amount0` int(11) NOT NULL DEFAULT '0',
  `amount1` int(11) NOT NULL DEFAULT '0',
  `amount2` int(11) NOT NULL DEFAULT '0',
  `base_amount0` int(11) NOT NULL DEFAULT '0',
  `base_amount1` int(11) NOT NULL DEFAULT '0',
  `base_amount2` int(11) NOT NULL DEFAULT '0',
  `maxDuration` int(11) NOT NULL DEFAULT '0',
  `remainTime` int(11) NOT NULL DEFAULT '0',
  `remainCharges` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`casterGuid`,`itemGuid`,`spell`,`effectMask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_aura`
--

LOCK TABLES `character_aura` WRITE;
/*!40000 ALTER TABLE `character_aura` DISABLE KEYS */;
INSERT INTO `character_aura` VALUES (1,1,0,48162,1,1,1,165,0,0,164,0,0,3600000,120135,0),(2,2,0,47524,1,1,1,0,0,0,0,0,0,-1,-1,0),(2,2,4611686018427387942,43827,1,1,1,0,0,0,0,0,0,-1,-1,0),(2,2,4611686018427387942,61983,3,3,1,0,310,0,0,309,0,-1,-1,0),(2,2,4611686018427387942,72523,1,1,1,0,0,0,0,0,0,-1,-1,0),(2,2,4611686018427388022,37806,1,0,1,0,0,0,0,0,0,-1,-1,0),(7,7,0,8326,7,7,1,-20,50,50,-21,49,49,-1,-1,0),(7,7,0,55164,7,7,1,0,280,100,0,279,99,-1,-1,0),(10,10,0,48469,7,7,1,1050,51,75,749,36,53,1800000,242447,0),(10,10,4611686018427388634,37806,1,0,1,0,0,0,0,0,0,-1,-1,0),(10,13,0,43002,1,1,1,60,0,0,59,0,0,3600000,1936447,0),(11,11,0,48263,7,7,1,60,45,-8,59,44,-9,-1,-1,0),(11,11,0,57340,1,1,1,43,0,0,42,0,0,-1,-1,0),(11,11,0,61261,3,3,1,8,600,0,7,599,0,-1,-1,0),(11,11,0,69127,1,1,1,-20,0,0,-21,0,0,-1,-1,0),(11,11,0,71905,1,1,6,180,0,0,29,0,0,60000,27195,0),(11,11,0,72523,1,1,1,0,0,0,0,0,0,-1,-1,0),(11,11,0,73828,7,7,1,30,30,30,29,29,29,-1,-1,0),(13,13,0,40733,1,1,1,-99,0,0,-100,0,0,-1,-1,0);
/*!40000 ALTER TABLE `character_aura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_banned`
--

DROP TABLE IF EXISTS `character_banned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_banned` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `bandate` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ban List';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_banned`
--

LOCK TABLES `character_banned` WRITE;
/*!40000 ALTER TABLE `character_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_banned` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_battleground_data`
--

DROP TABLE IF EXISTS `character_battleground_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_battleground_data` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `instanceId` int(10) unsigned NOT NULL COMMENT 'Instance Identifier',
  `team` smallint(5) unsigned NOT NULL,
  `joinX` float NOT NULL DEFAULT '0',
  `joinY` float NOT NULL DEFAULT '0',
  `joinZ` float NOT NULL DEFAULT '0',
  `joinO` float NOT NULL DEFAULT '0',
  `joinMapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `taxiStart` int(10) unsigned NOT NULL DEFAULT '0',
  `taxiEnd` int(10) unsigned NOT NULL DEFAULT '0',
  `mountSpell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_battleground_data`
--

LOCK TABLES `character_battleground_data` WRITE;
/*!40000 ALTER TABLE `character_battleground_data` DISABLE KEYS */;
INSERT INTO `character_battleground_data` VALUES (1,0,0,0,0,0,0,65535,0,0,0),(2,0,0,0,0,0,0,65535,0,0,0),(4,0,0,0,0,0,0,65535,0,0,0),(5,0,0,0,0,0,0,65535,0,0,0),(6,0,0,0,0,0,0,65535,0,0,0),(7,0,0,0,0,0,0,65535,0,0,0),(10,0,0,0,0,0,0,65535,0,0,0),(11,0,0,0,0,0,0,65535,0,0,0),(13,0,0,0,0,0,0,65535,0,0,0);
/*!40000 ALTER TABLE `character_battleground_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_battleground_random`
--

DROP TABLE IF EXISTS `character_battleground_random`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_battleground_random` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_battleground_random`
--

LOCK TABLES `character_battleground_random` WRITE;
/*!40000 ALTER TABLE `character_battleground_random` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_battleground_random` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_declinedname`
--

DROP TABLE IF EXISTS `character_declinedname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_declinedname` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `genitive` varchar(15) NOT NULL DEFAULT '',
  `dative` varchar(15) NOT NULL DEFAULT '',
  `accusative` varchar(15) NOT NULL DEFAULT '',
  `instrumental` varchar(15) NOT NULL DEFAULT '',
  `prepositional` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_declinedname`
--

LOCK TABLES `character_declinedname` WRITE;
/*!40000 ALTER TABLE `character_declinedname` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_declinedname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_equipmentsets`
--

DROP TABLE IF EXISTS `character_equipmentsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_equipmentsets` (
  `guid` int(10) NOT NULL DEFAULT '0',
  `setguid` bigint(20) NOT NULL AUTO_INCREMENT,
  `setindex` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(31) NOT NULL,
  `iconname` varchar(100) NOT NULL,
  `ignore_mask` int(11) unsigned NOT NULL DEFAULT '0',
  `item0` int(11) unsigned NOT NULL DEFAULT '0',
  `item1` int(11) unsigned NOT NULL DEFAULT '0',
  `item2` int(11) unsigned NOT NULL DEFAULT '0',
  `item3` int(11) unsigned NOT NULL DEFAULT '0',
  `item4` int(11) unsigned NOT NULL DEFAULT '0',
  `item5` int(11) unsigned NOT NULL DEFAULT '0',
  `item6` int(11) unsigned NOT NULL DEFAULT '0',
  `item7` int(11) unsigned NOT NULL DEFAULT '0',
  `item8` int(11) unsigned NOT NULL DEFAULT '0',
  `item9` int(11) unsigned NOT NULL DEFAULT '0',
  `item10` int(11) unsigned NOT NULL DEFAULT '0',
  `item11` int(11) unsigned NOT NULL DEFAULT '0',
  `item12` int(11) unsigned NOT NULL DEFAULT '0',
  `item13` int(11) unsigned NOT NULL DEFAULT '0',
  `item14` int(11) unsigned NOT NULL DEFAULT '0',
  `item15` int(11) unsigned NOT NULL DEFAULT '0',
  `item16` int(11) unsigned NOT NULL DEFAULT '0',
  `item17` int(11) unsigned NOT NULL DEFAULT '0',
  `item18` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`setguid`),
  UNIQUE KEY `idx_set` (`guid`,`setguid`,`setindex`),
  KEY `Idx_setindex` (`setindex`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_equipmentsets`
--

LOCK TABLES `character_equipmentsets` WRITE;
/*!40000 ALTER TABLE `character_equipmentsets` DISABLE KEYS */;
INSERT INTO `character_equipmentsets` VALUES (2,1,0,'sexos','INV_Misc_Bandana_03',0,79,90,85,118,80,81,84,82,111,83,86,87,96,0,92,38,0,117,0),(2,2,1,'sexy2','INV_Boots_Fabric_01',0,79,90,85,118,80,81,84,339,111,83,86,87,96,0,92,38,0,117,0),(10,3,0,'bala pve','inv_misc_rubysanctum2',0,415,504,462,730,464,511,512,513,517,414,489,521,482,520,505,487,522,523,731),(10,4,1,'resto pvp','inv_shoulder_128',0,470,553,472,0,473,554,471,479,481,474,490,556,555,478,480,475,477,476,0),(13,5,0,'arc pve','inv_misc_rubysanctum2',0,631,646,629,617,630,643,719,693,644,632,641,642,639,640,645,638,647,649,0),(13,6,1,'Frost PvP','inv_misc_rubysanctum2',0,697,708,699,617,698,710,718,706,709,702,641,721,639,720,707,638,703,701,704);
/*!40000 ALTER TABLE `character_equipmentsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_gifts`
--

DROP TABLE IF EXISTS `character_gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_gifts` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `item_guid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_guid`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_gifts`
--

LOCK TABLES `character_gifts` WRITE;
/*!40000 ALTER TABLE `character_gifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_glyphs`
--

DROP TABLE IF EXISTS `character_glyphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_glyphs` (
  `guid` int(10) unsigned NOT NULL,
  `talentGroup` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `glyph1` smallint(5) unsigned DEFAULT '0',
  `glyph2` smallint(5) unsigned DEFAULT '0',
  `glyph3` smallint(5) unsigned DEFAULT '0',
  `glyph4` smallint(5) unsigned DEFAULT '0',
  `glyph5` smallint(5) unsigned DEFAULT '0',
  `glyph6` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`guid`,`talentGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_glyphs`
--

LOCK TABLES `character_glyphs` WRITE;
/*!40000 ALTER TABLE `character_glyphs` DISABLE KEYS */;
INSERT INTO `character_glyphs` VALUES (1,0,0,0,0,0,0,0),(2,0,0,0,0,0,0,0),(4,0,0,0,0,0,0,0),(5,0,0,0,0,0,0,0),(6,0,0,0,0,0,0,0),(7,0,0,0,0,0,0,0),(10,0,0,0,0,0,0,0),(10,1,178,0,0,169,0,175),(11,0,0,0,0,0,0,0),(11,1,0,0,0,0,0,0),(13,0,312,0,0,651,451,328),(13,1,315,0,0,700,451,329);
/*!40000 ALTER TABLE `character_glyphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_homebind`
--

DROP TABLE IF EXISTS `character_homebind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_homebind` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `zoneId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Zone Identifier',
  `posX` float NOT NULL DEFAULT '0',
  `posY` float NOT NULL DEFAULT '0',
  `posZ` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_homebind`
--

LOCK TABLES `character_homebind` WRITE;
/*!40000 ALTER TABLE `character_homebind` DISABLE KEYS */;
INSERT INTO `character_homebind` VALUES (1,530,3431,10349.6,-6357.29,33.4026),(2,1,141,10311.3,832.463,1326.41),(4,1,141,10311.3,832.463,1326.41),(5,0,12,-8949.95,-132.493,83.5312),(6,0,12,-8949.95,-132.493,83.5312),(7,1,14,-618.518,-4251.67,38.718),(10,1,141,10311.3,832.463,1326.41),(11,0,1519,-8832.95,623.306,93.7561),(13,0,12,-8949.95,-132.493,83.5312);
/*!40000 ALTER TABLE `character_homebind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_instance`
--

DROP TABLE IF EXISTS `character_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_instance` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `instance` int(10) unsigned NOT NULL DEFAULT '0',
  `permanent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `extendState` tinyint(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`,`instance`),
  KEY `instance` (`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_instance`
--

LOCK TABLES `character_instance` WRITE;
/*!40000 ALTER TABLE `character_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_inventory`
--

DROP TABLE IF EXISTS `character_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_inventory` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `bag` int(10) unsigned NOT NULL DEFAULT '0',
  `slot` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Item Global Unique Identifier',
  PRIMARY KEY (`item`),
  UNIQUE KEY `guid` (`guid`,`bag`,`slot`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_inventory`
--

LOCK TABLES `character_inventory` WRITE;
/*!40000 ALTER TABLE `character_inventory` DISABLE KEYS */;
INSERT INTO `character_inventory` VALUES (1,0,0,254),(1,0,4,255),(1,0,7,256),(1,0,19,354),(1,0,20,356),(1,0,21,353),(1,0,22,352),(1,0,23,10),(1,0,24,6),(1,0,25,8),(1,0,26,2),(1,0,27,75),(1,0,28,78),(1,0,29,20),(1,0,30,4),(1,0,32,74),(1,0,33,217),(1,0,34,210),(1,0,35,355),(1,0,36,12),(1,0,38,18),(1,0,118,164),(2,0,0,79),(2,0,1,90),(2,0,2,85),(2,0,3,118),(2,0,4,80),(2,0,5,81),(2,0,6,84),(2,0,7,339),(2,0,8,111),(2,0,9,83),(2,0,10,86),(2,0,11,87),(2,0,12,96),(2,0,14,92),(2,0,15,38),(2,0,17,117),(2,0,19,30),(2,0,20,32),(2,0,21,34),(2,0,22,36),(2,0,23,28),(2,0,24,100),(2,0,25,22),(2,0,26,24),(2,0,28,110),(2,0,29,91),(2,0,30,26),(2,0,31,115),(2,0,32,104),(2,0,33,171),(2,0,34,109),(2,0,35,82),(2,0,36,103),(2,0,118,163),(4,0,4,60),(4,0,6,62),(4,0,15,58),(4,0,19,66),(4,0,20,68),(4,0,21,70),(4,0,22,72),(4,0,23,64),(5,0,3,120),(5,0,6,124),(5,0,7,122),(5,0,15,128),(5,0,16,130),(5,0,17,126),(5,0,19,134),(5,0,20,136),(5,0,21,138),(5,0,22,140),(5,0,23,132),(5,0,24,345),(5,0,25,346),(6,0,3,142),(6,0,6,146),(6,0,7,144),(6,0,15,150),(6,0,16,152),(6,0,17,148),(6,0,19,156),(6,0,20,158),(6,0,21,160),(6,0,22,162),(6,0,23,154),(7,0,3,313),(7,0,6,315),(7,0,7,317),(7,0,15,311),(7,0,16,319),(7,0,17,321),(7,0,19,325),(7,0,20,327),(7,0,21,329),(7,0,22,331),(7,0,23,323),(10,0,0,415),(10,0,1,504),(10,0,2,462),(10,0,3,730),(10,0,4,464),(10,0,5,511),(10,0,6,512),(10,0,7,513),(10,0,8,517),(10,0,9,414),(10,0,10,489),(10,0,11,521),(10,0,12,482),(10,0,13,520),(10,0,14,505),(10,0,15,487),(10,0,16,522),(10,0,17,523),(10,0,18,731),(10,0,19,403),(10,0,20,405),(10,0,21,407),(10,0,22,409),(10,0,23,401),(10,0,24,570),(10,0,25,572),(10,0,26,526),(10,0,27,527),(10,0,28,547),(10,0,29,537),(10,0,30,530),(10,0,31,538),(10,0,32,573),(10,0,33,577),(10,0,34,560),(10,0,35,605),(10,0,36,606),(10,0,37,607),(10,0,118,757),(10,403,0,481),(10,403,4,480),(10,403,8,479),(10,403,12,478),(10,403,16,483),(10,403,17,484),(10,403,18,485),(10,403,19,555),(10,403,20,490),(10,403,21,491),(10,403,22,556),(10,403,24,553),(10,403,28,554),(10,403,35,578),(10,405,0,470),(10,405,4,472),(10,405,8,473),(10,405,12,471),(10,405,16,474),(10,405,24,475),(10,405,28,477),(10,405,32,476),(10,407,0,413),(10,407,3,468),(10,407,4,461),(10,407,7,466),(10,407,8,463),(10,407,11,467),(10,407,12,418),(10,407,13,419),(10,407,15,465),(10,407,16,417),(10,407,19,469),(10,409,0,739),(10,409,4,738),(10,409,8,737),(10,409,28,399),(10,409,32,395),(10,409,33,397),(11,0,0,514),(11,0,2,519),(11,0,3,503),(11,0,4,506),(11,0,6,516),(11,0,9,509),(11,0,10,752),(11,0,11,755),(11,0,15,497),(11,0,19,495),(11,0,20,492),(11,0,21,493),(11,0,22,494),(11,0,23,453),(11,0,24,456),(11,0,25,457),(11,0,26,458),(11,0,27,455),(11,0,28,496),(11,0,29,460),(11,0,30,498),(11,0,31,582),(11,0,32,500),(11,0,33,753),(11,0,34,421),(11,0,35,423),(11,0,36,439),(11,0,37,425),(11,0,38,427),(11,0,118,756),(11,495,0,429),(11,495,1,431),(11,495,2,433),(11,495,3,435),(11,495,4,437),(11,495,5,441),(11,495,6,451),(11,495,8,507),(11,495,9,508),(11,495,11,510),(11,495,12,754),(11,495,13,515),(11,495,15,518),(13,0,0,631),(13,0,1,646),(13,0,2,629),(13,0,3,723),(13,0,4,630),(13,0,5,643),(13,0,6,719),(13,0,7,693),(13,0,8,644),(13,0,9,632),(13,0,10,641),(13,0,11,642),(13,0,12,639),(13,0,13,640),(13,0,14,645),(13,0,15,638),(13,0,16,647),(13,0,17,649),(13,0,19,636),(13,0,20,637),(13,0,21,634),(13,0,22,635),(13,0,23,619),(13,0,24,782),(13,0,25,783),(13,0,27,772),(13,0,28,791),(13,0,29,792),(13,0,67,785),(13,0,68,790),(13,0,69,784),(13,0,70,788),(13,0,71,789),(13,0,72,786),(13,0,73,787),(13,0,118,795),(13,635,0,771),(13,635,1,727),(13,635,2,724),(13,635,4,735),(13,635,5,736),(13,635,6,734),(13,636,0,697),(13,636,4,698),(13,636,8,699),(13,636,12,628),(13,636,16,702),(13,636,20,705),(13,636,28,720),(13,636,32,721),(13,637,0,709),(13,637,1,704),(13,637,4,706),(13,637,8,708),(13,637,12,707),(13,637,16,710),(13,637,20,700),(13,637,21,718),(13,637,28,701),(13,637,32,703),(13,787,0,747),(13,787,1,759),(13,787,2,758),(13,787,3,732),(13,787,8,768),(13,787,9,763),(13,787,10,733),(13,787,12,769),(13,787,13,764),(13,787,14,742),(13,787,16,765),(13,787,17,760),(13,787,18,748),(13,787,20,767),(13,787,21,762),(13,787,22,617),(13,787,24,766),(13,787,25,761);
/*!40000 ALTER TABLE `character_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_pet`
--

DROP TABLE IF EXISTS `character_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_pet` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` int(10) unsigned NOT NULL DEFAULT '0',
  `owner` int(10) unsigned NOT NULL DEFAULT '0',
  `modelid` int(10) unsigned DEFAULT '0',
  `CreatedBySpell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `PetType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `level` smallint(5) unsigned NOT NULL DEFAULT '1',
  `exp` int(10) unsigned NOT NULL DEFAULT '0',
  `Reactstate` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(21) NOT NULL DEFAULT 'Pet',
  `renamed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `slot` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `curhealth` int(10) unsigned NOT NULL DEFAULT '1',
  `curmana` int(10) unsigned NOT NULL DEFAULT '0',
  `curhappiness` int(10) unsigned NOT NULL DEFAULT '0',
  `savetime` int(10) unsigned NOT NULL DEFAULT '0',
  `abdata` text,
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`),
  KEY `idx_slot` (`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pet System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_pet`
--

LOCK TABLES `character_pet` WRITE;
/*!40000 ALTER TABLE `character_pet` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_pet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_pet_declinedname`
--

DROP TABLE IF EXISTS `character_pet_declinedname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_pet_declinedname` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `owner` int(10) unsigned NOT NULL DEFAULT '0',
  `genitive` varchar(12) NOT NULL DEFAULT '',
  `dative` varchar(12) NOT NULL DEFAULT '',
  `accusative` varchar(12) NOT NULL DEFAULT '',
  `instrumental` varchar(12) NOT NULL DEFAULT '',
  `prepositional` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `owner_key` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_pet_declinedname`
--

LOCK TABLES `character_pet_declinedname` WRITE;
/*!40000 ALTER TABLE `character_pet_declinedname` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_pet_declinedname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus`
--

DROP TABLE IF EXISTS `character_queststatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `explored` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timer` int(10) unsigned NOT NULL DEFAULT '0',
  `mobcount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `mobcount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `mobcount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `mobcount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `itemcount1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `itemcount2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `itemcount3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `itemcount4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `playercount` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`quest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus`
--

LOCK TABLES `character_queststatus` WRITE;
/*!40000 ALTER TABLE `character_queststatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_queststatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus_daily`
--

DROP TABLE IF EXISTS `character_queststatus_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus_daily` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus_daily`
--

LOCK TABLES `character_queststatus_daily` WRITE;
/*!40000 ALTER TABLE `character_queststatus_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_queststatus_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus_monthly`
--

DROP TABLE IF EXISTS `character_queststatus_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus_monthly` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus_monthly`
--

LOCK TABLES `character_queststatus_monthly` WRITE;
/*!40000 ALTER TABLE `character_queststatus_monthly` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_queststatus_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus_rewarded`
--

DROP TABLE IF EXISTS `character_queststatus_rewarded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus_rewarded` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `active` tinyint(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`,`quest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus_rewarded`
--

LOCK TABLES `character_queststatus_rewarded` WRITE;
/*!40000 ALTER TABLE `character_queststatus_rewarded` DISABLE KEYS */;
INSERT INTO `character_queststatus_rewarded` VALUES (1,24811,1),(10,24808,1),(13,24811,1);
/*!40000 ALTER TABLE `character_queststatus_rewarded` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus_seasonal`
--

DROP TABLE IF EXISTS `character_queststatus_seasonal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus_seasonal` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `event` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Identifier',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus_seasonal`
--

LOCK TABLES `character_queststatus_seasonal` WRITE;
/*!40000 ALTER TABLE `character_queststatus_seasonal` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_queststatus_seasonal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_queststatus_weekly`
--

DROP TABLE IF EXISTS `character_queststatus_weekly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_queststatus_weekly` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_queststatus_weekly`
--

LOCK TABLES `character_queststatus_weekly` WRITE;
/*!40000 ALTER TABLE `character_queststatus_weekly` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_queststatus_weekly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_reputation`
--

DROP TABLE IF EXISTS `character_reputation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_reputation` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `faction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `standing` int(11) NOT NULL DEFAULT '0',
  `flags` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`faction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_reputation`
--

LOCK TABLES `character_reputation` WRITE;
/*!40000 ALTER TABLE `character_reputation` DISABLE KEYS */;
INSERT INTO `character_reputation` VALUES (1,21,0,64),(1,46,0,4),(1,47,0,6),(1,54,0,6),(1,59,0,16),(1,67,0,25),(1,68,0,17),(1,69,0,6),(1,70,0,2),(1,72,0,6),(1,76,0,17),(1,81,0,17),(1,83,0,4),(1,86,0,4),(1,87,0,2),(1,92,0,2),(1,93,0,2),(1,169,0,12),(1,270,0,16),(1,289,0,4),(1,349,0,0),(1,369,0,64),(1,469,0,14),(1,470,0,64),(1,471,0,22),(1,509,0,2),(1,510,0,16),(1,529,0,0),(1,530,0,17),(1,549,0,4),(1,550,0,4),(1,551,0,4),(1,569,0,4),(1,570,0,4),(1,571,0,4),(1,574,0,4),(1,576,0,2),(1,577,0,64),(1,589,0,6),(1,609,0,0),(1,729,0,16),(1,730,0,2),(1,749,0,0),(1,809,0,16),(1,889,0,16),(1,890,0,6),(1,891,0,0),(1,892,0,24),(1,909,0,16),(1,910,0,2),(1,911,0,17),(1,922,0,16),(1,930,0,6),(1,932,0,80),(1,933,0,16),(1,934,0,80),(1,935,0,16),(1,936,0,28),(1,941,0,16),(1,942,0,16),(1,946,0,2),(1,947,0,16),(1,948,0,8),(1,949,0,24),(1,952,0,0),(1,967,0,16),(1,970,0,0),(1,978,0,2),(1,980,0,0),(1,989,0,16),(1,990,0,16),(1,1005,0,4),(1,1011,0,16),(1,1012,0,16),(1,1015,0,2),(1,1031,0,16),(1,1037,0,6),(1,1038,0,16),(1,1050,0,6),(1,1052,0,152),(1,1064,0,16),(1,1067,0,16),(1,1068,0,6),(1,1073,0,16),(1,1077,0,16),(1,1082,0,2),(1,1085,0,16),(1,1090,0,17),(1,1091,0,16),(1,1094,0,6),(1,1097,0,0),(1,1098,0,16),(1,1104,0,16),(1,1105,0,16),(1,1106,0,16),(1,1117,0,12),(1,1118,0,12),(1,1119,0,2),(1,1124,0,16),(1,1126,0,2),(1,1136,0,4),(1,1137,0,4),(1,1154,0,4),(1,1155,0,4),(1,1156,42999,17),(2,21,0,64),(2,46,0,4),(2,47,0,17),(2,54,0,17),(2,59,0,16),(2,67,0,14),(2,68,0,6),(2,69,0,17),(2,70,0,2),(2,72,0,17),(2,76,0,6),(2,81,0,6),(2,83,0,4),(2,86,0,4),(2,87,0,2),(2,92,0,2),(2,93,0,2),(2,169,0,12),(2,270,0,16),(2,289,0,4),(2,349,0,0),(2,369,0,64),(2,469,0,25),(2,470,0,64),(2,471,0,20),(2,509,0,16),(2,510,0,2),(2,529,0,0),(2,530,0,6),(2,549,0,4),(2,550,0,4),(2,551,0,4),(2,569,0,4),(2,570,0,4),(2,571,0,4),(2,574,0,4),(2,576,0,2),(2,577,0,64),(2,589,0,0),(2,609,0,0),(2,729,0,2),(2,730,0,16),(2,749,0,0),(2,809,0,16),(2,889,0,6),(2,890,0,16),(2,891,0,24),(2,892,0,14),(2,909,0,16),(2,910,0,2),(2,911,0,6),(2,922,0,6),(2,930,0,17),(2,932,0,80),(2,933,0,16),(2,934,0,80),(2,935,0,16),(2,936,0,28),(2,941,0,6),(2,942,0,16),(2,946,0,16),(2,947,0,2),(2,948,0,8),(2,949,0,24),(2,952,0,0),(2,967,0,16),(2,970,0,0),(2,978,0,16),(2,980,0,0),(2,989,0,16),(2,990,0,16),(2,1005,0,4),(2,1011,0,16),(2,1012,0,16),(2,1015,0,2),(2,1031,0,16),(2,1037,0,136),(2,1038,0,16),(2,1050,0,16),(2,1052,0,2),(2,1064,0,6),(2,1067,0,2),(2,1068,0,16),(2,1073,0,16),(2,1077,0,16),(2,1082,0,4),(2,1085,0,6),(2,1090,0,17),(2,1091,0,16),(2,1094,0,16),(2,1097,0,0),(2,1098,0,16),(2,1104,0,16),(2,1105,0,16),(2,1106,0,17),(2,1117,0,12),(2,1118,0,12),(2,1119,0,2),(2,1124,0,6),(2,1126,0,16),(2,1136,0,4),(2,1137,0,4),(2,1154,0,4),(2,1155,0,4),(2,1156,2680,17),(4,21,0,64),(4,46,0,4),(4,47,0,17),(4,54,0,17),(4,59,0,16),(4,67,0,14),(4,68,0,6),(4,69,0,17),(4,70,0,2),(4,72,0,17),(4,76,0,6),(4,81,0,6),(4,83,0,4),(4,86,0,4),(4,87,0,2),(4,92,0,2),(4,93,0,2),(4,169,0,12),(4,270,0,16),(4,289,0,4),(4,349,0,0),(4,369,0,64),(4,469,0,25),(4,470,0,64),(4,471,0,20),(4,509,0,16),(4,510,0,2),(4,529,0,0),(4,530,0,6),(4,549,0,4),(4,550,0,4),(4,551,0,4),(4,569,0,4),(4,570,0,4),(4,571,0,4),(4,574,0,4),(4,576,0,2),(4,577,0,64),(4,589,0,0),(4,609,0,0),(4,729,0,2),(4,730,0,16),(4,749,0,0),(4,809,0,16),(4,889,0,6),(4,890,0,16),(4,891,0,24),(4,892,0,14),(4,909,0,16),(4,910,0,2),(4,911,0,6),(4,922,0,6),(4,930,0,17),(4,932,0,80),(4,933,0,16),(4,934,0,80),(4,935,0,16),(4,936,0,28),(4,941,0,6),(4,942,0,16),(4,946,0,16),(4,947,0,2),(4,948,0,8),(4,949,0,24),(4,952,0,0),(4,967,0,16),(4,970,0,0),(4,978,0,16),(4,980,0,0),(4,989,0,16),(4,990,0,16),(4,1005,0,4),(4,1011,0,16),(4,1012,0,16),(4,1015,0,2),(4,1031,0,16),(4,1037,0,136),(4,1038,0,16),(4,1050,0,16),(4,1052,0,2),(4,1064,0,6),(4,1067,0,2),(4,1068,0,16),(4,1073,0,16),(4,1077,0,16),(4,1082,0,4),(4,1085,0,6),(4,1090,0,16),(4,1091,0,16),(4,1094,0,16),(4,1097,0,0),(4,1098,0,16),(4,1104,0,16),(4,1105,0,16),(4,1106,0,16),(4,1117,0,12),(4,1118,0,12),(4,1119,0,2),(4,1124,0,6),(4,1126,0,16),(4,1136,0,4),(4,1137,0,4),(4,1154,0,4),(4,1155,0,4),(4,1156,0,16),(5,21,0,64),(5,46,0,4),(5,47,0,17),(5,54,0,17),(5,59,0,16),(5,67,0,14),(5,68,0,6),(5,69,0,17),(5,70,0,2),(5,72,0,17),(5,76,0,6),(5,81,0,6),(5,83,0,4),(5,86,0,4),(5,87,0,2),(5,92,0,2),(5,93,0,2),(5,169,0,12),(5,270,0,16),(5,289,0,4),(5,349,0,0),(5,369,0,64),(5,469,0,25),(5,470,0,64),(5,471,0,20),(5,509,0,16),(5,510,0,2),(5,529,0,0),(5,530,0,6),(5,549,0,4),(5,550,0,4),(5,551,0,4),(5,569,0,4),(5,570,0,4),(5,571,0,4),(5,574,0,4),(5,576,0,2),(5,577,0,64),(5,589,0,0),(5,609,0,0),(5,729,0,2),(5,730,0,16),(5,749,0,0),(5,809,0,16),(5,889,0,6),(5,890,0,16),(5,891,0,24),(5,892,0,14),(5,909,0,16),(5,910,0,2),(5,911,0,6),(5,922,0,6),(5,930,0,17),(5,932,0,80),(5,933,0,16),(5,934,0,80),(5,935,0,16),(5,936,0,28),(5,941,0,6),(5,942,0,16),(5,946,0,16),(5,947,0,2),(5,948,0,8),(5,949,0,24),(5,952,0,0),(5,967,0,16),(5,970,0,0),(5,978,0,16),(5,980,0,0),(5,989,0,16),(5,990,0,16),(5,1005,0,4),(5,1011,0,16),(5,1012,0,16),(5,1015,0,2),(5,1031,0,16),(5,1037,0,136),(5,1038,0,16),(5,1050,0,16),(5,1052,0,2),(5,1064,0,6),(5,1067,0,2),(5,1068,0,16),(5,1073,0,16),(5,1077,0,16),(5,1082,0,4),(5,1085,0,6),(5,1090,0,16),(5,1091,0,16),(5,1094,0,16),(5,1097,0,0),(5,1098,0,16),(5,1104,0,16),(5,1105,0,16),(5,1106,0,16),(5,1117,0,12),(5,1118,0,12),(5,1119,0,2),(5,1124,0,6),(5,1126,0,16),(5,1136,0,4),(5,1137,0,4),(5,1154,0,4),(5,1155,0,4),(5,1156,0,16),(6,21,0,64),(6,46,0,4),(6,47,0,17),(6,54,0,17),(6,59,0,16),(6,67,0,14),(6,68,0,6),(6,69,0,17),(6,70,0,2),(6,72,0,17),(6,76,0,6),(6,81,0,6),(6,83,0,4),(6,86,0,4),(6,87,0,2),(6,92,0,2),(6,93,0,2),(6,169,0,12),(6,270,0,16),(6,289,0,4),(6,349,0,0),(6,369,0,64),(6,469,0,25),(6,470,0,64),(6,471,0,20),(6,509,0,16),(6,510,0,2),(6,529,0,0),(6,530,0,6),(6,549,0,4),(6,550,0,4),(6,551,0,4),(6,569,0,4),(6,570,0,4),(6,571,0,4),(6,574,0,4),(6,576,0,2),(6,577,0,64),(6,589,0,0),(6,609,0,0),(6,729,0,2),(6,730,0,16),(6,749,0,0),(6,809,0,16),(6,889,0,6),(6,890,0,16),(6,891,0,24),(6,892,0,14),(6,909,0,16),(6,910,0,0),(6,911,0,6),(6,922,0,6),(6,930,0,17),(6,932,0,80),(6,933,0,16),(6,934,0,80),(6,935,0,16),(6,936,0,28),(6,941,0,6),(6,942,0,16),(6,946,0,16),(6,947,0,0),(6,948,0,8),(6,949,0,24),(6,952,0,0),(6,967,0,16),(6,970,0,0),(6,978,0,16),(6,980,0,0),(6,989,0,16),(6,990,0,16),(6,1005,0,4),(6,1011,0,16),(6,1012,0,16),(6,1015,0,2),(6,1031,0,16),(6,1037,0,136),(6,1038,0,16),(6,1050,0,16),(6,1052,0,0),(6,1064,0,6),(6,1067,0,0),(6,1068,0,16),(6,1073,0,16),(6,1077,0,16),(6,1082,0,4),(6,1085,0,6),(6,1090,0,16),(6,1091,0,16),(6,1094,0,16),(6,1097,0,0),(6,1098,0,16),(6,1104,0,16),(6,1105,0,16),(6,1106,0,16),(6,1117,0,12),(6,1118,0,12),(6,1119,0,0),(6,1124,0,6),(6,1126,0,16),(6,1136,0,4),(6,1137,0,4),(6,1154,0,4),(6,1155,0,4),(6,1156,0,16),(7,21,0,64),(7,46,0,4),(7,47,0,6),(7,54,0,6),(7,59,0,16),(7,67,0,25),(7,68,0,17),(7,69,0,6),(7,70,0,2),(7,72,0,6),(7,76,0,17),(7,81,0,17),(7,83,0,4),(7,86,0,4),(7,87,0,2),(7,92,0,2),(7,93,0,2),(7,169,0,12),(7,270,0,16),(7,289,0,4),(7,349,0,0),(7,369,0,64),(7,469,0,14),(7,470,0,64),(7,471,0,22),(7,509,0,2),(7,510,0,16),(7,529,0,0),(7,530,0,17),(7,549,0,4),(7,550,0,4),(7,551,0,4),(7,569,0,4),(7,570,0,4),(7,571,0,4),(7,574,0,4),(7,576,0,2),(7,577,0,64),(7,589,0,6),(7,609,0,0),(7,729,0,16),(7,730,0,2),(7,749,0,0),(7,809,0,16),(7,889,0,16),(7,890,0,6),(7,891,0,14),(7,892,0,24),(7,909,0,16),(7,910,0,2),(7,911,0,17),(7,922,0,16),(7,930,0,6),(7,932,0,80),(7,933,0,16),(7,934,0,80),(7,935,0,16),(7,936,0,28),(7,941,0,16),(7,942,0,16),(7,946,0,2),(7,947,0,16),(7,948,0,8),(7,949,0,24),(7,952,0,0),(7,967,0,16),(7,970,0,0),(7,978,0,2),(7,980,0,0),(7,989,0,16),(7,990,0,16),(7,1005,0,4),(7,1011,0,16),(7,1012,0,16),(7,1015,0,2),(7,1031,0,16),(7,1037,0,6),(7,1038,0,16),(7,1050,0,6),(7,1052,0,152),(7,1064,0,16),(7,1067,0,16),(7,1068,0,6),(7,1073,0,16),(7,1077,0,16),(7,1082,0,2),(7,1085,0,16),(7,1090,0,16),(7,1091,0,16),(7,1094,0,6),(7,1097,0,0),(7,1098,0,16),(7,1104,0,16),(7,1105,0,16),(7,1106,0,16),(7,1117,0,12),(7,1118,0,12),(7,1119,0,2),(7,1124,0,16),(7,1126,0,2),(7,1136,0,4),(7,1137,0,4),(7,1154,0,4),(7,1155,0,4),(7,1156,0,16),(10,21,0,64),(10,46,0,4),(10,47,0,17),(10,54,0,17),(10,59,0,16),(10,67,0,14),(10,68,0,6),(10,69,0,17),(10,70,0,2),(10,72,0,17),(10,76,0,6),(10,81,0,6),(10,83,0,4),(10,86,0,4),(10,87,0,2),(10,92,0,2),(10,93,0,2),(10,169,0,12),(10,270,0,16),(10,289,0,4),(10,349,0,0),(10,369,0,64),(10,469,0,25),(10,470,0,64),(10,471,0,20),(10,509,0,16),(10,510,0,2),(10,529,0,0),(10,530,0,6),(10,549,0,4),(10,550,0,4),(10,551,0,4),(10,569,0,4),(10,570,0,4),(10,571,0,4),(10,574,0,4),(10,576,0,2),(10,577,0,64),(10,589,0,0),(10,609,0,0),(10,729,0,2),(10,730,0,16),(10,749,0,0),(10,809,0,16),(10,889,0,6),(10,890,0,16),(10,891,0,24),(10,892,0,14),(10,909,0,16),(10,910,0,2),(10,911,0,6),(10,922,0,6),(10,930,0,17),(10,932,0,80),(10,933,0,16),(10,934,0,80),(10,935,0,16),(10,936,0,28),(10,941,0,6),(10,942,0,16),(10,946,0,16),(10,947,0,2),(10,948,0,8),(10,949,0,24),(10,952,0,0),(10,967,0,16),(10,970,0,0),(10,978,0,16),(10,980,0,0),(10,989,0,16),(10,990,0,16),(10,1005,0,4),(10,1011,0,16),(10,1012,0,16),(10,1015,0,2),(10,1031,0,16),(10,1037,0,136),(10,1038,0,16),(10,1050,0,16),(10,1052,0,2),(10,1064,0,6),(10,1067,0,2),(10,1068,0,16),(10,1073,0,16),(10,1077,0,16),(10,1082,0,4),(10,1085,0,6),(10,1090,0,16),(10,1091,0,16),(10,1094,0,16),(10,1097,0,0),(10,1098,0,16),(10,1104,0,16),(10,1105,0,16),(10,1106,0,17),(10,1117,0,12),(10,1118,0,12),(10,1119,0,2),(10,1124,0,6),(10,1126,0,16),(10,1136,0,4),(10,1137,0,4),(10,1154,0,4),(10,1155,0,4),(10,1156,42999,17),(11,21,0,64),(11,46,0,4),(11,47,0,17),(11,54,0,17),(11,59,0,16),(11,67,0,14),(11,68,0,6),(11,69,0,17),(11,70,0,2),(11,72,0,17),(11,76,0,6),(11,81,0,6),(11,83,0,4),(11,86,0,4),(11,87,0,2),(11,92,0,2),(11,93,0,2),(11,169,0,12),(11,270,0,16),(11,289,0,4),(11,349,0,0),(11,369,0,64),(11,469,0,25),(11,470,0,64),(11,471,0,20),(11,509,0,16),(11,510,0,2),(11,529,0,0),(11,530,0,6),(11,549,0,4),(11,550,0,4),(11,551,0,4),(11,569,0,4),(11,570,0,4),(11,571,0,4),(11,574,0,4),(11,576,0,2),(11,577,0,64),(11,589,0,0),(11,609,0,0),(11,729,0,2),(11,730,0,16),(11,749,0,0),(11,809,0,16),(11,889,0,6),(11,890,0,16),(11,891,0,24),(11,892,0,14),(11,909,0,16),(11,910,0,2),(11,911,0,6),(11,922,0,6),(11,930,0,17),(11,932,0,80),(11,933,0,16),(11,934,0,80),(11,935,0,16),(11,936,0,28),(11,941,0,6),(11,942,0,16),(11,946,0,16),(11,947,0,2),(11,948,0,8),(11,949,0,24),(11,952,0,0),(11,967,0,16),(11,970,0,0),(11,978,0,16),(11,980,0,0),(11,989,0,16),(11,990,0,16),(11,1005,0,0),(11,1011,0,16),(11,1012,0,16),(11,1015,0,2),(11,1031,0,16),(11,1037,0,136),(11,1038,0,16),(11,1050,0,16),(11,1052,0,2),(11,1064,0,6),(11,1067,0,2),(11,1068,0,16),(11,1073,0,16),(11,1077,0,16),(11,1082,0,4),(11,1085,0,6),(11,1090,0,16),(11,1091,0,16),(11,1094,0,16),(11,1097,0,0),(11,1098,0,16),(11,1104,0,16),(11,1105,0,16),(11,1106,0,16),(11,1117,0,12),(11,1118,0,12),(11,1119,0,2),(11,1124,0,6),(11,1126,0,16),(11,1136,0,4),(11,1137,0,4),(11,1154,0,4),(11,1155,0,4),(11,1156,42999,17),(13,21,0,64),(13,46,0,4),(13,47,0,17),(13,54,0,17),(13,59,0,16),(13,67,0,14),(13,68,0,6),(13,69,0,17),(13,70,0,2),(13,72,0,17),(13,76,0,6),(13,81,0,6),(13,83,0,4),(13,86,0,4),(13,87,0,2),(13,92,0,2),(13,93,0,2),(13,169,0,12),(13,270,0,16),(13,289,0,4),(13,349,0,0),(13,369,0,64),(13,469,0,25),(13,470,0,64),(13,471,0,20),(13,509,0,16),(13,510,0,2),(13,529,0,0),(13,530,0,6),(13,549,0,4),(13,550,0,4),(13,551,0,4),(13,569,0,4),(13,570,0,4),(13,571,0,4),(13,574,0,4),(13,576,0,2),(13,577,0,64),(13,589,0,0),(13,609,0,0),(13,729,0,2),(13,730,0,16),(13,749,0,0),(13,809,0,16),(13,889,0,6),(13,890,0,16),(13,891,0,24),(13,892,0,14),(13,909,0,16),(13,910,0,2),(13,911,0,6),(13,922,0,6),(13,930,0,17),(13,932,0,80),(13,933,0,16),(13,934,0,80),(13,935,0,16),(13,936,0,28),(13,941,0,6),(13,942,0,16),(13,946,0,16),(13,947,0,2),(13,948,0,8),(13,949,0,24),(13,952,0,0),(13,967,0,16),(13,970,0,0),(13,978,0,16),(13,980,0,0),(13,989,0,16),(13,990,0,16),(13,1005,0,4),(13,1011,0,16),(13,1012,0,16),(13,1015,0,2),(13,1031,0,16),(13,1037,0,136),(13,1038,0,16),(13,1050,0,16),(13,1052,0,2),(13,1064,0,6),(13,1067,0,2),(13,1068,0,16),(13,1073,0,16),(13,1077,0,16),(13,1082,0,4),(13,1085,0,6),(13,1090,0,17),(13,1091,0,16),(13,1094,0,16),(13,1097,0,0),(13,1098,0,16),(13,1104,0,16),(13,1105,0,16),(13,1106,0,16),(13,1117,0,12),(13,1118,0,12),(13,1119,0,2),(13,1124,0,6),(13,1126,0,16),(13,1136,0,4),(13,1137,0,4),(13,1154,0,4),(13,1155,0,4),(13,1156,42999,17);
/*!40000 ALTER TABLE `character_reputation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_skills`
--

DROP TABLE IF EXISTS `character_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_skills` (
  `guid` int(10) unsigned NOT NULL COMMENT 'Global Unique Identifier',
  `skill` smallint(5) unsigned NOT NULL,
  `value` smallint(5) unsigned NOT NULL,
  `max` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`guid`,`skill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_skills`
--

LOCK TABLES `character_skills` WRITE;
/*!40000 ALTER TABLE `character_skills` DISABLE KEYS */;
INSERT INTO `character_skills` VALUES (1,43,400,400),(1,44,400,400),(1,54,400,400),(1,55,400,400),(1,95,400,400),(1,109,300,300),(1,137,300,300),(1,160,400,400),(1,162,400,400),(1,171,1,75),(1,172,400,400),(1,183,400,400),(1,184,400,400),(1,229,400,400),(1,267,400,400),(1,293,1,1),(1,413,1,1),(1,414,1,1),(1,415,1,1),(1,433,1,1),(1,594,400,400),(1,756,400,400),(1,762,300,300),(1,777,400,400),(1,778,400,400),(2,54,405,405),(2,95,405,405),(2,98,300,300),(2,113,300,300),(2,126,405,405),(2,134,405,405),(2,136,405,405),(2,160,405,405),(2,162,405,405),(2,173,405,405),(2,183,405,405),(2,229,405,405),(2,414,1,1),(2,415,1,1),(2,473,405,405),(2,573,405,405),(2,574,405,405),(2,762,300,300),(2,777,405,405),(2,778,405,405),(4,54,1,400),(4,95,1,400),(4,98,300,300),(4,113,300,300),(4,126,5,400),(4,134,5,400),(4,136,1,400),(4,162,1,400),(4,173,1,400),(4,183,5,400),(4,414,1,1),(4,415,1,1),(4,573,5,400),(4,574,5,400),(4,777,1,400),(4,778,1,400),(5,38,5,10),(5,39,5,10),(5,95,6,10),(5,98,300,300),(5,118,5,10),(5,162,1,10),(5,173,7,10),(5,176,1,10),(5,183,5,10),(5,253,5,10),(5,414,1,1),(5,415,1,1),(5,754,5,10),(5,777,1,10),(5,778,1,10),(6,38,5,5),(6,39,5,5),(6,95,1,5),(6,98,300,300),(6,118,5,5),(6,162,1,5),(6,173,1,5),(6,176,1,5),(6,183,5,5),(6,253,5,5),(6,414,1,1),(6,415,1,1),(6,754,5,5),(6,777,1,5),(6,778,1,5),(7,38,5,400),(7,39,5,400),(7,95,14,400),(7,109,300,300),(7,118,5,400),(7,162,1,400),(7,173,7,400),(7,176,3,400),(7,183,5,400),(7,253,5,400),(7,315,300,300),(7,414,1,1),(7,415,1,1),(7,733,5,400),(7,777,1,400),(7,778,1,400),(10,54,400,400),(10,95,400,400),(10,98,300,300),(10,113,300,300),(10,126,400,400),(10,134,400,400),(10,136,400,400),(10,160,400,400),(10,162,400,400),(10,173,400,400),(10,183,400,400),(10,229,400,400),(10,414,1,1),(10,415,1,1),(10,473,400,400),(10,573,400,400),(10,574,400,400),(10,762,300,300),(10,777,400,400),(10,778,400,400),(11,43,400,400),(11,44,400,400),(11,54,400,400),(11,55,400,400),(11,95,400,400),(11,98,300,300),(11,118,400,400),(11,129,270,300),(11,160,400,400),(11,162,400,400),(11,172,400,400),(11,183,400,400),(11,229,400,400),(11,293,1,1),(11,413,1,1),(11,414,1,1),(11,415,1,1),(11,754,400,400),(11,762,300,300),(11,770,400,400),(11,771,400,400),(11,772,400,400),(11,776,1,1),(11,777,400,400),(11,778,400,400),(13,6,400,400),(13,8,400,400),(13,43,400,400),(13,95,400,400),(13,98,300,300),(13,129,450,450),(13,136,400,400),(13,162,400,400),(13,171,450,450),(13,173,400,400),(13,183,400,400),(13,185,450,450),(13,197,450,450),(13,228,400,400),(13,237,400,400),(13,415,1,1),(13,754,400,400),(13,755,450,450),(13,762,300,300),(13,773,450,450),(13,777,400,400),(13,778,400,400);
/*!40000 ALTER TABLE `character_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_social`
--

DROP TABLE IF EXISTS `character_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_social` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
  `friend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Friend Global Unique Identifier',
  `flags` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Friend Flags',
  `note` varchar(48) NOT NULL DEFAULT '' COMMENT 'Friend Note',
  PRIMARY KEY (`guid`,`friend`,`flags`),
  KEY `friend` (`friend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_social`
--

LOCK TABLES `character_social` WRITE;
/*!40000 ALTER TABLE `character_social` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_spell`
--

DROP TABLE IF EXISTS `character_spell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_spell` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell Identifier',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `disabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_spell`
--

LOCK TABLES `character_spell` WRITE;
/*!40000 ALTER TABLE `character_spell` DISABLE KEYS */;
INSERT INTO `character_spell` VALUES (1,227,1,0),(1,2259,1,0),(1,5176,1,0),(1,7328,1,0),(1,20250,1,0),(1,26540,1,0),(1,34091,1,0),(1,39803,1,0),(1,41276,1,0),(1,54197,1,0),(1,59573,1,0),(1,65917,1,0),(2,674,1,0),(2,5487,1,0),(2,6795,1,0),(2,34091,1,0),(2,48717,1,0),(2,53201,1,0),(2,54197,1,0),(2,61983,1,0),(2,71810,1,0),(10,768,1,0),(10,770,1,0),(10,783,1,0),(10,1066,1,0),(10,2782,1,0),(10,2893,1,0),(10,5209,1,0),(10,5215,1,0),(10,5225,1,0),(10,5229,1,0),(10,5420,1,1),(10,6795,1,0),(10,8946,1,0),(10,8983,1,0),(10,9634,1,0),(10,16818,1,0),(10,16820,1,0),(10,16822,1,0),(10,16835,1,0),(10,16847,1,1),(10,16857,1,0),(10,16864,1,0),(10,16899,1,0),(10,16913,1,0),(10,16924,1,0),(10,17051,1,0),(10,17061,1,0),(10,17065,1,0),(10,17077,1,1),(10,17108,1,1),(10,17113,1,1),(10,17116,1,1),(10,17120,1,1),(10,18562,1,1),(10,18658,1,0),(10,20719,1,0),(10,22812,1,0),(10,22842,1,0),(10,24858,1,0),(10,24946,1,1),(10,26995,1,0),(10,29166,1,0),(10,33357,1,0),(10,33591,1,0),(10,33596,1,0),(10,33602,1,0),(10,33607,1,0),(10,33786,1,0),(10,33831,1,0),(10,33880,1,1),(10,33883,1,1),(10,33890,1,1),(10,33891,1,1),(10,34091,1,0),(10,34153,1,1),(10,35364,1,0),(10,39803,1,0),(10,40120,1,0),(10,40733,1,0),(10,48378,1,0),(10,48396,1,0),(10,48412,1,0),(10,48441,1,0),(10,48443,1,0),(10,48447,1,0),(10,48451,1,0),(10,48461,1,0),(10,48463,1,0),(10,48465,1,0),(10,48467,1,0),(10,48468,1,0),(10,48469,1,0),(10,48470,1,0),(10,48477,1,0),(10,48480,1,0),(10,48511,1,0),(10,48525,1,0),(10,48537,1,1),(10,48560,1,0),(10,48562,1,0),(10,48568,1,0),(10,48570,1,0),(10,48572,1,0),(10,48574,1,0),(10,48575,1,0),(10,48577,1,0),(10,48579,1,0),(10,49800,1,0),(10,49802,1,0),(10,49803,1,0),(10,50213,1,0),(10,50464,1,0),(10,50763,1,0),(10,52610,1,0),(10,53201,1,0),(10,53307,1,0),(10,53308,1,0),(10,53312,1,0),(10,54197,1,0),(10,57810,1,0),(10,57851,1,0),(10,57865,1,0),(10,59573,1,0),(10,61346,1,0),(10,62078,1,0),(10,62600,1,0),(10,63411,1,1),(10,65139,1,1),(11,3714,1,0),(11,34091,1,0),(11,39803,1,0),(11,42650,1,0),(11,42777,1,0),(11,45524,1,0),(11,45529,1,0),(11,46584,1,0),(11,47476,1,0),(11,47528,1,0),(11,47568,1,0),(11,48263,1,0),(11,48265,1,0),(11,48707,1,0),(11,48743,1,0),(11,48792,1,0),(11,49895,1,0),(11,49909,1,0),(11,49921,1,0),(11,49924,1,0),(11,49930,1,0),(11,49938,1,0),(11,49941,1,0),(11,50842,1,0),(11,51425,1,0),(11,53323,1,0),(11,53331,1,0),(11,53342,1,0),(11,53344,1,0),(11,53428,1,0),(11,54197,1,0),(11,54446,1,0),(11,54447,1,0),(11,56222,1,0),(11,56815,1,0),(11,57623,1,0),(11,59573,1,0),(11,61999,1,0),(11,62158,1,0),(11,65917,1,0),(11,70164,1,0),(13,66,1,0),(13,130,1,0),(13,475,1,0),(13,1953,1,0),(13,2139,1,0),(13,2331,1,0),(13,2332,1,0),(13,2333,1,0),(13,2334,1,0),(13,2335,1,0),(13,2336,1,0),(13,2337,1,0),(13,2385,1,0),(13,2386,1,0),(13,2389,1,0),(13,2392,1,0),(13,2393,1,0),(13,2394,1,0),(13,2395,1,0),(13,2396,1,0),(13,2397,1,0),(13,2399,1,0),(13,2401,1,0),(13,2402,1,0),(13,2403,1,0),(13,2406,1,0),(13,2539,1,0),(13,2541,1,0),(13,2542,1,0),(13,2543,1,0),(13,2544,1,0),(13,2545,1,0),(13,2546,1,0),(13,2547,1,0),(13,2548,1,0),(13,2549,1,0),(13,2795,1,0),(13,2964,1,0),(13,3170,1,0),(13,3171,1,0),(13,3172,1,0),(13,3173,1,0),(13,3174,1,0),(13,3175,1,0),(13,3176,1,0),(13,3177,1,0),(13,3188,1,0),(13,3230,1,0),(13,3276,1,0),(13,3277,1,0),(13,3278,1,0),(13,3370,1,0),(13,3371,1,0),(13,3372,1,0),(13,3373,1,0),(13,3376,1,0),(13,3377,1,0),(13,3397,1,0),(13,3398,1,0),(13,3399,1,0),(13,3400,1,0),(13,3447,1,0),(13,3448,1,0),(13,3449,1,0),(13,3450,1,0),(13,3451,1,0),(13,3452,1,0),(13,3453,1,0),(13,3454,1,0),(13,3755,1,0),(13,3757,1,0),(13,3758,1,0),(13,3813,1,0),(13,3839,1,0),(13,3840,1,0),(13,3841,1,0),(13,3842,1,0),(13,3843,1,0),(13,3844,1,0),(13,3845,1,0),(13,3847,1,0),(13,3848,1,0),(13,3849,1,0),(13,3850,1,0),(13,3851,1,0),(13,3852,1,0),(13,3854,1,0),(13,3855,1,0),(13,3856,1,0),(13,3857,1,0),(13,3858,1,0),(13,3859,1,0),(13,3860,1,0),(13,3861,1,0),(13,3862,1,0),(13,3863,1,0),(13,3864,1,0),(13,3865,1,0),(13,3866,1,0),(13,3868,1,0),(13,3869,1,0),(13,3870,1,0),(13,3871,1,0),(13,3872,1,0),(13,3873,1,0),(13,3914,1,0),(13,4094,1,0),(13,4508,1,0),(13,4942,1,0),(13,6412,1,0),(13,6413,1,0),(13,6414,1,0),(13,6415,1,0),(13,6416,1,0),(13,6417,1,0),(13,6418,1,0),(13,6419,1,0),(13,6499,1,0),(13,6500,1,0),(13,6501,1,0),(13,6521,1,0),(13,6617,1,0),(13,6618,1,0),(13,6624,1,0),(13,6686,1,0),(13,6688,1,0),(13,6690,1,0),(13,6692,1,0),(13,6693,1,0),(13,6695,1,0),(13,7179,1,0),(13,7181,1,0),(13,7213,1,0),(13,7255,1,0),(13,7256,1,0),(13,7257,1,0),(13,7258,1,0),(13,7259,1,0),(13,7301,1,0),(13,7623,1,0),(13,7624,1,0),(13,7629,1,0),(13,7630,1,0),(13,7633,1,0),(13,7636,1,0),(13,7639,1,0),(13,7643,1,0),(13,7751,1,0),(13,7752,1,0),(13,7753,1,0),(13,7754,1,0),(13,7755,1,0),(13,7827,1,0),(13,7828,1,0),(13,7836,1,0),(13,7837,1,0),(13,7841,1,0),(13,7845,1,0),(13,7892,1,0),(13,7893,1,0),(13,7928,1,0),(13,7929,1,0),(13,7934,1,0),(13,7935,1,0),(13,8238,1,0),(13,8240,1,0),(13,8465,1,0),(13,8467,1,0),(13,8483,1,0),(13,8489,1,0),(13,8607,1,0),(13,8758,1,0),(13,8760,1,0),(13,8762,1,0),(13,8764,1,0),(13,8766,1,0),(13,8770,1,0),(13,8772,1,0),(13,8774,1,0),(13,8776,1,0),(13,8778,1,0),(13,8780,1,0),(13,8782,1,0),(13,8784,1,0),(13,8786,1,0),(13,8789,1,0),(13,8791,1,0),(13,8793,1,0),(13,8795,1,0),(13,8797,1,0),(13,8799,1,0),(13,8802,1,0),(13,8804,1,0),(13,9513,1,0),(13,10840,1,0),(13,10841,1,0),(13,11448,1,0),(13,11449,1,0),(13,11450,1,0),(13,11451,1,0),(13,11452,1,0),(13,11453,1,0),(13,11456,1,0),(13,11457,1,0),(13,11458,1,0),(13,11459,1,0),(13,11460,1,0),(13,11461,1,0),(13,11464,1,0),(13,11465,1,0),(13,11466,1,0),(13,11467,1,0),(13,11468,1,0),(13,11472,1,0),(13,11473,1,0),(13,11476,1,0),(13,11477,1,0),(13,11478,1,0),(13,11479,1,0),(13,11480,1,0),(13,11958,1,1),(13,12042,1,0),(13,12043,1,0),(13,12045,1,0),(13,12046,1,0),(13,12047,1,0),(13,12048,1,0),(13,12049,1,0),(13,12050,1,0),(13,12051,1,0),(13,12052,1,0),(13,12053,1,0),(13,12055,1,0),(13,12056,1,0),(13,12059,1,0),(13,12060,1,0),(13,12061,1,0),(13,12062,1,0),(13,12063,1,0),(13,12064,1,0),(13,12065,1,0),(13,12066,1,0),(13,12067,1,0),(13,12068,1,0),(13,12069,1,0),(13,12070,1,0),(13,12071,1,0),(13,12072,1,0),(13,12073,1,0),(13,12074,1,0),(13,12075,1,0),(13,12076,1,0),(13,12077,1,0),(13,12078,1,0),(13,12079,1,0),(13,12080,1,0),(13,12081,1,0),(13,12082,1,0),(13,12083,1,0),(13,12084,1,0),(13,12085,1,0),(13,12086,1,0),(13,12087,1,0),(13,12088,1,0),(13,12089,1,0),(13,12090,1,0),(13,12091,1,0),(13,12092,1,0),(13,12093,1,0),(13,12469,1,0),(13,12472,1,0),(13,12496,1,0),(13,12503,1,0),(13,12569,1,1),(13,12577,1,0),(13,12592,1,0),(13,12598,1,1),(13,12606,1,1),(13,12609,1,0),(13,12826,1,0),(13,12840,1,0),(13,12953,1,1),(13,12983,1,1),(13,13028,1,0),(13,15047,1,1),(13,15060,1,0),(13,15833,1,0),(13,15853,1,0),(13,15855,1,0),(13,15856,1,0),(13,15861,1,0),(13,15863,1,0),(13,15865,1,0),(13,15906,1,0),(13,15910,1,0),(13,15915,1,0),(13,15933,1,0),(13,15935,1,0),(13,16758,1,1),(13,16766,1,1),(13,16770,1,0),(13,17187,1,0),(13,17551,1,0),(13,17552,1,0),(13,17553,1,0),(13,17554,1,0),(13,17555,1,0),(13,17556,1,0),(13,17557,1,0),(13,17559,1,0),(13,17560,1,0),(13,17561,1,0),(13,17562,1,0),(13,17563,1,0),(13,17564,1,0),(13,17565,1,0),(13,17566,1,0),(13,17570,1,0),(13,17571,1,0),(13,17572,1,0),(13,17573,1,0),(13,17574,1,0),(13,17575,1,0),(13,17576,1,0),(13,17577,1,0),(13,17578,1,0),(13,17579,1,0),(13,17580,1,0),(13,17632,1,0),(13,17634,1,0),(13,17635,1,0),(13,17636,1,0),(13,17637,1,0),(13,17638,1,0),(13,18238,1,0),(13,18239,1,0),(13,18240,1,0),(13,18241,1,0),(13,18242,1,0),(13,18243,1,0),(13,18244,1,0),(13,18245,1,0),(13,18246,1,0),(13,18247,1,0),(13,18401,1,0),(13,18402,1,0),(13,18403,1,0),(13,18404,1,0),(13,18405,1,0),(13,18406,1,0),(13,18407,1,0),(13,18408,1,0),(13,18409,1,0),(13,18410,1,0),(13,18411,1,0),(13,18412,1,0),(13,18413,1,0),(13,18414,1,0),(13,18415,1,0),(13,18416,1,0),(13,18417,1,0),(13,18418,1,0),(13,18419,1,0),(13,18420,1,0),(13,18421,1,0),(13,18422,1,0),(13,18423,1,0),(13,18424,1,0),(13,18434,1,0),(13,18436,1,0),(13,18437,1,0),(13,18438,1,0),(13,18439,1,0),(13,18440,1,0),(13,18441,1,0),(13,18442,1,0),(13,18444,1,0),(13,18445,1,0),(13,18446,1,0),(13,18447,1,0),(13,18448,1,0),(13,18449,1,0),(13,18450,1,0),(13,18451,1,0),(13,18452,1,0),(13,18453,1,0),(13,18454,1,0),(13,18455,1,0),(13,18456,1,0),(13,18457,1,0),(13,18458,1,0),(13,18463,1,0),(13,18560,1,0),(13,18629,1,0),(13,18630,1,0),(13,19435,1,0),(13,20626,1,0),(13,20848,1,0),(13,20849,1,0),(13,20916,1,0),(13,21143,1,0),(13,21144,1,0),(13,21175,1,0),(13,21923,1,0),(13,21945,1,0),(13,22480,1,0),(13,22732,1,0),(13,22759,1,0),(13,22761,1,0),(13,22808,1,0),(13,22813,1,0),(13,22866,1,0),(13,22867,1,0),(13,22868,1,0),(13,22869,1,0),(13,22870,1,0),(13,22902,1,0),(13,23662,1,0),(13,23663,1,0),(13,23664,1,0),(13,23665,1,0),(13,23666,1,0),(13,23667,1,0),(13,23787,1,0),(13,24091,1,0),(13,24092,1,0),(13,24093,1,0),(13,24266,1,0),(13,24365,1,0),(13,24366,1,0),(13,24367,1,0),(13,24368,1,0),(13,24418,1,0),(13,24801,1,0),(13,24901,1,0),(13,24902,1,0),(13,24903,1,0),(13,25146,1,0),(13,25278,1,0),(13,25280,1,0),(13,25283,1,0),(13,25284,1,0),(13,25287,1,0),(13,25305,1,0),(13,25317,1,0),(13,25318,1,0),(13,25320,1,0),(13,25321,1,0),(13,25323,1,0),(13,25339,1,0),(13,25490,1,0),(13,25498,1,0),(13,25610,1,0),(13,25612,1,0),(13,25613,1,0),(13,25614,1,0),(13,25615,1,0),(13,25617,1,0),(13,25618,1,0),(13,25619,1,0),(13,25620,1,0),(13,25621,1,0),(13,25622,1,0),(13,25659,1,0),(13,25704,1,0),(13,25954,1,0),(13,26085,1,0),(13,26086,1,0),(13,26087,1,0),(13,26277,1,0),(13,26403,1,0),(13,26407,1,0),(13,26656,1,0),(13,26745,1,0),(13,26746,1,0),(13,26747,1,0),(13,26749,1,0),(13,26750,1,0),(13,26751,1,0),(13,26752,1,0),(13,26753,1,0),(13,26754,1,0),(13,26755,1,0),(13,26756,1,0),(13,26757,1,0),(13,26758,1,0),(13,26759,1,0),(13,26760,1,0),(13,26761,1,0),(13,26762,1,0),(13,26763,1,0),(13,26764,1,0),(13,26765,1,0),(13,26770,1,0),(13,26771,1,0),(13,26772,1,0),(13,26773,1,0),(13,26774,1,0),(13,26775,1,0),(13,26776,1,0),(13,26777,1,0),(13,26778,1,0),(13,26779,1,0),(13,26780,1,0),(13,26781,1,0),(13,26782,1,0),(13,26783,1,0),(13,26784,1,0),(13,26797,1,0),(13,26798,1,0),(13,26801,1,0),(13,26872,1,0),(13,26873,1,0),(13,26874,1,0),(13,26875,1,0),(13,26876,1,0),(13,26878,1,0),(13,26880,1,0),(13,26881,1,0),(13,26882,1,0),(13,26883,1,0),(13,26885,1,0),(13,26887,1,0),(13,26896,1,0),(13,26897,1,0),(13,26900,1,0),(13,26902,1,0),(13,26903,1,0),(13,26906,1,0),(13,26907,1,0),(13,26908,1,0),(13,26909,1,0),(13,26910,1,0),(13,26911,1,0),(13,26912,1,0),(13,26914,1,0),(13,26915,1,0),(13,26916,1,0),(13,26918,1,0),(13,26920,1,0),(13,26926,1,0),(13,26927,1,0),(13,26928,1,0),(13,27032,1,0),(13,27033,1,0),(13,27090,1,0),(13,27658,1,0),(13,27659,1,0),(13,27660,1,0),(13,27724,1,0),(13,27725,1,0),(13,28205,1,0),(13,28207,1,0),(13,28208,1,0),(13,28209,1,0),(13,28210,1,0),(13,28267,1,0),(13,28332,1,0),(13,28480,1,0),(13,28481,1,0),(13,28482,1,0),(13,28543,1,0),(13,28544,1,0),(13,28545,1,0),(13,28546,1,0),(13,28549,1,0),(13,28550,1,0),(13,28551,1,0),(13,28552,1,0),(13,28553,1,0),(13,28554,1,0),(13,28555,1,0),(13,28556,1,0),(13,28557,1,0),(13,28558,1,0),(13,28562,1,0),(13,28563,1,0),(13,28564,1,0),(13,28565,1,0),(13,28566,1,0),(13,28567,1,0),(13,28568,1,0),(13,28569,1,0),(13,28570,1,0),(13,28571,1,0),(13,28572,1,0),(13,28573,1,0),(13,28575,1,0),(13,28576,1,0),(13,28577,1,0),(13,28578,1,0),(13,28579,1,0),(13,28580,1,0),(13,28581,1,0),(13,28582,1,0),(13,28583,1,0),(13,28584,1,0),(13,28585,1,0),(13,28586,1,0),(13,28587,1,0),(13,28588,1,0),(13,28589,1,0),(13,28590,1,0),(13,28591,1,0),(13,28593,1,1),(13,28672,1,0),(13,28675,1,0),(13,28677,1,0),(13,28903,1,0),(13,28905,1,0),(13,28906,1,0),(13,28907,1,0),(13,28910,1,0),(13,28912,1,0),(13,28914,1,0),(13,28915,1,0),(13,28916,1,0),(13,28917,1,0),(13,28918,1,0),(13,28924,1,0),(13,28925,1,0),(13,28927,1,0),(13,28933,1,0),(13,28936,1,0),(13,28938,1,0),(13,28944,1,0),(13,28947,1,0),(13,28948,1,0),(13,28950,1,0),(13,28953,1,0),(13,28955,1,0),(13,28957,1,0),(13,29440,1,0),(13,29444,1,1),(13,29688,1,0),(13,30449,1,0),(13,31048,1,0),(13,31049,1,0),(13,31050,1,0),(13,31051,1,0),(13,31052,1,0),(13,31053,1,0),(13,31054,1,0),(13,31055,1,0),(13,31056,1,0),(13,31057,1,0),(13,31058,1,0),(13,31060,1,0),(13,31061,1,0),(13,31062,1,0),(13,31063,1,0),(13,31064,1,0),(13,31065,1,0),(13,31066,1,0),(13,31067,1,0),(13,31068,1,0),(13,31070,1,0),(13,31071,1,0),(13,31072,1,0),(13,31076,1,0),(13,31077,1,0),(13,31078,1,0),(13,31079,1,0),(13,31080,1,0),(13,31081,1,0),(13,31082,1,0),(13,31083,1,0),(13,31084,1,0),(13,31085,1,0),(13,31087,1,0),(13,31088,1,0),(13,31089,1,0),(13,31090,1,0),(13,31091,1,0),(13,31092,1,0),(13,31094,1,0),(13,31095,1,0),(13,31096,1,0),(13,31097,1,0),(13,31098,1,0),(13,31099,1,0),(13,31100,1,0),(13,31101,1,0),(13,31102,1,0),(13,31103,1,0),(13,31104,1,0),(13,31105,1,0),(13,31106,1,0),(13,31107,1,0),(13,31108,1,0),(13,31109,1,0),(13,31110,1,0),(13,31111,1,0),(13,31112,1,0),(13,31113,1,0),(13,31149,1,0),(13,31252,1,0),(13,31373,1,0),(13,31430,1,0),(13,31431,1,0),(13,31432,1,0),(13,31433,1,0),(13,31434,1,0),(13,31435,1,0),(13,31437,1,0),(13,31438,1,0),(13,31440,1,0),(13,31441,1,0),(13,31442,1,0),(13,31443,1,0),(13,31444,1,0),(13,31448,1,0),(13,31449,1,0),(13,31450,1,0),(13,31451,1,0),(13,31452,1,0),(13,31453,1,0),(13,31454,1,0),(13,31455,1,0),(13,31456,1,0),(13,31459,1,0),(13,31460,1,0),(13,31461,1,0),(13,31572,1,0),(13,31583,1,0),(13,31588,1,0),(13,31678,1,1),(13,31683,1,1),(13,31687,1,1),(13,32178,1,0),(13,32179,1,0),(13,32765,1,0),(13,32766,1,0),(13,32801,1,0),(13,32807,1,0),(13,32808,1,0),(13,32809,1,0),(13,32810,1,0),(13,32866,1,0),(13,32867,1,0),(13,32868,1,0),(13,32869,1,0),(13,32870,1,0),(13,32871,1,0),(13,32872,1,0),(13,32873,1,0),(13,32874,1,0),(13,33276,1,0),(13,33277,1,0),(13,33278,1,0),(13,33279,1,0),(13,33284,1,0),(13,33285,1,0),(13,33286,1,0),(13,33287,1,0),(13,33288,1,0),(13,33289,1,0),(13,33290,1,0),(13,33291,1,0),(13,33292,1,0),(13,33293,1,0),(13,33294,1,0),(13,33295,1,0),(13,33296,1,0),(13,33717,1,0),(13,33732,1,0),(13,33733,1,0),(13,33738,1,0),(13,33740,1,0),(13,33741,1,0),(13,34069,1,0),(13,34091,1,0),(13,34590,1,0),(13,34955,1,0),(13,34959,1,0),(13,34960,1,0),(13,34961,1,0),(13,35581,1,0),(13,36210,1,0),(13,36315,1,0),(13,36316,1,0),(13,36317,1,0),(13,36318,1,0),(13,36523,1,0),(13,36524,1,0),(13,36525,1,0),(13,36526,1,0),(13,36665,1,0),(13,36667,1,0),(13,36668,1,0),(13,36669,1,0),(13,36670,1,0),(13,36672,1,0),(13,36686,1,0),(13,37818,1,0),(13,37836,1,0),(13,37855,1,0),(13,37873,1,0),(13,37882,1,0),(13,37883,1,0),(13,37884,1,0),(13,38068,1,0),(13,38070,1,0),(13,38175,1,0),(13,38503,1,0),(13,38504,1,0),(13,38867,1,0),(13,38868,1,0),(13,38960,1,0),(13,38961,1,0),(13,38962,1,0),(13,39451,1,0),(13,39452,1,0),(13,39455,1,0),(13,39458,1,0),(13,39462,1,0),(13,39463,1,0),(13,39466,1,0),(13,39467,1,0),(13,39470,1,0),(13,39471,1,0),(13,39636,1,0),(13,39637,1,0),(13,39638,1,0),(13,39639,1,0),(13,39705,1,0),(13,39706,1,0),(13,39710,1,0),(13,39711,1,0),(13,39712,1,0),(13,39713,1,0),(13,39714,1,0),(13,39715,1,0),(13,39716,1,0),(13,39717,1,0),(13,39718,1,0),(13,39719,1,0),(13,39720,1,0),(13,39721,1,0),(13,39722,1,0),(13,39723,1,0),(13,39724,1,0),(13,39725,1,0),(13,39727,1,0),(13,39728,1,0),(13,39729,1,0),(13,39730,1,0),(13,39731,1,0),(13,39732,1,0),(13,39733,1,0),(13,39734,1,0),(13,39735,1,0),(13,39736,1,0),(13,39737,1,0),(13,39738,1,0),(13,39739,1,0),(13,39740,1,0),(13,39741,1,0),(13,39742,1,0),(13,39803,1,0),(13,39961,1,0),(13,39963,1,0),(13,40020,1,0),(13,40021,1,0),(13,40023,1,0),(13,40024,1,0),(13,40060,1,0),(13,40192,1,0),(13,40514,1,0),(13,40733,1,0),(13,41205,1,0),(13,41206,1,0),(13,41207,1,0),(13,41208,1,0),(13,41252,1,0),(13,41414,1,0),(13,41415,1,0),(13,41418,1,0),(13,41420,1,0),(13,41429,1,0),(13,41458,1,0),(13,41500,1,0),(13,41501,1,0),(13,41502,1,0),(13,41503,1,0),(13,42296,1,0),(13,42302,1,0),(13,42305,1,0),(13,42558,1,0),(13,42588,1,0),(13,42589,1,0),(13,42590,1,0),(13,42591,1,0),(13,42592,1,0),(13,42593,1,0),(13,42736,1,0),(13,42777,1,0),(13,42833,1,0),(13,42842,1,0),(13,42846,1,0),(13,42859,1,0),(13,42873,1,0),(13,42897,1,0),(13,42914,1,0),(13,42917,1,0),(13,42921,1,0),(13,42926,1,0),(13,42931,1,0),(13,42940,1,0),(13,42956,1,0),(13,42985,1,0),(13,42995,1,0),(13,43002,1,0),(13,43008,1,0),(13,43010,1,0),(13,43012,1,0),(13,43015,1,0),(13,43017,1,0),(13,43020,1,0),(13,43024,1,0),(13,43039,1,1),(13,43046,1,0),(13,43493,1,0),(13,43707,1,0),(13,43758,1,0),(13,43761,1,0),(13,43765,1,0),(13,43772,1,0),(13,43779,1,0),(13,44379,1,0),(13,44397,1,0),(13,44403,1,0),(13,44545,1,1),(13,44548,1,1),(13,44557,1,1),(13,44571,1,1),(13,44572,1,1),(13,44794,1,0),(13,44950,1,0),(13,44958,1,0),(13,45022,1,0),(13,45061,1,0),(13,45363,1,0),(13,45438,1,0),(13,45542,1,0),(13,45545,1,0),(13,45546,1,0),(13,45549,1,0),(13,45550,1,0),(13,45551,1,0),(13,45552,1,0),(13,45553,1,0),(13,45554,1,0),(13,45555,1,0),(13,45556,1,0),(13,45557,1,0),(13,45558,1,0),(13,45559,1,0),(13,45560,1,0),(13,45561,1,0),(13,45562,1,0),(13,45563,1,0),(13,45564,1,0),(13,45565,1,0),(13,45566,1,0),(13,45567,1,0),(13,45568,1,0),(13,45569,1,0),(13,45570,1,0),(13,45571,1,0),(13,45695,1,0),(13,46122,1,0),(13,46123,1,0),(13,46124,1,0),(13,46125,1,0),(13,46126,1,0),(13,46127,1,0),(13,46128,1,0),(13,46129,1,0),(13,46130,1,0),(13,46131,1,0),(13,46403,1,0),(13,46404,1,0),(13,46405,1,0),(13,46597,1,0),(13,46601,1,0),(13,46684,1,0),(13,46688,1,0),(13,46775,1,0),(13,46776,1,0),(13,46777,1,0),(13,46778,1,0),(13,46779,1,0),(13,46803,1,0),(13,47046,1,0),(13,47048,1,0),(13,47049,1,0),(13,47050,1,0),(13,47053,1,0),(13,47054,1,0),(13,47055,1,0),(13,47056,1,0),(13,47280,1,0),(13,47610,1,0),(13,48121,1,0),(13,48247,1,0),(13,48248,1,0),(13,48789,1,0),(13,49677,1,0),(13,50194,1,0),(13,50598,1,0),(13,50599,1,0),(13,50600,1,0),(13,50601,1,0),(13,50602,1,0),(13,50603,1,0),(13,50604,1,0),(13,50605,1,0),(13,50606,1,0),(13,50607,1,0),(13,50608,1,0),(13,50609,1,0),(13,50610,1,0),(13,50611,1,0),(13,50612,1,0),(13,50614,1,0),(13,50616,1,0),(13,50617,1,0),(13,50618,1,0),(13,50619,1,0),(13,50620,1,0),(13,50644,1,0),(13,50647,1,0),(13,51296,1,0),(13,51304,1,0),(13,51309,1,0),(13,51311,1,0),(13,52175,1,0),(13,52739,1,0),(13,52840,1,0),(13,52843,1,0),(13,53042,1,0),(13,53056,1,0),(13,53462,1,0),(13,53771,1,0),(13,53773,1,0),(13,53774,1,0),(13,53775,1,0),(13,53776,1,0),(13,53777,1,0),(13,53779,1,0),(13,53780,1,0),(13,53781,1,0),(13,53782,1,0),(13,53783,1,0),(13,53784,1,0),(13,53812,1,0),(13,53830,1,0),(13,53831,1,0),(13,53832,1,0),(13,53834,1,0),(13,53835,1,0),(13,53836,1,0),(13,53837,1,0),(13,53838,1,0),(13,53839,1,0),(13,53840,1,0),(13,53841,1,0),(13,53842,1,0),(13,53843,1,0),(13,53844,1,0),(13,53845,1,0),(13,53847,1,0),(13,53848,1,0),(13,53852,1,0),(13,53853,1,0),(13,53854,1,0),(13,53855,1,0),(13,53856,1,0),(13,53857,1,0),(13,53859,1,0),(13,53860,1,0),(13,53861,1,0),(13,53862,1,0),(13,53863,1,0),(13,53864,1,0),(13,53865,1,0),(13,53866,1,0),(13,53867,1,0),(13,53868,1,0),(13,53869,1,0),(13,53870,1,0),(13,53871,1,0),(13,53872,1,0),(13,53873,1,0),(13,53874,1,0),(13,53875,1,0),(13,53876,1,0),(13,53877,1,0),(13,53878,1,0),(13,53879,1,0),(13,53880,1,0),(13,53881,1,0),(13,53882,1,0),(13,53883,1,0),(13,53884,1,0),(13,53885,1,0),(13,53886,1,0),(13,53887,1,0),(13,53888,1,0),(13,53889,1,0),(13,53890,1,0),(13,53891,1,0),(13,53892,1,0),(13,53893,1,0),(13,53894,1,0),(13,53895,1,0),(13,53898,1,0),(13,53899,1,0),(13,53900,1,0),(13,53901,1,0),(13,53902,1,0),(13,53903,1,0),(13,53904,1,0),(13,53905,1,0),(13,53916,1,0),(13,53917,1,0),(13,53918,1,0),(13,53919,1,0),(13,53920,1,0),(13,53921,1,0),(13,53922,1,0),(13,53923,1,0),(13,53924,1,0),(13,53925,1,0),(13,53926,1,0),(13,53927,1,0),(13,53928,1,0),(13,53929,1,0),(13,53930,1,0),(13,53931,1,0),(13,53932,1,0),(13,53933,1,0),(13,53934,1,0),(13,53936,1,0),(13,53937,1,0),(13,53938,1,0),(13,53939,1,0),(13,53940,1,0),(13,53941,1,0),(13,53942,1,0),(13,53943,1,0),(13,53945,1,0),(13,53946,1,0),(13,53947,1,0),(13,53948,1,0),(13,53949,1,0),(13,53950,1,0),(13,53951,1,0),(13,53952,1,0),(13,53953,1,0),(13,53954,1,0),(13,53955,1,0),(13,53956,1,0),(13,53957,1,0),(13,53958,1,0),(13,53959,1,0),(13,53960,1,0),(13,53961,1,0),(13,53962,1,0),(13,53963,1,0),(13,53964,1,0),(13,53965,1,0),(13,53966,1,0),(13,53967,1,0),(13,53968,1,0),(13,53969,1,0),(13,53970,1,0),(13,53971,1,0),(13,53972,1,0),(13,53973,1,0),(13,53974,1,0),(13,53975,1,0),(13,53976,1,0),(13,53977,1,0),(13,53978,1,0),(13,53979,1,0),(13,53980,1,0),(13,53981,1,0),(13,53982,1,0),(13,53983,1,0),(13,53984,1,0),(13,53985,1,0),(13,53986,1,0),(13,53987,1,0),(13,53988,1,0),(13,53989,1,0),(13,53990,1,0),(13,53991,1,0),(13,53992,1,0),(13,53993,1,0),(13,53994,1,0),(13,53995,1,0),(13,53996,1,0),(13,53997,1,0),(13,53998,1,0),(13,54000,1,0),(13,54001,1,0),(13,54002,1,0),(13,54003,1,0),(13,54004,1,0),(13,54005,1,0),(13,54006,1,0),(13,54007,1,0),(13,54008,1,0),(13,54009,1,0),(13,54010,1,0),(13,54011,1,0),(13,54012,1,0),(13,54013,1,0),(13,54014,1,0),(13,54017,1,0),(13,54019,1,0),(13,54020,1,0),(13,54023,1,0),(13,54197,1,0),(13,54213,1,0),(13,54218,1,0),(13,54220,1,0),(13,54221,1,0),(13,54222,1,0),(13,54490,1,0),(13,54646,1,0),(13,54734,1,0),(13,55092,1,1),(13,55094,1,0),(13,55340,1,0),(13,55342,1,0),(13,55384,1,0),(13,55386,1,0),(13,55387,1,0),(13,55388,1,0),(13,55389,1,0),(13,55390,1,0),(13,55392,1,0),(13,55393,1,0),(13,55394,1,0),(13,55395,1,0),(13,55396,1,0),(13,55397,1,0),(13,55398,1,0),(13,55399,1,0),(13,55400,1,0),(13,55401,1,0),(13,55402,1,0),(13,55403,1,0),(13,55404,1,0),(13,55405,1,0),(13,55407,1,0),(13,55534,1,0),(13,55642,1,0),(13,55769,1,0),(13,55777,1,0),(13,55898,1,0),(13,55899,1,0),(13,55900,1,0),(13,55901,1,0),(13,55902,1,0),(13,55903,1,0),(13,55904,1,0),(13,55906,1,0),(13,55907,1,0),(13,55908,1,0),(13,55910,1,0),(13,55911,1,0),(13,55913,1,0),(13,55914,1,0),(13,55919,1,0),(13,55920,1,0),(13,55921,1,0),(13,55922,1,0),(13,55923,1,0),(13,55924,1,0),(13,55925,1,0),(13,55941,1,0),(13,55943,1,0),(13,55993,1,0),(13,55994,1,0),(13,55995,1,0),(13,55996,1,0),(13,55997,1,0),(13,55998,1,0),(13,55999,1,0),(13,56000,1,0),(13,56001,1,0),(13,56002,1,0),(13,56003,1,0),(13,56004,1,0),(13,56005,1,0),(13,56006,1,0),(13,56007,1,0),(13,56008,1,0),(13,56009,1,0),(13,56010,1,0),(13,56011,1,0),(13,56014,1,0),(13,56015,1,0),(13,56016,1,0),(13,56017,1,0),(13,56018,1,0),(13,56019,1,0),(13,56020,1,0),(13,56021,1,0),(13,56022,1,0),(13,56023,1,0),(13,56024,1,0),(13,56025,1,0),(13,56026,1,0),(13,56027,1,0),(13,56028,1,0),(13,56029,1,0),(13,56030,1,0),(13,56031,1,0),(13,56034,1,0),(13,56039,1,0),(13,56048,1,0),(13,56049,1,0),(13,56052,1,0),(13,56053,1,0),(13,56054,1,0),(13,56055,1,0),(13,56056,1,0),(13,56074,1,0),(13,56076,1,0),(13,56077,1,0),(13,56079,1,0),(13,56081,1,0),(13,56083,1,0),(13,56084,1,0),(13,56085,1,0),(13,56086,1,0),(13,56087,1,0),(13,56088,1,0),(13,56089,1,0),(13,56193,1,0),(13,56194,1,0),(13,56195,1,0),(13,56196,1,0),(13,56197,1,0),(13,56199,1,0),(13,56201,1,0),(13,56202,1,0),(13,56203,1,0),(13,56205,1,0),(13,56206,1,0),(13,56208,1,0),(13,56496,1,0),(13,56497,1,0),(13,56498,1,0),(13,56499,1,0),(13,56500,1,0),(13,56501,1,0),(13,56519,1,0),(13,56530,1,0),(13,56531,1,0),(13,56943,1,0),(13,56944,1,0),(13,56945,1,0),(13,56946,1,0),(13,56947,1,0),(13,56948,1,0),(13,56949,1,0),(13,56950,1,0),(13,56951,1,0),(13,56952,1,0),(13,56953,1,0),(13,56954,1,0),(13,56955,1,0),(13,56956,1,0),(13,56957,1,0),(13,56958,1,0),(13,56959,1,0),(13,56960,1,0),(13,56961,1,0),(13,56963,1,0),(13,56965,1,0),(13,56968,1,0),(13,56971,1,0),(13,56972,1,0),(13,56973,1,0),(13,56974,1,0),(13,56975,1,0),(13,56976,1,0),(13,56977,1,0),(13,56978,1,0),(13,56979,1,0),(13,56980,1,0),(13,56981,1,0),(13,56982,1,0),(13,56983,1,0),(13,56984,1,0),(13,56985,1,0),(13,56986,1,0),(13,56987,1,0),(13,56988,1,0),(13,56989,1,0),(13,56990,1,0),(13,56991,1,0),(13,56994,1,0),(13,56995,1,0),(13,56996,1,0),(13,56997,1,0),(13,56998,1,0),(13,56999,1,0),(13,57000,1,0),(13,57001,1,0),(13,57002,1,0),(13,57003,1,0),(13,57004,1,0),(13,57005,1,0),(13,57006,1,0),(13,57007,1,0),(13,57008,1,0),(13,57009,1,0),(13,57010,1,0),(13,57011,1,0),(13,57012,1,0),(13,57013,1,0),(13,57014,1,0),(13,57019,1,0),(13,57020,1,0),(13,57021,1,0),(13,57022,1,0),(13,57023,1,0),(13,57024,1,0),(13,57025,1,0),(13,57026,1,0),(13,57027,1,0),(13,57028,1,0),(13,57029,1,0),(13,57030,1,0),(13,57031,1,0),(13,57032,1,0),(13,57033,1,0),(13,57034,1,0),(13,57035,1,0),(13,57036,1,0),(13,57112,1,0),(13,57113,1,0),(13,57114,1,0),(13,57115,1,0),(13,57116,1,0),(13,57117,1,0),(13,57119,1,0),(13,57120,1,0),(13,57121,1,0),(13,57122,1,0),(13,57123,1,0),(13,57124,1,0),(13,57125,1,0),(13,57126,1,0),(13,57127,1,0),(13,57128,1,0),(13,57129,1,0),(13,57130,1,0),(13,57131,1,0),(13,57132,1,0),(13,57133,1,0),(13,57151,1,0),(13,57152,1,0),(13,57153,1,0),(13,57154,1,0),(13,57155,1,0),(13,57156,1,0),(13,57157,1,0),(13,57158,1,0),(13,57159,1,0),(13,57160,1,0),(13,57161,1,0),(13,57162,1,0),(13,57163,1,0),(13,57164,1,0),(13,57165,1,0),(13,57166,1,0),(13,57167,1,0),(13,57168,1,0),(13,57169,1,0),(13,57170,1,0),(13,57172,1,0),(13,57181,1,0),(13,57183,1,0),(13,57184,1,0),(13,57185,1,0),(13,57186,1,0),(13,57187,1,0),(13,57188,1,0),(13,57189,1,0),(13,57190,1,0),(13,57191,1,0),(13,57192,1,0),(13,57193,1,0),(13,57194,1,0),(13,57195,1,0),(13,57196,1,0),(13,57197,1,0),(13,57198,1,0),(13,57199,1,0),(13,57200,1,0),(13,57201,1,0),(13,57202,1,0),(13,57207,1,0),(13,57208,1,0),(13,57209,1,0),(13,57210,1,0),(13,57211,1,0),(13,57212,1,0),(13,57213,1,0),(13,57214,1,0),(13,57215,1,0),(13,57216,1,0),(13,57217,1,0),(13,57218,1,0),(13,57219,1,0),(13,57220,1,0),(13,57221,1,0),(13,57222,1,0),(13,57223,1,0),(13,57224,1,0),(13,57225,1,0),(13,57226,1,0),(13,57227,1,0),(13,57228,1,0),(13,57229,1,0),(13,57230,1,0),(13,57231,1,0),(13,57232,1,0),(13,57233,1,0),(13,57234,1,0),(13,57235,1,0),(13,57236,1,0),(13,57237,1,0),(13,57238,1,0),(13,57239,1,0),(13,57240,1,0),(13,57241,1,0),(13,57242,1,0),(13,57243,1,0),(13,57244,1,0),(13,57245,1,0),(13,57246,1,0),(13,57247,1,0),(13,57248,1,0),(13,57249,1,0),(13,57250,1,0),(13,57251,1,0),(13,57252,1,0),(13,57253,1,0),(13,57257,1,0),(13,57258,1,0),(13,57259,1,0),(13,57260,1,0),(13,57261,1,0),(13,57262,1,0),(13,57263,1,0),(13,57264,1,0),(13,57265,1,0),(13,57266,1,0),(13,57267,1,0),(13,57268,1,0),(13,57269,1,0),(13,57270,1,0),(13,57271,1,0),(13,57272,1,0),(13,57273,1,0),(13,57274,1,0),(13,57275,1,0),(13,57276,1,0),(13,57277,1,0),(13,57421,1,0),(13,57423,1,0),(13,57425,1,0),(13,57427,1,0),(13,57433,1,0),(13,57434,1,0),(13,57435,1,0),(13,57436,1,0),(13,57437,1,0),(13,57438,1,0),(13,57439,1,0),(13,57440,1,0),(13,57441,1,0),(13,57442,1,0),(13,57443,1,0),(13,57703,1,0),(13,57704,1,0),(13,57706,1,0),(13,57707,1,0),(13,57708,1,0),(13,57709,1,0),(13,57710,1,0),(13,57711,1,0),(13,57712,1,0),(13,57713,1,0),(13,57714,1,0),(13,57715,1,0),(13,57716,1,0),(13,57719,1,0),(13,58065,1,0),(13,58141,1,0),(13,58142,1,0),(13,58143,1,0),(13,58144,1,0),(13,58145,1,0),(13,58146,1,0),(13,58147,1,0),(13,58148,1,0),(13,58149,1,0),(13,58150,1,0),(13,58286,1,0),(13,58287,1,0),(13,58288,1,0),(13,58289,1,0),(13,58296,1,0),(13,58297,1,0),(13,58298,1,0),(13,58299,1,0),(13,58300,1,0),(13,58301,1,0),(13,58302,1,0),(13,58303,1,0),(13,58305,1,0),(13,58306,1,0),(13,58307,1,0),(13,58308,1,0),(13,58310,1,0),(13,58311,1,0),(13,58312,1,0),(13,58313,1,0),(13,58314,1,0),(13,58315,1,0),(13,58316,1,0),(13,58317,1,0),(13,58318,1,0),(13,58319,1,0),(13,58320,1,0),(13,58321,1,0),(13,58322,1,0),(13,58323,1,0),(13,58324,1,0),(13,58325,1,0),(13,58326,1,0),(13,58327,1,0),(13,58328,1,0),(13,58329,1,0),(13,58330,1,0),(13,58331,1,0),(13,58332,1,0),(13,58333,1,0),(13,58336,1,0),(13,58337,1,0),(13,58338,1,0),(13,58339,1,0),(13,58340,1,0),(13,58341,1,0),(13,58342,1,0),(13,58343,1,0),(13,58344,1,0),(13,58345,1,0),(13,58346,1,0),(13,58347,1,0),(13,58472,1,0),(13,58473,1,0),(13,58476,1,0),(13,58478,1,0),(13,58480,1,0),(13,58481,1,0),(13,58482,1,0),(13,58483,1,0),(13,58484,1,0),(13,58485,1,0),(13,58486,1,0),(13,58487,1,0),(13,58488,1,0),(13,58489,1,0),(13,58490,1,0),(13,58491,1,0),(13,58492,1,0),(13,58507,1,0),(13,58512,1,0),(13,58521,1,0),(13,58523,1,0),(13,58525,1,0),(13,58527,1,0),(13,58528,1,0),(13,58565,1,0),(13,58659,1,0),(13,58868,1,0),(13,58871,1,0),(13,58954,1,0),(13,59315,1,0),(13,59326,1,0),(13,59338,1,0),(13,59339,1,0),(13,59340,1,0),(13,59387,1,0),(13,59390,1,0),(13,59475,1,0),(13,59478,1,0),(13,59480,1,0),(13,59484,1,0),(13,59486,1,0),(13,59487,1,0),(13,59488,1,0),(13,59489,1,0),(13,59490,1,0),(13,59491,1,0),(13,59493,1,0),(13,59494,1,0),(13,59495,1,0),(13,59496,1,0),(13,59497,1,0),(13,59498,1,0),(13,59499,1,0),(13,59500,1,0),(13,59501,1,0),(13,59502,1,0),(13,59503,1,0),(13,59504,1,0),(13,59559,1,0),(13,59560,1,0),(13,59561,1,0),(13,59573,1,0),(13,59582,1,0),(13,59583,1,0),(13,59584,1,0),(13,59585,1,0),(13,59586,1,0),(13,59587,1,0),(13,59588,1,0),(13,59589,1,0),(13,59759,1,0),(13,60336,1,0),(13,60337,1,0),(13,60350,1,0),(13,60354,1,0),(13,60355,1,0),(13,60356,1,0),(13,60357,1,0),(13,60365,1,0),(13,60366,1,0),(13,60367,1,0),(13,60396,1,0),(13,60403,1,0),(13,60405,1,0),(13,60893,1,0),(13,60969,1,0),(13,60971,1,0),(13,60990,1,0),(13,60993,1,0),(13,60994,1,0),(13,61117,1,0),(13,61118,1,0),(13,61119,1,0),(13,61120,1,0),(13,61177,1,0),(13,61288,1,0),(13,61677,1,0),(13,62044,1,0),(13,62045,1,0),(13,62049,1,0),(13,62050,1,0),(13,62051,1,0),(13,62162,1,0),(13,62213,1,0),(13,62242,1,0),(13,62350,1,0),(13,62409,1,0),(13,62410,1,0),(13,62941,1,0),(13,63203,1,0),(13,63204,1,0),(13,63205,1,0),(13,63206,1,0),(13,63732,1,0),(13,63742,1,0),(13,63743,1,0),(13,63844,1,0),(13,63924,1,0),(13,64051,1,0),(13,64053,1,0),(13,64054,1,0),(13,64246,1,0),(13,64247,1,0),(13,64248,1,0),(13,64249,1,0),(13,64250,1,0),(13,64251,1,0),(13,64252,1,0),(13,64253,1,0),(13,64254,1,0),(13,64255,1,0),(13,64256,1,0),(13,64257,1,0),(13,64258,1,0),(13,64259,1,0),(13,64260,1,0),(13,64261,1,0),(13,64262,1,0),(13,64266,1,0),(13,64267,1,0),(13,64268,1,0),(13,64270,1,0),(13,64271,1,0),(13,64273,1,0),(13,64274,1,0),(13,64275,1,0),(13,64276,1,0),(13,64277,1,0),(13,64278,1,0),(13,64279,1,0),(13,64280,1,0),(13,64281,1,0),(13,64282,1,0),(13,64283,1,0),(13,64284,1,0),(13,64285,1,0),(13,64286,1,0),(13,64287,1,0),(13,64288,1,0),(13,64289,1,0),(13,64291,1,0),(13,64294,1,0),(13,64295,1,0),(13,64296,1,0),(13,64297,1,0),(13,64298,1,0),(13,64299,1,0),(13,64300,1,0),(13,64302,1,0),(13,64303,1,0),(13,64304,1,0),(13,64305,1,0),(13,64307,1,0),(13,64308,1,0),(13,64309,1,0),(13,64310,1,0),(13,64311,1,0),(13,64312,1,0),(13,64313,1,0),(13,64314,1,0),(13,64315,1,0),(13,64316,1,0),(13,64317,1,0),(13,64318,1,0),(13,64358,1,0),(13,64725,1,0),(13,64726,1,0),(13,64727,1,0),(13,64728,1,0),(13,64729,1,0),(13,64730,1,0),(13,65245,1,0),(13,65454,1,0),(13,65917,1,0),(13,66034,1,0),(13,66035,1,0),(13,66036,1,0),(13,66037,1,0),(13,66038,1,0),(13,66338,1,0),(13,66428,1,0),(13,66429,1,0),(13,66430,1,0),(13,66431,1,0),(13,66432,1,0),(13,66433,1,0),(13,66434,1,0),(13,66435,1,0),(13,66436,1,0),(13,66437,1,0),(13,66438,1,0),(13,66439,1,0),(13,66440,1,0),(13,66441,1,0),(13,66442,1,0),(13,66443,1,0),(13,66444,1,0),(13,66445,1,0),(13,66446,1,0),(13,66447,1,0),(13,66448,1,0),(13,66449,1,0),(13,66450,1,0),(13,66451,1,0),(13,66452,1,0),(13,66453,1,0),(13,66497,1,0),(13,66498,1,0),(13,66499,1,0),(13,66500,1,0),(13,66501,1,0),(13,66502,1,0),(13,66503,1,0),(13,66504,1,0),(13,66505,1,0),(13,66506,1,0),(13,66553,1,0),(13,66554,1,0),(13,66555,1,0),(13,66556,1,0),(13,66557,1,0),(13,66558,1,0),(13,66559,1,0),(13,66560,1,0),(13,66561,1,0),(13,66562,1,0),(13,66563,1,0),(13,66564,1,0),(13,66565,1,0),(13,66566,1,0),(13,66567,1,0),(13,66568,1,0),(13,66569,1,0),(13,66570,1,0),(13,66571,1,0),(13,66572,1,0),(13,66573,1,0),(13,66574,1,0),(13,66575,1,0),(13,66576,1,0),(13,66577,1,0),(13,66578,1,0),(13,66579,1,0),(13,66580,1,0),(13,66581,1,0),(13,66582,1,0),(13,66583,1,0),(13,66584,1,0),(13,66585,1,0),(13,66586,1,0),(13,66587,1,0),(13,66658,1,0),(13,66659,1,0),(13,66660,1,0),(13,66662,1,0),(13,66663,1,0),(13,66664,1,0),(13,67025,1,0),(13,67064,1,0),(13,67065,1,0),(13,67066,1,0),(13,67079,1,0),(13,67144,1,0),(13,67145,1,0),(13,67146,1,0),(13,67147,1,0),(13,67600,1,0),(13,68166,1,0),(13,68253,1,0),(13,69385,1,0),(13,70550,1,0),(13,70551,1,0),(13,70552,1,0),(13,70553,1,0),(13,71015,1,0),(13,71101,1,0),(13,71102,1,0),(13,71342,1,0),(13,72286,1,0),(13,75597,1,0),(13,75614,1,0);
/*!40000 ALTER TABLE `character_spell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_spell_cooldown`
--

DROP TABLE IF EXISTS `character_spell_cooldown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_spell_cooldown` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier, Low part',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell Identifier',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Item Identifier',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `categoryId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell category Id',
  `categoryEnd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_spell_cooldown`
--

LOCK TABLES `character_spell_cooldown` WRITE;
/*!40000 ALTER TABLE `character_spell_cooldown` DISABLE KEYS */;
/*!40000 ALTER TABLE `character_spell_cooldown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_stats`
--

DROP TABLE IF EXISTS `character_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_stats` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier, Low part',
  `maxhealth` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower1` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower2` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower3` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower4` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower5` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower6` int(10) unsigned NOT NULL DEFAULT '0',
  `maxpower7` int(10) unsigned NOT NULL DEFAULT '0',
  `strength` int(10) unsigned NOT NULL DEFAULT '0',
  `agility` int(10) unsigned NOT NULL DEFAULT '0',
  `stamina` int(10) unsigned NOT NULL DEFAULT '0',
  `intellect` int(10) unsigned NOT NULL DEFAULT '0',
  `spirit` int(10) unsigned NOT NULL DEFAULT '0',
  `armor` int(10) unsigned NOT NULL DEFAULT '0',
  `resHoly` int(10) unsigned NOT NULL DEFAULT '0',
  `resFire` int(10) unsigned NOT NULL DEFAULT '0',
  `resNature` int(10) unsigned NOT NULL DEFAULT '0',
  `resFrost` int(10) unsigned NOT NULL DEFAULT '0',
  `resShadow` int(10) unsigned NOT NULL DEFAULT '0',
  `resArcane` int(10) unsigned NOT NULL DEFAULT '0',
  `blockPct` float unsigned NOT NULL DEFAULT '0',
  `dodgePct` float unsigned NOT NULL DEFAULT '0',
  `parryPct` float unsigned NOT NULL DEFAULT '0',
  `critPct` float unsigned NOT NULL DEFAULT '0',
  `rangedCritPct` float unsigned NOT NULL DEFAULT '0',
  `spellCritPct` float unsigned NOT NULL DEFAULT '0',
  `attackPower` int(10) unsigned NOT NULL DEFAULT '0',
  `rangedAttackPower` int(10) unsigned NOT NULL DEFAULT '0',
  `spellPower` int(10) unsigned NOT NULL DEFAULT '0',
  `resilience` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_stats`
--

LOCK TABLES `character_stats` WRITE;
/*!40000 ALTER TABLE `character_stats` DISABLE KEYS */;
INSERT INTO `character_stats` VALUES (1,9814,5644,1000,0,100,0,0,1000,148,92,306,102,104,191,0,0,0,0,0,0,5,5.0303,0,5.0349,0,0,516,82,6000,0),(2,9337,81186,1000,0,100,0,0,1000,98,138,210,5198,194,6316,0,0,0,0,0,0,0,94.639,0,13.4336,0,0,176,128,5081,8),(5,72,0,1000,0,100,0,0,1000,22,24,22,20,20,50,0,0,0,0,0,0,0,19.8418,0,9.881,9.641,0,30,16,0,0),(7,8524,0,1000,0,100,0,0,1000,114,191,110,39,68,384,0,0,0,0,0,0,0,0,0,0,0,0,445,261,0,0),(10,23107,28401,1000,0,100,0,0,1000,149,150,1587,1679,295,4702,75,75,75,75,75,75,0,8.76254,0,34.5445,18.5445,0,278,140,9346,0),(11,28497,0,1000,0,100,0,0,1000,13448,112,1398,35,61,18139,0,0,0,0,0,0,0,64.9885,114.289,193.453,177.453,0,27116,102,0,0),(13,21893,30888,1000,0,100,0,0,1000,46,53,1511,1860,350,2422,0,0,0,0,0,0,0,4.56475,0,30.6332,30.6332,0,36,43,9233,0);
/*!40000 ALTER TABLE `character_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_talent`
--

DROP TABLE IF EXISTS `character_talent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_talent` (
  `guid` int(10) unsigned NOT NULL,
  `spell` mediumint(8) unsigned NOT NULL,
  `talentGroup` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`,`talentGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_talent`
--

LOCK TABLES `character_talent` WRITE;
/*!40000 ALTER TABLE `character_talent` DISABLE KEYS */;
INSERT INTO `character_talent` VALUES (10,5570,0),(10,5570,1),(10,16818,0),(10,16818,1),(10,16820,0),(10,16820,1),(10,16822,0),(10,16822,1),(10,16835,0),(10,16835,1),(10,16847,0),(10,16864,0),(10,16864,1),(10,16899,1),(10,16913,1),(10,16924,1),(10,17051,0),(10,17051,1),(10,17061,1),(10,17065,1),(10,17066,0),(10,17077,0),(10,17108,0),(10,17113,0),(10,17116,0),(10,17120,0),(10,18562,0),(10,24858,1),(10,24946,0),(10,33591,1),(10,33596,1),(10,33602,1),(10,33607,1),(10,33831,1),(10,33880,0),(10,33883,0),(10,33890,0),(10,34153,0),(10,35364,0),(10,35364,1),(10,48396,1),(10,48412,0),(10,48412,1),(10,48505,1),(10,48511,1),(10,48525,1),(10,48537,0),(10,57810,1),(10,57814,0),(10,57851,1),(10,57865,0),(10,57865,1),(10,61346,1),(10,63411,0),(10,65139,0),(13,11426,1),(13,11958,1),(13,12042,0),(13,12043,0),(13,12467,1),(13,12469,0),(13,12472,0),(13,12472,1),(13,12496,0),(13,12497,1),(13,12503,0),(13,12569,1),(13,12575,1),(13,12577,0),(13,12592,0),(13,12592,1),(13,12598,1),(13,12606,1),(13,12840,0),(13,12840,1),(13,12953,1),(13,12983,1),(13,15047,1),(13,15060,0),(13,16758,1),(13,16766,1),(13,16770,0),(13,18463,0),(13,28332,0),(13,28593,1),(13,29440,0),(13,29444,1),(13,31572,0),(13,31583,0),(13,31588,0),(13,31678,1),(13,31683,1),(13,31687,1),(13,35581,0),(13,44379,0),(13,44397,0),(13,44403,0),(13,44545,1),(13,44548,1),(13,44557,1),(13,44571,1),(13,44572,1),(13,54490,0),(13,54646,0),(13,54646,1),(13,54734,0),(13,55092,1),(13,55094,0),(13,55094,1),(13,55340,0),(13,55340,1);
/*!40000 ALTER TABLE `character_talent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characters`
--

DROP TABLE IF EXISTS `characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characters` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `account` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier',
  `name` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `xp` int(10) unsigned NOT NULL DEFAULT '0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `skin` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `face` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hairStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hairColor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `facialStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `bankSlots` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `restState` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `playerFlags` int(10) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0',
  `instance_mode_mask` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `taximask` text NOT NULL,
  `online` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `cinematic` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `totaltime` int(10) unsigned NOT NULL DEFAULT '0',
  `leveltime` int(10) unsigned NOT NULL DEFAULT '0',
  `logout_time` int(10) unsigned NOT NULL DEFAULT '0',
  `is_logout_resting` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rest_bonus` float NOT NULL DEFAULT '0',
  `resettalents_cost` int(10) unsigned NOT NULL DEFAULT '0',
  `resettalents_time` int(10) unsigned NOT NULL DEFAULT '0',
  `trans_x` float NOT NULL DEFAULT '0',
  `trans_y` float NOT NULL DEFAULT '0',
  `trans_z` float NOT NULL DEFAULT '0',
  `trans_o` float NOT NULL DEFAULT '0',
  `transguid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `extra_flags` smallint(5) unsigned NOT NULL DEFAULT '0',
  `stable_slots` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `at_login` smallint(5) unsigned NOT NULL DEFAULT '0',
  `zone` smallint(5) unsigned NOT NULL DEFAULT '0',
  `death_expire_time` int(10) unsigned NOT NULL DEFAULT '0',
  `taxi_path` text,
  `arenaPoints` int(10) unsigned NOT NULL DEFAULT '0',
  `totalHonorPoints` int(10) unsigned NOT NULL DEFAULT '0',
  `todayHonorPoints` int(10) unsigned NOT NULL DEFAULT '0',
  `yesterdayHonorPoints` int(10) unsigned NOT NULL DEFAULT '0',
  `totalKills` int(10) unsigned NOT NULL DEFAULT '0',
  `todayKills` smallint(5) unsigned NOT NULL DEFAULT '0',
  `yesterdayKills` smallint(5) unsigned NOT NULL DEFAULT '0',
  `chosenTitle` int(10) unsigned NOT NULL DEFAULT '0',
  `knownCurrencies` bigint(20) unsigned NOT NULL DEFAULT '0',
  `watchedFaction` int(10) unsigned NOT NULL DEFAULT '0',
  `drunk` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `health` int(10) unsigned NOT NULL DEFAULT '0',
  `power1` int(10) unsigned NOT NULL DEFAULT '0',
  `power2` int(10) unsigned NOT NULL DEFAULT '0',
  `power3` int(10) unsigned NOT NULL DEFAULT '0',
  `power4` int(10) unsigned NOT NULL DEFAULT '0',
  `power5` int(10) unsigned NOT NULL DEFAULT '0',
  `power6` int(10) unsigned NOT NULL DEFAULT '0',
  `power7` int(10) unsigned NOT NULL DEFAULT '0',
  `latency` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `talentGroupsCount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `activeTalentGroup` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `exploredZones` longtext,
  `equipmentCache` longtext,
  `ammoId` int(10) unsigned NOT NULL DEFAULT '0',
  `knownTitles` longtext,
  `actionBars` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `grantableLevels` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleteInfos_Account` int(10) unsigned DEFAULT NULL,
  `deleteInfos_Name` varchar(12) DEFAULT NULL,
  `deleteDate` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `idx_account` (`account`),
  KEY `idx_online` (`online`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characters`
--

LOCK TABLES `characters` WRITE;
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` VALUES (1,1,'Mew',10,2,1,80,4270900095,38206,8,0,5,0,2,0,2,33800,-8844.66,607.332,92.6301,0,0,0,3.70362,'0 0 131072 4 0 0 1048576 0 0 0 0 0 0 0 ',0,1,34502,5021,1463200279,0,0,0,0,0,0,0,0,0,5,0,0,1519,1462977070,'',0,0,0,0,0,0,0,0,268435456,4294967295,0,9814,5644,0,0,100,0,0,0,0,1,0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 16 0 0 0 65544 0 67108864 268435456 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2097152 128 0 0 0 0 0 0 64 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4096 0 0 0 0 0 1024 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','12064 0 0 0 0 0 0 0 2586 0 0 0 0 0 11508 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1977 0 1977 0 1977 0 1977 0 ',0,'0 0 0 0 8192 0 ',0,0,NULL,NULL,NULL),(2,5,'Domino',4,11,1,81,0,99135807,0,7,0,4,6,0,2,34824,16219.7,16237.2,25.4926,1,0,0,1.68339,'100663296 0 0 8 0 0 1048576 0 0 2097152 0 0 0 0 ',0,1,37342,34373,1465678913,0,0,0,0,0,0,0,0,0,65,0,0,876,1463483757,'',0,0,0,0,0,0,0,83,268435456,4294967295,0,9337,81186,0,0,100,0,0,0,18,1,0,'64 0 0 1073741824 0 0 0 0 0 0 0 0 0 0 0 0 0 131088 0 0 0 65544 4 67108864 268435456 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2147483648 0 0 0 0 0 0 0 0 0 0 0 512 0 0 0 0 0 0 2097152 130 0 268435456 8388608 2134024 0 0 262720 2151677952 2048 32768 0 0 0 0 0 0 0 67108864 0 0 0 0 0 4096 0 0 0 0 0 1024 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','32235 0 26541 0 28647 0 100006 0 14562 0 14559 0 3315 0 11508 0 1298 0 14564 0 12947 0 18970 0 8688 0 0 0 26513 0 900001 3854 0 0 51423 0 0 0 23162 0 23162 0 23162 0 23162 0 ',0,'0 0 524288 0 0 0 ',0,0,NULL,NULL,NULL),(4,5,'Test',4,11,0,80,0,0,1,8,2,4,3,0,2,0,10318.1,827.292,1326.38,1,0,0,6.21625,'100663296 0 0 8 0 0 1048576 0 0 0 0 0 0 0 ',0,1,40,10,1462779550,0,0,0,0,0,0,0,0,0,0,0,0,141,0,'',0,0,0,0,0,0,0,0,0,4294967295,0,8207,5361,0,0,100,0,0,0,0,1,0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 131072 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','0 0 0 0 0 0 0 0 6123 0 0 0 6124 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 3661 0 0 0 0 0 0 0 23162 0 23162 0 23162 0 23162 0 ',0,'0 0 0 0 0 0 ',0,0,NULL,NULL,NULL),(5,10,'Tests',1,4,1,2,120,0,0,7,9,1,1,0,2,0,-8927.54,-61.7778,89.6416,0,0,0,0.120167,'2 0 0 8 0 0 0 0 0 0 0 0 0 0 ',0,1,1995,834,1463066363,0,0.779032,0,0,0,0,0,0,0,4,0,0,12,0,'',0,0,0,0,0,0,0,0,0,4294967295,0,72,0,0,0,100,0,0,0,22,1,0,'0 0 0 536870912 128 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','0 0 0 0 0 0 49 0 0 0 0 0 48 0 47 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2092 0 50055 0 28979 0 0 0 23162 0 23162 0 23162 0 23162 0 ',0,'0 0 0 0 0 0 ',0,0,NULL,NULL,NULL),(6,10000,'Sporkish',1,4,1,1,0,0,5,4,8,0,5,0,2,0,-8945.51,-128.051,83.4966,0,0,0,0,'2 0 0 8 0 0 0 0 0 0 0 0 0 0 ',0,1,115,115,1462970614,0,0.0161889,0,0,0,0,0,0,0,4,0,0,12,0,'',0,0,0,0,0,0,0,0,0,4294967295,0,55,0,0,0,100,0,0,0,16,1,0,'0 0 0 536870912 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','0 0 0 0 0 0 49 0 0 0 0 0 48 0 47 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2092 0 50055 0 28979 0 0 0 23162 0 23162 0 23162 0 23162 0 ',0,'0 0 0 0 0 0 ',0,0,NULL,NULL,NULL),(7,11,'Spork',8,4,1,1,0,0,3,3,0,9,4,0,2,16,6447.49,2060.86,564.03,571,0,0,6.23467,'4194304 0 0 4 0 0 1048576 0 0 0 0 0 0 0 ',0,1,2341,63,1463338517,0,0.299971,0,0,0,0,0,0,0,4,0,0,210,0,'',0,0,0,0,0,0,0,0,0,4294967295,0,1,0,0,0,0,0,0,0,23,1,0,'0 0 33554432 2147483648 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 8 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 128 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4096 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','0 0 0 0 0 0 6136 0 0 0 0 0 6137 0 6138 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2092 0 50055 0 25861 0 0 0 23162 0 23162 0 23162 0 23162 0 ',0,'0 0 0 0 0 0 ',0,0,NULL,NULL,NULL),(10,10,'Cybroon',4,11,1,80,0,2140121746,6,7,0,4,6,0,2,2049,16221.9,16259.9,13.2245,1,0,0,2.57605,'100663296 0 0 8 0 0 1048576 0 0 0 0 0 0 0 ',0,1,12804,11967,1463490342,0,0,0,0,0,0,0,0,0,0,0,0,876,0,'',0,0,0,0,0,0,0,0,268435456,4294967295,0,23107,28401,0,0,100,0,0,0,27,2,1,'0 0 0 1073741824 0 0 0 0 0 0 0 0 0 0 0 0 0 131072 0 0 0 65544 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 128 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4096 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','30989 3820 50724 0 51424 3810 100006 0 35026 3832 50613 0 35024 3719 50699 3232 54582 2332 35022 3246 50398 0 50714 0 54588 0 50365 0 54583 3831 50734 3834 50719 0 50457 0 51534 0 1977 0 1977 0 1977 0 1977 0 ',0,'0 0 0 0 8192 0 ',15,0,NULL,NULL,NULL),(11,1,'Sanka',1,6,1,80,0,2079993491,0,15,14,8,4,1,2,3072,-409.799,2275.41,44.5653,631,0,0,2.66618,'4294967295 2483027967 829882367 8 16384 1310944 3251642388 73752 896 67111952 2281701376 4190109713 1049856 12582912 ',0,1,3280,2928,1463090703,0,0,0,0,0,0,0,0,0,0,0,0,4812,0,'',0,0,0,0,0,0,0,0,268435456,4294967295,0,28497,0,0,0,100,0,0,520,13,2,0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 65544 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4096 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','51312 0 0 0 51314 0 100006 0 51310 0 0 0 51313 0 0 0 0 0 51311 0 12947 0 12947 0 0 0 0 0 0 0 49623 0 0 0 0 0 0 0 184 0 184 0 184 0 184 0 ',0,'0 0 0 0 8192 0 ',0,0,NULL,NULL,NULL),(13,11,'Della',1,8,1,80,0,401537915,1,3,14,0,4,7,2,3072,16228,16238.8,13.7294,1,0,0,4.63114,'2 0 0 8 0 0 1048576 0 0 0 0 0 0 0 ',0,1,12360,12262,1463340721,0,0,0,0,0,0,0,0,0,64,0,0,876,0,'',0,0,0,0,0,0,0,0,268435456,4294967295,0,21893,30888,0,0,100,0,0,0,716,2,0,'0 0 0 1610612736 0 0 0 0 0 0 0 0 0 0 0 0 0 16 0 0 0 65544 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2097152 128 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4096 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ','51281 3820 50724 0 39894 3810 100006 0 15050 3832 9636 0 6835 0 50699 3232 54582 2332 10550 3246 50398 0 50644 0 54588 0 50365 0 54583 3831 30733 3834 50719 0 50684 0 0 0 1977 0 1977 0 1977 0 1977 0 ',0,'0 0 0 0 8192 0 ',15,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corpse`
--

DROP TABLE IF EXISTS `corpse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpse` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
  `posX` float NOT NULL DEFAULT '0',
  `posY` float NOT NULL DEFAULT '0',
  `posZ` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
  `phaseMask` int(10) unsigned NOT NULL DEFAULT '1',
  `displayId` int(10) unsigned NOT NULL DEFAULT '0',
  `itemCache` text NOT NULL,
  `bytes1` int(10) unsigned NOT NULL DEFAULT '0',
  `bytes2` int(10) unsigned NOT NULL DEFAULT '0',
  `guildId` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dynFlags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `corpseType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier',
  PRIMARY KEY (`guid`),
  KEY `idx_type` (`corpseType`),
  KEY `idx_instance` (`instanceId`),
  KEY `idx_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Death System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corpse`
--

LOCK TABLES `corpse` WRITE;
/*!40000 ALTER TABLE `corpse` DISABLE KEYS */;
INSERT INTO `corpse` VALUES (7,44.6379,2208.53,30.1158,6.23467,631,1,1479,'0 0 0 67118976 0 0 117450626 134227843 0 0 0 0 0 0 0 218110250 369105194 419451177 0 ',50399232,67698691,0,4,0,1463338517,1,0);
/*!40000 ALTER TABLE `corpse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature_respawn`
--

DROP TABLE IF EXISTS `creature_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature_respawn` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
  `mapId` smallint(10) unsigned NOT NULL DEFAULT '0',
  `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier',
  PRIMARY KEY (`guid`,`instanceId`),
  KEY `idx_instance` (`instanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Grid Loading System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature_respawn`
--

LOCK TABLES `creature_respawn` WRITE;
/*!40000 ALTER TABLE `creature_respawn` DISABLE KEYS */;
INSERT INTO `creature_respawn` VALUES (12199,1463336943,1,0),(13078,1463336991,1,0),(13080,1463336931,1,0),(44457,1463487368,571,0),(80104,1463075926,0,0),(97122,1463487247,571,0),(121168,1463336674,571,0);
/*!40000 ALTER TABLE `creature_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_transmogrification`
--

DROP TABLE IF EXISTS `custom_transmogrification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_transmogrification` (
  `GUID` int(10) unsigned NOT NULL COMMENT 'Item guidLow',
  `FakeEntry` int(10) unsigned NOT NULL COMMENT 'Item entry',
  `Owner` int(10) unsigned NOT NULL COMMENT 'Player guidLow',
  PRIMARY KEY (`GUID`),
  KEY `Owner` (`Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='6_2';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_transmogrification`
--

LOCK TABLES `custom_transmogrification` WRITE;
/*!40000 ALTER TABLE `custom_transmogrification` DISABLE KEYS */;
INSERT INTO `custom_transmogrification` VALUES (414,35022,10),(415,30989,10),(462,51424,10),(464,35026,10),(512,35024,10),(629,39894,13),(630,15050,13),(632,10550,13),(638,30733,13),(643,9636,13),(698,15050,13),(699,39894,13),(702,10550,13),(710,9636,13),(718,6835,13),(719,6835,13);
/*!40000 ALTER TABLE `custom_transmogrification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_transmogrification_sets`
--

DROP TABLE IF EXISTS `custom_transmogrification_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_transmogrification_sets` (
  `Owner` int(10) unsigned NOT NULL COMMENT 'Player guidlow',
  `PresetID` tinyint(3) unsigned NOT NULL COMMENT 'Preset identifier',
  `SetName` text COMMENT 'SetName',
  `SetData` text COMMENT 'Slot1 Entry1 Slot2 Entry2',
  PRIMARY KEY (`Owner`,`PresetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='6_1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_transmogrification_sets`
--

LOCK TABLES `custom_transmogrification_sets` WRITE;
/*!40000 ALTER TABLE `custom_transmogrification_sets` DISABLE KEYS */;
INSERT INTO `custom_transmogrification_sets` VALUES (13,0,'EzPz1','2 39894 4 15050 5 9636 6 6835 9 10550 15 30733 ');
/*!40000 ALTER TABLE `custom_transmogrification_sets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_condition_save`
--

DROP TABLE IF EXISTS `game_event_condition_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_condition_save` (
  `eventEntry` tinyint(3) unsigned NOT NULL,
  `condition_id` int(10) unsigned NOT NULL DEFAULT '0',
  `done` float DEFAULT '0',
  PRIMARY KEY (`eventEntry`,`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_condition_save`
--

LOCK TABLES `game_event_condition_save` WRITE;
/*!40000 ALTER TABLE `game_event_condition_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_condition_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_event_save`
--

DROP TABLE IF EXISTS `game_event_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_event_save` (
  `eventEntry` tinyint(3) unsigned NOT NULL,
  `state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `next_start` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`eventEntry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_event_save`
--

LOCK TABLES `game_event_save` WRITE;
/*!40000 ALTER TABLE `game_event_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_event_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameobject_respawn`
--

DROP TABLE IF EXISTS `gameobject_respawn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameobject_respawn` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
  `mapId` smallint(10) unsigned NOT NULL DEFAULT '0',
  `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier',
  PRIMARY KEY (`guid`,`instanceId`),
  KEY `idx_instance` (`instanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Grid Loading System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameobject_respawn`
--

LOCK TABLES `gameobject_respawn` WRITE;
/*!40000 ALTER TABLE `gameobject_respawn` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameobject_respawn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gm_subsurvey`
--

DROP TABLE IF EXISTS `gm_subsurvey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gm_subsurvey` (
  `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionId` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` int(10) unsigned NOT NULL DEFAULT '0',
  `answerComment` text NOT NULL,
  PRIMARY KEY (`surveyId`,`questionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gm_subsurvey`
--

LOCK TABLES `gm_subsurvey` WRITE;
/*!40000 ALTER TABLE `gm_subsurvey` DISABLE KEYS */;
/*!40000 ALTER TABLE `gm_subsurvey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gm_survey`
--

DROP TABLE IF EXISTS `gm_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gm_survey` (
  `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `mainSurvey` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` longtext NOT NULL,
  `createTime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`surveyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gm_survey`
--

LOCK TABLES `gm_survey` WRITE;
/*!40000 ALTER TABLE `gm_survey` DISABLE KEYS */;
/*!40000 ALTER TABLE `gm_survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gm_ticket`
--

DROP TABLE IF EXISTS `gm_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gm_ticket` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 open, 1 closed, 2 character deleted',
  `playerGuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier of ticket creator',
  `name` varchar(12) NOT NULL COMMENT 'Name of ticket creator',
  `description` text NOT NULL,
  `createTime` int(10) unsigned NOT NULL DEFAULT '0',
  `mapId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `posX` float NOT NULL DEFAULT '0',
  `posY` float NOT NULL DEFAULT '0',
  `posZ` float NOT NULL DEFAULT '0',
  `lastModifiedTime` int(10) unsigned NOT NULL DEFAULT '0',
  `closedBy` int(10) NOT NULL DEFAULT '0',
  `assignedTo` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'GUID of admin to whom ticket is assigned',
  `comment` text NOT NULL,
  `response` text NOT NULL,
  `completed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `escalated` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `viewed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `needMoreHelp` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `resolvedBy` int(10) NOT NULL DEFAULT '0' COMMENT 'GUID of GM who resolved the ticket',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gm_ticket`
--

LOCK TABLES `gm_ticket` WRITE;
/*!40000 ALTER TABLE `gm_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `gm_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_instance`
--

DROP TABLE IF EXISTS `group_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_instance` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `instance` int(10) unsigned NOT NULL DEFAULT '0',
  `permanent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`instance`),
  KEY `instance` (`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_instance`
--

LOCK TABLES `group_instance` WRITE;
/*!40000 ALTER TABLE `group_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_member`
--

DROP TABLE IF EXISTS `group_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_member` (
  `guid` int(10) unsigned NOT NULL,
  `memberGuid` int(10) unsigned NOT NULL,
  `memberFlags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subgroup` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `roles` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`memberGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_member`
--

LOCK TABLES `group_member` WRITE;
/*!40000 ALTER TABLE `group_member` DISABLE KEYS */;
INSERT INTO `group_member` VALUES (1,2,0,0,0),(1,7,0,0,0),(1,10,0,0,0),(1,13,0,0,0);
/*!40000 ALTER TABLE `group_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `guid` int(10) unsigned NOT NULL,
  `leaderGuid` int(10) unsigned NOT NULL,
  `lootMethod` tinyint(3) unsigned NOT NULL,
  `looterGuid` int(10) unsigned NOT NULL,
  `lootThreshold` tinyint(3) unsigned NOT NULL,
  `icon1` bigint(20) unsigned NOT NULL,
  `icon2` bigint(20) unsigned NOT NULL,
  `icon3` bigint(20) unsigned NOT NULL,
  `icon4` bigint(20) unsigned NOT NULL,
  `icon5` bigint(20) unsigned NOT NULL,
  `icon6` bigint(20) unsigned NOT NULL,
  `icon7` bigint(20) unsigned NOT NULL,
  `icon8` bigint(20) unsigned NOT NULL,
  `groupType` tinyint(3) unsigned NOT NULL,
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `raidDifficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `masterLooterGuid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`guid`),
  KEY `leaderGuid` (`leaderGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,10,3,7,2,0,0,0,0,0,0,0,0,2,0,0,0);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild`
--

DROP TABLE IF EXISTS `guild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(24) NOT NULL DEFAULT '',
  `leaderguid` int(10) unsigned NOT NULL DEFAULT '0',
  `EmblemStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `EmblemColor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `BorderStyle` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `BorderColor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `BackgroundColor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `info` varchar(500) NOT NULL DEFAULT '',
  `motd` varchar(128) NOT NULL DEFAULT '',
  `createdate` int(10) unsigned NOT NULL DEFAULT '0',
  `BankMoney` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guildid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild`
--

LOCK TABLES `guild` WRITE;
/*!40000 ALTER TABLE `guild` DISABLE KEYS */;
INSERT INTO `guild` VALUES (1,'Asterion GM Tým',1,0,0,0,0,0,'','',1462710895,999999968075),(2,'Tiger Tooths',13,0,0,0,0,0,' ',' ',1463081410,0);
/*!40000 ALTER TABLE `guild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_bank_eventlog`
--

DROP TABLE IF EXISTS `guild_bank_eventlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_bank_eventlog` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Guild Identificator',
  `LogGuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log record identificator - auxiliary column',
  `TabId` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Guild bank TabId',
  `EventType` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Event type',
  `PlayerGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `ItemOrMoney` int(10) unsigned NOT NULL DEFAULT '0',
  `ItemStackCount` smallint(5) unsigned NOT NULL DEFAULT '0',
  `DestTabId` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Destination Tab Id',
  `TimeStamp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Event UNIX time',
  PRIMARY KEY (`guildid`,`LogGuid`,`TabId`),
  KEY `guildid_key` (`guildid`),
  KEY `Idx_PlayerGuid` (`PlayerGuid`),
  KEY `Idx_LogGuid` (`LogGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_bank_eventlog`
--

LOCK TABLES `guild_bank_eventlog` WRITE;
/*!40000 ALTER TABLE `guild_bank_eventlog` DISABLE KEYS */;
INSERT INTO `guild_bank_eventlog` VALUES (1,0,100,6,1,2,0,0,1463002304),(1,1,100,6,1,1,0,0,1463002304),(1,2,100,6,1,1,0,0,1463002304),(1,3,100,6,1,31920,0,0,1463002304);
/*!40000 ALTER TABLE `guild_bank_eventlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_bank_item`
--

DROP TABLE IF EXISTS `guild_bank_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_bank_item` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0',
  `TabId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SlotId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `item_guid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guildid`,`TabId`,`SlotId`),
  KEY `guildid_key` (`guildid`),
  KEY `Idx_item_guid` (`item_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_bank_item`
--

LOCK TABLES `guild_bank_item` WRITE;
/*!40000 ALTER TABLE `guild_bank_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_bank_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_bank_right`
--

DROP TABLE IF EXISTS `guild_bank_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_bank_right` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0',
  `TabId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `rid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `gbright` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SlotPerDay` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guildid`,`TabId`,`rid`),
  KEY `guildid_key` (`guildid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_bank_right`
--

LOCK TABLES `guild_bank_right` WRITE;
/*!40000 ALTER TABLE `guild_bank_right` DISABLE KEYS */;
INSERT INTO `guild_bank_right` VALUES (1,0,0,255,4294967295),(1,0,1,7,1000),(1,0,2,7,2),(1,0,3,7,2),(1,0,4,7,2),(1,0,5,3,0),(1,0,6,3,0),(1,0,7,3,0),(1,0,8,3,2),(1,0,9,3,0);
/*!40000 ALTER TABLE `guild_bank_right` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_bank_tab`
--

DROP TABLE IF EXISTS `guild_bank_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_bank_tab` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0',
  `TabId` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `TabName` varchar(16) NOT NULL DEFAULT '',
  `TabIcon` varchar(100) NOT NULL DEFAULT '',
  `TabText` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`guildid`,`TabId`),
  KEY `guildid_key` (`guildid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_bank_tab`
--

LOCK TABLES `guild_bank_tab` WRITE;
/*!40000 ALTER TABLE `guild_bank_tab` DISABLE KEYS */;
INSERT INTO `guild_bank_tab` VALUES (1,0,'','',NULL);
/*!40000 ALTER TABLE `guild_bank_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_eventlog`
--

DROP TABLE IF EXISTS `guild_eventlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_eventlog` (
  `guildid` int(10) unsigned NOT NULL COMMENT 'Guild Identificator',
  `LogGuid` int(10) unsigned NOT NULL COMMENT 'Log record identificator - auxiliary column',
  `EventType` tinyint(3) unsigned NOT NULL COMMENT 'Event type',
  `PlayerGuid1` int(10) unsigned NOT NULL COMMENT 'Player 1',
  `PlayerGuid2` int(10) unsigned NOT NULL COMMENT 'Player 2',
  `NewRank` tinyint(3) unsigned NOT NULL COMMENT 'New rank(in case promotion/demotion)',
  `TimeStamp` int(10) unsigned NOT NULL COMMENT 'Event UNIX time',
  PRIMARY KEY (`guildid`,`LogGuid`),
  KEY `Idx_PlayerGuid1` (`PlayerGuid1`),
  KEY `Idx_PlayerGuid2` (`PlayerGuid2`),
  KEY `Idx_LogGuid` (`LogGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild Eventlog';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_eventlog`
--

LOCK TABLES `guild_eventlog` WRITE;
/*!40000 ALTER TABLE `guild_eventlog` DISABLE KEYS */;
INSERT INTO `guild_eventlog` VALUES (1,0,2,1,0,0,1462710895),(1,1,2,2,0,0,1462730273),(2,0,2,13,0,0,1463081410);
/*!40000 ALTER TABLE `guild_eventlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_house`
--

DROP TABLE IF EXISTS `guild_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_house` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `guildId` bigint(20) NOT NULL DEFAULT '0',
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `map` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_house`
--

LOCK TABLES `guild_house` WRITE;
/*!40000 ALTER TABLE `guild_house` DISABLE KEYS */;
INSERT INTO `guild_house` VALUES (1,1,16226.200195,16257,13.2022,1,'GM Island');
/*!40000 ALTER TABLE `guild_house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_member`
--

DROP TABLE IF EXISTS `guild_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_member` (
  `guildid` int(10) unsigned NOT NULL COMMENT 'Guild Identificator',
  `guid` int(10) unsigned NOT NULL,
  `rank` tinyint(3) unsigned NOT NULL,
  `pnote` varchar(31) NOT NULL DEFAULT '',
  `offnote` varchar(31) NOT NULL DEFAULT '',
  UNIQUE KEY `guid_key` (`guid`),
  KEY `guildid_key` (`guildid`),
  KEY `guildid_rank_key` (`guildid`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_member`
--

LOCK TABLES `guild_member` WRITE;
/*!40000 ALTER TABLE `guild_member` DISABLE KEYS */;
INSERT INTO `guild_member` VALUES (1,1,0,' ',' '),(1,2,1,' ',' '),(2,13,0,' ',' ');
/*!40000 ALTER TABLE `guild_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_member_withdraw`
--

DROP TABLE IF EXISTS `guild_member_withdraw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_member_withdraw` (
  `guid` int(10) unsigned NOT NULL,
  `tab0` int(10) unsigned NOT NULL DEFAULT '0',
  `tab1` int(10) unsigned NOT NULL DEFAULT '0',
  `tab2` int(10) unsigned NOT NULL DEFAULT '0',
  `tab3` int(10) unsigned NOT NULL DEFAULT '0',
  `tab4` int(10) unsigned NOT NULL DEFAULT '0',
  `tab5` int(10) unsigned NOT NULL DEFAULT '0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild Member Daily Withdraws';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_member_withdraw`
--

LOCK TABLES `guild_member_withdraw` WRITE;
/*!40000 ALTER TABLE `guild_member_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_member_withdraw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_rank`
--

DROP TABLE IF EXISTS `guild_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_rank` (
  `guildid` int(10) unsigned NOT NULL DEFAULT '0',
  `rid` tinyint(3) unsigned NOT NULL,
  `rname` varchar(20) NOT NULL DEFAULT '',
  `rights` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `BankMoneyPerDay` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guildid`,`rid`),
  KEY `Idx_rid` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_rank`
--

LOCK TABLES `guild_rank` WRITE;
/*!40000 ALTER TABLE `guild_rank` DISABLE KEYS */;
INSERT INTO `guild_rank` VALUES (1,0,'Head Admin',1962495,4294967295),(1,1,'Admin',1962495,0),(1,2,'Head Dev',1962495,1215740000),(1,3,'HGM',1962483,1316120000),(1,4,'HEM',1962495,1215742192),(1,5,'DEV',1572931,50000000),(1,6,'GM',1572931,50000000),(1,7,'EM',1572931,50000000),(1,8,'<ZD> GM',1572931,25000000),(1,9,'<ZD> EM',1572931,25000000),(2,0,'King of Tooths',1962495,4294967295),(2,1,'KING OF POP',1962495,0),(2,2,'Pokrocil',67,0),(2,3,'Autista',67,0),(2,4,'Flegmatik',67,0);
/*!40000 ALTER TABLE `guild_rank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance`
--

DROP TABLE IF EXISTS `instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `resettime` int(10) unsigned NOT NULL DEFAULT '0',
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `completedEncounters` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `map` (`map`),
  KEY `resettime` (`resettime`),
  KEY `difficulty` (`difficulty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance`
--

LOCK TABLES `instance` WRITE;
/*!40000 ALTER TABLE `instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance_reset`
--

DROP TABLE IF EXISTS `instance_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance_reset` (
  `mapid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `resettime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mapid`,`difficulty`),
  KEY `difficulty` (`difficulty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance_reset`
--

LOCK TABLES `instance_reset` WRITE;
/*!40000 ALTER TABLE `instance_reset` DISABLE KEYS */;
INSERT INTO `instance_reset` VALUES (249,0,1466222400),(249,1,1466222400),(269,1,1465790400),(309,0,1465963200),(409,0,1466222400),(469,0,1466222400),(509,0,1465963200),(531,0,1466222400),(532,0,1466222400),(533,0,1466222400),(533,1,1466222400),(534,0,1466222400),(540,1,1465790400),(542,1,1465790400),(543,1,1465790400),(544,0,1466222400),(545,1,1465790400),(546,1,1465790400),(547,1,1465790400),(548,0,1466222400),(550,0,1466222400),(552,1,1465790400),(553,1,1465790400),(554,1,1465790400),(555,1,1465790400),(556,1,1465790400),(557,1,1465790400),(558,1,1465790400),(560,1,1465790400),(564,0,1466222400),(565,0,1466222400),(568,0,1465963200),(574,1,1465790400),(575,1,1465790400),(576,1,1465790400),(578,1,1465790400),(580,0,1466222400),(585,1,1465790400),(595,1,1465790400),(598,1,1465790400),(599,1,1465790400),(600,1,1465790400),(601,1,1465790400),(602,1,1465790400),(603,0,1466222400),(603,1,1466222400),(604,1,1465790400),(608,1,1465790400),(615,0,1466222400),(615,1,1466222400),(616,0,1466222400),(616,1,1466222400),(619,1,1465790400),(624,0,1466222400),(624,1,1466222400),(631,0,1466222400),(631,1,1466222400),(631,2,1466222400),(631,3,1466222400),(632,1,1465790400),(649,0,1466222400),(649,1,1466222400),(649,2,1466222400),(649,3,1466222400),(650,1,1465790400),(658,1,1465790400),(668,1,1465790400),(724,0,1466222400),(724,1,1466222400),(724,2,1466222400),(724,3,1466222400);
/*!40000 ALTER TABLE `instance_reset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_instance`
--

DROP TABLE IF EXISTS `item_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_instance` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `itemEntry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `owner_guid` int(10) unsigned NOT NULL DEFAULT '0',
  `creatorGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `giftCreatorGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `count` int(10) unsigned NOT NULL DEFAULT '1',
  `duration` int(10) NOT NULL DEFAULT '0',
  `charges` tinytext,
  `flags` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `enchantments` text NOT NULL,
  `randomPropertyId` smallint(5) NOT NULL DEFAULT '0',
  `durability` smallint(5) unsigned NOT NULL DEFAULT '0',
  `playedTime` int(10) unsigned NOT NULL DEFAULT '0',
  `text` text,
  PRIMARY KEY (`guid`),
  KEY `idx_owner_guid` (`owner_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_instance`
--

LOCK TABLES `item_instance` WRITE;
/*!40000 ALTER TABLE `item_instance` DISABLE KEYS */;
INSERT INTO `item_instance` VALUES (2,24143,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(4,24145,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(6,24146,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(8,23346,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(10,6948,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(12,23162,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(18,23162,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(20,2586,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(22,3661,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,23,0,''),(24,6123,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,32,0,''),(26,6124,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,23,0,''),(28,6948,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(30,23162,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(32,23162,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(34,23162,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(36,23162,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(38,900001,2,0,0,1,0,'0 0 0 0 0 ',1,'3854 0 0 0 0 0 2949 0 0 2688 0 0 3317 0 0 3821 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,999,7200,''),(58,3661,4,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(60,6123,4,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(62,6124,4,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(64,6948,4,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(66,23162,4,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(68,23162,4,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(70,23162,4,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(72,23162,4,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(74,49623,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,145,0,''),(75,100004,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(78,22033,1,0,0,105,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(79,32235,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3627 0 0 3591 0 0 0 0 0 2868 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,0,''),(80,14562,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,0,''),(81,14559,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,20,0,''),(82,14560,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(83,14564,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(84,3315,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,50,0,''),(85,28647,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,0,''),(86,12947,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(87,18970,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(90,26541,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(91,26135,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(92,26513,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(96,8688,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(100,45574,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(103,192,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(104,17,2,0,0,1,0,'100 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(109,17142,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,95,0,''),(110,13315,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(111,1298,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,20,0,''),(115,40754,2,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(117,51423,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,7165,''),(118,100006,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(120,49,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(122,47,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(124,48,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(126,28979,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(128,2092,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(130,50055,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(132,6948,5,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(134,23162,5,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(136,23162,5,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(138,23162,5,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(140,23162,5,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(142,49,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(144,47,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(146,48,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(148,28979,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(150,2092,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(152,50055,6,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(154,6948,6,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(156,23162,6,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(158,23162,6,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(160,23162,6,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(162,23162,6,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(163,49426,2,0,0,11,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(164,49426,1,0,0,8,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(171,35947,2,0,0,1,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(210,2586,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(217,29981,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,1318,''),(254,12064,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,20,0,''),(255,2586,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(256,11508,1,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(311,2092,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,15,0,''),(313,6136,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(315,6137,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,23,0,''),(317,6138,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(319,50055,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,15,0,''),(321,25861,7,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(323,6948,7,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(325,23162,7,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(327,23162,7,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(329,23162,7,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(331,23162,7,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(339,11508,2,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(345,7073,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(346,7074,5,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(352,1977,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(353,1977,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(354,1977,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(355,1977,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(356,1977,1,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(395,3661,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(397,6123,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,0,''),(399,6124,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(401,6948,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(403,1977,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(405,1977,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(407,1977,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(409,1977,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(413,51296,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1318,''),(414,51291,10,0,0,1,0,'0 0 0 0 0 ',1,'3246 0 0 0 0 0 3520 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,4293,''),(415,51290,10,0,0,1,0,'0 0 0 0 0 ',1,'3820 0 0 0 0 0 3621 0 0 3520 0 0 0 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,63,3480,''),(417,51295,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,1316,''),(418,51297,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,90,1177,''),(419,51293,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,90,1156,''),(421,34652,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,0,''),(423,34655,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,0,''),(425,34659,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(427,34650,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,115,0,''),(429,34653,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,0,''),(431,34649,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,0,''),(433,34651,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,0,''),(435,34656,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,85,0,''),(437,34648,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,55,0,''),(439,34657,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(441,34658,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(451,38147,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(453,41751,11,0,0,10,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(455,23162,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(456,23162,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(457,23162,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(458,23162,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(460,38633,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,0,''),(461,51299,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1624,''),(462,51292,10,0,0,1,0,'0 0 0 0 0 ',1,'3810 0 0 0 0 0 3520 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,62,2716,''),(463,51298,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,1597,''),(464,51294,10,0,0,1,0,'0 0 0 0 0 ',1,'3832 0 0 0 0 0 3520 0 0 3559 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,108,1730,''),(465,51303,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,90,1207,''),(466,51304,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1503,''),(467,51300,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,1464,''),(468,51302,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1936,''),(469,51301,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,2166,''),(470,51421,10,0,0,1,0,'0 0 0 0 0 ',1,'3796 0 0 0 0 0 3627 0 0 3561 0 0 0 0 0 3352 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1472,''),(471,51422,10,0,0,1,0,'0 0 0 0 0 ',1,'3721 0 0 0 0 0 3561 0 0 3546 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,90,5904,''),(472,51424,10,0,0,1,0,'0 0 0 0 0 ',1,'3794 0 0 0 0 0 3530 0 0 0 0 0 0 0 0 2890 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,70,1673,''),(473,51419,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3561 0 0 3530 0 0 0 0 0 3600 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,2269,''),(474,51420,10,0,0,1,0,'0 0 0 0 0 ',1,'3246 0 0 0 0 0 3546 0 0 0 0 0 0 0 0 2890 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,1356,''),(475,51454,10,0,0,1,0,'0 0 0 0 0 ',1,'3834 0 0 0 0 0 3545 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,105,731,''),(476,51423,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,789,''),(477,51409,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,524,''),(478,51377,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(479,51341,10,0,0,1,0,'0 0 0 0 0 ',1,'3232 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 2878 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,1978,''),(480,51334,10,0,0,1,0,'0 0 0 0 0 ',1,'3243 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,1496,''),(481,51342,10,0,0,1,0,'0 0 0 0 0 ',1,'2332 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,1466,''),(482,54588,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(483,54589,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(484,54591,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(485,54590,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(487,50734,10,0,0,1,0,'0 0 0 0 0 ',1,'3834 0 0 0 0 0 3520 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,95,0,''),(489,50398,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3559 0 0 0 0 0 0 0 0 3596 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(490,50400,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 2367 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(491,50404,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(492,184,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(493,184,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(494,184,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(495,184,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(496,184,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(497,49623,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,131,0,''),(498,22033,11,0,0,185,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(500,22033,11,0,0,150,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(503,100006,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(504,50724,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3545 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(505,54583,10,0,0,1,0,'0 0 0 0 0 ',1,'3831 0 0 0 0 0 3559 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(506,51310,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,52,490,''),(507,51305,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,165,977,''),(508,51306,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,965,''),(509,51311,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,19,485,''),(510,51307,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,55,981,''),(511,50613,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3545 0 0 3520 0 0 3559 0 0 3602 0 0 3729 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,32,0,''),(512,50694,10,0,0,1,0,'0 0 0 0 0 ',1,'3719 0 0 0 0 0 3520 0 0 3545 0 0 3559 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,68,474,''),(513,50699,10,0,0,1,0,'0 0 0 0 0 ',1,'3232 0 0 0 0 0 3545 0 0 3520 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,44,0,''),(514,51312,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,29,483,''),(515,51308,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,968,''),(516,51313,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,36,487,''),(517,54582,10,0,0,1,0,'0 0 0 0 0 ',1,'2332 0 0 0 0 0 3520 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,32,0,''),(518,51309,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,987,''),(519,51314,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,30,483,''),(520,50365,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(521,50714,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3545 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(522,50719,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(523,50457,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,453,''),(526,40113,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(527,40113,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(530,40113,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(537,40113,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(538,40152,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(547,40133,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(553,50658,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(554,50705,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3530 0 0 3545 0 0 3561 0 0 3602 0 0 3729 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,0,''),(555,50726,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(556,54585,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(560,40154,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(570,40134,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(572,40134,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(573,40134,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(577,40127,10,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(578,51898,10,0,0,1,0,'0 0 0 0 0 ',1,'3854 0 0 0 0 0 3534 0 0 3534 0 0 3534 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,0,''),(582,50730,11,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,48,0,''),(605,40908,10,0,0,1,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(606,40906,10,0,0,1,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(607,45623,10,0,0,1,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(617,6096,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(619,6948,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(628,51282,13,0,0,1,0,'0 0 0 0 0 ',1,'3719 0 0 0 0 0 3545 0 0 3531 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,1598,''),(629,51284,13,0,0,1,0,'0 0 0 0 0 ',1,'3810 0 0 0 0 0 3531 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,24,1616,''),(630,51283,13,0,0,1,0,'0 0 0 0 0 ',1,'3832 0 0 0 0 0 3563 0 0 3545 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,1740,''),(631,51281,13,0,0,1,0,'0 0 0 0 0 ',1,'3820 0 0 0 0 0 3621 0 0 3545 0 0 0 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,24,960,''),(632,51280,13,0,0,1,0,'0 0 0 0 0 ',1,'3246 0 0 0 0 0 3545 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,1774,''),(634,1977,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(635,1977,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(636,1977,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(637,1977,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(638,50732,13,0,0,1,0,'0 0 0 0 0 ',1,'3834 0 0 0 0 0 3520 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,44,400,''),(639,54588,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(640,50365,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(641,50398,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3531 0 0 0 0 0 0 0 0 3596 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(642,50644,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3531 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(643,50613,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3545 0 0 3520 0 0 3531 0 0 3602 0 0 3729 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,17,545,''),(644,54582,13,0,0,1,0,'0 0 0 0 0 ',1,'2332 0 0 0 0 0 3563 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,17,0,''),(645,54583,13,0,0,1,0,'0 0 0 0 0 ',1,'3831 0 0 0 0 0 3563 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(646,50724,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3545 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(647,50719,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(649,50684,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3531 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,33,0,''),(693,50699,13,0,0,1,0,'0 0 0 0 0 ',1,'3232 0 0 0 0 0 3545 0 0 3520 0 0 0 0 0 3602 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,20,0,''),(697,51465,13,0,0,1,0,'0 0 0 0 0 ',1,'3796 0 0 0 0 0 3635 0 0 3561 0 0 0 0 0 3821 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,1569,''),(698,51463,13,0,0,1,0,'0 0 0 0 0 ',1,'3245 0 0 0 0 0 3561 0 0 3530 0 0 0 0 0 3307 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,1571,''),(699,51467,13,0,0,1,0,'0 0 0 0 0 ',1,'3794 0 0 0 0 0 3530 0 0 0 0 0 0 0 0 2868 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,1571,''),(700,51466,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,1584,''),(701,51451,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,613,''),(702,51464,13,0,0,1,0,'0 0 0 0 0 ',1,'3246 0 0 0 0 0 3548 0 0 0 0 0 0 0 0 2872 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,1616,''),(703,51407,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,1306,''),(704,51534,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(705,51398,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,1589,''),(706,51338,13,0,0,1,0,'0 0 0 0 0 ',1,'3232 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 2878 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,50,919,''),(707,51332,13,0,0,1,0,'0 0 0 0 0 ',1,'3243 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,914,''),(708,51335,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,914,''),(709,51339,13,0,0,1,0,'0 0 0 0 0 ',1,'2332 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,1572,''),(710,51337,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3548 0 0 3530 0 0 0 0 0 2872 0 0 3729 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,993,''),(718,50694,13,0,0,1,0,'0 0 0 0 0 ',1,'3719 0 0 0 0 0 3561 0 0 3548 0 0 3531 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,113,''),(719,50694,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3563 0 0 3545 0 0 3531 0 0 3753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,33,441,''),(720,47059,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(721,54585,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 3561 0 0 0 0 0 0 0 0 3752 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(723,100006,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(724,22033,13,0,0,255,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(727,22033,13,0,0,150,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,31,''),(730,100006,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(731,51534,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(732,15050,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,0,''),(733,6835,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,45,0,''),(734,5407,13,0,0,247,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(735,5407,13,0,0,255,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(736,5407,13,0,0,255,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(737,35026,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,120,1153,''),(738,35024,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,90,1153,''),(739,35022,10,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,40,1936,''),(742,9636,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,25,0,''),(747,39894,13,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2803 0 0 2804 0 0 0 0 0 0 0 0 0 0 0 ',-69,45,0,''),(748,10550,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,16,0,''),(752,12947,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(753,12947,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(754,12947,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(755,12947,11,0,0,1,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(756,49426,11,0,0,2,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(757,49426,10,0,0,8,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(758,50734,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,105,0,''),(759,30733,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,105,0,''),(760,35096,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,748,''),(761,35097,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,751,''),(762,35098,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,749,''),(763,35099,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,740,''),(764,35100,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,745,''),(765,33757,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,538,''),(766,33758,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,60,752,''),(767,33759,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,35,750,''),(768,33760,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,100,592,''),(769,33761,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,75,744,''),(771,22021,13,0,0,248,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(772,33312,13,0,0,1,0,'0 -3 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(782,43015,13,0,0,245,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(783,43015,13,0,0,255,0,'-1 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(784,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(785,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(786,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(787,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(788,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(789,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(790,23162,13,0,0,1,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(791,17020,13,0,0,95,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(792,17020,13,0,0,100,0,'0 0 0 0 0 ',0,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,''),(795,49426,13,0,0,6,0,'0 0 0 0 0 ',1,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ',0,0,0,'');
/*!40000 ALTER TABLE `item_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_loot_items`
--

DROP TABLE IF EXISTS `item_loot_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_loot_items` (
  `container_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'guid of container (item_instance.guid)',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'loot item entry (item_instance.itemEntry)',
  `item_count` int(10) NOT NULL DEFAULT '0' COMMENT 'stack size',
  `follow_rules` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'follow loot rules',
  `ffa` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'free-for-all',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `counted` tinyint(1) NOT NULL DEFAULT '0',
  `under_threshold` tinyint(1) NOT NULL DEFAULT '0',
  `needs_quest` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'quest drop',
  `rnd_prop` int(10) NOT NULL DEFAULT '0' COMMENT 'random enchantment added when originally rolled',
  `rnd_suffix` int(10) NOT NULL DEFAULT '0' COMMENT 'random suffix added when originally rolled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_loot_items`
--

LOCK TABLES `item_loot_items` WRITE;
/*!40000 ALTER TABLE `item_loot_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_loot_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_loot_money`
--

DROP TABLE IF EXISTS `item_loot_money`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_loot_money` (
  `container_id` int(10) NOT NULL DEFAULT '0' COMMENT 'guid of container (item_instance.guid)',
  `money` int(10) NOT NULL DEFAULT '0' COMMENT 'money loot (in copper)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_loot_money`
--

LOCK TABLES `item_loot_money` WRITE;
/*!40000 ALTER TABLE `item_loot_money` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_loot_money` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_refund_instance`
--

DROP TABLE IF EXISTS `item_refund_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_refund_instance` (
  `item_guid` int(10) unsigned NOT NULL COMMENT 'Item GUID',
  `player_guid` int(10) unsigned NOT NULL COMMENT 'Player GUID',
  `paidMoney` int(10) unsigned NOT NULL DEFAULT '0',
  `paidExtendedCost` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_guid`,`player_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item Refund System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_refund_instance`
--

LOCK TABLES `item_refund_instance` WRITE;
/*!40000 ALTER TABLE `item_refund_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_refund_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_soulbound_trade_data`
--

DROP TABLE IF EXISTS `item_soulbound_trade_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_soulbound_trade_data` (
  `itemGuid` int(10) unsigned NOT NULL COMMENT 'Item GUID',
  `allowedPlayers` text NOT NULL COMMENT 'Space separated GUID list of players who can receive this item in trade',
  PRIMARY KEY (`itemGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Item Refund System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_soulbound_trade_data`
--

LOCK TABLES `item_soulbound_trade_data` WRITE;
/*!40000 ALTER TABLE `item_soulbound_trade_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_soulbound_trade_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lag_reports`
--

DROP TABLE IF EXISTS `lag_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lag_reports` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `lagType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mapId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `posX` float NOT NULL DEFAULT '0',
  `posY` float NOT NULL DEFAULT '0',
  `posZ` float NOT NULL DEFAULT '0',
  `latency` int(10) unsigned NOT NULL DEFAULT '0',
  `createTime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lag_reports`
--

LOCK TABLES `lag_reports` WRITE;
/*!40000 ALTER TABLE `lag_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `lag_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lfg_data`
--

DROP TABLE IF EXISTS `lfg_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lfg_data` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `dungeon` int(10) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='LFG Data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lfg_data`
--

LOCK TABLES `lfg_data` WRITE;
/*!40000 ALTER TABLE `lfg_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `lfg_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logy_gm`
--

DROP TABLE IF EXISTS `logy_gm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logy_gm` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `player` varchar(12) NOT NULL,
  `account` int(9) NOT NULL,
  `command` varchar(255) NOT NULL,
  `position` varchar(96) NOT NULL,
  `selected` varchar(96) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  KEY `player` (`player`)
) ENGINE=InnoDB AUTO_INCREMENT=1301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logy_gm`
--

LOCK TABLES `logy_gm` WRITE;
/*!40000 ALTER TABLE `logy_gm` DISABLE KEYS */;
INSERT INTO `logy_gm` VALUES (1,'2016-05-09 20:32:06','Mew',1,'.np i','X: 16222.499023 Y: 16388.816406 Z: -64.378891 Map: 1','Ashen Rings (GUID: 1777407864)'),(2,'2016-05-09 20:32:39','Mew',1,'.te stormwind','X: 16217.667969 Y: 16416.195312 Z: -64.378143 Map: 1',' (GUID: 1777391448)'),(3,'2016-05-09 20:41:48','Mew',1,'.np i','X: -8812.822266 Y: 614.712158 Z: 95.072235 Map: 0','Ashen Rings (GUID: 3078713880)'),(4,'2016-05-09 20:43:56','Mew',1,'.lo faction argent','X: -8821.009766 Y: 615.203125 Z: 94.963112 Map: 0','Ashen Rings (GUID: 3124727320)'),(5,'2016-05-09 20:44:01','Mew',1,'.lo faction ashen','X: -8821.009766 Y: 615.203125 Z: 94.963112 Map: 0','Ashen Rings (GUID: 3124727224)'),(6,'2016-05-09 20:44:09','Mew',1,'.mod rep 1156 20000','X: -8821.009766 Y: 615.203125 Z: 94.963112 Map: 0',' (GUID: 3074459880)'),(7,'2016-05-09 20:44:14','Mew',1,'.mod rep 1156 exalted','X: -8821.009766 Y: 615.203125 Z: 94.963112 Map: 0',' (GUID: 3074459400)'),(8,'2016-05-10 03:16:30','Mew',1,'.gm off','X: -8827.384766 Y: 642.527527 Z: 94.378479 Map: 0',' (GUID: 2538349784)'),(9,'2016-05-10 03:16:35','Mew',1,'.gm on','X: -8827.384766 Y: 642.527527 Z: 94.378479 Map: 0','Stormwind City Patroller (GUID: 2538369656)'),(10,'2016-05-10 03:16:37','Mew',1,'.te gmi','X: -8827.384766 Y: 642.527527 Z: 94.378479 Map: 0',' (GUID: 2538350104)'),(11,'2016-05-10 03:16:41','Mew',1,'.gm off','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1',' (GUID: 2538352024)'),(12,'2016-05-10 03:17:52','Mew',1,'.server restart 0','X: 16227.701172 Y: 16258.473633 Z: 13.388804 Map: 1',' (GUID: 2538352104)'),(13,'2016-05-10 03:25:23','Mew',1,'.gm on','X: 16228.959961 Y: 16262.430664 Z: 13.406143 Map: 1',' (GUID: 2469467352)'),(14,'2016-05-10 04:02:34','Mew',1,'.server restart 0','X: 16223.149414 Y: 16264.535156 Z: 13.234891 Map: 1',' (GUID: 1400894040)'),(15,'2016-05-10 04:04:29','Mew',1,'.server restart 0 Jj','X: 16221.615234 Y: 16260.549805 Z: 13.244930 Map: 1',' (GUID: 2679104408)'),(16,'2016-05-10 04:07:29','Mew',1,'.server restart 3 Yolo:)','X: 16224.637695 Y: 16272.662109 Z: 13.175374 Map: 1',' (GUID: 959443704)'),(17,'2016-05-10 04:17:02','Mew',1,'.server restart 3 Yolo:)','X: 16225.331055 Y: 16295.704102 Z: 13.175374 Map: 1',' (GUID: 674239224)'),(18,'2016-05-10 04:26:14','Mew',1,'.server restart 3 Yolo:)','X: 16220.935547 Y: 16262.667969 Z: 13.298393 Map: 1',' (GUID: 3736089336)'),(19,'2016-05-10 04:37:16','Mew',1,'.server restart 3 Yolo:)','X: 16235.902344 Y: 16263.646484 Z: 14.011292 Map: 1',' (GUID: 4118106872)'),(20,'2016-05-10 04:39:06','Mew',1,'.lo item event mark','X: 16233.337891 Y: 16256.665039 Z: 13.995345 Map: 1','Event Vendor (GUID: 2293229432)'),(21,'2016-05-10 04:39:12','Mew',1,'.add 22033 255','X: 16229.135742 Y: 16257.778320 Z: 13.465807 Map: 1',' (GUID: 2292356312)'),(22,'2016-05-10 04:40:00','Mew',1,'.server info','X: 16236.862305 Y: 16263.431641 Z: 14.220384 Map: 1',' (GUID: 2292356632)'),(23,'2016-05-10 04:42:11','Mew',1,'.server restart 3 Yolo:)','X: 16236.862305 Y: 16263.431641 Z: 14.220384 Map: 1',' (GUID: 2292356552)'),(24,'2016-05-10 04:53:39','Mew',1,'.server info','X: 16202.500000 Y: 16255.799805 Z: 21.136900 Map: 1',' (GUID: 2993689976)'),(25,'2016-05-10 10:33:29','Domino',5,'.add 32235','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 495632328)'),(26,'2016-05-10 10:33:29','Domino',5,'.add 14562','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502632568)'),(27,'2016-05-10 10:33:29','Domino',5,'.add 14559','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502632648)'),(28,'2016-05-10 10:33:29','Domino',5,'.add 14560','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502632728)'),(29,'2016-05-10 10:33:29','Domino',5,'.add 14564','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502632808)'),(30,'2016-05-10 10:33:29','Domino',5,'.add 3315','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502632888)'),(31,'2016-05-10 10:33:29','Domino',5,'.add 28647','X: -8835.583984 Y: 632.790771 Z: 94.242050 Map: 0',' (GUID: 502633048)'),(32,'2016-05-10 10:33:52','Domino',5,'.lo i cursed','X: -8830.205078 Y: 637.497192 Z: 94.284378 Map: 0',' (GUID: 502633848)'),(33,'2016-05-10 10:34:15','Domino',5,'.lo i test','X: -8816.872070 Y: 644.277588 Z: 94.228699 Map: 0',' (GUID: 502633928)'),(34,'2016-05-10 10:36:35','Domino',5,'.lo i dodge','X: -8813.042969 Y: 628.315186 Z: 94.229195 Map: 0',' (GUID: 495629128)'),(35,'2016-05-10 10:38:28','Domino',5,'.lo i alex','X: -10751.166016 Y: 2448.511963 Z: 6.549391 Map: 1','Season Rewards (GUID: 498819064)'),(36,'2016-05-10 10:38:37','Domino',5,'.add  |cffe6cc80|Hitem:12947:0:0:0:0:0:0:0:81|h[Alex\'s Ring of Audacity]|h|r','X: -10745.847656 Y: 2431.185547 Z: 6.412024 Map: 1','Season Rewards (GUID: 498819256)'),(37,'2016-05-10 10:39:12','Domino',5,'.lo i ring of critical','X: -10722.616211 Y: 2444.231201 Z: 7.606963 Map: 1',' (GUID: 502634248)'),(38,'2016-05-10 10:39:18','Domino',5,'.add 18970','X: -10722.616211 Y: 2444.231201 Z: 7.606963 Map: 1',' (GUID: 502634408)'),(39,'2016-05-10 10:39:20','Domino',5,'.lo i critical','X: -10722.616211 Y: 2444.231201 Z: 7.606963 Map: 1',' (GUID: 502634488)'),(40,'2016-05-10 10:41:34','Domino',5,'.lo i depreceted','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 478636904)'),(41,'2016-05-10 10:41:39','Domino',5,'.lo i depcrecated','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502633928)'),(42,'2016-05-10 10:41:44','Domino',5,'.lo i deprecated','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634248)'),(43,'2016-05-10 10:42:45','Domino',5,'.add  |cffffffff|Hitem:5522:0:0:0:0:0:0:0:81|h[Spellstone (DEPRECATED)]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 478634344)'),(44,'2016-05-10 10:44:49','Domino',5,'.lo i unused','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 495628408)'),(45,'2016-05-10 10:44:57','Domino',5,'.add  |cff0070dd|Hitem:37430:0:0:0:0:0:0:0:81|h[Solid Sky Sapphire (Unused)]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 495628568)'),(46,'2016-05-10 10:45:26','Domino',5,'.lo i test','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634248)'),(47,'2016-05-10 10:46:14','Domino',5,'.add  |cff1eff00|Hitem:26541:0:0:0:0:0:0:0:81|h[68 TEST Green Cloth Necklace]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634568)'),(48,'2016-05-10 10:46:34','Domino',5,'.add  |cff1eff00|Hitem:26135:0:0:0:0:0:0:0:81|h[68 TEST Green Cloth Wrist]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634648)'),(49,'2016-05-10 10:46:36','Domino',5,'.add  |cff1eff00|Hitem:26513:0:0:0:0:0:0:0:81|h[68 TEST Green Cloth Cloak]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634808)'),(50,'2016-05-10 10:47:18','Domino',5,'.lo i socket','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634968)'),(51,'2016-05-10 10:47:21','Domino',5,'.lo i gem','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502635048)'),(52,'2016-05-10 10:47:35','Domino',5,'.lo i meta gem','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502635128)'),(53,'2016-05-10 10:48:24','Domino',5,'.lo i test','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634568)'),(54,'2016-05-10 10:48:43','Domino',5,'.add  |cff0070dd|Hitem:40232:0:0:0:0:0:0:0:81|h[Test Dazzling Talasite]|h|r','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502634808)'),(55,'2016-05-10 10:48:56','Domino',5,'.te jail','X: -10729.747070 Y: 2421.373535 Z: 7.575033 Map: 1',' (GUID: 502635208)'),(56,'2016-05-10 10:49:15','Domino',5,'.help','X: 16226.081055 Y: 16403.357422 Z: -64.378899 Map: 1',' (GUID: 502633528)'),(57,'2016-05-10 10:51:06','Domino',5,'.tele add gmk','X: 16226.081055 Y: 16403.357422 Z: -64.378899 Map: 1',' (GUID: 502634808)'),(58,'2016-05-10 10:51:09','Domino',5,'.tele add gmkocka','X: 16226.081055 Y: 16403.357422 Z: -64.378899 Map: 1',' (GUID: 502635208)'),(59,'2016-05-10 10:54:42','Domino',5,'.lo i bind on acquire','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 495629208)'),(60,'2016-05-10 10:54:44','Domino',5,'.add 8688','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 495632168)'),(61,'2016-05-10 10:55:11','Domino',5,'.lo i test ruby','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502632888)'),(62,'2016-05-10 10:55:15','Domino',5,'.add 28388','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502633048)'),(63,'2016-05-10 10:55:49','Domino',5,'.lo i talasite','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 495628728)'),(64,'2016-05-10 10:56:18','Domino',5,'.add  |cff0070dd|Hitem:40232:0:0:0:0:0:0:0:81|h[Test Dazzling Talasite]|h|r','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 495632248)'),(65,'2016-05-10 10:56:31','Domino',5,'.lo i yellow','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502633128)'),(66,'2016-05-10 10:56:35','Domino',5,'.lo i yellow gem','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502633208)'),(67,'2016-05-10 10:56:40','Domino',5,'.lo i yellow socket','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502633288)'),(68,'2016-05-10 10:57:13','Domino',5,'.add  |cff1eff00|Hitem:23235:0:0:0:0:0:0:0:81|h[Yellow Bryanite of Stamina]|h|r','X: 16227.555664 Y: 16400.566406 Z: -64.378677 Map: 1',' (GUID: 502633368)'),(69,'2016-05-10 10:58:16','Domino',5,'.lo i tatoo','X: 16220.633789 Y: 16388.857422 Z: -64.378822 Map: 1','Scott the Merciful (GUID: 495775992)'),(70,'2016-05-10 10:58:19','Domino',5,'.lo i taatoo','X: 16220.633789 Y: 16388.857422 Z: -64.378822 Map: 1','Scott the Merciful (GUID: 495775992)'),(71,'2016-05-10 10:58:20','Domino',5,'.lo i taa','X: 16220.633789 Y: 16388.857422 Z: -64.378822 Map: 1','Scott the Merciful (GUID: 495775992)'),(72,'2016-05-10 10:58:22','Domino',5,'.lo i tat','X: 16220.633789 Y: 16388.857422 Z: -64.378822 Map: 1','Scott the Merciful (GUID: 495775992)'),(73,'2016-05-10 10:59:14','Domino',5,'.lo i war paint','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635448)'),(74,'2016-05-10 10:59:43','Domino',5,'.lo i not good for ladies','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635528)'),(75,'2016-05-10 10:59:45','Domino',5,'.lo i for ladies','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635608)'),(76,'2016-05-10 11:00:10','Domino',5,'.lo i not','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635688)'),(77,'2016-05-10 11:00:16','Domino',5,'.lo i war paint (shirt)','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635768)'),(78,'2016-05-10 11:00:25','Domino',5,'.lo i paint','X: 16220.881836 Y: 16407.669922 Z: -64.378212 Map: 1',' (GUID: 502635848)'),(79,'2016-05-10 11:01:28','Domino',5,'.lo i tato','X: 16223.834961 Y: 16403.033203 Z: -64.377541 Map: 1',' (GUID: 502635928)'),(80,'2016-05-10 11:02:24','Domino',5,'.lo i shard','X: 16225.039062 Y: 16395.996094 Z: -64.379639 Map: 1',' (GUID: 502636008)'),(81,'2016-05-10 11:02:30','Domino',5,'.lo i shard','X: 16225.583984 Y: 16403.519531 Z: -64.379051 Map: 1',' (GUID: 502636088)'),(82,'2016-05-10 11:02:44','Domino',5,'.lo i stormwind tabard','X: 16228.718750 Y: 16409.164062 Z: -64.378593 Map: 1',' (GUID: 502636168)'),(83,'2016-05-10 11:03:09','Domino',5,'.add 45574','X: 16232.181641 Y: 16415.300781 Z: -64.379082 Map: 1',' (GUID: 502636248)'),(84,'2016-05-10 11:03:12','Domino',5,'.add 45575','X: 16232.181641 Y: 16415.300781 Z: -64.378761 Map: 1',' (GUID: 502636328)'),(85,'2016-05-10 11:03:23','Domino',5,'.add 45570','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691981560)'),(86,'2016-05-10 11:04:11','Domino',5,'.lo i test trinket','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691982120)'),(87,'2016-05-10 11:04:15','Domino',5,'.lo i trinket','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691982200)'),(88,'2016-05-10 11:05:07','Domino',5,'.lo i unused','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691982200)'),(89,'2016-05-10 11:06:29','Domino',5,'.lo i martin','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691982280)'),(90,'2016-05-10 11:06:40','Domino',5,'.add 192','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 502640760)'),(91,'2016-05-10 11:06:41','Domino',5,'.add 17','X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 502640840)'),(92,'2016-05-10 11:07:38','Domino',5,'.lo spell dual wield','X: 16221.390625 Y: 16405.355469 Z: -64.378685 Map: 1',' (GUID: 502640920)'),(93,'2016-05-10 11:08:31','Domino',5,'.learn 48717','X: 16221.390625 Y: 16405.355469 Z: -64.377502 Map: 1',' (GUID: 502641000)'),(94,'2016-05-10 11:09:25','Domino',5,'.add  |cffa335ee|Hitem:51393:0:0:0:0:0:0:0:81|h[Wrathful Gladiator\'s Claymore]|h|r','X: 16228.533203 Y: 16388.179688 Z: -64.379303 Map: 1','Trapjaw Rix (GUID: 495776856)'),(95,'2016-05-10 11:09:27','Domino',5,'.add  |cffa335ee|Hitem:51393:0:0:0:0:0:0:0:81|h[Wrathful Gladiator\'s Claymore]|h|r','X: 16228.533203 Y: 16388.179688 Z: -64.379303 Map: 1','Trapjaw Rix (GUID: 495776760)'),(96,'2016-05-10 11:10:45','Domino',5,'.add  |cffa335ee|Hitem:51391:0:0:0:0:0:0:0:81|h[Wrathful Gladiator\'s Crusher]|h|r','X: 16230.300781 Y: 16386.523438 Z: -64.379547 Map: 1','Trapjaw Rix (GUID: 495775896)'),(97,'2016-05-10 11:10:46','Domino',5,'.add  |cffa335ee|Hitem:51391:0:0:0:0:0:0:0:81|h[Wrathful Gladiator\'s Crusher]|h|r','X: 16230.300781 Y: 16386.523438 Z: -64.379547 Map: 1','Trapjaw Rix (GUID: 495777240)'),(98,'2016-05-10 11:11:48','Domino',5,'.learn 674','X: 16224.239258 Y: 16398.550781 Z: -64.377838 Map: 1','Trapjaw Rix (GUID: 495777144)'),(99,'2016-05-10 11:12:27','Domino',5,'.learn 42459','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1','Trapjaw Rix (GUID: 495775992)'),(100,'2016-05-10 11:12:42','Domino',5,'.unlearn 42459','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1',' (GUID: 502643000)'),(101,'2016-05-10 11:12:45','Domino',5,'.unlearn 674','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1',' (GUID: 502643080)'),(102,'2016-05-10 11:13:06','Domino',5,'.lo i test','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1',' (GUID: 502634008)'),(103,'2016-05-10 11:13:18','Domino',5,'.lo i shard of the defiler','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1',' (GUID: 502634088)'),(104,'2016-05-10 11:13:23','Domino',5,'.add 17142','X: 16228.113281 Y: 16406.570312 Z: -64.379356 Map: 1',' (GUID: 502634248)'),(105,'2016-05-10 11:13:45','Domino',5,'.learn 674','X: 16227.535156 Y: 16396.855469 Z: -64.378769 Map: 1',' (GUID: 502634488)'),(106,'2016-05-10 11:15:43','Domino',5,'.add 13315','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502632728)'),(107,'2016-05-10 11:16:38','Domino',5,'.lo i deprecated','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 495628488)'),(108,'2016-05-10 11:18:04','Domino',5,'.add  |cff9d9d9d|Hitem:1298:0:0:0:0:0:0:0:81|h[Deprecated Night Mage Wristguards]|h|r','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 495628568)'),(109,'2016-05-10 11:18:33','Domino',5,'.add 40848','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502633928)'),(110,'2016-05-10 11:18:47','Domino',5,'.ad 40848','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634488)'),(111,'2016-05-10 11:18:57','Domino',5,'.ad 40484','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634568)'),(112,'2016-05-10 11:19:31','Domino',5,'.add  |cff0070dd|Hitem:40754:0:0:0:0:0:0:0:81|h[DEPRECATED Flame Red Ejector Seat]|h|r','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634728)'),(113,'2016-05-10 11:19:45','Domino',5,'.lo i test glyph','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634808)'),(114,'2016-05-10 11:19:50','Domino',5,'.lo i glyph','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 495628568)'),(115,'2016-05-10 11:20:13','Domino',5,'.add 40948','X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502632808)'),(116,'2016-05-10 11:20:50','Domino',5,'.gm on','X: 16222.414062 Y: 16402.736328 Z: -64.379715 Map: 1',' (GUID: 502634888)'),(117,'2016-05-10 11:22:34','Domino',5,'.lo i relic','X: 16227.686523 Y: 16386.300781 Z: -64.378853 Map: 1','Lieutenant Tristia (GUID: 495776856)'),(118,'2016-05-10 11:23:17','Domino',5,'.te dalaran','X: 16222.941406 Y: 16411.648438 Z: -64.377861 Map: 1',' (GUID: 495632248)'),(119,'2016-05-10 11:23:46','Domino',5,'.mod sp 10','X: 5784.833496 Y: 610.267029 Z: 648.768799 Map: 571',' (GUID: 531391304)'),(120,'2016-05-10 11:24:29','Domino',5,'.add  |cffa335ee|Hitem:51423:0:0:0:0:0:0:0:81|h[Wrathful Gladiator\'s Idol of Tenacity]|h|r','X: 5754.409668 Y: 584.132690 Z: 613.735352 Map: 571','Nargle Lashcord (GUID: 532234136)'),(121,'2016-05-10 11:24:37','Domino',5,'.te gmi','X: 5827.014160 Y: 689.242493 Z: 609.885986 Map: 571',' (GUID: 531391544)'),(122,'2016-05-10 11:24:45','Domino',5,'.mod sp 1','X: 16225.398438 Y: 16267.481445 Z: 13.148890 Map: 1',' (GUID: 531391624)'),(123,'2016-05-10 11:24:47','Domino',5,'.gm fly off','X: 16225.724609 Y: 16263.365234 Z: 13.269677 Map: 1',' (GUID: 531391704)'),(124,'2016-05-10 11:28:06','Domino',5,'.lo i event mark','X: 16229.475586 Y: 16271.699219 Z: 15.434261 Map: 1',' (GUID: 531392584)'),(125,'2016-05-10 11:28:12','Domino',5,'.lo i blood elfka','X: 16233.968750 Y: 16261.014648 Z: 13.949268 Map: 1',' (GUID: 531391784)'),(126,'2016-05-10 11:28:15','Domino',5,'.add 100006','X: 16239.218750 Y: 16263.927734 Z: 14.600876 Map: 1',' (GUID: 531391864)'),(127,'2016-05-10 11:28:52','Domino',5,'.mod sp 10','X: 16224.299805 Y: 16279.544922 Z: 13.175746 Map: 1',' (GUID: 480881752)'),(128,'2016-05-10 11:28:56','Domino',5,'.mod sp 5','X: 16222.630859 Y: 16295.184570 Z: 13.176085 Map: 1',' (GUID: 480881752)'),(129,'2016-05-10 11:29:24','Domino',5,'.mod sp 1','X: 16201.220703 Y: 16268.396484 Z: 12.350666 Map: 1',' (GUID: 480881832)'),(130,'2016-05-10 13:25:42','Mew',1,'.server restart 3 Yolo:)','X: 16220.819336 Y: 16258.981445 Z: 14.010292 Map: 1','Domino (GUID: 502636008)'),(131,'2016-05-10 13:46:02','Domino',5,'.te gmi','X: 4277.967773 Y: -2773.658691 Z: 7.118993 Map: 0',' (GUID: 4264129832)'),(132,'2016-05-10 14:15:26','Domino',5,'.gm i','X: 16225.108398 Y: 16262.746094 Z: 13.267747 Map: 1',' (GUID: 1081107272)'),(133,'2016-05-10 14:15:36','Domino',5,'.gm fly on','X: 16222.895508 Y: 16270.725586 Z: 13.000453 Map: 1',' (GUID: 1081107352)'),(134,'2016-05-10 14:15:41','Domino',5,'.gm fly off','X: 16224.147461 Y: 16271.429688 Z: 21.895380 Map: 1',' (GUID: 1081107432)'),(135,'2016-05-10 14:15:42','Domino',5,'.te gmk','X: 16224.147461 Y: 16271.429688 Z: 19.483992 Map: 1',' (GUID: 1081107512)'),(136,'2016-05-10 14:16:33','Domino',5,'.te gmi','X: 16221.830078 Y: 16407.402344 Z: -64.380005 Map: 1',' (GUID: 1081107672)'),(137,'2016-05-10 14:16:37','Domino',5,'.gps','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1',' (GUID: 1081107832)'),(138,'2016-05-10 14:17:44','Domino',5,'.guild rank Domino 0','X: 16226.200195 Y: 16257.000000 Z: 13.207838 Map: 1',' (GUID: 1081107912)'),(139,'2016-05-10 14:17:51','Domino',5,'.te gmk','X: 16225.649414 Y: 16263.927734 Z: 13.256889 Map: 1',' (GUID: 1081107992)'),(140,'2016-05-10 14:18:02','Domino',5,'.lo i guild','X: 16218.746094 Y: 16412.408203 Z: -64.376144 Map: 1','Tyrael (GUID: 1081088728)'),(141,'2016-05-10 14:18:31','Domino',5,'.add 28389','X: 16218.746094 Y: 16412.408203 Z: -64.378876 Map: 1','Tyrael (GUID: 1081088728)'),(142,'2016-05-10 14:18:40','Domino',5,'.add 28389','X: 16215.530273 Y: 16418.162109 Z: -64.377892 Map: 1','Tyrael (GUID: 1081088728)'),(143,'2016-05-10 14:19:04','Domino',5,'.guild rank Domino 1','X: 16223.694336 Y: 16263.654297 Z: 13.253228 Map: 1',' (GUID: 1081107672)'),(144,'2016-05-10 14:38:11','Domino',5,'.server shutdown 0','X: 16231.021484 Y: 16264.701172 Z: 13.379725 Map: 1',' (GUID: 1081108152)'),(145,'2016-05-10 14:42:39','Domino',5,'.te gmk','X: 16221.966797 Y: 16267.458008 Z: 13.163903 Map: 1',' (GUID: 2439907000)'),(146,'2016-05-10 14:45:53','Domino',5,'.nameann asd','X: 16217.506836 Y: 16415.589844 Z: -64.379028 Map: 1',' (GUID: 2439907080)'),(147,'2016-05-10 14:46:03','Domino',5,'.gmann asd','X: 16225.641602 Y: 16401.468750 Z: -64.379044 Map: 1',' (GUID: 2439907240)'),(148,'2016-05-10 14:46:12','Domino',5,'.ann asd','X: 16219.178711 Y: 16399.214844 Z: -64.378860 Map: 1',' (GUID: 2439907400)'),(149,'2016-05-10 14:52:50','Domino',5,'.pinfo tests','X: 16231.265625 Y: 16409.625000 Z: -64.378716 Map: 1',' (GUID: 2451543016)'),(150,'2016-05-10 14:53:03','Domino',5,'.gm i','X: 16232.243164 Y: 16411.160156 Z: -64.379135 Map: 1',' (GUID: 2451542536)'),(151,'2016-05-10 14:53:10','Domino',5,'.server shutdown 2 ahoj','X: 16232.243164 Y: 16411.160156 Z: -64.379135 Map: 1',' (GUID: 2451542456)'),(152,'2016-05-10 14:56:09','Domino',5,'.server shutdown 0','X: 16224.996094 Y: 16405.140625 Z: -64.378059 Map: 1',' (GUID: 2695488248)'),(153,'2016-05-10 14:57:16','Domino',5,'.guild rank Domino 0','X: 16233.295898 Y: 16405.533203 Z: -64.378540 Map: 1',' (GUID: 2118087416)'),(154,'2016-05-10 14:58:02','Domino',5,'.te gmi','X: 16233.295898 Y: 16405.533203 Z: -64.378540 Map: 1',' (GUID: 2080436424)'),(155,'2016-05-10 14:58:09','Domino',5,'.mod money 99999999','X: 16221.442383 Y: 16267.295898 Z: 13.172451 Map: 1',' (GUID: 2080436344)'),(156,'2016-05-10 15:01:06','Domino',5,'.guild rank Domino 1','X: 16221.442383 Y: 16267.295898 Z: 13.172049 Map: 1',' (GUID: 2080436344)'),(157,'2016-05-10 16:35:44','Domino',5,'.gm i','X: 16228.484375 Y: 16255.683594 Z: 13.322938 Map: 1',' (GUID: 1802597192)'),(158,'2016-05-10 16:39:37','Domino',5,'.te icecrown','X: 16225.655273 Y: 16265.214844 Z: 13.227791 Map: 1',' (GUID: 1802597192)'),(159,'2016-05-11 14:20:20','Mew',1,'.server restart 3 Yolo:)','X: 16223.414062 Y: 16270.168945 Z: 13.026626 Map: 1',' (GUID: 4086113016)'),(160,'2016-05-11 14:22:10','Mew',1,'.te kocka','X: 16223.911133 Y: 16262.154297 Z: 13.268312 Map: 1',' (GUID: 3516572728)'),(161,'2016-05-11 14:22:14','Mew',1,'.gm on','X: 16231.193359 Y: 16399.871094 Z: -64.376457 Map: 1','Lieutenant Tristia (GUID: 3517390264)'),(162,'2016-05-11 14:22:45','Mew',1,'.app domino','X: 16227.216797 Y: 16404.640625 Z: -64.378838 Map: 1',' (GUID: 3516572968)'),(163,'2016-05-11 14:22:48','Domino',5,'.gm off','X: 5791.344238 Y: 2075.619141 Z: 636.064209 Map: 571',' (GUID: 3516573048)'),(164,'2016-05-11 14:22:56','Domino',5,'.aura 65126','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573128)'),(165,'2016-05-11 14:22:56','Domino',5,'.aura 72525','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573208)'),(166,'2016-05-11 14:22:56','Domino',5,'.aura 64112','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573288)'),(167,'2016-05-11 14:22:56','Domino',5,'.aura 47008','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573368)'),(168,'2016-05-11 14:22:56','Domino',5,'.aura 40733','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573448)'),(169,'2016-05-11 14:22:56','Domino',5,'.aura 64238','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573528)'),(170,'2016-05-11 14:22:56','Domino',5,'.aura 68378','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573608)'),(171,'2016-05-11 14:22:56','Domino',5,'.aura 61715','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573688)'),(172,'2016-05-11 14:22:56','Domino',5,'.mod sp 8','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573768)'),(173,'2016-05-11 14:22:56','Domino',5,'.mod sc 1','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516573848)'),(174,'2016-05-11 14:22:57','Domino',5,'.aura 65126','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516573928)'),(175,'2016-05-11 14:22:57','Domino',5,'.aura 72525','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574008)'),(176,'2016-05-11 14:22:57','Domino',5,'.aura 64112','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574088)'),(177,'2016-05-11 14:22:57','Domino',5,'.aura 47008','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574168)'),(178,'2016-05-11 14:22:57','Domino',5,'.aura 40733','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574248)'),(179,'2016-05-11 14:22:57','Domino',5,'.aura 64238','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574328)'),(180,'2016-05-11 14:22:57','Domino',5,'.aura 68378','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574408)'),(181,'2016-05-11 14:22:57','Domino',5,'.aura 61715','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574488)'),(182,'2016-05-11 14:22:57','Domino',5,'.mod sp 8','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574568)'),(183,'2016-05-11 14:22:57','Domino',5,'.mod sc 1','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571',' (GUID: 3516574648)'),(184,'2016-05-11 14:23:03','Domino',5,'.mod sc 1','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Mew (GUID: 3516574728)'),(185,'2016-05-11 14:23:05','Domino',5,'.mod sc 1','X: 5791.330566 Y: 2074.541992 Z: 636.064209 Map: 571','Domino (GUID: 3516574808)'),(186,'2016-05-11 14:23:25','Mew',1,'.gm off','X: 5790.000000 Y: 2071.479980 Z: 636.065002 Map: 571',' (GUID: 3516575288)'),(187,'2016-05-11 14:23:53','Mew',1,'.maxskill','X: -125.968948 Y: 2226.759033 Z: 35.233700 Map: 631','Mew (GUID: 3516575368)'),(188,'2016-05-11 14:24:07','Domino',5,'.maxskill','X: -201.450806 Y: 2213.729248 Z: 35.233612 Map: 631','Domino (GUID: 3478132728)'),(189,'2016-05-11 14:24:13','Domino',5,'.lo spell starfall','X: -203.366165 Y: 2213.313232 Z: 35.233536 Map: 631','Domino (GUID: 3516575368)'),(190,'2016-05-11 14:24:17','Mew',1,'.lo spell starfall','X: -318.233032 Y: 2209.233887 Z: 42.565273 Map: 631','Mew (GUID: 3516575448)'),(191,'2016-05-11 14:24:37','Mew',1,'.learn 26540','X: -318.233032 Y: 2209.233887 Z: 42.565273 Map: 631','Mew (GUID: 3516575528)'),(192,'2016-05-11 14:25:02','Domino',5,'.learn 64378','X: -346.168945 Y: 2210.317139 Z: 42.500713 Map: 631','Domino (GUID: 3516575608)'),(193,'2016-05-11 14:25:27','Domino',5,'.unlearn 64378','X: -569.360291 Y: 2203.569824 Z: 49.476692 Map: 631','Domino (GUID: 3516575688)'),(194,'2016-05-11 14:25:35','Domino',5,'.learn 53201','X: -569.360291 Y: 2203.569824 Z: 49.476692 Map: 631','Domino (GUID: 3517382776)'),(195,'2016-05-11 14:25:52','Domino',5,'.gm fly on','X: -637.032104 Y: 2208.976318 Z: 51.551659 Map: 631',' (GUID: 3517382856)'),(196,'2016-05-11 14:26:05','Domino',5,'.app mew','X: -645.822510 Y: 2213.615234 Z: 189.328217 Map: 631',' (GUID: 3517382936)'),(197,'2016-05-11 14:26:09','Mew',1,'.app','X: -631.718445 Y: 2208.825928 Z: 192.550247 Map: 631','Domino (GUID: 3517383096)'),(198,'2016-05-11 14:26:19','Domino',5,'.rev','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383336)'),(199,'2016-05-11 14:26:20','Domino',5,'.aura 65126','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383416)'),(200,'2016-05-11 14:26:20','Domino',5,'.aura 72525','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383496)'),(201,'2016-05-11 14:26:20','Domino',5,'.aura 64112','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383576)'),(202,'2016-05-11 14:26:20','Domino',5,'.aura 47008','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383656)'),(203,'2016-05-11 14:26:20','Domino',5,'.aura 40733','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383736)'),(204,'2016-05-11 14:26:20','Domino',5,'.aura 64238','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383816)'),(205,'2016-05-11 14:26:20','Domino',5,'.aura 68378','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383896)'),(206,'2016-05-11 14:26:20','Domino',5,'.aura 61715','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517383976)'),(207,'2016-05-11 14:26:20','Domino',5,'.mod sp 8','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517384056)'),(208,'2016-05-11 14:26:20','Domino',5,'.mod scale 1','X: -634.727661 Y: 2209.741455 Z: -25.841557 Map: 631','Domino (GUID: 3517384136)'),(209,'2016-05-11 14:26:21','Domino',5,'.aura 65126','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384216)'),(210,'2016-05-11 14:26:21','Domino',5,'.aura 72525','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384296)'),(211,'2016-05-11 14:26:21','Domino',5,'.aura 64112','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384376)'),(212,'2016-05-11 14:26:21','Domino',5,'.aura 47008','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3516572648)'),(213,'2016-05-11 14:26:21','Domino',5,'.aura 40733','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3516572728)'),(214,'2016-05-11 14:26:21','Domino',5,'.aura 64238','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384456)'),(215,'2016-05-11 14:26:21','Domino',5,'.aura 68378','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384536)'),(216,'2016-05-11 14:26:21','Domino',5,'.aura 61715','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384616)'),(217,'2016-05-11 14:26:21','Domino',5,'.mod sp 8','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384696)'),(218,'2016-05-11 14:26:21','Domino',5,'.mod scale 1','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384776)'),(219,'2016-05-11 14:26:22','Mew',1,'.te icecrowncitadel','X: 6440.523438 Y: 2060.100342 Z: 563.355591 Map: 571',' (GUID: 3517384856)'),(220,'2016-05-11 14:26:30','Domino',5,'.gm on','X: -634.727661 Y: 2209.741455 Z: -26.498621 Map: 631',' (GUID: 3517384936)'),(221,'2016-05-11 14:26:32','Mew',1,'.rev','X: 12.160906 Y: 2210.848633 Z: 30.000000 Map: 631','Domino (GUID: 3517385096)'),(222,'2016-05-11 14:26:32','Domino',5,'.rev','X: -634.592285 Y: 2209.855957 Z: -26.719585 Map: 631',' (GUID: 3517385256)'),(223,'2016-05-11 14:26:33','Mew',1,'.summ','X: 12.160906 Y: 2210.848633 Z: 30.000000 Map: 631','Domino (GUID: 3517385336)'),(224,'2016-05-11 14:26:37','Domino',5,'.aura 65126','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385416)'),(225,'2016-05-11 14:26:37','Domino',5,'.aura 72525','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385496)'),(226,'2016-05-11 14:26:37','Domino',5,'.aura 64112','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385576)'),(227,'2016-05-11 14:26:37','Domino',5,'.aura 47008','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385656)'),(228,'2016-05-11 14:26:37','Domino',5,'.aura 40733','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385736)'),(229,'2016-05-11 14:26:37','Domino',5,'.aura 64238','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385816)'),(230,'2016-05-11 14:26:37','Domino',5,'.aura 68378','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385896)'),(231,'2016-05-11 14:26:37','Domino',5,'.aura 61715','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517385976)'),(232,'2016-05-11 14:26:37','Domino',5,'.mod sp 8','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517386056)'),(233,'2016-05-11 14:26:37','Domino',5,'.mod scale 1','X: 5.407677 Y: 2210.559326 Z: 30.115658 Map: 631','Mew (GUID: 3517386136)'),(234,'2016-05-11 14:26:40','Domino',5,'.aura 65126','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386216)'),(235,'2016-05-11 14:26:40','Domino',5,'.aura 72525','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386296)'),(236,'2016-05-11 14:26:40','Domino',5,'.aura 64112','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386376)'),(237,'2016-05-11 14:26:40','Domino',5,'.aura 47008','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386456)'),(238,'2016-05-11 14:26:40','Domino',5,'.aura 40733','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386536)'),(239,'2016-05-11 14:26:40','Domino',5,'.aura 64238','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386616)'),(240,'2016-05-11 14:26:40','Domino',5,'.aura 68378','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517386696)'),(241,'2016-05-11 14:26:40','Domino',5,'.aura 61715','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664715896)'),(242,'2016-05-11 14:26:40','Domino',5,'.mod sp 8','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664715976)'),(243,'2016-05-11 14:26:40','Domino',5,'.mod scale 1','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716056)'),(244,'2016-05-11 14:26:41','Domino',5,'.aura 65126','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716136)'),(245,'2016-05-11 14:26:41','Domino',5,'.aura 72525','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716216)'),(246,'2016-05-11 14:26:41','Domino',5,'.aura 64112','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716296)'),(247,'2016-05-11 14:26:41','Domino',5,'.aura 47008','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716376)'),(248,'2016-05-11 14:26:41','Domino',5,'.aura 40733','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716456)'),(249,'2016-05-11 14:26:41','Domino',5,'.aura 64238','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716536)'),(250,'2016-05-11 14:26:41','Domino',5,'.aura 68378','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716616)'),(251,'2016-05-11 14:26:41','Domino',5,'.aura 61715','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716696)'),(252,'2016-05-11 14:26:41','Domino',5,'.mod sp 8','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3517382936)'),(253,'2016-05-11 14:26:41','Domino',5,'.mod scale 1','X: -5.258737 Y: 2208.302734 Z: 30.115658 Map: 631','Mew (GUID: 3664716776)'),(254,'2016-05-11 14:26:41','Domino',5,'.aura 65126','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664716856)'),(255,'2016-05-11 14:26:41','Domino',5,'.aura 72525','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664716936)'),(256,'2016-05-11 14:26:41','Domino',5,'.aura 64112','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717016)'),(257,'2016-05-11 14:26:41','Domino',5,'.aura 47008','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717096)'),(258,'2016-05-11 14:26:41','Domino',5,'.aura 40733','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717176)'),(259,'2016-05-11 14:26:41','Domino',5,'.aura 64238','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717256)'),(260,'2016-05-11 14:26:41','Domino',5,'.aura 68378','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717336)'),(261,'2016-05-11 14:26:41','Domino',5,'.aura 61715','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717416)'),(262,'2016-05-11 14:26:41','Domino',5,'.mod sp 8','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717496)'),(263,'2016-05-11 14:26:41','Domino',5,'.mod scale 1','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3664717576)'),(264,'2016-05-11 14:26:41','Domino',5,'.aura 65126','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516572808)'),(265,'2016-05-11 14:26:41','Domino',5,'.aura 72525','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516572888)'),(266,'2016-05-11 14:26:41','Domino',5,'.aura 64112','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516572968)'),(267,'2016-05-11 14:26:41','Domino',5,'.aura 47008','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573048)'),(268,'2016-05-11 14:26:41','Domino',5,'.aura 40733','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573128)'),(269,'2016-05-11 14:26:41','Domino',5,'.aura 64238','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573208)'),(270,'2016-05-11 14:26:41','Domino',5,'.aura 68378','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573288)'),(271,'2016-05-11 14:26:41','Domino',5,'.aura 61715','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573368)'),(272,'2016-05-11 14:26:41','Domino',5,'.mod sp 8','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573448)'),(273,'2016-05-11 14:26:41','Domino',5,'.mod scale 1','X: -9.635560 Y: 2205.278564 Z: 30.115658 Map: 631','Mew (GUID: 3516573528)'),(274,'2016-05-11 14:26:44','Domino',5,'.aura 65126','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516573608)'),(275,'2016-05-11 14:26:44','Domino',5,'.aura 72525','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516573688)'),(276,'2016-05-11 14:26:44','Domino',5,'.aura 64112','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516573768)'),(277,'2016-05-11 14:26:44','Domino',5,'.aura 47008','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516573848)'),(278,'2016-05-11 14:26:44','Domino',5,'.aura 40733','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516573928)'),(279,'2016-05-11 14:26:44','Domino',5,'.aura 64238','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516574008)'),(280,'2016-05-11 14:26:44','Domino',5,'.aura 68378','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516574088)'),(281,'2016-05-11 14:26:44','Domino',5,'.aura 61715','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516574168)'),(282,'2016-05-11 14:26:44','Domino',5,'.mod sp 8','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516574248)'),(283,'2016-05-11 14:26:44','Domino',5,'.mod scale 1','X: -81.580765 Y: 2211.781738 Z: 27.905169 Map: 631','Domino (GUID: 3516574328)'),(284,'2016-05-11 14:27:12','Domino',5,'.die','X: -647.792175 Y: 2192.734131 Z: 253.358932 Map: 631','Spire Frostwyrm (GUID: 3507795256)'),(285,'2016-05-11 14:27:15','Domino',5,'.die','X: -647.677063 Y: 2212.303955 Z: 274.273010 Map: 631','Spire Frostwyrm (GUID: 3507795256)'),(286,'2016-05-11 14:27:37','Mew',1,'.app','X: -612.380676 Y: 2212.182373 Z: 49.750069 Map: 631','Domino (GUID: 3507897880)'),(287,'2016-05-11 14:27:40','Domino',5,'.gm off','X: -458.801971 Y: 1928.427124 Z: 215.504272 Map: 631',' (GUID: 3483936280)'),(288,'2016-05-11 14:28:43','Domino',5,'.aura 65126','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(289,'2016-05-11 14:28:43','Domino',5,'.aura 72525','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(290,'2016-05-11 14:28:43','Domino',5,'.aura 64112','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(291,'2016-05-11 14:28:43','Domino',5,'.aura 47008','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(292,'2016-05-11 14:28:43','Domino',5,'.aura 40733','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(293,'2016-05-11 14:28:43','Domino',5,'.aura 64238','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(294,'2016-05-11 14:28:43','Domino',5,'.aura 68378','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(295,'2016-05-11 14:28:43','Domino',5,'.aura 61715','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(296,'2016-05-11 14:28:43','Domino',5,'.mod sp 8','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(297,'2016-05-11 14:28:43','Domino',5,'.mod scale 1','X: -324.588684 Y: 2379.754639 Z: 432.086151 Map: 631','Horde Gunship Cannon (GUID: 3593210744)'),(298,'2016-05-11 14:28:51','Mew',1,'.app','X: -418.332123 Y: 2439.670166 Z: 391.745575 Map: 631','Domino (GUID: 3516572648)'),(299,'2016-05-11 14:29:27','Domino',5,'.die','X: -422.326080 Y: 2425.637695 Z: 475.450256 Map: 631','The Skybreaker (GUID: 3478444376)'),(300,'2016-05-11 14:29:33','Domino',5,'.respawn','X: -427.909882 Y: 2420.130615 Z: 475.497040 Map: 631','The Skybreaker (GUID: 3478441496)'),(301,'2016-05-11 14:29:47','Mew',1,'.app','X: -433.571503 Y: 2425.458740 Z: 471.695923 Map: 631','Domino (GUID: 3476116696)'),(302,'2016-05-11 14:29:55','Domino',5,'.die','X: -439.086853 Y: 2464.794434 Z: 469.780670 Map: 631','Domino (GUID: 3476115736)'),(303,'2016-05-11 14:30:13','Mew',1,'.rev','X: -437.446991 Y: 2032.510010 Z: 191.233994 Map: 631','Mew (GUID: 3516574488)'),(304,'2016-05-11 14:30:24','Mew',1,'.summ','X: -437.469299 Y: 2006.369995 Z: 191.233948 Map: 631','Domino (GUID: 3476118456)'),(305,'2016-05-11 14:30:29','Domino',5,'.mod sc 1','X: -437.377655 Y: 2000.874634 Z: 191.233902 Map: 631',' (GUID: 3476117976)'),(306,'2016-05-11 14:30:30','Domino',5,'.mod sc 1','X: -437.299591 Y: 1981.978394 Z: 198.261017 Map: 631','Mew (GUID: 3516572648)'),(307,'2016-05-11 14:30:32','Domino',5,'.aura 65126','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516572728)'),(308,'2016-05-11 14:30:33','Domino',5,'.aura 72525','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516572808)'),(309,'2016-05-11 14:30:33','Domino',5,'.aura 64112','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516572888)'),(310,'2016-05-11 14:30:33','Domino',5,'.aura 47008','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516572968)'),(311,'2016-05-11 14:30:33','Domino',5,'.aura 40733','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573048)'),(312,'2016-05-11 14:30:33','Domino',5,'.aura 64238','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573128)'),(313,'2016-05-11 14:30:33','Domino',5,'.aura 68378','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573208)'),(314,'2016-05-11 14:30:33','Domino',5,'.aura 61715','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573288)'),(315,'2016-05-11 14:30:33','Domino',5,'.mod sp 8','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573368)'),(316,'2016-05-11 14:30:33','Domino',5,'.mod scale 1','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516573448)'),(317,'2016-05-11 14:30:34','Domino',5,'.aura 65126','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573528)'),(318,'2016-05-11 14:30:34','Domino',5,'.aura 72525','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573608)'),(319,'2016-05-11 14:30:34','Domino',5,'.aura 64112','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573688)'),(320,'2016-05-11 14:30:34','Domino',5,'.aura 47008','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573768)'),(321,'2016-05-11 14:30:34','Domino',5,'.aura 40733','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573848)'),(322,'2016-05-11 14:30:34','Domino',5,'.aura 64238','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516573928)'),(323,'2016-05-11 14:30:34','Domino',5,'.aura 68378','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516574008)'),(324,'2016-05-11 14:30:34','Domino',5,'.aura 61715','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516574088)'),(325,'2016-05-11 14:30:34','Domino',5,'.mod sp 8','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516574168)'),(326,'2016-05-11 14:30:34','Domino',5,'.mod scale 1','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631',' (GUID: 3516574248)'),(327,'2016-05-11 14:30:34','Domino',5,'.aura 65126','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3553829784)'),(328,'2016-05-11 14:30:34','Domino',5,'.aura 72525','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3553829864)'),(329,'2016-05-11 14:30:34','Domino',5,'.aura 64112','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3553830984)'),(330,'2016-05-11 14:30:34','Domino',5,'.aura 47008','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3476118696)'),(331,'2016-05-11 14:30:34','Domino',5,'.aura 40733','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3553829544)'),(332,'2016-05-11 14:30:34','Domino',5,'.aura 64238','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3553830264)'),(333,'2016-05-11 14:30:34','Domino',5,'.aura 68378','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3476118376)'),(334,'2016-05-11 14:30:34','Domino',5,'.aura 61715','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3476118296)'),(335,'2016-05-11 14:30:34','Domino',5,'.mod sp 8','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516574248)'),(336,'2016-05-11 14:30:34','Domino',5,'.mod scale 1','X: -437.402039 Y: 1950.614014 Z: 213.910492 Map: 631','Mew (GUID: 3516574648)'),(337,'2016-05-11 14:30:35','Domino',5,'.aura 65126','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516574728)'),(338,'2016-05-11 14:30:35','Domino',5,'.aura 72525','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516574808)'),(339,'2016-05-11 14:30:35','Domino',5,'.aura 64112','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575208)'),(340,'2016-05-11 14:30:35','Domino',5,'.aura 47008','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575288)'),(341,'2016-05-11 14:30:35','Domino',5,'.aura 40733','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575368)'),(342,'2016-05-11 14:30:35','Domino',5,'.aura 64238','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575448)'),(343,'2016-05-11 14:30:35','Domino',5,'.aura 68378','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575528)'),(344,'2016-05-11 14:30:35','Domino',5,'.aura 61715','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3516575608)'),(345,'2016-05-11 14:30:35','Domino',5,'.mod sp 8','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3517382776)'),(346,'2016-05-11 14:30:35','Domino',5,'.mod scale 1','X: -437.167419 Y: 1949.665039 Z: 214.184753 Map: 631',' (GUID: 3517382856)'),(347,'2016-05-11 14:30:48','Mew',1,'.app','X: -526.095581 Y: 1995.543457 Z: 188.108322 Map: 631','Domino (GUID: 3553829384)'),(348,'2016-05-11 14:30:50','Domino',5,'.np i','X: -460.381836 Y: 1919.988647 Z: 215.560089 Map: 631','High Overlord Saurfang (GUID: 3477317272)'),(349,'2016-05-11 14:32:03','Domino',5,'.aura 65126','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(350,'2016-05-11 14:32:03','Domino',5,'.aura 72525','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(351,'2016-05-11 14:32:03','Domino',5,'.aura 64112','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(352,'2016-05-11 14:32:03','Domino',5,'.aura 47008','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(353,'2016-05-11 14:32:03','Domino',5,'.aura 40733','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(354,'2016-05-11 14:32:03','Domino',5,'.aura 64238','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(355,'2016-05-11 14:32:03','Domino',5,'.aura 68378','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(356,'2016-05-11 14:32:03','Domino',5,'.aura 61715','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(357,'2016-05-11 14:32:03','Domino',5,'.mod sp 8','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(358,'2016-05-11 14:32:03','Domino',5,'.mod scale 1','X: -432.604919 Y: 2414.587158 Z: 471.599701 Map: 631','Horde Gunship Cannon (GUID: 3477317368)'),(359,'2016-05-11 14:33:30','Domino',5,'.mod sc 1','X: -532.520508 Y: 2208.544922 Z: 539.291504 Map: 631','Mew (GUID: 3517382856)'),(360,'2016-05-11 14:39:30','Mew',1,'.te kocka','X: 4521.957520 Y: 2769.864502 Z: 351.101349 Map: 631',' (GUID: 3516574408)'),(361,'2016-05-11 14:39:40','Mew',1,'.gm on','X: 16226.099609 Y: 16403.400391 Z: -64.378899 Map: 1','Niby the Almighty (GUID: 3594140824)'),(362,'2016-05-11 14:39:43','Mew',1,'.unaura all','X: 16226.099609 Y: 16403.400391 Z: -64.378899 Map: 1','Mew (GUID: 3517385736)'),(363,'2016-05-11 14:39:46','Mew',1,'.mod sc 1','X: 16230.025391 Y: 16403.261719 Z: -64.379074 Map: 1','Mew (GUID: 3517385816)'),(364,'2016-05-11 14:39:48','Mew',1,'.reset stats','X: 16230.025391 Y: 16403.261719 Z: -64.379074 Map: 1','Mew (GUID: 3517385896)'),(365,'2016-05-11 14:45:09','Domino',5,'.te gmi','X: 4217.059570 Y: 2766.643555 Z: 353.096375 Map: 631',' (GUID: 3516572808)'),(366,'2016-05-11 14:45:32','Domino',5,'.gm fly off','X: 16227.351562 Y: 16258.791016 Z: 13.364380 Map: 1',' (GUID: 3475380696)'),(367,'2016-05-11 14:45:34','Domino',5,'.mod sp 1','X: 16222.185547 Y: 16262.162109 Z: 13.283236 Map: 1',' (GUID: 3475380616)'),(368,'2016-05-11 14:59:14','Domino',5,'.gm i','X: 16217.325195 Y: 16265.333008 Z: 13.421409 Map: 1',' (GUID: 3516572968)'),(369,'2016-05-11 14:59:18','Domino',5,'.gm on','X: 16207.980469 Y: 16276.013672 Z: 12.275187 Map: 1',' (GUID: 3516573048)'),(370,'2016-05-11 14:59:19','Domino',5,'.gm i','X: 16210.265625 Y: 16271.147461 Z: 12.953422 Map: 1',' (GUID: 3516573128)'),(371,'2016-05-11 14:59:39','Domino',5,'.name caf','X: 16225.162109 Y: 16252.323242 Z: 12.725519 Map: 1',' (GUID: 3516573288)'),(372,'2016-05-11 15:00:51','Domino',5,'.mod sc 1','X: 16230.527344 Y: 16307.031250 Z: 20.860222 Map: 1',' (GUID: 3516573208)'),(373,'2016-05-11 15:00:57','Domino',5,'.mod sc 1','X: 16225.795898 Y: 16276.424805 Z: 20.915016 Map: 1',' (GUID: 3516573608)'),(374,'2016-05-11 20:26:48','Domino',5,'.te gmk','X: 16224.977539 Y: 16266.730469 Z: 13.176785 Map: 1',' (GUID: 947385400)'),(375,'2016-05-11 20:26:57','Mew',1,'.npc add 200204','X: 16232.984375 Y: 16403.138672 Z: -64.378746 Map: 1',' (GUID: 947385560)'),(376,'2016-05-11 20:28:03','Mew',1,'.npc add 200204','X: 16233.871094 Y: 16403.041016 Z: -64.378746 Map: 1','GM OUTFIT (GUID: 947405624)'),(377,'2016-05-11 20:28:06','Mew',1,'.npc del','X: 16233.871094 Y: 16403.041016 Z: -64.378746 Map: 1',' (GUID: 947405720)'),(378,'2016-05-11 20:54:59','Mew',1,'.gm on','X: 16232.174805 Y: 16404.957031 Z: -64.379616 Map: 1','GM OUTFIT (GUID: 2817643896)'),(379,'2016-05-11 20:55:10','Mew',1,'.dev off','X: 16232.174805 Y: 16404.957031 Z: -64.378563 Map: 1','GM OUTFIT (GUID: 2817644952)'),(380,'2016-05-11 20:55:37','Mew',1,'.dev off','X: 16233.451172 Y: 16407.218750 Z: -64.378143 Map: 1','GM OUTFIT (GUID: 2817645144)'),(381,'2016-05-11 20:57:00','Domino',5,'.gm off','X: 16231.831055 Y: 16403.259766 Z: -64.378647 Map: 1','GM OUTFIT (GUID: 2817644088)'),(382,'2016-05-11 20:57:04','Domino',5,'.gm on','X: 16231.831055 Y: 16403.259766 Z: -64.378479 Map: 1','GM OUTFIT (GUID: 2817644088)'),(383,'2016-05-11 21:01:04','Domino',5,'.gm on','X: 16230.822266 Y: 16403.121094 Z: -64.378792 Map: 1','GM OUTFIT (GUID: 2817643704)'),(384,'2016-05-11 21:03:05','Mew',1,'.te gmk','X: 4278.557129 Y: -2773.553955 Z: 7.542201 Map: 0',' (GUID: 2817572984)'),(385,'2016-05-11 21:05:45','Domino',5,'.gm off','X: 16226.894531 Y: 16401.791016 Z: -64.379944 Map: 1',' (GUID: 2817641000)'),(386,'2016-05-11 21:10:00','Mew',1,'.server restart 3 Yolo:)','X: 16231.390625 Y: 16403.361328 Z: -64.378532 Map: 1','GM OUTFIT (GUID: 2817643224)'),(387,'2016-05-11 21:32:51','Mew',1,'.gm off','X: 16231.154297 Y: 16402.398438 Z: -64.378860 Map: 1','GM OUTFIT (GUID: 4171987864)'),(388,'2016-05-11 21:32:56','Mew',1,'.gm on','X: 16230.220703 Y: 16401.763672 Z: -64.378860 Map: 1','GM OUTFIT (GUID: 4171987864)'),(389,'2016-05-11 21:35:12','Mew',1,'.gm off','X: 16231.558594 Y: 16406.318359 Z: -64.378860 Map: 1','GM OUTFIT (GUID: 3353976568)'),(390,'2016-05-11 21:36:52','Mew',1,'.gm on','X: 16231.945312 Y: 16405.347656 Z: -64.378860 Map: 1','GM OUTFIT (GUID: 3354293144)'),(391,'2016-05-11 21:43:04','Mew',1,'.lo spell staves','X: 16231.041992 Y: 16400.712891 Z: -64.378609 Map: 1',' (GUID: 3354404216)'),(392,'2016-05-11 21:44:52','Mew',1,'.add 29981','X: 16231.759766 Y: 16402.730469 Z: -64.378609 Map: 1','GM OUTFIT (GUID: 3354293432)'),(393,'2016-05-11 21:50:51','Mew',1,'.gm off','X: 16230.425781 Y: 16403.078125 Z: -64.379486 Map: 1','GM OUTFIT (GUID: 3354295160)'),(394,'2016-05-11 21:50:57','Mew',1,'.gm on','X: 16230.425781 Y: 16403.078125 Z: -64.379486 Map: 1','GM OUTFIT (GUID: 3354295160)'),(395,'2016-05-11 21:56:35','Alder',1,'.te gmk','X: -8949.950195 Y: -132.492996 Z: 83.531197 Map: 0',' (GUID: 3303651480)'),(396,'2016-05-11 21:56:41','Alder',1,'.gm on','X: 16231.174805 Y: 16402.136719 Z: -64.378899 Map: 1','GM OUTFIT (GUID: 3297867064)'),(397,'2016-05-11 21:57:15','Alder',1,'.add 29981','X: 16230.755859 Y: 16401.890625 Z: -64.378548 Map: 1','GM OUTFIT (GUID: 3297867160)'),(398,'2016-05-11 21:57:21','Alder',1,'.levelup 80','X: 16230.755859 Y: 16401.890625 Z: -64.378548 Map: 1','Alder (GUID: 3303652200)'),(399,'2016-05-11 21:59:12','Andra',1,'.te gmk','X: -8949.950195 Y: -132.492996 Z: 83.531197 Map: 0',' (GUID: 3303651480)'),(400,'2016-05-11 21:59:16','Andra',1,'.levelup 79','X: 16226.099609 Y: 16403.400391 Z: -64.378899 Map: 1',' (GUID: 3303652520)'),(401,'2016-05-11 21:59:25','Andra',1,'.gm on','X: 16230.890625 Y: 16401.279297 Z: -64.379539 Map: 1','GM OUTFIT (GUID: 3297985176)'),(402,'2016-05-11 21:59:50','Andra',1,'.add 29981','X: 16232.233398 Y: 16401.898438 Z: -64.379539 Map: 1','GM OUTFIT (GUID: 3297867160)'),(403,'2016-05-11 22:11:13','Mew',1,'.te goldshire','X: 16234.393555 Y: 16411.521484 Z: -64.379875 Map: 1',' (GUID: 3297957960)'),(404,'2016-05-11 22:11:26','Mew',1,'.np i','X: -9489.430664 Y: 72.457733 Z: 56.094177 Map: 0','Remy \"Two Times\" (GUID: 3313113400)'),(405,'2016-05-11 22:11:47','Mew',1,'.gm off','X: -9489.430664 Y: 72.457733 Z: 56.094120 Map: 0','Remy \"Two Times\" (GUID: 3313113976)'),(406,'2016-05-11 22:11:53','Mew',1,'.rev','X: -9494.487305 Y: 72.642708 Z: 56.239231 Map: 0','Remy \"Two Times\" (GUID: 3313116088)'),(407,'2016-05-11 22:11:57','Mew',1,'.rev','X: -9492.738281 Y: 73.255981 Z: 56.103191 Map: 0','Remy \"Two Times\" (GUID: 3313116088)'),(408,'2016-05-11 22:11:58','Mew',1,'.resp','X: -9492.738281 Y: 73.255981 Z: 56.103191 Map: 0','Remy \"Two Times\" (GUID: 3313116184)'),(409,'2016-05-11 22:12:13','Mew',1,'.te icecrown','X: -9487.205078 Y: 68.399841 Z: 56.151897 Map: 0',' (GUID: 3354405736)'),(410,'2016-05-11 22:12:23','Mew',1,'.npc i','X: 7254.607422 Y: 1651.400513 Z: 433.888611 Map: 571','Ymirheim Defender (GUID: 3274220248)'),(411,'2016-05-11 22:12:53','Mew',1,'.mod hp 9999999999','X: 7245.233398 Y: 1674.422729 Z: 446.023407 Map: 571','Mew (GUID: 3274735736)'),(412,'2016-05-11 22:13:10','Mew',1,'.damage 1000','X: 7250.060059 Y: 1665.798950 Z: 438.363983 Map: 571','Ymirheim Defender (GUID: 3275105304)'),(413,'2016-05-11 22:13:13','Mew',1,'.damage 10000','X: 7250.060059 Y: 1665.798950 Z: 438.363983 Map: 571','Ymirheim Defender (GUID: 3275105304)'),(414,'2016-05-11 22:13:35','Mew',1,'.learn 41276','X: 7249.949219 Y: 1664.973877 Z: 438.081604 Map: 571','Mew (GUID: 3274736136)'),(415,'2016-05-11 22:14:17','Mew',1,'.te gmk','X: 7249.214355 Y: 1661.776611 Z: 437.056213 Map: 571',' (GUID: 3274736216)'),(416,'2016-05-11 22:14:21','Mew',1,'.gm on','X: 16226.099609 Y: 16403.400391 Z: -64.378899 Map: 1','Niby the Almighty (GUID: 3275105304)'),(417,'2016-05-11 23:56:35','Mew',1,'.aura 8696','X: 16235.741211 Y: 16387.542969 Z: -64.379593 Map: 1',' (GUID: 4210496488)'),(418,'2016-05-12 00:00:56','Mew',1,'.dev on','X: 16231.045898 Y: 16410.509766 Z: -64.379868 Map: 1',' (GUID: 4258431096)'),(419,'2016-05-12 00:01:21','Mew',1,'.gm fly on','X: 16228.858398 Y: 16401.257812 Z: -64.375008 Map: 1',' (GUID: 4210496568)'),(420,'2016-05-12 00:01:25','Mew',1,'.gm off','X: 16222.514648 Y: 16405.460938 Z: -57.695183 Map: 1',' (GUID: 4210497288)'),(421,'2016-05-12 00:01:33','Mew',1,'.gm fly off','X: 16229.984375 Y: 16388.525391 Z: -56.210258 Map: 1',' (GUID: 4210497368)'),(422,'2016-05-12 00:01:36','Mew',1,'.gm on','X: 16229.711914 Y: 16390.179688 Z: -64.378639 Map: 1',' (GUID: 4210497528)'),(423,'2016-05-12 00:12:14','Mew',1,'.server restart 3 Yolo:)','X: 16234.428711 Y: 16392.597656 Z: -64.378639 Map: 1',' (GUID: 4210497528)'),(424,'2016-05-12 00:41:46','Mew',1,'.gm on','X: 16233.461914 Y: 16402.109375 Z: -64.378677 Map: 1','GM OUTFIT (GUID: 728727192)'),(425,'2016-05-12 00:54:04','Mew',1,'.char level 1','X: 16233.222656 Y: 16402.382812 Z: -64.378677 Map: 1','Mew (GUID: 728736824)'),(426,'2016-05-12 01:15:43','Mew',1,'.gm off','X: 16231.304688 Y: 16406.361328 Z: -64.377762 Map: 1','Mew (GUID: 728737384)'),(427,'2016-05-12 01:17:58','Mew',1,'.gm on','X: 16231.393555 Y: 16407.408203 Z: -64.379959 Map: 1','Mew (GUID: 728737384)'),(428,'2016-05-12 01:19:56','Mew',1,'.gm off','X: 16234.261719 Y: 16413.312500 Z: -64.378769 Map: 1',' (GUID: 728737544)'),(429,'2016-05-12 01:20:01','Mew',1,'.gm on','X: 16234.261719 Y: 16413.312500 Z: -64.378769 Map: 1',' (GUID: 728738264)'),(430,'2016-05-12 12:41:57','Domino',5,'.gm on','X: 16236.981445 Y: 16406.380859 Z: -64.378830 Map: 1','GM OUTFIT (GUID: 706409400)'),(431,'2016-05-12 12:42:48','Domino',5,'.server restart 0','X: 16225.431641 Y: 16403.296875 Z: -64.378403 Map: 1','GM OUTFIT (GUID: 706409496)'),(432,'2016-05-12 12:44:14','Domino',5,'.addmark','X: 16224.238281 Y: 16410.085938 Z: -64.380112 Map: 1',' (GUID: 2654125144)'),(433,'2016-05-12 12:44:16','Domino',5,'.addmark 5','X: 16229.730469 Y: 16412.980469 Z: -64.368851 Map: 1',' (GUID: 2654125224)'),(434,'2016-05-12 12:46:16','Domino',5,'.account','X: 16221.522461 Y: 16405.097656 Z: -64.377792 Map: 1',' (GUID: 2654125624)'),(435,'2016-05-12 12:47:58','Domino',5,'.acc','X: 16215.185547 Y: 16403.466797 Z: -64.377548 Map: 1',' (GUID: 2654125464)'),(436,'2016-05-12 12:48:06','Domino',5,'.acc','X: 16228.732422 Y: 16403.382812 Z: -63.140381 Map: 1',' (GUID: 2654125704)'),(437,'2016-05-12 12:48:17','Domino',5,'.addmark','X: 16226.204102 Y: 16397.146484 Z: -64.377319 Map: 1',' (GUID: 2654125784)'),(438,'2016-05-12 12:48:18','Domino',5,'.addmark','X: 16227.045898 Y: 16402.992188 Z: -64.378479 Map: 1',' (GUID: 2654125864)'),(439,'2016-05-12 12:48:36','Domino',5,'.name acc','X: 16235.363281 Y: 16396.101562 Z: -64.378899 Map: 1',' (GUID: 2654125944)'),(440,'2016-05-12 13:09:01','Domino',5,'.acc','X: 16230.275391 Y: 16401.396484 Z: -64.378128 Map: 1',' (GUID: 2698149592)'),(441,'2016-05-12 13:09:22','Domino',5,'.npc mov','X: 16225.166016 Y: 16384.630859 Z: -64.378838 Map: 1','Ashen Rings (GUID: 2653865048)'),(442,'2016-05-12 13:09:25','Domino',5,'.te gmi','X: 16231.291016 Y: 16394.757812 Z: -64.378731 Map: 1',' (GUID: 2866725064)'),(443,'2016-05-12 14:11:20','Domino',5,'.acc','X: 16203.926758 Y: 16275.193359 Z: 11.635524 Map: 1',' (GUID: 2866725224)'),(444,'2016-05-12 14:13:36','Domino',5,'.aura 65126','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2698148072)'),(445,'2016-05-12 14:13:36','Domino',5,'.aura 72525','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2772368392)'),(446,'2016-05-12 14:13:36','Domino',5,'.aura 64112','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725064)'),(447,'2016-05-12 14:13:36','Domino',5,'.aura 47008','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725384)'),(448,'2016-05-12 14:13:36','Domino',5,'.aura 40733','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725544)'),(449,'2016-05-12 14:13:36','Domino',5,'.aura 64238','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725704)'),(450,'2016-05-12 14:13:36','Domino',5,'.aura 68378','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725784)'),(451,'2016-05-12 14:13:36','Domino',5,'.aura 61715','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725864)'),(452,'2016-05-12 14:13:36','Domino',5,'.mod sp 8','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866725944)'),(453,'2016-05-12 14:13:36','Domino',5,'.mod scale 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726024)'),(454,'2016-05-12 14:14:51','Domino',5,'.aura 65126','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726104)'),(455,'2016-05-12 14:14:51','Domino',5,'.aura 72525','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726184)'),(456,'2016-05-12 14:14:51','Domino',5,'.aura 64112','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726264)'),(457,'2016-05-12 14:14:51','Domino',5,'.aura 47008','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726344)'),(458,'2016-05-12 14:14:51','Domino',5,'.aura 40733','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726424)'),(459,'2016-05-12 14:14:51','Domino',5,'.aura 64238','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726504)'),(460,'2016-05-12 14:14:51','Domino',5,'.aura 68378','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726584)'),(461,'2016-05-12 14:14:51','Domino',5,'.aura 61715','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726664)'),(462,'2016-05-12 14:14:51','Domino',5,'.mod sp 8','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726744)'),(463,'2016-05-12 14:14:51','Domino',5,'.mod scale 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726824)'),(464,'2016-05-12 14:14:51','Domino',5,'.unaura 65126','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726904)'),(465,'2016-05-12 14:14:51','Domino',5,'.unaura 72525','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866726984)'),(466,'2016-05-12 14:14:51','Domino',5,'.unaura 64112','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2654125064)'),(467,'2016-05-12 14:14:51','Domino',5,'.unaura 47008','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2654125144)'),(468,'2016-05-12 14:14:51','Domino',5,'.unaura 40733','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727064)'),(469,'2016-05-12 14:14:51','Domino',5,'.unaura 64238','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727144)'),(470,'2016-05-12 14:14:51','Domino',5,'.unaura 68378','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727224)'),(471,'2016-05-12 14:14:51','Domino',5,'.unaura 61715','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727304)'),(472,'2016-05-12 14:14:51','Domino',5,'.mod sp 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727384)'),(473,'2016-05-12 14:14:51','Domino',5,'.mod scale 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727464)'),(474,'2016-05-12 14:14:54','Domino',5,'.unaura 65126','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727544)'),(475,'2016-05-12 14:14:54','Domino',5,'.unaura 72525','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727624)'),(476,'2016-05-12 14:14:54','Domino',5,'.unaura 64112','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727704)'),(477,'2016-05-12 14:14:54','Domino',5,'.unaura 47008','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727784)'),(478,'2016-05-12 14:14:54','Domino',5,'.unaura 40733','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727864)'),(479,'2016-05-12 14:14:54','Domino',5,'.unaura 64238','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866727944)'),(480,'2016-05-12 14:14:54','Domino',5,'.unaura 68378','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866728024)'),(481,'2016-05-12 14:14:54','Domino',5,'.unaura 61715','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866728104)'),(482,'2016-05-12 14:14:54','Domino',5,'.mod sp 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866728184)'),(483,'2016-05-12 14:14:54','Domino',5,'.mod scale 1','X: 16203.926758 Y: 16275.193359 Z: 11.635060 Map: 1',' (GUID: 2866728264)'),(484,'2016-05-12 14:15:44','Domino',5,'.te gmk','X: 16219.926758 Y: 16259.000977 Z: 13.280533 Map: 1','Season Rewards (GUID: 2653334008)'),(485,'2016-05-12 14:16:55','Domino',5,'.lo spell echo of','X: 16229.283203 Y: 16405.250000 Z: -64.378494 Map: 1',' (GUID: 2866728824)'),(486,'2016-05-12 14:21:44','Domino',5,'.gm i','X: 16218.854492 Y: 16402.923828 Z: -64.379028 Map: 1',' (GUID: 2654125464)'),(487,'2016-05-12 14:24:23','Domino',5,'.server info','X: 16231.645508 Y: 16401.134766 Z: -64.376518 Map: 1',' (GUID: 2654125464)'),(488,'2016-05-12 14:27:28','Domino',5,'.reload config','X: 16237.289062 Y: 16408.974609 Z: -64.376648 Map: 1',' (GUID: 3542010408)'),(489,'2016-05-12 14:33:42','Domino',5,'.aura 65126','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161352)'),(490,'2016-05-12 14:33:42','Domino',5,'.aura 72525','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161432)'),(491,'2016-05-12 14:33:42','Domino',5,'.aura 64112','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161512)'),(492,'2016-05-12 14:33:42','Domino',5,'.aura 47008','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161592)'),(493,'2016-05-12 14:33:42','Domino',5,'.aura 40733','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161672)'),(494,'2016-05-12 14:33:42','Domino',5,'.aura 64238','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161752)'),(495,'2016-05-12 14:33:42','Domino',5,'.aura 68378','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161832)'),(496,'2016-05-12 14:33:42','Domino',5,'.aura 61715','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161912)'),(497,'2016-05-12 14:33:42','Domino',5,'.mod sp 8','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540161992)'),(498,'2016-05-12 14:33:42','Domino',5,'.mod scale 1','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540162072)'),(499,'2016-05-12 14:33:50','Domino',5,'.mod sc 1','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540162152)'),(500,'2016-05-12 14:33:53','Domino',5,'.mod sc 1','X: 3604.435303 Y: 191.918961 Z: -110.682007 Map: 571',' (GUID: 3540162232)'),(501,'2016-05-12 14:34:01','Domino',5,'.gm off','X: 3241.859131 Y: 530.914734 Z: 87.501106 Map: 724','Orb Rotation Focus (GUID: 2622278520)'),(502,'2016-05-12 14:34:06','Domino',5,'.die','X: 3199.088623 Y: 539.286804 Z: 73.442474 Map: 724','Saviana Ragefire (GUID: 2622278808)'),(503,'2016-05-12 14:34:09','Domino',5,'.die','X: 3191.887939 Y: 529.572205 Z: 73.081879 Map: 724','Baltharus the Warborn (GUID: 2622279096)'),(504,'2016-05-12 14:34:11','Domino',5,'.die','X: 3173.793945 Y: 528.079407 Z: 72.889282 Map: 724','General Zarithrian (GUID: 2622279864)'),(505,'2016-05-12 14:34:49','Domino',5,'.aura 65126','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540162712)'),(506,'2016-05-12 14:34:49','Domino',5,'.aura 72525','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540162792)'),(507,'2016-05-12 14:34:49','Domino',5,'.aura 64112','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540162872)'),(508,'2016-05-12 14:34:49','Domino',5,'.aura 47008','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540162952)'),(509,'2016-05-12 14:34:49','Domino',5,'.aura 40733','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 2653405128)'),(510,'2016-05-12 14:34:49','Domino',5,'.aura 64238','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540163032)'),(511,'2016-05-12 14:34:49','Domino',5,'.aura 68378','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540163112)'),(512,'2016-05-12 14:34:49','Domino',5,'.aura 61715','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540163192)'),(513,'2016-05-12 14:34:49','Domino',5,'.mod sp 8','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540163272)'),(514,'2016-05-12 14:34:49','Domino',5,'.mod scale 1','X: 3142.808594 Y: 530.414307 Z: 72.886719 Map: 724','Domino (GUID: 3540163352)'),(515,'2016-05-12 14:36:51','Domino',5,'.gm on','X: 3169.663818 Y: 540.562805 Z: 72.889000 Map: 724',' (GUID: 3540162632)'),(516,'2016-05-12 14:37:36','Domino',5,'.gm off','X: 3170.927002 Y: 526.248962 Z: 72.888901 Map: 724','Halion (GUID: 2622279192)'),(517,'2016-05-12 14:37:51','Domino',5,'.gm on','X: 3170.048096 Y: 529.042480 Z: 72.889061 Map: 724',' (GUID: 3540163752)'),(518,'2016-05-12 14:44:46','Domino',5,'.reload autobroadcast','X: 3587.162109 Y: 215.150482 Z: -120.053917 Map: 571',' (GUID: 2604966248)'),(519,'2016-05-12 14:45:14','Domino',5,'.nameann name','X: 3589.314697 Y: 243.652451 Z: -120.046501 Map: 571',' (GUID: 2698148232)'),(520,'2016-05-12 14:45:26','Domino',5,'.mod sp 1','X: 3594.071289 Y: 255.602295 Z: -120.090714 Map: 571',' (GUID: 2698148312)'),(521,'2016-05-12 14:47:11','Domino',5,'.pinfo test','X: 3598.364258 Y: 266.442688 Z: -118.952393 Map: 571',' (GUID: 2604967768)'),(522,'2016-05-12 14:47:20','Domino',5,'.pinfo test','X: 3598.364258 Y: 266.442688 Z: -118.952415 Map: 571',' (GUID: 2604968888)'),(523,'2016-05-12 14:47:21','Domino',5,'.pinfo tests','X: 3598.364258 Y: 266.442688 Z: -118.952415 Map: 571',' (GUID: 2604968408)'),(524,'2016-05-12 15:19:38','Domino',5,'.mod sc 1','X: 3598.364258 Y: 266.441681 Z: -118.953957 Map: 571',' (GUID: 2335787800)'),(525,'2016-05-12 15:19:41','Domino',5,'.mod sc 1','X: 3598.364258 Y: 266.441681 Z: -118.953957 Map: 571',' (GUID: 2335787880)'),(526,'2016-05-12 15:19:43','Domino',5,'.reload rbac','X: 3598.364258 Y: 266.441681 Z: -118.953957 Map: 571',' (GUID: 2335787960)'),(527,'2016-05-12 15:19:49','Domino',5,'.unaura 65126','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788040)'),(528,'2016-05-12 15:19:49','Domino',5,'.unaura 72525','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788120)'),(529,'2016-05-12 15:19:49','Domino',5,'.unaura 64112','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788200)'),(530,'2016-05-12 15:19:49','Domino',5,'.unaura 47008','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788280)'),(531,'2016-05-12 15:19:49','Domino',5,'.unaura 40733','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788360)'),(532,'2016-05-12 15:19:49','Domino',5,'.unaura 64238','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788440)'),(533,'2016-05-12 15:19:49','Domino',5,'.unaura 68378','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788520)'),(534,'2016-05-12 15:19:49','Domino',5,'.unaura 61715','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788600)'),(535,'2016-05-12 15:19:49','Domino',5,'.mod sp 1','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788680)'),(536,'2016-05-12 15:19:49','Domino',5,'.mod scale 1','X: 3587.769287 Y: 271.608154 Z: -115.678139 Map: 571',' (GUID: 2335788760)'),(537,'2016-05-12 15:19:54','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2335788840)'),(538,'2016-05-12 15:19:55','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2335788920)'),(539,'2016-05-12 15:19:55','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2335789000)'),(540,'2016-05-12 15:19:56','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2314821752)'),(541,'2016-05-12 15:19:57','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2314821832)'),(542,'2016-05-12 15:19:57','Domino',5,'.reload rbac','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2314821912)'),(543,'2016-05-12 15:20:00','Domino',5,'.acc','X: 3574.935791 Y: 277.885803 Z: -114.035446 Map: 571',' (GUID: 2314821992)'),(544,'2016-05-12 15:21:33','Domino',5,'.reload rbac','X: 3582.891846 Y: 264.849365 Z: -112.622826 Map: 571',' (GUID: 2314824232)'),(545,'2016-05-12 15:22:23','Domino',5,'.reload rbac','X: 3582.891846 Y: 264.849365 Z: -112.622826 Map: 571',' (GUID: 2314824312)'),(546,'2016-05-12 15:22:25','Domino',5,'.acc','X: 3582.891846 Y: 264.849365 Z: -112.622826 Map: 571',' (GUID: 2314824392)'),(547,'2016-05-12 15:32:52','Mew',1,'.addmark 10','X: 16223.787109 Y: 16392.076172 Z: -64.376945 Map: 1','Lieutenant Tristia (GUID: 2314336408)'),(548,'2016-05-12 15:34:15','Mew',1,'.lo i 20bag','X: 16231.015625 Y: 16298.625000 Z: 20.851711 Map: 1',' (GUID: 2314823912)'),(549,'2016-05-12 15:34:18','Mew',1,'.lo i 20','X: 16231.015625 Y: 16298.625000 Z: 20.851711 Map: 1',' (GUID: 2314824632)'),(550,'2016-05-12 15:34:23','Mew',1,'.add 1977','X: 16231.015625 Y: 16298.625000 Z: 20.851711 Map: 1',' (GUID: 2314824792)'),(551,'2016-05-12 15:35:24','Mew',1,'.add 1977','X: 16226.506836 Y: 16266.287109 Z: 13.212278 Map: 1',' (GUID: 2314825512)'),(552,'2016-05-12 15:35:25','Mew',1,'.add 1977','X: 16226.506836 Y: 16266.287109 Z: 13.212278 Map: 1',' (GUID: 2314825672)'),(553,'2016-05-12 15:35:26','Mew',1,'.add 1977','X: 16226.506836 Y: 16266.287109 Z: 13.212278 Map: 1',' (GUID: 2332827848)'),(554,'2016-05-12 15:35:26','Mew',1,'.add 1977','X: 16226.506836 Y: 16266.287109 Z: 13.212278 Map: 1',' (GUID: 2332828008)'),(555,'2016-05-12 15:36:23','Domino',5,'.learn  |cff71d5ff|Hspell:5176|h[Wrath]|h|r','X: 16223.373047 Y: 16265.346680 Z: 13.214744 Map: 1','Mew (GUID: 2332829208)'),(556,'2016-05-12 15:40:27','Livery',1,'.te gmi','X: 1669.656006 Y: 1679.146606 Z: 121.024551 Map: 0',' (GUID: 2332827848)'),(557,'2016-05-12 15:43:03','Domino',5,'.guild invite Livery \"Asterion GM Tým\"','X: 16222.074219 Y: 16269.865234 Z: 13.037814 Map: 1','Livery (GUID: 2332828248)'),(558,'2016-05-12 15:45:04','Domino',5,'.reload config','X: 16222.276367 Y: 16269.535156 Z: 13.051769 Map: 1','Livery (GUID: 3252603432)'),(559,'2016-05-12 15:45:36','Livery',1,'.rev','X: 16263.416992 Y: 16247.801758 Z: 24.222313 Map: 1','Livery (GUID: 2301673992)'),(560,'2016-05-12 15:45:47','Domino',5,'.summ','X: 16242.614258 Y: 16251.036133 Z: 18.256897 Map: 1','Livery (GUID: 2301677112)'),(561,'2016-05-12 15:45:48','Domino',5,'.rev','X: 16242.614258 Y: 16251.036133 Z: 18.256897 Map: 1','Livery (GUID: 2301676072)'),(562,'2016-05-12 15:45:50','Domino',5,'.freeze','X: 16242.614258 Y: 16251.036133 Z: 18.256897 Map: 1','Livery (GUID: 2301676152)'),(563,'2016-05-12 15:45:53','Domino',5,'.unfreeze','X: 16242.614258 Y: 16251.036133 Z: 18.256897 Map: 1','Livery (GUID: 2314824952)'),(564,'2016-05-12 15:46:30','Domino',5,'.freeze','X: 16227.675781 Y: 16257.254883 Z: 13.325463 Map: 1','Livery (GUID: 2314823512)'),(565,'2016-05-12 15:46:33','Domino',5,'.unfreeze','X: 16227.675781 Y: 16257.254883 Z: 13.325463 Map: 1','Livery (GUID: 2314821752)'),(566,'2016-05-12 15:50:25','Cybroon',5,'.te gmk','X: 10327.673828 Y: 817.850769 Z: 1326.807251 Map: 1',' (GUID: 3250756856)'),(567,'2016-05-12 15:50:35','Livery',1,'.te gmk','X: 16206.492188 Y: 16253.912109 Z: 19.205839 Map: 1',' (GUID: 3250756936)'),(568,'2016-05-12 15:50:42','Livery',1,'.rev','X: 16225.407227 Y: 16403.425781 Z: -64.378899 Map: 1',' (GUID: 3250757576)'),(569,'2016-05-12 15:50:45','Cybroon',5,'.freeze','X: 16229.417969 Y: 16409.642578 Z: -64.379326 Map: 1','Livery (GUID: 3251089528)'),(570,'2016-05-12 15:50:50','Cybroon',5,'.unfreeze','X: 16227.691406 Y: 16410.767578 Z: -64.377007 Map: 1','Livery (GUID: 3251089608)'),(571,'2016-05-12 15:50:56','Livery',1,'.gm on','X: 16235.673828 Y: 16423.037109 Z: -64.379471 Map: 1',' (GUID: 2301676232)'),(572,'2016-05-12 15:51:30','Cybroon',5,'.add  |cffa335ee|Hitem:51146:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Trousers]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2373835448)'),(573,'2016-05-12 15:51:32','Cybroon',5,'.add  |cffa335ee|Hitem:51145:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Vestment]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2373834488)'),(574,'2016-05-12 15:52:27','Cybroon',5,'.add |cffa335ee|Hitem:51304:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Pauldrons]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2312697688)'),(575,'2016-05-12 15:53:00','Cybroon',5,'.add  |cffa335ee|Hitem:51296:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Headguard]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 3108164504)'),(576,'2016-05-12 15:53:13','Cybroon',5,'.add  |cffa335ee|Hitem:51291:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Gloves]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 3108164504)'),(577,'2016-05-12 15:54:50','Cybroon',5,'.add  |cffa335ee|Hitem:51290:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Cover]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2288348600)'),(578,'2016-05-12 15:55:22','Cybroon',5,'.add  |cffa335ee|Hitem:51291:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Gloves]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2302656952)'),(579,'2016-05-12 15:55:25','Cybroon',5,'.add  |cffa335ee|Hitem:51295:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Handgrips]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2302656856)'),(580,'2016-05-12 15:55:38','Cybroon',5,'.add  |cffa335ee|Hitem:51297:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Legguards]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2288348120)'),(581,'2016-05-12 15:56:09','Cybroon',5,'.add  |cffa335ee|Hitem:51293:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Trousers]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2312699416)'),(582,'2016-05-12 15:56:24','Cybroon',5,'.add  |cffa335ee|Hitem:51299:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Shoulderpads]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2312701624)'),(583,'2016-05-12 15:56:33','Cybroon',5,'.add  |cffa335ee|Hitem:51292:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Mantle]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2302657624)'),(584,'2016-05-12 15:56:48','Cybroon',5,'.add  |cffa335ee|Hitem:51298:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Raiment]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2290525304)'),(585,'2016-05-12 15:57:03','Cybroon',5,'.add  |cffa335ee|Hitem:51294:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Vestment]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2290525400)'),(586,'2016-05-12 15:57:28','Cybroon',5,'.add  |cffa335ee|Hitem:51303:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Legplates]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2290525496)'),(587,'2016-05-12 15:57:31','Cybroon',5,'.add  |cffa335ee|Hitem:51304:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Pauldrons]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2320771992)'),(588,'2016-05-12 15:57:33','Cybroon',5,'.add  |cffa335ee|Hitem:51300:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Robes]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2320771896)'),(589,'2016-05-12 15:57:42','Cybroon',5,'.add  |cffa335ee|Hitem:51302:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Helmet]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2301016568)'),(590,'2016-05-12 15:57:50','Cybroon',5,'.add  |cffa335ee|Hitem:51301:0:0:0:0:0:0:0:1|h[Sanctified Lasherweave Gauntlets]|h|r','X: 16210.357422 Y: 16387.150391 Z: -64.379570 Map: 1','Alana Moonstrike (GUID: 2301016472)'),(591,'2016-05-12 15:58:35','Cybroon',5,'.add  |cffa335ee|Hitem:51421:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Kodohide Helm]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2301016568)'),(592,'2016-05-12 15:58:39','Cybroon',5,'.add   |cffa335ee|Hitem:51422:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Kodohide Legguards]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2301016568)'),(593,'2016-05-12 15:58:41','Cybroon',5,'.add  |cffa335ee|Hitem:51424:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Kodohide Spaulders]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2301016568)'),(594,'2016-05-12 15:58:55','Cybroon',5,'.add   |cffa335ee|Hitem:51419:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Kodohide Robes]|h|r?','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2288348120)'),(595,'2016-05-12 15:59:05','Cybroon',5,'.add  |cffa335ee|Hitem:51420:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Kodohide Gloves]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2288349464)'),(596,'2016-05-12 15:59:18','Cybroon',5,'.add  |cffa335ee|Hitem:51454:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Salvation]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Trapjaw Rix (GUID: 2288349464)'),(597,'2016-05-12 15:59:30','Cybroon',5,'.add  |cffa335ee|Hitem:51423:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Idol of Tenacity]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2288348120)'),(598,'2016-05-12 15:59:42','Cybroon',5,'.add  |cffa335ee|Hitem:51409:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Reprieve]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Nargle Lashcord (GUID: 2288349752)'),(599,'2016-05-12 16:00:18','Cybroon',5,'.add  |cffa335ee|Hitem:51377:0:0:0:0:0:0:0:1|h[Medallion of the Alliance]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Lieutenant Tristia (GUID: 2288348120)'),(600,'2016-05-12 16:00:25','Cybroon',5,'.add  |cffa335ee|Hitem:51341:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Boots of Salvation]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Lieutenant Tristia (GUID: 2288348120)'),(601,'2016-05-12 16:00:27','Cybroon',5,'.add  |cffa335ee|Hitem:51334:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Cloak of Ascendancy]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Lieutenant Tristia (GUID: 2288348120)'),(602,'2016-05-12 16:00:33','Cybroon',5,'.add  |cffa335ee|Hitem:51342:0:0:0:0:0:0:0:1|h[Wrathful Gladiator\'s Armwraps of Salvation]|h|r','X: 16228.006836 Y: 16385.191406 Z: -64.378860 Map: 1','Lieutenant Tristia (GUID: 2288348120)'),(603,'2016-05-12 16:00:55','Cybroon',5,'.lo i twilight','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2290452952)'),(604,'2016-05-12 16:00:57','Cybroon',5,'.lo i twilight scale','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2290453512)'),(605,'2016-05-12 16:01:14','Cybroon',5,'.add  |cffa335ee|Hitem:54588:0:0:0:0:0:0:0:1|h[Charred Twilight Scale]|h|r','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2290453592)'),(606,'2016-05-12 16:01:17','Cybroon',5,'.add  |cffa335ee|Hitem:54589:0:0:0:0:0:0:0:1|h[Glowing Twilight Scale]|h|r','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2312528392)'),(607,'2016-05-12 16:01:36','Cybroon',5,'.add  |cffa335ee|Hitem:54591:0:0:0:0:0:0:0:1|h[Petrified Twilight Scale]|h|r','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2301503080)'),(608,'2016-05-12 16:01:38','Cybroon',5,'.add  |cffa335ee|Hitem:54590:0:0:0:0:0:0:0:1|h[Sharpened Twilight Scale]|h|r','X: 16230.406250 Y: 16394.720703 Z: -64.378860 Map: 1',' (GUID: 2301503000)'),(609,'2016-05-12 16:02:12','Cybroon',5,'.lo i royal','X: 16231.580078 Y: 16400.927734 Z: -64.377068 Map: 1',' (GUID: 2290453752)'),(610,'2016-05-12 16:02:22','Sanka',1,'.char level 80','X: -8809.459961 Y: 622.391785 Z: 94.530014 Map: 0','Sanka (GUID: 2312528232)'),(611,'2016-05-12 16:02:23','Cybroon',5,'.add  |cffa335ee|Hitem:50734:0:0:0:0:0:0:0:1|h[Royal Scepter of Terenas II]|h|r','X: 16225.115234 Y: 16400.125000 Z: -64.377365 Map: 1',' (GUID: 2312528472)'),(612,'2016-05-12 16:04:14','Cybroon',5,'.char level 80','X: 16226.231445 Y: 16404.529297 Z: -64.378395 Map: 1',' (GUID: 2312528712)'),(613,'2016-05-12 16:06:14','Cybroon',5,'.lo crea lord thorval','X: 16235.213867 Y: 16419.585938 Z: -64.379890 Map: 1',' (GUID: 2312528472)'),(614,'2016-05-12 16:06:18','Cybroon',5,'.npc add 29196','X: 16235.213867 Y: 16419.585938 Z: -64.379890 Map: 1',' (GUID: 2312528712)'),(615,'2016-05-12 16:06:29','Sanka',1,'.te gmk','X: -8791.673828 Y: 639.522583 Z: 94.437767 Map: 0',' (GUID: 2312528232)'),(616,'2016-05-12 16:06:42','Cybroon',5,'.mod money 9999999','X: 16225.525391 Y: 16417.361328 Z: -64.378616 Map: 1','Cybroon (GUID: 2301504600)'),(617,'2016-05-12 16:06:49','Cybroon',5,'.mod money 9999999','X: 16225.525391 Y: 16417.361328 Z: -64.378540 Map: 1','Druid Trainer (GUID: 2301015704)'),(618,'2016-05-12 16:07:00','Sanka',1,'.mod money 214000000','X: 16236.507812 Y: 16417.835938 Z: -64.378380 Map: 1','Sanka (GUID: 2301504760)'),(619,'2016-05-12 16:07:03','Sanka',1,'.mod money 214000000000','X: 16236.507812 Y: 16417.835938 Z: -64.378380 Map: 1','Sanka (GUID: 2301504120)'),(620,'2016-05-12 16:07:05','Sanka',1,'.mod money 214000000000','X: 16236.507812 Y: 16417.835938 Z: -64.378380 Map: 1','Sanka (GUID: 2301504200)'),(621,'2016-05-12 16:07:07','Sanka',1,'.mod money 21400000000','X: 16236.507812 Y: 16417.835938 Z: -64.378380 Map: 1','Sanka (GUID: 2301504760)'),(622,'2016-05-12 16:07:09','Sanka',1,'.mod money 2140000000','X: 16236.507812 Y: 16417.835938 Z: -64.378380 Map: 1','Sanka (GUID: 2301504040)'),(623,'2016-05-12 16:07:18','Sanka',1,'.mod money 2140000000','X: 16235.353516 Y: 16418.269531 Z: -64.378380 Map: 1','Cybroon (GUID: 2312528072)'),(624,'2016-05-12 16:08:00','Cybroon',5,'.lo fac ashen','X: 16235.208984 Y: 16396.697266 Z: -64.378326 Map: 1',' (GUID: 2312528072)'),(625,'2016-05-12 16:08:04','Cybroon',5,'.mod rep 1156 80000','X: 16236.832031 Y: 16402.757812 Z: -64.378685 Map: 1',' (GUID: 2290453752)'),(626,'2016-05-12 16:08:08','Cybroon',5,'.mod rep 1156 80000','X: 16230.673828 Y: 16404.654297 Z: -64.379173 Map: 1','Sanka (GUID: 2312529352)'),(627,'2016-05-12 16:08:35','Cybroon',5,'.lo i ashen band','X: 16223.926758 Y: 16403.884766 Z: -64.376938 Map: 1',' (GUID: 2301502840)'),(628,'2016-05-12 16:08:52','Cybroon',5,'.add  |cffa335ee|Hitem:50400:0:0:0:0:0:0:0:80|h[Ashen Band of Endless Wisdom]|h|r','X: 16223.926758 Y: 16403.884766 Z: -64.376938 Map: 1',' (GUID: 2312528872)'),(629,'2016-05-12 16:09:05','Cybroon',5,'.add  |cffa335ee|Hitem:50404:0:0:0:0:0:0:0:80|h[Ashen Band of Endless Courage]|h|r','X: 16223.926758 Y: 16403.884766 Z: -64.376938 Map: 1',' (GUID: 2312529912)'),(630,'2016-05-12 16:10:10','Cybroon',5,'.lo i 20-slot','X: 16227.935547 Y: 16404.611328 Z: -64.378716 Map: 1',' (GUID: 2290453752)'),(631,'2016-05-12 16:10:31','Sanka',1,'.pi','X: 16223.711914 Y: 16402.824219 Z: -64.378525 Map: 1','Cybroon (GUID: 2312528792)'),(632,'2016-05-12 16:10:48','Cybroon',5,'.lo i forror','X: 16227.935547 Y: 16404.611328 Z: -64.379402 Map: 1',' (GUID: 2290452872)'),(633,'2016-05-12 16:10:52','Cybroon',5,'.lo i foror','X: 16227.935547 Y: 16404.611328 Z: -64.379402 Map: 1',' (GUID: 2290453032)'),(634,'2016-05-12 16:32:21','Cybroon',5,'.lo i phyla','X: 16226.064453 Y: 16400.472656 Z: -64.378883 Map: 1','GM OUTFIT (GUID: 1111415576)'),(635,'2016-05-12 16:32:25','Sanka',1,'.lo i yolobag','X: 16220.467773 Y: 16408.568359 Z: -64.379051 Map: 1',' (GUID: 1109757416)'),(636,'2016-05-12 16:32:27','Sanka',1,'.add 184','X: 16220.467773 Y: 16408.568359 Z: -64.379051 Map: 1',' (GUID: 1109757576)'),(637,'2016-05-12 16:32:28','Sanka',1,'.add 184','X: 16220.467773 Y: 16408.568359 Z: -64.379051 Map: 1',' (GUID: 1109757736)'),(638,'2016-05-12 16:32:28','Sanka',1,'.add 184','X: 16220.467773 Y: 16408.568359 Z: -64.379051 Map: 1',' (GUID: 1109757896)'),(639,'2016-05-12 16:32:28','Sanka',1,'.add 184','X: 16220.467773 Y: 16408.568359 Z: -64.379051 Map: 1',' (GUID: 1165738184)'),(640,'2016-05-12 16:32:36','Sanka',1,'.add 184','X: 16224.610352 Y: 16407.814453 Z: -64.379051 Map: 1',' (GUID: 1165738984)'),(641,'2016-05-12 16:33:08','Cybroon',5,'.lo crea dummy','X: 16226.064453 Y: 16400.472656 Z: -64.378487 Map: 1','Sanka (GUID: 1109757416)'),(642,'2016-05-12 16:33:17','Cybroon',5,'.lo i test dummy','X: 16226.064453 Y: 16400.472656 Z: -64.378487 Map: 1','Sanka (GUID: 1109757576)'),(643,'2016-05-12 16:33:19','Cybroon',5,'.lo i test','X: 16226.064453 Y: 16400.472656 Z: -64.378487 Map: 1','Sanka (GUID: 1109757736)'),(644,'2016-05-12 16:33:22','Cybroon',5,'.lo crea test dummy','X: 16226.064453 Y: 16400.472656 Z: -64.378487 Map: 1','Sanka (GUID: 1109757896)'),(645,'2016-05-12 16:33:24','Cybroon',5,'.npc add 1000','X: 16226.064453 Y: 16400.472656 Z: -64.378487 Map: 1','Sanka (GUID: 1165738264)'),(646,'2016-05-12 16:33:34','Sanka',1,'.maxskill','X: 16227.665039 Y: 16399.611328 Z: -64.380280 Map: 1','Sanka (GUID: 1165738344)'),(647,'2016-05-12 16:33:37','Sanka',1,'.lo item shadowmourne','X: 16227.665039 Y: 16399.611328 Z: -64.380280 Map: 1','Sanka (GUID: 1165738344)'),(648,'2016-05-12 16:33:39','Sanka',1,'.add 49623','X: 16227.665039 Y: 16399.611328 Z: -64.380280 Map: 1','Sanka (GUID: 1165738584)'),(649,'2016-05-12 16:34:27','Sanka',1,'.addmark 2000','X: 16210.803711 Y: 16394.572266 Z: -64.378716 Map: 1','Sanka (GUID: 1165739464)'),(650,'2016-05-12 16:34:34','Sanka',1,'.addmark 2000','X: 16211.179688 Y: 16395.169922 Z: -64.378716 Map: 1','Event Vendor (GUID: 1936437688)'),(651,'2016-05-12 16:34:38','Sanka',1,'.addmark 2000','X: 16211.934570 Y: 16395.382812 Z: -64.378716 Map: 1','Event Vendor (GUID: 1164746360)'),(652,'2016-05-12 16:35:06','Cybroon',5,'.lo i blood queen','X: 16229.284180 Y: 16401.312500 Z: -64.378647 Map: 1','Sanka (GUID: 1165742024)'),(653,'2016-05-12 16:35:11','Cybroon',5,'.add 50724','X: 16229.284180 Y: 16401.312500 Z: -64.378647 Map: 1',' (GUID: 1165739784)'),(654,'2016-05-12 16:35:26','Cybroon',5,'.lo i cloak','X: 16229.284180 Y: 16401.312500 Z: -64.378593 Map: 1','Sanka (GUID: 1165739304)'),(655,'2016-05-12 16:35:56','Cybroon',5,'.add 54583','X: 16229.284180 Y: 16401.312500 Z: -64.378593 Map: 1',' (GUID: 1165739944)'),(656,'2016-05-12 16:36:38','Sanka',1,'.add  |cffa335ee|Hitem:51310:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Battleplate]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1111415576)'),(657,'2016-05-12 16:36:40','Sanka',1,'.add  |cffa335ee|Hitem:51305:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Chestguard]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1164746360)'),(658,'2016-05-12 16:36:42','Sanka',1,'.add  |cffa335ee|Hitem:51306:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Faceguard]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1162159608)'),(659,'2016-05-12 16:36:51','Sanka',1,'.add  |cffa335ee|Hitem:51311:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Gauntlets]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1111420856)'),(660,'2016-05-12 16:36:54','Sanka',1,'.add  |cffa335ee|Hitem:51307:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Handguards]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1111420760)'),(661,'2016-05-12 16:36:54','Cybroon',5,'.add 50613','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1165741464)'),(662,'2016-05-12 16:37:03','Cybroon',5,'.add 50694','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1165741784)'),(663,'2016-05-12 16:37:16','Cybroon',5,'.add 50699','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110651320)'),(664,'2016-05-12 16:37:18','Sanka',1,'.add  |cffa335ee|Hitem:51312:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Helmet]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378712)'),(665,'2016-05-12 16:37:20','Sanka',1,'.add  |cffa335ee|Hitem:51308:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Legguards]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378808)'),(666,'2016-05-12 16:37:22','Sanka',1,'.add  |cffa335ee|Hitem:51313:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Legplates]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378904)'),(667,'2016-05-12 16:37:23','Cybroon',5,'.add 54582','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110652600)'),(668,'2016-05-12 16:37:24','Sanka',1,'.add  |cffa335ee|Hitem:51309:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Pauldrons]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128379000)'),(669,'2016-05-12 16:37:26','Sanka',1,'.add  |cffa335ee|Hitem:51314:0:0:0:0:0:0:0:80|h[Sanctified Scourgelord Shoulderplates]|h|r','X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128379096)'),(670,'2016-05-12 16:37:32','Cybroon',5,'.add 50365','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653480)'),(671,'2016-05-12 16:37:41','Cybroon',5,'.add 50714','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653640)'),(672,'2016-05-12 16:37:46','Cybroon',5,'.add 50719','X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653800)'),(673,'2016-05-12 16:38:21','Cybroon',5,'.add 50457','X: 16229.284180 Y: 16401.312500 Z: -64.378510 Map: 1',' (GUID: 1110653960)'),(674,'2016-05-12 16:38:39','Cybroon',5,'.gm on','X: 16227.436523 Y: 16403.621094 Z: -62.961143 Map: 1',' (GUID: 1110651080)'),(675,'2016-05-12 16:38:54','Mew',1,'.te gmk','X: 16247.980469 Y: 16267.358398 Z: 14.988154 Map: 1',' (GUID: 1110652680)'),(676,'2016-05-12 17:22:58','Cybroon',5,'.lo i amulet of silent','X: 16230.359375 Y: 16401.314453 Z: -64.379990 Map: 1',' (GUID: 1163114968)'),(677,'2016-05-12 17:23:00','Cybroon',5,'.lo i amulet o','X: 16230.359375 Y: 16401.314453 Z: -64.379990 Map: 1',' (GUID: 1163115048)'),(678,'2016-05-12 17:23:11','Cybroon',5,'.lo i silent','X: 16230.359375 Y: 16401.314453 Z: -64.379990 Map: 1',' (GUID: 1163115128)'),(679,'2016-05-12 17:23:15','Cybroon',5,'.add  |cffa335ee|Hitem:50658:0:0:0:0:0:0:0:80|h[Amulet of the Silent Eulogy]|h|r','X: 16230.359375 Y: 16401.314453 Z: -64.379990 Map: 1',' (GUID: 1163115288)'),(680,'2016-05-12 17:23:29','Cybroon',5,'.lo i profesor','X: 16230.359375 Y: 16401.314453 Z: -64.377258 Map: 1',' (GUID: 1163115528)'),(681,'2016-05-12 17:23:33','Cybroon',5,'.lo i professor','X: 16230.359375 Y: 16401.314453 Z: -64.377258 Map: 1',' (GUID: 1163115608)'),(682,'2016-05-12 17:23:37','Cybroon',5,'.add 50705','X: 16230.359375 Y: 16401.314453 Z: -64.377258 Map: 1',' (GUID: 1163115848)'),(683,'2016-05-12 17:23:57','Cybroon',5,'.lo i bauble','X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1165738104)'),(684,'2016-05-12 17:24:06','Cybroon',5,'.add 50726','X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1165738264)'),(685,'2016-05-12 17:24:22','Cybroon',5,'.lo i phased','X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1109757416)'),(686,'2016-05-12 17:24:32','Cybroon',5,'.add 54585','X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1110654360)'),(687,'2016-05-12 17:32:37','Sanka',1,'.te stormwind','X: 16222.383789 Y: 16403.828125 Z: -64.375839 Map: 1',' (GUID: 1110651560)'),(688,'2016-05-12 17:32:50','Cybroon',5,'.np del','X: 16221.522461 Y: 16401.162109 Z: -64.378601 Map: 1',' (GUID: 1057213400)'),(689,'2016-05-12 17:34:55','Sanka',1,'.summ cybroon','X: -8726.000000 Y: 334.285889 Z: 102.145859 Map: 0','Grandmaster\'s Training Dummy (GUID: 1109857176)'),(690,'2016-05-12 17:35:13','Cybroon',5,'.gm off','X: -8720.762695 Y: 334.918365 Z: 101.019485 Map: 0','Sanka (GUID: 1057122616)'),(691,'2016-05-12 17:35:15','Sanka',1,'.gm off','X: -8722.823242 Y: 336.932556 Z: 101.020042 Map: 0','Cybroon (GUID: 1057122776)'),(692,'2016-05-12 17:37:18','Cybroon',5,'.lo i Mag\'hari Chieftain\'s Staff','X: -8718.028320 Y: 337.285614 Z: 101.019630 Map: 0','Sanka (GUID: 1057122696)'),(693,'2016-05-12 17:37:23','Cybroon',5,'.add 51898','X: -8718.028320 Y: 337.285614 Z: 101.019630 Map: 0',' (GUID: 1057122936)'),(694,'2016-05-12 17:37:27','Cybroon',5,'.te gmi','X: -8718.028320 Y: 337.285614 Z: 101.021248 Map: 0',' (GUID: 1057123016)'),(695,'2016-05-12 17:37:27','Sanka',1,'.te gmi','X: -8721.553711 Y: 335.439545 Z: 101.019592 Map: 0',' (GUID: 1057123096)'),(696,'2016-05-12 17:40:16','Cybroon',5,'.te gmk','X: 16222.381836 Y: 16249.804688 Z: 12.308878 Map: 1','Sanka (GUID: 1057121176)'),(697,'2016-05-12 17:40:19','Sanka',1,'.te gmk','X: 16224.892578 Y: 16252.874023 Z: 12.764998 Map: 1','Cybroon (GUID: 1057121256)'),(698,'2016-05-12 17:41:32','Cybroon',5,'.gm on','X: 16211.373047 Y: 16408.746094 Z: -64.379196 Map: 1','Enchantment (GUID: 1057211000)'),(699,'2016-05-12 17:41:44','Cybroon',5,'.gm off','X: 16218.755859 Y: 16403.734375 Z: -64.373917 Map: 1','Enchantment (GUID: 1057211000)'),(700,'2016-05-12 17:43:44','Sanka',1,'.te gmi','X: 16228.776367 Y: 16402.246094 Z: -64.378998 Map: 1','Cybroon (GUID: 1053602792)'),(701,'2016-05-12 17:43:46','Cybroon',5,'.te gmi','X: 16228.935547 Y: 16404.023438 Z: -63.468548 Map: 1','Sanka (GUID: 1053602792)'),(702,'2016-05-12 17:44:17','Sanka',1,'.lo i gloren','X: 16228.955078 Y: 16261.414062 Z: 13.423162 Map: 1',' (GUID: 1057120456)'),(703,'2016-05-12 17:44:27','Sanka',1,'.add 50730','X: 16228.955078 Y: 16261.414062 Z: 13.423162 Map: 1',' (GUID: 1057120696)'),(704,'2016-05-12 17:46:20','Sanka',1,'.bank','X: 16225.646484 Y: 16262.305664 Z: 13.290578 Map: 1','Cybroon (GUID: 1057121736)'),(705,'2016-05-12 17:50:34','Sanka',1,'.mod hp 99999999','X: 16248.012695 Y: 16262.770508 Z: 16.569859 Map: 1','Sanka (GUID: 1057120776)'),(706,'2016-05-12 17:50:35','Cybroon',5,'.te gmk','X: 16236.219727 Y: 16258.887695 Z: 14.805301 Map: 1','Sanka (GUID: 1057120856)'),(707,'2016-05-12 17:50:44','Cybroon',5,'.te gmi','X: 16226.270508 Y: 16416.865234 Z: -64.378899 Map: 1','Druid Trainer (GUID: 1057211096)'),(708,'2016-05-12 17:54:11','Lynnie',1,'.lo i yolobag','X: -8949.950195 Y: -132.492996 Z: 83.531197 Map: 0',' (GUID: 1110653400)'),(709,'2016-05-12 17:54:18','Lynnie',1,'.add 184 4','X: -8949.950195 Y: -132.492996 Z: 83.531197 Map: 0',' (GUID: 1110653800)'),(710,'2016-05-12 17:54:51','Lynnie',1,'.te gmi','X: -8890.602539 Y: -119.124962 Z: 82.033455 Map: 0',' (GUID: 1163115128)'),(711,'2016-05-12 17:57:42','Cybroon',5,'.te gmi','X: 16228.673828 Y: 16262.743164 Z: 13.389757 Map: 1','Lynnie (GUID: 1057120376)'),(712,'2016-05-12 17:57:43','Cybroon',5,'.te gmk','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1','Lynnie (GUID: 1057120536)'),(713,'2016-05-12 17:58:49','Cybroon',5,'.lo i glyphs','X: 16233.105469 Y: 16410.927734 Z: -64.377335 Map: 1',' (GUID: 1057120536)'),(714,'2016-05-12 17:58:53','Cybroon',5,'.lo crea glyph','X: 16241.870117 Y: 16415.507812 Z: -64.379295 Map: 1',' (GUID: 1057121656)'),(715,'2016-05-12 17:59:15','Cybroon',5,'.lo i glyph of innervate','X: 16216.535156 Y: 16402.388672 Z: -64.379311 Map: 1',' (GUID: 1057121656)'),(716,'2016-05-12 17:59:17','Cybroon',5,'.add 40908','X: 16216.535156 Y: 16402.388672 Z: -64.379311 Map: 1',' (GUID: 1110653240)'),(717,'2016-05-12 17:59:23','Cybroon',5,'.lo i glyph of swiftmend','X: 16223.132812 Y: 16405.035156 Z: -64.378815 Map: 1',' (GUID: 1110653400)'),(718,'2016-05-12 17:59:25','Cybroon',5,'.add 40906','X: 16223.132812 Y: 16405.035156 Z: -64.378815 Map: 1',' (GUID: 1110653480)'),(719,'2016-05-12 17:59:32','Cybroon',5,'.lo i glyph of barkskin','X: 16223.132812 Y: 16405.035156 Z: -64.378815 Map: 1',' (GUID: 1110653560)'),(720,'2016-05-12 17:59:34','Cybroon',5,'.add 45623','X: 16223.132812 Y: 16405.035156 Z: -64.378815 Map: 1',' (GUID: 1110653640)'),(721,'2016-05-12 18:00:37','Cybroon',5,'.te gmi','X: 16230.357422 Y: 16401.093750 Z: -64.378685 Map: 1',' (GUID: 1057121096)'),(722,'2016-05-12 18:19:04','Yara',5,'.te gmk','X: -8926.307617 Y: -136.108826 Z: 81.496918 Map: 0',' (GUID: 1110654520)'),(723,'2016-05-12 18:34:30','Yara',5,'.te gmi','X: -8936.358398 Y: -135.193878 Z: 83.491753 Map: 0',' (GUID: 1366024312)'),(724,'2016-05-12 18:34:33','Yara',5,'.te gmk','X: 16231.831055 Y: 16257.447266 Z: 13.828954 Map: 1',' (GUID: 1366026152)'),(725,'2016-05-12 18:34:49','Yara',5,'.add  |cffa335ee|Hitem:51282:0:0:0:0:0:0:0:1|h[Sanctified Bloodmage Leggings]|h|r','X: 16214.394531 Y: 16387.546875 Z: -64.379158 Map: 1','Malfus Grimfrost (GUID: 1348363480)'),(726,'2016-05-12 18:34:51','Yara',5,'.add  |cffa335ee|Hitem:51284:0:0:0:0:0:0:0:1|h[Sanctified Bloodmage Shoulderpads]|h|r','X: 16214.394531 Y: 16387.546875 Z: -64.379158 Map: 1','Malfus Grimfrost (GUID: 1348363384)'),(727,'2016-05-12 18:34:52','Yara',5,'.add  |cffa335ee|Hitem:51283:0:0:0:0:0:0:0:1|h[Sanctified Bloodmage Robe]|h|r','X: 16214.394531 Y: 16387.546875 Z: -64.379158 Map: 1','Malfus Grimfrost (GUID: 1723250648)'),(728,'2016-05-12 18:34:59','Yara',5,'.add  |cffa335ee|Hitem:51281:0:0:0:0:0:0:0:1|h[Sanctified Bloodmage Hood]|h|r','X: 16214.394531 Y: 16387.546875 Z: -64.379158 Map: 1','Malfus Grimfrost (GUID: 1357692568)'),(729,'2016-05-12 18:35:01','Yara',5,'.add   |cffa335ee|Hitem:51280:0:0:0:0:0:0:0:1|h[Sanctified Bloodmage Gloves]|h|r','X: 16214.394531 Y: 16387.546875 Z: -64.379158 Map: 1','Malfus Grimfrost (GUID: 2139279512)'),(730,'2016-05-12 18:35:14','Yara',5,'.levelup 79','X: 16237.494141 Y: 16411.826172 Z: -64.378304 Map: 1',' (GUID: 1366028072)'),(731,'2016-05-12 18:35:38','Yara',5,'.lo i 20-slot','X: 16220.463867 Y: 16390.130859 Z: -64.372131 Map: 1',' (GUID: 1348465240)'),(732,'2016-05-12 18:35:43','Yara',5,'.add 1977 4','X: 16220.463867 Y: 16390.130859 Z: -64.372131 Map: 1',' (GUID: 1348465720)'),(733,'2016-05-12 18:36:14','Yara',5,'.lo i blood','X: 16220.463867 Y: 16390.130859 Z: -64.372131 Map: 1',' (GUID: 1348467400)'),(734,'2016-05-12 18:36:17','Yara',5,'.lo i bloodsurge','X: 16220.463867 Y: 16390.130859 Z: -64.372131 Map: 1',' (GUID: 1348467480)'),(735,'2016-05-12 18:36:21','Yara',5,'.add 50732','X: 16220.463867 Y: 16390.130859 Z: -64.378059 Map: 1',' (GUID: 1348467800)'),(736,'2016-05-12 18:36:59','Yara',5,'.lo i scale','X: 16224.712891 Y: 16410.054688 Z: -64.378990 Map: 1',' (GUID: 1348464840)'),(737,'2016-05-12 18:37:46','Yara',5,'.lo i scale','X: 16223.525391 Y: 16403.498047 Z: -64.379646 Map: 1',' (GUID: 1348464920)'),(738,'2016-05-12 18:38:02','Yara',5,'.lo i twilight','X: 16223.525391 Y: 16403.498047 Z: -64.379646 Map: 1',' (GUID: 1348465000)'),(739,'2016-05-12 18:38:07','Yara',5,'.lo i twilight scale','X: 16223.525391 Y: 16403.498047 Z: -64.380081 Map: 1',' (GUID: 1348465080)'),(740,'2016-05-12 18:38:12','Yara',5,'.add 54588','X: 16223.525391 Y: 16403.498047 Z: -64.380081 Map: 1',' (GUID: 1348465320)'),(741,'2016-05-12 18:38:24','Yara',5,'.lo i phyla','X: 16219.405273 Y: 16400.542969 Z: -64.377327 Map: 1','Trainer (GUID: 1348364056)'),(742,'2016-05-12 18:38:26','Yara',5,'.lo i phylacer','X: 16219.405273 Y: 16400.542969 Z: -64.377327 Map: 1','Trainer (GUID: 1348364056)'),(743,'2016-05-12 18:38:28','Yara',5,'.lo i phyla','X: 16219.405273 Y: 16400.542969 Z: -64.377327 Map: 1','Trainer (GUID: 1348364056)'),(744,'2016-05-12 18:38:32','Yara',5,'.add 50365','X: 16219.405273 Y: 16400.542969 Z: -64.377327 Map: 1','Trainer (GUID: 1348364056)'),(745,'2016-05-12 18:38:50','Yara',5,'.lo faction ashen','X: 16226.997070 Y: 16396.066406 Z: -64.378075 Map: 1','Trainer (GUID: 1407922456)'),(746,'2016-05-12 18:38:54','Yara',5,'.mod rep 1156 60000','X: 16226.997070 Y: 16396.066406 Z: -64.378075 Map: 1','Trainer (GUID: 1407922456)'),(747,'2016-05-12 18:39:19','Yara',5,'.lo i loop of endless','X: 16230.841797 Y: 16392.453125 Z: -64.378929 Map: 1','Ashen Rings (GUID: 1407922072)'),(748,'2016-05-12 18:39:26','Yara',5,'.lo i loop of endl','X: 16230.841797 Y: 16392.453125 Z: -64.378929 Map: 1','Ashen Rings (GUID: 1348363672)'),(749,'2016-05-12 18:39:40','Mew',1,'.te stormwind','X: 16225.951172 Y: 16400.121094 Z: -64.378464 Map: 1',' (GUID: 1444131496)'),(750,'2016-05-12 18:39:50','Yara',5,'.lo i ring of maddening','X: 16230.841797 Y: 16392.453125 Z: -64.378929 Map: 1','Ashen Rings (GUID: 1397172920)'),(751,'2016-05-12 18:39:52','Yara',5,'.add 50644','X: 16230.841797 Y: 16392.453125 Z: -64.377205 Map: 1','Ashen Rings (GUID: 1397172920)'),(752,'2016-05-12 18:40:02','Yara',5,'.lo i crushing','X: 16226.590820 Y: 16398.169922 Z: -64.328941 Map: 1','Ashen Rings (GUID: 1397172728)'),(753,'2016-05-12 18:40:09','Yara',5,'.add 50613','X: 16226.590820 Y: 16398.169922 Z: -64.328941 Map: 1','Ashen Rings (GUID: 1397172728)'),(754,'2016-05-12 18:40:21','Mew',1,'.npc i','X: -8718.559570 Y: 354.073059 Z: 101.019356 Map: 0','Grandmaster\'s Training Dummy (GUID: 1397172920)'),(755,'2016-05-12 18:40:29','Mew',1,'.te gmk','X: -8724.861328 Y: 349.882629 Z: 101.019371 Map: 0',' (GUID: 1372992888)'),(756,'2016-05-12 18:40:31','Yara',5,'.lo i bracers','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173400)'),(757,'2016-05-12 18:40:35','Yara',5,'.lo i bracers of fier','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173400)'),(758,'2016-05-12 18:40:37','Yara',5,'.add 54582','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173400)'),(759,'2016-05-12 18:40:44','Yara',5,'.lo i cloak of the burning','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173400)'),(760,'2016-05-12 18:40:46','Yara',5,'.lo i cloak of burn','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173400)'),(761,'2016-05-12 18:40:46','Mew',1,'.npc add 31144','X: 16239.934570 Y: 16399.078125 Z: -64.379822 Map: 1',' (GUID: 1372992168)'),(762,'2016-05-12 18:40:49','Yara',5,'.add 54583','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397174072)'),(763,'2016-05-12 18:40:51','Mew',1,'.npc add 31144','X: 16239.999023 Y: 16407.164062 Z: -64.379822 Map: 1',' (GUID: 1372993528)'),(764,'2016-05-12 18:40:54','Yara',5,'.lo i blood queen','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397174744)'),(765,'2016-05-12 18:40:56','Yara',5,'.add 50724','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397174744)'),(766,'2016-05-12 18:41:00','Yara',5,'.lo i shadow ','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397175032)'),(767,'2016-05-12 18:41:07','Yara',5,'.lo i shadow  silk','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397175032)'),(768,'2016-05-12 18:41:10','Yara',5,'.lo i shadow silk','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1348363672)'),(769,'2016-05-12 18:41:13','Yara',5,'.add 50719','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397172920)'),(770,'2016-05-12 18:41:19','Yara',5,'.lo i corpse impaling','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397172920)'),(771,'2016-05-12 18:41:22','Yara',5,'.lo i corpse ','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397172920)'),(772,'2016-05-12 18:41:31','Yara',5,'.lo i corpse impaling','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397175032)'),(773,'2016-05-12 18:41:53','Yara',5,'.lo i Corpse Impaling','X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397175032)'),(774,'2016-05-12 18:42:02','Yara',5,'.lo i impaling','X: 16226.590820 Y: 16398.169922 Z: -64.377045 Map: 1','Ashen Rings (GUID: 1348363672)'),(775,'2016-05-12 18:42:07','Yara',5,'.add 50864','X: 16226.590820 Y: 16398.169922 Z: -64.377541 Map: 1','Ashen Rings (GUID: 1348363672)'),(776,'2016-05-12 18:42:11','Yara',5,'.add 50684','X: 16226.590820 Y: 16398.169922 Z: -64.376686 Map: 1','Ashen Rings (GUID: 1348363672)'),(777,'2016-05-12 18:43:45','Yara',5,'.lo i Runed Cardinal Ruby','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(778,'2016-05-12 18:43:50','Yara',5,'.add 40113 10','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(779,'2016-05-12 18:43:57','Yara',5,'.lo i Quick King\'s Amber','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(780,'2016-05-12 18:43:59','Yara',5,'.add 40128 10','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(781,'2016-05-12 18:44:06','Yara',5,'.lo i Purified Dreadstone','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(782,'2016-05-12 18:44:09','Yara',5,'.add 40133 10','X: 16225.416992 Y: 16402.042969 Z: -64.379234 Map: 1','Grandmaster\'s Training Dummy (GUID: 1348363672)'),(783,'2016-05-12 18:46:24','Yara',5,'.lo i plague','X: 16236.535156 Y: 16389.822266 Z: -64.378532 Map: 1','Orange Gems (GUID: 1348363672)'),(784,'2016-05-12 18:46:26','Yara',5,'.lo i plague scienti','X: 16236.535156 Y: 16389.822266 Z: -64.378532 Map: 1','Orange Gems (GUID: 1348363672)'),(785,'2016-05-12 18:46:29','Yara',5,'.add 50699','X: 16236.535156 Y: 16389.822266 Z: -64.378532 Map: 1','Orange Gems (GUID: 1348364440)'),(786,'2016-05-12 18:47:34','Yara',5,'.gm on','X: 16228.837891 Y: 16406.601562 Z: -64.378181 Map: 1',' (GUID: 1348465400)'),(787,'2016-05-12 18:51:33','Yara',5,'.save','X: 16229.271484 Y: 16407.013672 Z: -64.379189 Map: 1',' (GUID: 2779920216)'),(788,'2016-05-12 19:02:39','Yara',5,'.rev','X: 16230.766602 Y: 16399.453125 Z: -64.379417 Map: 1','Grandmaster\'s Training Dummy (GUID: 2789276760)'),(789,'2016-05-12 19:05:32','Yara',5,'.rev','X: 16230.766602 Y: 16399.453125 Z: -64.379417 Map: 1','Yara (GUID: 2779920856)'),(790,'2016-05-12 19:08:02','Yara',5,'.lo i Glyph of Arcane Missiles','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(791,'2016-05-12 19:08:04','Yara',5,'.add 42735','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(792,'2016-05-12 19:08:09','Yara',5,'.lo i Glyph of Arcane Blast','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(793,'2016-05-12 19:08:11','Yara',5,'.add 44955','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(794,'2016-05-12 19:08:17','Yara',5,'.lo i Glyph of Molten Armor','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(795,'2016-05-12 19:08:20','Yara',5,'.add 42751','X: 16226.377930 Y: 16400.648438 Z: -64.377365 Map: 1','Grandmaster\'s Training Dummy (GUID: 2779942904)'),(796,'2016-05-12 19:08:37','Yara',5,'.cooldown','X: 16228.623047 Y: 16398.839844 Z: -64.379166 Map: 1','Yara (GUID: 2779921656)'),(797,'2016-05-12 19:08:39','Yara',5,'.rev','X: 16228.317383 Y: 16398.835938 Z: -64.379166 Map: 1','Yara (GUID: 2779921736)'),(798,'2016-05-12 19:10:12','Cybroon',5,'.te gmk','X: 16222.239258 Y: 16258.883789 Z: 13.203399 Map: 1',' (GUID: 2779921896)'),(799,'2016-05-12 19:10:54','Cybroon',5,'.gm on','X: 16224.437500 Y: 16398.873047 Z: -64.378731 Map: 1',' (GUID: 2779921976)'),(800,'2016-05-12 19:14:20','Cybroon',5,'.respawn','X: 16217.277344 Y: 16390.787109 Z: -64.378830 Map: 1','Jedebia (GUID: 2779942808)'),(801,'2016-05-12 19:14:27','Cybroon',5,'.respawn','X: 16217.277344 Y: 16390.787109 Z: -64.378830 Map: 1',' (GUID: 2779922056)'),(802,'2016-05-12 19:16:18','Yara',5,'.gm off','X: 16229.042969 Y: 16386.078125 Z: -64.376816 Map: 1','Nargle Lashcord (GUID: 2779942520)'),(803,'2016-05-12 19:16:44','Yara',5,'.add  |cffa335ee|Hitem:51465:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Silk Cowl]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2779942616)'),(804,'2016-05-12 19:16:46','Yara',5,'.add  |cffa335ee|Hitem:51463:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Silk Raiment]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2779942520)'),(805,'2016-05-12 19:16:47','Yara',5,'.add  |cffa335ee|Hitem:51467:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Silk Amice]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789274264)'),(806,'2016-05-12 19:16:49','Yara',5,'.add  |cffa335ee|Hitem:51466:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Silk Trousers]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277432)'),(807,'2016-05-12 19:17:05','Yara',5,'.add  |cffa335ee|Hitem:51451:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Wand of Alacrity]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277144)'),(808,'2016-05-12 19:17:55','Yara',5,'.add  |cffa335ee|Hitem:51464:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Silk Handguards]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2779942520)'),(809,'2016-05-12 19:18:29','Yara',5,'.add  |cffa335ee|Hitem:51407:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Compendium]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Nargle Lashcord (GUID: 2779942616)'),(810,'2016-05-12 19:18:54','Yara',5,'.add  |cffa335ee|Hitem:51534:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Tabard]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Trapjaw Rix (GUID: 2779942616)'),(811,'2016-05-12 19:19:05','Yara',5,'.add  |cffa335ee|Hitem:51398:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Blade of Celerity]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Trapjaw Rix (GUID: 2779942520)'),(812,'2016-05-12 19:19:15','Yara',5,'.add  |cffa335ee|Hitem:51338:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Treads of Alacrity]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942616)'),(813,'2016-05-12 19:19:23','Yara',5,'.add  |cffa335ee|Hitem:51332:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Cloak of Subjugation]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942616)'),(814,'2016-05-12 19:19:34','Yara',5,'.add  |cffa335ee|Hitem:51335:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Pendant of Ascendancy]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942616)'),(815,'2016-05-12 19:19:38','Yara',5,'.add  |cffa335ee|Hitem:51339:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Cuffs of Alacrity]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942616)'),(816,'2016-05-12 19:20:05','Yara',5,'.add  |cffa335ee|Hitem:51337:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Cord of Alacrity]|h|r','X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779940696)'),(817,'2016-05-12 19:21:43','Yara',5,'.lo i Plaguebringe','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779518232)'),(818,'2016-05-12 19:21:48','Yara',5,'.add 50694 2','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779518232)'),(819,'2016-05-12 19:23:43','Yara',5,'.lo i solace','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942520)'),(820,'2016-05-12 19:23:47','Yara',5,'.add 47059','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942520)'),(821,'2016-05-12 19:23:57','Yara',5,'.lo i ring of phased','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942520)'),(822,'2016-05-12 19:24:01','Yara',5,'.add 54585','X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942520)'),(823,'2016-05-12 19:25:22','Yara',5,'.add  |cffa335ee|Hitem:40127:0:0:0:0:0:0:0:80|h[Mystic King\'s Amber]|h|r 2','X: 16239.409180 Y: 16389.898438 Z: -64.378906 Map: 1','Orange Gems (GUID: 2779940024)'),(824,'2016-05-12 19:25:36','Yara',5,'.add  |cffa335ee|Hitem:40154:0:0:0:0:0:0:0:80|h[Durable Ametrine]|h|r','X: 16239.409180 Y: 16389.898438 Z: -64.379028 Map: 1','Orange Gems (GUID: 2779940216)'),(825,'2016-05-12 19:26:58','Yara',5,'.mod money 214000000','X: 16226.107422 Y: 16417.830078 Z: -64.379707 Map: 1',' (GUID: 2779566280)'),(826,'2016-05-12 19:27:17','Yara',5,'.lo i glyph of evocation','X: 16224.677734 Y: 16411.978516 Z: -64.378334 Map: 1',' (GUID: 2779566520)'),(827,'2016-05-12 19:27:19','Yara',5,'.add 42738','X: 16224.677734 Y: 16411.978516 Z: -64.378334 Map: 1',' (GUID: 2779567320)'),(828,'2016-05-12 19:27:32','Yara',5,'.lo i glyph of frost','X: 16210.877930 Y: 16405.580078 Z: -64.378418 Map: 1','Trainer (GUID: 2779940408)'),(829,'2016-05-12 19:27:44','Yara',5,'.gm on','X: 16211.369141 Y: 16407.642578 Z: -64.379066 Map: 1','Teleporter (GUID: 2779940504)'),(830,'2016-05-12 19:30:10','Mew',1,'.guild create \"Tiger Tooths\"','X: 16240.648438 Y: 16401.291016 Z: -64.378830 Map: 1','Yara (GUID: 2779566280)'),(831,'2016-05-12 19:30:33','Yara',5,'.lo i glyph of icy viens','X: 16240.286133 Y: 16398.929688 Z: -64.378441 Map: 1',' (GUID: 2779566360)'),(832,'2016-05-12 19:31:01','Yara',5,'.lo i glyph of ice barrier','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Mew (GUID: 2779567640)'),(833,'2016-05-12 19:31:04','Yara',5,'.add 45740','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Mew (GUID: 2779567880)'),(834,'2016-05-12 19:31:11','Yara',5,'.add 45740 -1','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Mew (GUID: 2779567960)'),(835,'2016-05-12 19:31:12','Yara',5,'.lo i glyph of ice barrier','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779568040)'),(836,'2016-05-12 19:31:15','Yara',5,'.add 45740','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779568120)'),(837,'2016-05-12 19:31:23','Yara',5,'.lo i glyph of polymorph','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779568200)'),(838,'2016-05-12 19:31:25','Yara',5,'.add 42752','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779570040)'),(839,'2016-05-12 19:31:31','Yara',5,'.lo i glyph of slowfall','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779886408)'),(840,'2016-05-12 19:31:36','Yara',5,'.lo i glyph of slowfall','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779886568)'),(841,'2016-05-12 19:31:38','Yara',5,'.lo i glyph of slow','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779886808)'),(842,'2016-05-12 19:31:41','Yara',5,'.add 43364','X: 16241.318359 Y: 16398.722656 Z: -64.378502 Map: 1','Yara (GUID: 2779886968)'),(843,'2016-05-12 19:31:53','Yara',5,'.lo i glyph of mana gem','X: 16234.859375 Y: 16399.355469 Z: -64.379059 Map: 1','Yara (GUID: 2779887208)'),(844,'2016-05-12 19:31:55','Yara',5,'.add 42750','X: 16234.859375 Y: 16399.355469 Z: -64.379059 Map: 1','Yara (GUID: 2779887288)'),(845,'2016-05-12 19:38:10','Cybroon',5,'.gm off','X: 16232.401367 Y: 16398.427734 Z: -64.379387 Map: 1',' (GUID: 2779566680)'),(846,'2016-05-12 19:38:53','Cybroon',5,'.cooldown','X: 16232.051758 Y: 16411.248047 Z: -64.378654 Map: 1',' (GUID: 2779566840)'),(847,'2016-05-12 19:38:57','Cybroon',5,'.combat','X: 16232.051758 Y: 16411.248047 Z: -64.377884 Map: 1',' (GUID: 2779567160)'),(848,'2016-05-12 19:43:31','Mew',1,'.server restart 3 Yolo:)','X: 16238.032227 Y: 16394.818359 Z: -64.378639 Map: 1',' (GUID: 2779566680)'),(849,'2016-05-12 21:19:54','Yara',1,'.gm off','X: 16228.894531 Y: 16406.388672 Z: -64.378662 Map: 1',' (GUID: 2008786456)'),(850,'2016-05-12 21:20:01','Yara',1,'.char changefaction','X: 16236.341797 Y: 16404.966797 Z: -64.376457 Map: 1',' (GUID: 2008786616)'),(851,'2016-05-12 21:21:33','Della',1,'.addmark 5000','X: 16228.060547 Y: 16402.035156 Z: -64.379120 Map: 1',' (GUID: 2008787656)'),(852,'2016-05-12 21:21:48','Della',1,'.rev','X: 16221.742188 Y: 16389.820312 Z: -64.378807 Map: 1',' (GUID: 2008786936)'),(853,'2016-05-12 21:22:15','Della',1,'.rev','X: 16210.608398 Y: 16397.062500 Z: -64.379448 Map: 1','Event Vendor (GUID: 2008958552)'),(854,'2016-05-12 21:22:19','Della',1,'.addmark 5000','X: 16210.608398 Y: 16397.062500 Z: -64.379456 Map: 1','Event Vendor (GUID: 2008958552)'),(855,'2016-05-12 21:22:22','Cybroon',5,'.addmark 70','X: 16213.925781 Y: 16399.583984 Z: -64.380035 Map: 1','Della (GUID: 2008788456)'),(856,'2016-05-12 21:22:24','Della',1,'.addmark 5000','X: 16210.608398 Y: 16397.062500 Z: -64.379456 Map: 1','Event Vendor (GUID: 2008958456)'),(857,'2016-05-12 21:22:25','Cybroon',5,'.addmark 70','X: 16211.275391 Y: 16398.748047 Z: -64.380035 Map: 1',' (GUID: 2008788696)'),(858,'2016-05-12 21:23:11','Cybroon',5,'.add  |cffa335ee|Hitem:51534:0:0:0:0:0:0:0:80|h[Wrathful Gladiator\'s Tabard]|h|r','X: 16227.137695 Y: 16385.740234 Z: -64.378571 Map: 1','Cybroon (GUID: 2008787976)'),(859,'2016-05-12 21:23:53','Cybroon',5,'.lo i brutal gladiator','X: 16227.735352 Y: 16390.943359 Z: -64.378571 Map: 1',' (GUID: 2008787096)'),(860,'2016-05-12 21:24:11','Cybroon',5,'.lo i kirin tor','X: 16227.735352 Y: 16390.943359 Z: -64.378571 Map: 1',' (GUID: 2008787176)'),(861,'2016-05-12 21:24:52','Cybroon',5,'.add 15050','X: 16229.515625 Y: 16397.912109 Z: -64.379257 Map: 1','Della (GUID: 2008787416)'),(862,'2016-05-12 21:24:59','Della',1,'.lo item tuxedo','X: 16226.707031 Y: 16395.095703 Z: -64.379227 Map: 1',' (GUID: 2008787496)'),(863,'2016-05-12 21:25:06','Della',1,'.add 6835','X: 16226.707031 Y: 16395.095703 Z: -64.379227 Map: 1',' (GUID: 2008787816)'),(864,'2016-05-12 21:25:11','Cybroon',5,'.lo i dragonscale','X: 16229.515625 Y: 16397.912109 Z: -64.379257 Map: 1','Della (GUID: 2008788536)'),(865,'2016-05-12 21:25:19','Della',1,'.lo i transmog','X: 16211.594727 Y: 16407.984375 Z: -64.379501 Map: 1','Transmogrification (GUID: 2007995416)'),(866,'2016-05-12 21:25:23','Della',1,'.add 5407 255','X: 16211.594727 Y: 16407.984375 Z: -64.379501 Map: 1','Transmogrification (GUID: 2007995608)'),(867,'2016-05-12 21:25:24','Della',1,'.add 5407 255','X: 16211.594727 Y: 16407.984375 Z: -64.379501 Map: 1','Transmogrification (GUID: 2007995608)'),(868,'2016-05-12 21:25:24','Della',1,'.add 5407 255','X: 16211.594727 Y: 16407.984375 Z: -64.379501 Map: 1','Transmogrification (GUID: 2007995608)'),(869,'2016-05-12 21:27:29','Cybroon',5,'.lo i brutal','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008212456)'),(870,'2016-05-12 21:27:47','Cybroon',5,'.lo i brutal  gladiator','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008212536)'),(871,'2016-05-12 21:27:53','Cybroon',5,'.lo i brutal gladiator','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008212616)'),(872,'2016-05-12 21:27:53','Della',1,'.reload config','X: 16211.594727 Y: 16407.984375 Z: -64.379501 Map: 1','Transmogrification (GUID: 2007991480)'),(873,'2016-05-12 21:29:01','Cybroon',5,'.add 35026','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008788136)'),(874,'2016-05-12 21:29:03','Cybroon',5,'.add 35024','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008787896)'),(875,'2016-05-12 21:29:06','Cybroon',5,'.add 35022','X: 16215.202148 Y: 16407.378906 Z: -64.379105 Map: 1',' (GUID: 2008787976)'),(876,'2016-05-12 21:29:15','Cybroon',5,'.lo i transmog token','X: 16211.168945 Y: 16408.099609 Z: -64.378464 Map: 1',' (GUID: 2008788216)'),(877,'2016-05-12 21:29:17','Cybroon',5,'.add 5407 3','X: 16211.168945 Y: 16408.099609 Z: -64.378464 Map: 1',' (GUID: 2009052776)'),(878,'2016-05-12 21:29:20','Cybroon',5,'.add 5407','X: 16211.168945 Y: 16408.099609 Z: -64.378464 Map: 1',' (GUID: 2009052936)'),(879,'2016-05-12 21:29:46','Della',1,'.add 9636','X: 16213.058594 Y: 16407.574219 Z: -64.379501 Map: 1','Della (GUID: 2008211576)'),(880,'2016-05-12 21:30:53','Cybroon',5,'.lo i glyph of starfire','X: 16219.305664 Y: 16407.402344 Z: -64.379829 Map: 1','Della (GUID: 2008211656)'),(881,'2016-05-12 21:31:02','Cybroon',5,'.lo i glyph of starfall','X: 16219.305664 Y: 16407.402344 Z: -64.379829 Map: 1','Cybroon (GUID: 2008211816)'),(882,'2016-05-12 21:31:05','Cybroon',5,'.add 40921','X: 16219.305664 Y: 16407.402344 Z: -64.379829 Map: 1','Cybroon (GUID: 2008787816)'),(883,'2016-05-12 21:31:18','Cybroon',5,'.add |cffffffff|Hitem:40908:0:0:0:0:0:0:0:80|h[Glyph of Innervate]|h|r','X: 16219.262695 Y: 16406.511719 Z: -64.379829 Map: 1','Cybroon (GUID: 2008787896)'),(884,'2016-05-12 21:31:38','Della',1,'.add 12109','X: 16220.895508 Y: 16406.183594 Z: -64.379501 Map: 1','Della (GUID: 2008788136)'),(885,'2016-05-12 21:31:45','Cybroon',5,'.lo i glyph of starfire','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2008211576)'),(886,'2016-05-12 21:31:49','Cybroon',5,'.add 40916','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2008211736)'),(887,'2016-05-12 21:31:59','Cybroon',5,'.lo i glyph of gift','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2008211976)'),(888,'2016-05-12 21:32:05','Cybroon',5,'.lo i glyph of mak','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2008788216)'),(889,'2016-05-12 21:32:06','Cybroon',5,'.lo i glyph of mark','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2009052776)'),(890,'2016-05-12 21:32:08','Cybroon',5,'.lo i glyph of wild','X: 16219.262695 Y: 16406.511719 Z: -64.378738 Map: 1','Cybroon (GUID: 2009053016)'),(891,'2016-05-12 21:32:32','Della',1,'.add 39894','X: 16220.895508 Y: 16406.183594 Z: -64.379501 Map: 1','Della (GUID: 2009053096)'),(892,'2016-05-12 21:32:39','Cybroon',5,'.lo i cowl of','X: 16219.232422 Y: 16406.511719 Z: -64.378738 Map: 1',' (GUID: 2009053416)'),(893,'2016-05-12 21:33:05','Cybroon',5,'.lo i cowl of abso','X: 16219.232422 Y: 16406.511719 Z: -64.378738 Map: 1','Della (GUID: 2009053256)'),(894,'2016-05-12 21:34:06','Della',1,'.add 10550','X: 16216.589844 Y: 16407.556641 Z: -64.380203 Map: 1','Transmogrification (GUID: 2007993208)'),(895,'2016-05-12 21:34:08','Cybroon',5,'.lo i insignia','X: 16219.232422 Y: 16406.511719 Z: -64.378738 Map: 1',' (GUID: 2009053416)'),(896,'2016-05-12 21:34:51','Della',1,'.te icecrowncitadel','X: 16218.937500 Y: 16408.720703 Z: -64.379211 Map: 1',' (GUID: 2009053576)'),(897,'2016-05-12 21:34:57','Della',1,'.summ','X: 5873.819824 Y: 2110.979980 Z: 636.010986 Map: 571',' (GUID: 2009055016)'),(898,'2016-05-12 21:35:08','Cybroon',5,'.lo i lightbr','X: 5871.069336 Y: 2109.782227 Z: 636.033875 Map: 571',' (GUID: 2009055096)'),(899,'2016-05-12 21:35:09','Della',1,'.gm on','X: 5792.508301 Y: 2075.571777 Z: 636.064697 Map: 571','Cybroon (GUID: 2009055176)'),(900,'2016-05-12 21:35:13','Della',1,'.gm off','X: 5792.508301 Y: 2075.571777 Z: 636.064697 Map: 571','Della (GUID: 2833840680)'),(901,'2016-05-12 21:35:16','Cybroon',5,'.add 30989','X: 5824.934570 Y: 2084.765869 Z: 636.066406 Map: 571',' (GUID: 2009053496)'),(902,'2016-05-12 21:35:34','Cybroon',5,'.lo i warp','X: 5792.659668 Y: 2071.144287 Z: 636.064392 Map: 571',' (GUID: 2902627384)'),(903,'2016-05-12 21:35:36','Cybroon',5,'.lo i warpeav','X: 5792.659668 Y: 2071.144287 Z: 636.064392 Map: 571',' (GUID: 2902627464)'),(904,'2016-05-12 21:35:38','Cybroon',5,'.lo crea warp','X: 5792.659668 Y: 2071.144287 Z: 636.064392 Map: 571',' (GUID: 2902627544)'),(905,'2016-05-12 21:35:39','Cybroon',5,'.lo crea warpea','X: 5789.142090 Y: 2069.934814 Z: 636.064392 Map: 571',' (GUID: 2902628104)'),(906,'2016-05-12 21:36:43','Cybroon',5,'.maxskill','X: -95.438400 Y: 2220.144531 Z: 27.902618 Map: 631','Cybroon (GUID: 1959459768)'),(907,'2016-05-12 21:37:15','Della',1,'.maxskill','X: -73.082886 Y: 2213.078613 Z: 27.923323 Map: 631','Della (GUID: 1959460248)'),(908,'2016-05-12 21:38:24','Cybroon',5,'.rev','X: -203.617798 Y: 2202.489746 Z: 35.233566 Map: 631',' (GUID: 2008786776)'),(909,'2016-05-12 21:38:28','Cybroon',5,'.rev','X: -196.981110 Y: 2209.577637 Z: 35.233566 Map: 631','Cybroon (GUID: 2008787096)'),(910,'2016-05-12 21:39:52','Cybroon',5,'.gm on','X: -110.975067 Y: 2208.620361 Z: 33.474213 Map: 631',' (GUID: 1968519800)'),(911,'2016-05-12 21:39:54','Della',1,'.rev','X: -127.986549 Y: 2203.305908 Z: 35.233555 Map: 631','Della (GUID: 1968519560)'),(912,'2016-05-12 21:40:06','Cybroon',5,'.gm off','X: -144.635086 Y: 2210.268066 Z: 35.233753 Map: 631','The Damned (GUID: 1975580056)'),(913,'2016-05-12 21:40:59','Della',1,'.gm on','X: -149.571030 Y: 2210.924805 Z: 35.233654 Map: 631',' (GUID: 1959460328)'),(914,'2016-05-12 21:41:09','Cybroon',5,'.gm on','X: -175.948975 Y: 2213.065918 Z: 35.233829 Map: 631',' (GUID: 1959459048)'),(915,'2016-05-12 21:41:14','Cybroon',5,'.gm fly on','X: -205.398315 Y: 2210.838623 Z: 35.233551 Map: 631',' (GUID: 1959458888)'),(916,'2016-05-12 21:41:16','Cybroon',5,'.mod sp 10','X: -209.985123 Y: 2210.614014 Z: 37.932106 Map: 631',' (GUID: 2172493368)'),(917,'2016-05-12 21:41:38','Della',1,'.gm off','X: -356.476562 Y: 2208.290039 Z: 42.344070 Map: 631','Lord Marrowgar (GUID: 1968212856)'),(918,'2016-05-12 21:41:40','Della',1,'.gm on','X: -356.476562 Y: 2208.290039 Z: 42.344070 Map: 631','Lord Marrowgar (GUID: 1968212856)'),(919,'2016-05-12 21:43:10','Cybroon',5,'.gm off','X: -106.509476 Y: 2211.414795 Z: 42.739304 Map: 631','Ebon Champion (GUID: 1968319928)'),(920,'2016-05-12 21:43:13','Cybroon',5,'.gm on','X: -106.509476 Y: 2211.414795 Z: 42.739304 Map: 631','Ebon Champion (GUID: 1968319928)'),(921,'2016-05-12 21:43:46','Della',1,'.resp','X: -125.812820 Y: 2208.726074 Z: 35.233719 Map: 631','The Damned (GUID: 1968319928)'),(922,'2016-05-12 21:43:49','Della',1,'.resp','X: -125.812820 Y: 2208.726074 Z: 35.233841 Map: 631',' (GUID: 1968519160)'),(923,'2016-05-12 21:44:52','Cybroon',5,'.np move','X: -131.082352 Y: 2228.674072 Z: 35.233803 Map: 631','Deathbound Ward (GUID: 2010829624)'),(924,'2016-05-12 21:45:10','Cybroon',5,'.npc yell kokot tu som','X: -114.543633 Y: 2225.347168 Z: 33.189095 Map: 631','Deathbound Ward (GUID: 2010829624)'),(925,'2016-05-12 21:45:28','Cybroon',5,'.die','X: -44.633709 Y: 2171.434814 Z: 19.532511 Map: 631','Deathbound Ward (GUID: 2010829720)'),(926,'2016-05-12 21:45:30','Cybroon',5,'.die','X: -44.633709 Y: 2171.434814 Z: 19.532511 Map: 631','Deathbound Ward (GUID: 2010829816)'),(927,'2016-05-12 21:45:52','Cybroon',5,'.respawn','X: -73.541573 Y: 2167.417969 Z: 20.316185 Map: 631','Deathbound Ward (GUID: 1968323192)'),(928,'2016-05-12 21:45:56','Cybroon',5,'.respawn','X: -73.541573 Y: 2167.417969 Z: 20.316185 Map: 631','Deathbound Ward (GUID: 1968323288)'),(929,'2016-05-12 21:46:03','Cybroon',5,'.np move','X: -133.286911 Y: 2228.444824 Z: 35.233582 Map: 631','Deathbound Ward (GUID: 1968323480)'),(930,'2016-05-12 21:46:05','Della',1,'.rev','X: -124.766106 Y: 2225.595703 Z: 35.233501 Map: 631','Della (GUID: 2008211576)'),(931,'2016-05-12 21:46:16','Cybroon',5,'.np mov','X: -129.291962 Y: 2227.979248 Z: 35.233601 Map: 631','Deathbound Ward (GUID: 1972638328)'),(932,'2016-05-12 21:46:22','Cybroon',5,'.respawn','X: -129.291962 Y: 2227.979248 Z: 35.233601 Map: 631','Deathbound Ward (GUID: 1972638328)'),(933,'2016-05-12 21:46:49','Della',1,'.resp','X: -125.223259 Y: 2214.167480 Z: 35.233715 Map: 631','Della (GUID: 2008211816)'),(934,'2016-05-12 21:47:31','Cybroon',5,'.die','X: -52.588108 Y: 2142.472412 Z: 17.658508 Map: 631','Deathbound Ward (GUID: 1960434648)'),(935,'2016-05-12 21:47:42','Cybroon',5,'.np move','X: -131.916565 Y: 2197.049072 Z: 35.233551 Map: 631','Deathbound Ward (GUID: 1960434648)'),(936,'2016-05-12 21:47:46','Cybroon',5,'.respawn','X: -131.916565 Y: 2197.049072 Z: 35.233551 Map: 631','Deathbound Ward (GUID: 1960434744)'),(937,'2016-05-12 21:47:53','Della',1,'.resp','X: -127.329445 Y: 2196.296387 Z: 35.233570 Map: 631','Deathbound Ward (GUID: 1960434744)'),(938,'2016-05-12 21:47:55','Cybroon',5,'.gm off','X: -120.206879 Y: 2198.135742 Z: 34.710548 Map: 631','Deathbound Ward (GUID: 1960434744)'),(939,'2016-05-12 21:48:02','Cybroon',5,'.mod sp 1','X: -93.440422 Y: 2209.511230 Z: 27.902611 Map: 631',' (GUID: 2008211896)'),(940,'2016-05-12 21:48:31','Della',1,'.resp','X: -89.529633 Y: 2219.883301 Z: 27.902637 Map: 631','Deathbound Ward (GUID: 1960434744)'),(941,'2016-05-12 21:48:31','Cybroon',5,'.gm on','X: -72.596024 Y: 2212.208984 Z: 27.902645 Map: 631',' (GUID: 2008212216)'),(942,'2016-05-12 21:48:33','Della',1,'.resp','X: -89.529633 Y: 2219.883301 Z: 27.902637 Map: 631',' (GUID: 2008212616)'),(943,'2016-05-12 21:48:39','Cybroon',5,'.gm off','X: -66.649742 Y: 2210.874023 Z: 30.696339 Map: 631','Deathbound Ward (GUID: 1964732504)'),(944,'2016-05-12 21:50:26','Cybroon',5,'.gm fly off','X: -117.334282 Y: 2215.785400 Z: 35.234390 Map: 631',' (GUID: 2008786456)'),(945,'2016-05-12 21:51:32','Cybroon',5,'.rev','X: -149.524948 Y: 2210.909180 Z: 35.233730 Map: 631','Cybroon (GUID: 2008786456)'),(946,'2016-05-12 21:52:07','Sanka',1,'.gm on','X: 16223.746094 Y: 16253.853516 Z: 12.846000 Map: 1',' (GUID: 2009052856)'),(947,'2016-05-12 21:52:08','Sanka',1,'.app cybroon','X: 16223.746094 Y: 16253.853516 Z: 12.846000 Map: 1',' (GUID: 2009052936)'),(948,'2016-05-12 21:52:11','Sanka',1,'.gm off','X: -154.085526 Y: 2212.489014 Z: 35.233471 Map: 631',' (GUID: 2009053096)'),(949,'2016-05-12 21:52:12','Cybroon',5,'.group join Della Sanka','X: -156.742661 Y: 2210.210938 Z: 35.233646 Map: 631',' (GUID: 2009053336)'),(950,'2016-05-12 21:53:01','Sanka',1,'.rev','X: -176.610641 Y: 2208.123047 Z: 35.233471 Map: 631',' (GUID: 2009053416)'),(951,'2016-05-12 21:53:22','Sanka',1,'.rev','X: -119.998650 Y: 2213.581055 Z: 35.233536 Map: 631',' (GUID: 2009053096)'),(952,'2016-05-12 21:53:23','Cybroon',5,'.rev','X: -101.044579 Y: 2209.562988 Z: 30.298008 Map: 631','Cybroon (GUID: 2009053736)'),(953,'2016-05-12 21:53:50','Cybroon',5,'.rev','X: -177.520538 Y: 2207.908936 Z: 36.273380 Map: 631',' (GUID: 2009053976)'),(954,'2016-05-12 21:53:56','Sanka',1,'.gm on','X: -196.827011 Y: 2221.620117 Z: 35.233532 Map: 631','Ancient Skeletal Soldier (GUID: 1955924888)'),(955,'2016-05-12 21:54:37','Cybroon',5,'.rev','X: -218.506882 Y: 2211.446777 Z: 36.428307 Map: 631','Cybroon (GUID: 2009054296)'),(956,'2016-05-12 21:57:44','Sanka',1,'.die','X: -319.020630 Y: 2192.665527 Z: 41.994976 Map: 631','Servant of the Throne (GUID: 1958203032)'),(957,'2016-05-12 21:57:48','Sanka',1,'.gm off','X: -302.386566 Y: 2203.061279 Z: 41.993767 Map: 631',' (GUID: 2008788536)'),(958,'2016-05-12 21:58:12','Cybroon',5,'.rev','X: -242.024368 Y: 2215.491943 Z: 42.564442 Map: 631','Cybroon (GUID: 2008786936)'),(959,'2016-05-12 21:58:34','Cybroon',5,'.gm on','X: -346.503052 Y: 2211.562988 Z: 42.495895 Map: 631',' (GUID: 2008787016)'),(960,'2016-05-12 21:58:39','Cybroon',5,'.gm off','X: -354.001587 Y: 2210.048096 Z: 42.381733 Map: 631','Cybroon (GUID: 2008787256)'),(961,'2016-05-12 21:58:45','Sanka',1,'.rev','X: -382.874146 Y: 2211.669922 Z: 41.992172 Map: 631',' (GUID: 2008211576)'),(962,'2016-05-12 21:58:54','Sanka',1,'.rev','X: -378.467072 Y: 2207.259766 Z: 41.992172 Map: 631',' (GUID: 2008211976)'),(963,'2016-05-12 21:58:57','Cybroon',5,'.rev','X: -369.294647 Y: 2181.328369 Z: 41.761684 Map: 631','Cybroon (GUID: 2008212376)'),(964,'2016-05-12 21:59:05','Sanka',1,'.rev','X: -397.247009 Y: 2199.908936 Z: 41.992172 Map: 631',' (GUID: 2008212456)'),(965,'2016-05-12 21:59:09','Cybroon',5,'.rev','X: -369.709961 Y: 2173.172363 Z: 41.761684 Map: 631','Lord Marrowgar (GUID: 2007991480)'),(966,'2016-05-12 21:59:19','Cybroon',5,'.lo i alex','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631',' (GUID: 2008211816)'),(967,'2016-05-12 21:59:22','Sanka',1,'.lo item alex','X: -390.351959 Y: 2198.489990 Z: 41.992172 Map: 631','Lord Marrowgar (GUID: 2007167896)'),(968,'2016-05-12 21:59:28','Cybroon',5,'.add  |cffe6cc80|Hitem:12947:0:0:0:0:0:0:0:80|h[Alex\'s Ring of Audacity]|h|r 2','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631','Sanka (GUID: 2008212216)'),(969,'2016-05-12 21:59:39','Cybroon',5,'.group leader','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631','Cybroon (GUID: 2008786376)'),(970,'2016-05-12 21:59:41','Sanka',1,'.add 12947 2','X: -390.351959 Y: 2198.489990 Z: 41.992172 Map: 631','Sanka (GUID: 2008787576)'),(971,'2016-05-12 21:59:42','Cybroon',5,'.summ sanka','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631','Cybroon (GUID: 2008787656)'),(972,'2016-05-12 21:59:45','Cybroon',5,'.rev sanka','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631','Cybroon (GUID: 2008787896)'),(973,'2016-05-12 21:59:47','Cybroon',5,'.rev','X: -377.824249 Y: 2165.193848 Z: 41.764473 Map: 631','Cybroon (GUID: 2008788056)'),(974,'2016-05-12 21:59:51','Sanka',1,'.gm on','X: -367.450134 Y: 2164.969482 Z: 41.763874 Map: 631','Sanka (GUID: 2008788136)'),(975,'2016-05-12 21:59:52','Cybroon',5,'.freeze','X: -377.704163 Y: 2157.139648 Z: 41.783203 Map: 631','Sanka (GUID: 2008788216)'),(976,'2016-05-12 21:59:56','Cybroon',5,'.rev','X: -377.704163 Y: 2157.139648 Z: 41.783203 Map: 631',' (GUID: 2008788376)'),(977,'2016-05-12 22:00:02','Cybroon',5,'.unfreeze','X: -363.581726 Y: 2169.960938 Z: 41.761616 Map: 631','Sanka (GUID: 2008211656)'),(978,'2016-05-12 22:00:11','Cybroon',5,'.rev','X: -347.286682 Y: 2198.926025 Z: 42.515411 Map: 631',' (GUID: 2009052776)'),(979,'2016-05-12 22:00:15','Sanka',1,'.rev','X: -370.980621 Y: 2185.656982 Z: 41.566147 Map: 631','Sanka (GUID: 2009052856)'),(980,'2016-05-12 22:00:19','Cybroon',5,'.gm on','X: -349.447754 Y: 2196.824219 Z: 42.497295 Map: 631','Lord Marrowgar (GUID: 2010831448)'),(981,'2016-05-12 22:00:19','Sanka',1,'.gm off','X: -357.218018 Y: 2191.917969 Z: 41.759727 Map: 631',' (GUID: 2009053016)'),(982,'2016-05-12 22:00:32','Cybroon',5,'.gm off','X: -353.983551 Y: 2202.276611 Z: 42.408615 Map: 631','Cybroon (GUID: 2009053096)'),(983,'2016-05-12 22:00:54','Cybroon',5,'.rev','X: -351.997986 Y: 2213.467773 Z: 47.927502 Map: 631','Cybroon (GUID: 2008212216)'),(984,'2016-05-12 22:00:54','Sanka',1,'.rev','X: -355.016846 Y: 2212.051514 Z: 42.365879 Map: 631',' (GUID: 2008212536)'),(985,'2016-05-12 22:01:07','Sanka',1,'.rev','X: -374.666107 Y: 2216.953613 Z: 41.971321 Map: 631','Cybroon (GUID: 2008787736)'),(986,'2016-05-12 22:01:11','Cybroon',5,'.repair','X: -357.251007 Y: 2227.146973 Z: 41.758633 Map: 631','Cybroon (GUID: 2009053336)'),(987,'2016-05-12 22:01:33','Cybroon',5,'.die','X: -350.827820 Y: 2211.402832 Z: 47.944981 Map: 631',' (GUID: 1964729432)'),(988,'2016-05-12 22:02:28','Cybroon',5,'.die','X: -390.136230 Y: 2266.239258 Z: 49.059658 Map: 631',' (GUID: 1960127096)'),(989,'2016-05-12 22:02:32','Cybroon',5,'.rev','X: -390.122223 Y: 2266.252686 Z: 45.560432 Map: 631','Cybroon (GUID: 2008786936)'),(990,'2016-05-12 22:02:32','Sanka',1,'.rev','X: -394.258026 Y: 2249.426025 Z: 41.959152 Map: 631','Cybroon (GUID: 2008787256)'),(991,'2016-05-12 22:02:45','Cybroon',5,'.rev','X: -388.761200 Y: 2270.745850 Z: 47.468174 Map: 631','Bone Spike (GUID: 1960431768)'),(992,'2016-05-12 22:02:47','Cybroon',5,'.die','X: -388.761200 Y: 2270.745850 Z: 47.468174 Map: 631',' (GUID: 1962516120)'),(993,'2016-05-12 22:03:02','Cybroon',5,'.die','X: -400.650482 Y: 2255.960449 Z: 47.462105 Map: 631',' (GUID: 1975581976)'),(994,'2016-05-12 22:03:08','Sanka',1,'.rev','X: -392.407288 Y: 2248.772705 Z: 41.956593 Map: 631','Cybroon (GUID: 2009053496)'),(995,'2016-05-12 22:03:58','Cybroon',5,'.die','X: -389.143951 Y: 2272.000244 Z: 47.487595 Map: 631',' (GUID: 1964735672)'),(996,'2016-05-12 22:04:18','Cybroon',5,'.die','X: -388.949554 Y: 2271.518066 Z: 47.479191 Map: 631',' (GUID: 1960124216)'),(997,'2016-05-12 22:04:25','Cybroon',5,'.rev','X: -388.764832 Y: 2272.288574 Z: 41.967976 Map: 631','Cybroon (GUID: 2009053656)'),(998,'2016-05-12 22:04:45','Cybroon',5,'.te gmk','X: -396.359772 Y: 2268.824219 Z: 42.203915 Map: 631',' (GUID: 2009053736)'),(999,'2016-05-12 22:05:02','Cybroon',5,'.lo i transmog token','X: 16211.370117 Y: 16404.615234 Z: -64.378059 Map: 1',' (GUID: 2902627544)'),(1000,'2016-05-12 22:05:03','Cybroon',5,'.add 5407','X: 16211.370117 Y: 16404.615234 Z: -64.378059 Map: 1',' (GUID: 2059760936)'),(1001,'2016-05-12 22:05:22','Della',1,'.app','X: -8943.360352 Y: -132.492996 Z: 83.682678 Map: 0',' (GUID: 2902629944)'),(1002,'2016-05-12 22:05:27','Della',1,'.lo i alex','X: 16212.445312 Y: 16404.117188 Z: -64.379478 Map: 1','Cybroon (GUID: 2902630024)'),(1003,'2016-05-12 22:17:15','Domino',5,'.te gmk','X: 16224.480469 Y: 16271.159180 Z: 12.991597 Map: 1',' (GUID: 967673448)'),(1004,'2016-05-14 03:39:53','Della',1,'.gm off','X: 16230.871094 Y: 16393.705078 Z: -64.378479 Map: 1','GM OUTFIT (GUID: 455456152)'),(1005,'2016-05-14 03:40:53','Della',1,'.lo i royal','X: 16230.856445 Y: 16393.701172 Z: -64.378479 Map: 1','GM OUTFIT (GUID: 455456920)'),(1006,'2016-05-14 04:30:00','Mew',1,'.app della','X: 16230.144531 Y: 16399.025391 Z: -64.378845 Map: 1',' (GUID: 4241570104)'),(1007,'2016-05-14 04:30:17','Mew',1,'.add  |cffffffff|Hitem:23162:0:0:0:0:0:0:0:80|h[Foror\'s Crate of Endless Resist Gear Storage]|h|r 7','X: -8844.663086 Y: 607.332275 Z: 92.630119 Map: 0','Della (GUID: 4241570424)'),(1008,'2016-05-15 18:35:05','Cybroon',10,'.gm i','X: 16225.930664 Y: 16397.523438 Z: -64.378990 Map: 1','Grandmaster\'s Training Dummy (GUID: 4285775096)'),(1009,'2016-05-15 18:35:09','Cybroon',10,'.gm on','X: 16225.930664 Y: 16397.523438 Z: -64.378532 Map: 1','Grandmaster\'s Training Dummy (GUID: 4285775096)'),(1010,'2016-05-15 18:35:10','Cybroon',10,'.gm i','X: 16225.930664 Y: 16397.523438 Z: -64.378532 Map: 1','Grandmaster\'s Training Dummy (GUID: 4285775096)'),(1011,'2016-05-15 18:35:24','Cybroon',10,'.gm off','X: 16225.151367 Y: 16397.939453 Z: -64.374168 Map: 1',' (GUID: 4285770840)'),(1012,'2016-05-15 18:39:21','Cybroon',10,'.te gmi','X: 16214.338867 Y: 16408.013672 Z: -64.378494 Map: 1',' (GUID: 898086424)'),(1013,'2016-05-15 18:40:52','Cybroon',10,'.respawn','X: 16247.908203 Y: 16262.596680 Z: 16.624737 Map: 1','Volatile Ooze (GUID: 957048)'),(1014,'2016-05-15 18:40:57','Cybroon',10,'.respawn','X: 16240.083008 Y: 16266.809570 Z: 14.206381 Map: 1','Volatile Ooze (GUID: 957144)'),(1015,'2016-05-15 18:42:49','Cybroon',10,'.respawn','X: 16236.004883 Y: 16264.547852 Z: 13.890631 Map: 1','Volatile Ooze (GUID: 4254441304)'),(1016,'2016-05-15 18:42:59','Cybroon',10,'.die','X: 16235.756836 Y: 16264.614258 Z: 13.836282 Map: 1','Volatile Ooze (GUID: 4260840728)'),(1017,'2016-05-15 18:43:02','Cybroon',10,'.respawn','X: 16235.756836 Y: 16264.614258 Z: 13.836282 Map: 1','Volatile Ooze (GUID: 4057784)'),(1018,'2016-05-15 18:51:47','Cybroon',10,'.lo spell blood elf female illusion','X: 16250.706055 Y: 16276.693359 Z: 13.951927 Map: 1','Spork (GUID: 4253831368)'),(1019,'2016-05-15 18:52:04','Cybroon',10,'.aura 37806','X: 16254.089844 Y: 16276.362305 Z: 14.543523 Map: 1','Spork (GUID: 4253831448)'),(1020,'2016-05-15 18:52:56','Cybroon',10,'.gm off','X: 16218.797852 Y: 16266.600586 Z: 13.295139 Map: 1',' (GUID: 4253832088)'),(1021,'2016-05-15 18:56:26','Cybroon',10,'.app della','X: 16224.385742 Y: 16256.734375 Z: 13.081405 Map: 1',' (GUID: 897752392)'),(1022,'2016-05-15 18:56:42','Cybroon',10,'.app della','X: -9344.617188 Y: 40.634556 Z: 61.486767 Map: 0',' (GUID: 4190919128)'),(1023,'2016-05-15 19:01:14','Cybroon',10,'.rev','X: -171.314148 Y: 2223.110840 Z: 35.233570 Map: 631','Cybroon (GUID: 4248611048)'),(1024,'2016-05-15 19:01:19','Cybroon',10,'.rev','X: -164.300018 Y: 2224.723633 Z: 35.232975 Map: 631','Della (GUID: 4248610408)'),(1025,'2016-05-15 19:02:40','Cybroon',10,'.rev','X: -170.840729 Y: 2231.343262 Z: 36.289631 Map: 631','Cybroon (GUID: 4253833848)'),(1026,'2016-05-15 19:02:43','Cybroon',10,'.repair','X: -178.565659 Y: 2235.954102 Z: 35.233818 Map: 631','Cybroon (GUID: 4253833768)'),(1027,'2016-05-15 19:02:46','Cybroon',10,'.repair','X: -178.565659 Y: 2235.954102 Z: 35.233818 Map: 631','Della (GUID: 4253831288)'),(1028,'2016-05-15 19:12:39','Cybroon',10,'.rev','X: -280.458893 Y: 2223.186279 Z: 42.564919 Map: 631','Cybroon (GUID: 4253831448)'),(1029,'2016-05-15 19:13:02','Cybroon',10,'.rev','X: -281.852936 Y: 2218.981201 Z: 42.564484 Map: 631',' (GUID: 4253831768)'),(1030,'2016-05-15 19:13:43','Cybroon',10,'.gm on','X: -345.426514 Y: 2216.273193 Z: 42.528267 Map: 631','Cybroon (GUID: 4253831448)'),(1031,'2016-05-15 19:15:44','Cybroon',10,'.lo spell divine shield','X: -357.621704 Y: 2216.676025 Z: 42.334839 Map: 631','Lord Marrowgar (GUID: 4232265720)'),(1032,'2016-05-15 19:16:08','Cybroon',10,'.learn 40733','X: -357.621704 Y: 2216.676025 Z: 42.334839 Map: 631','Cybroon (GUID: 4253833528)'),(1033,'2016-05-15 19:16:10','Cybroon',10,'.learn 40733','X: -357.621704 Y: 2216.676025 Z: 42.334839 Map: 631','Della (GUID: 4253833688)'),(1034,'2016-05-15 19:16:20','Cybroon',10,'.gm off','X: -360.826172 Y: 2223.553467 Z: 41.758587 Map: 631',' (GUID: 4253834088)'),(1035,'2016-05-15 19:20:06','Cybroon',10,'.rev','X: -491.141296 Y: 2235.846436 Z: 62.302395 Map: 631',' (GUID: 4253833768)'),(1036,'2016-05-15 19:21:03','Cybroon',10,'.mod hp 99999999999','X: -579.415100 Y: 2199.192627 Z: 49.476685 Map: 631','Cybroon (GUID: 4253833928)'),(1037,'2016-05-15 19:21:11','Cybroon',10,'.cooldown','X: -578.886963 Y: 2198.920654 Z: 49.476685 Map: 631','Cybroon (GUID: 4253834088)'),(1038,'2016-05-15 19:27:46','Cybroon',10,'.np i','X: -352.115204 Y: 1944.604370 Z: 255.421371 Map: 631','High Overlord Saurfang (GUID: 4232262168)'),(1039,'2016-05-15 19:28:56','Cybroon',10,'.damage 650000','X: -431.487213 Y: 2426.830322 Z: 472.300293 Map: 631','The Skybreaker (GUID: 4245618616)'),(1040,'2016-05-15 19:29:19','Cybroon',10,'.damage 10000','X: -431.487213 Y: 2426.830322 Z: 472.300293 Map: 631','The Skybreaker (GUID: 4193090456)'),(1041,'2016-05-15 19:30:22','Cybroon',10,'.te gmi','X: -543.080505 Y: 2217.452881 Z: 539.292725 Map: 631',' (GUID: 4253834968)'),(1042,'2016-05-16 11:02:54','Domino',5,'.lo i wrathful','X: 16232.321289 Y: 16389.550781 Z: -64.378899 Map: 1',' (GUID: 3220350568)'),(1043,'2016-05-16 11:02:58','Domino',5,'.add 50435','X: 16232.321289 Y: 16389.550781 Z: -64.378899 Map: 1',' (GUID: 3220350808)'),(1044,'2016-05-16 11:03:43','Domino',5,'.gm on','X: 16216.712891 Y: 16402.294922 Z: -64.380127 Map: 1','Trainer (GUID: 3220764888)'),(1045,'2016-05-16 11:03:51','Domino',5,'.te gmi','X: 16231.360352 Y: 16397.253906 Z: -64.378937 Map: 1','Trainer (GUID: 3220764888)'),(1046,'2016-05-16 11:04:28','Domino',5,'.te dalaran','X: 16235.762695 Y: 16252.103516 Z: 14.745522 Map: 1',' (GUID: 3220351048)'),(1047,'2016-05-16 11:06:37','Domino',5,'.cast 71810','X: 5793.087402 Y: 619.683411 Z: 647.392517 Map: 571',' (GUID: 3189764776)'),(1048,'2016-05-16 11:06:41','Domino',5,'.cast 71810','X: 5786.602051 Y: 615.799011 Z: 647.296936 Map: 571',' (GUID: 3189764936)'),(1049,'2016-05-16 11:07:01','Domino',5,'.te gmi','X: 5778.606934 Y: 701.754578 Z: 641.835510 Map: 571',' (GUID: 3189765016)'),(1050,'2016-05-16 11:07:05','Domino',5,'.te gmi','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1',' (GUID: 3189765096)'),(1051,'2016-05-16 11:07:07','Domino',5,'.cast 71810','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1',' (GUID: 3189765176)'),(1052,'2016-05-16 11:07:19','Domino',5,'.te dalaran','X: 16217.707031 Y: 16258.266602 Z: 13.478566 Map: 1',' (GUID: 3189477080)'),(1053,'2016-05-16 11:07:45','Domino',5,'.mod sp 10','X: 5796.193359 Y: 626.774963 Z: 647.392395 Map: 571',' (GUID: 3189765256)'),(1054,'2016-05-16 11:10:02','Domino',5,'.cast 71810','X: 5801.386230 Y: 459.931030 Z: 658.770325 Map: 571',' (GUID: 3171560296)'),(1055,'2016-05-16 11:11:56','Domino',5,'.lo spell invincible','X: 5806.740234 Y: 447.225067 Z: 658.759277 Map: 571',' (GUID: 3189765256)'),(1056,'2016-05-16 11:12:00','Domino',5,'.cast 72281','X: 5809.125488 Y: 441.337738 Z: 658.761353 Map: 571',' (GUID: 3189765336)'),(1057,'2016-05-16 11:12:13','Domino',5,'.cast 72286','X: 5813.139648 Y: 431.429993 Z: 658.772156 Map: 571',' (GUID: 3189765416)'),(1058,'2016-05-16 11:12:24','Domino',5,'.cast 72284','X: 5813.951172 Y: 410.420013 Z: 660.087341 Map: 571',' (GUID: 3189765496)'),(1059,'2016-05-16 11:12:31','Domino',5,'.cast 72282','X: 5830.757812 Y: 449.138519 Z: 658.771912 Map: 571',' (GUID: 3189765576)'),(1060,'2016-05-16 11:12:43','Domino',5,'.cast 72283','X: 5828.741211 Y: 465.325043 Z: 658.777222 Map: 571',' (GUID: 3189765656)'),(1061,'2016-05-16 11:12:52','Domino',5,'.cast 72286','X: 5816.698730 Y: 453.445190 Z: 658.757019 Map: 571',' (GUID: 3189765736)'),(1062,'2016-05-16 11:12:54','Domino',5,'.cast 72286','X: 5819.571289 Y: 459.742340 Z: 658.765808 Map: 571',' (GUID: 3189765816)'),(1063,'2016-05-16 11:19:23','Domino',5,'.lo spell frost wyrm','X: 5799.079102 Y: 463.325226 Z: 661.164551 Map: 571',' (GUID: 3189765896)'),(1064,'2016-05-16 11:19:52','Domino',5,'.cast 59319','X: 5803.179199 Y: 422.551453 Z: 658.651550 Map: 571',' (GUID: 3189765976)'),(1065,'2016-05-16 11:19:58','Domino',5,'.aura 59319','X: 5803.179199 Y: 422.551453 Z: 658.651367 Map: 571',' (GUID: 3189766056)'),(1066,'2016-05-16 11:20:02','Domino',5,'.unaura 59319','X: 5803.179199 Y: 422.551453 Z: 658.651367 Map: 571',' (GUID: 3189766136)'),(1067,'2016-05-16 11:20:32','Domino',5,'.aura 43810','X: 5802.420410 Y: 416.252014 Z: 657.589478 Map: 571',' (GUID: 3189766216)'),(1068,'2016-05-16 11:21:01','Domino',5,'.aura 51960','X: 5601.183594 Y: 230.855743 Z: 579.736572 Map: 571',' (GUID: 3189766296)'),(1069,'2016-05-16 11:21:37','Domino',5,'.cast 71810','X: 5815.077637 Y: 445.060852 Z: 658.755676 Map: 571',' (GUID: 3171108936)'),(1070,'2016-05-16 11:24:17','Domino',5,'.te gmi','X: 5842.114258 Y: 436.044464 Z: 657.677734 Map: 571',' (GUID: 3189766376)'),(1071,'2016-05-16 11:24:24','Domino',5,'.mod sp 1','X: 16217.728516 Y: 16234.685547 Z: 8.553318 Map: 1',' (GUID: 3202203848)'),(1072,'2016-05-16 11:24:31','Domino',5,'.cast 71810','X: 16217.412109 Y: 16229.570312 Z: 6.863729 Map: 1',' (GUID: 3170997384)'),(1073,'2016-05-16 12:19:15','Domino',5,'.aura  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16215.654297 Y: 16263.889648 Z: 13.669580 Map: 1',' (GUID: 1786332776)'),(1074,'2016-05-16 12:19:43','Domino',5,'.cast 71810','X: 16205.882812 Y: 16253.224609 Z: 19.303442 Map: 1',' (GUID: 1786332776)'),(1075,'2016-05-16 12:19:54','Domino',5,'.te dalaran','X: 16231.108398 Y: 16271.624023 Z: 13.329068 Map: 1',' (GUID: 1786332856)'),(1076,'2016-05-16 12:20:19','Domino',5,'.unlearn  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5783.894531 Y: 607.594421 Z: 649.524109 Map: 571',' (GUID: 1755316744)'),(1077,'2016-05-16 12:20:22','Domino',5,'.learn 71810','X: 5779.033203 Y: 604.522827 Z: 650.242676 Map: 571',' (GUID: 1755316824)'),(1078,'2016-05-16 12:20:30','Domino',5,'.mod sp 10','X: 5792.150879 Y: 576.174316 Z: 649.924988 Map: 571',' (GUID: 1755316904)'),(1079,'2016-05-16 13:54:07','Domino',5,'.te gmi','X: 5835.185059 Y: 470.598297 Z: 657.921082 Map: 571',' (GUID: 1000459512)'),(1080,'2016-05-16 13:54:23','Domino',5,'.aura  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16225.898438 Y: 16260.802734 Z: 13.287703 Map: 1',' (GUID: 1001637160)'),(1081,'2016-05-16 13:54:42','Domino',5,'.te stormwind','X: 16223.923828 Y: 16254.153320 Z: 12.887477 Map: 1',' (GUID: 1019437256)'),(1082,'2016-05-16 13:55:02','Domino',5,'.cast 71810','X: -8830.136719 Y: 634.489319 Z: 94.328041 Map: 0',' (GUID: 935303080)'),(1083,'2016-05-16 15:29:59','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: -8843.075195 Y: 779.482178 Z: 95.845261 Map: 0',' (GUID: 3495715496)'),(1084,'2016-05-16 15:31:42','Domino',5,'.cast 71809','X: -8834.125977 Y: 825.770996 Z: 98.995903 Map: 0',' (GUID: 3495715496)'),(1085,'2016-05-16 15:31:45','Domino',5,'.cast 71809','X: -8842.821289 Y: 848.128113 Z: 99.960609 Map: 0',' (GUID: 3495715576)'),(1086,'2016-05-16 15:31:48','Domino',5,'.cast 71809','X: -8825.265625 Y: 876.520081 Z: 98.739731 Map: 0',' (GUID: 3495715656)'),(1087,'2016-05-16 15:31:52','Domino',5,'.cast 71809','X: -8837.257812 Y: 904.298584 Z: 97.709061 Map: 0',' (GUID: 3495715736)'),(1088,'2016-05-16 15:31:57','Domino',5,'.cast 71809 ','X: -8845.845703 Y: 918.847839 Z: 100.821594 Map: 0',' (GUID: 3495715816)'),(1089,'2016-05-16 15:32:05','Domino',5,'.aura 65126','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495715896)'),(1090,'2016-05-16 15:32:05','Domino',5,'.aura 72525','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495715976)'),(1091,'2016-05-16 15:32:05','Domino',5,'.aura 64112','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716056)'),(1092,'2016-05-16 15:32:05','Domino',5,'.aura 47008','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716136)'),(1093,'2016-05-16 15:32:05','Domino',5,'.aura 40733','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716216)'),(1094,'2016-05-16 15:32:05','Domino',5,'.aura 64238','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716296)'),(1095,'2016-05-16 15:32:05','Domino',5,'.aura 68378','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716376)'),(1096,'2016-05-16 15:32:05','Domino',5,'.aura 61715','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495523384)'),(1097,'2016-05-16 15:32:05','Domino',5,'.mod sp 8','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716456)'),(1098,'2016-05-16 15:32:05','Domino',5,'.mod scale 1','X: -8818.703125 Y: 950.997498 Z: 103.141991 Map: 0',' (GUID: 3495716536)'),(1099,'2016-05-16 15:32:12','Domino',5,'.unaura 65126','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495716616)'),(1100,'2016-05-16 15:32:12','Domino',5,'.unaura 72525','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495716696)'),(1101,'2016-05-16 15:32:12','Domino',5,'.unaura 64112','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495716776)'),(1102,'2016-05-16 15:32:12','Domino',5,'.unaura 47008','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495716856)'),(1103,'2016-05-16 15:32:12','Domino',5,'.unaura 40733','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495716936)'),(1104,'2016-05-16 15:32:12','Domino',5,'.unaura 64238','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495717016)'),(1105,'2016-05-16 15:32:12','Domino',5,'.unaura 68378','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495717096)'),(1106,'2016-05-16 15:32:12','Domino',5,'.unaura 61715','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495717176)'),(1107,'2016-05-16 15:32:12','Domino',5,'.mod sp 1','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495717256)'),(1108,'2016-05-16 15:32:12','Domino',5,'.mod scale 1','X: -8811.575195 Y: 947.157349 Z: 101.241615 Map: 0',' (GUID: 3495717336)'),(1109,'2016-05-16 15:32:25','Domino',5,'.cast 71811','X: -8787.500977 Y: 920.246582 Z: 100.126228 Map: 0',' (GUID: 3495717416)'),(1110,'2016-05-16 15:32:28','Domino',5,'.cast 71811','X: -8778.524414 Y: 910.219299 Z: 100.199959 Map: 0',' (GUID: 3495717496)'),(1111,'2016-05-16 16:01:21','Domino',5,'.cast 61983','X: -8757.003906 Y: 896.565430 Z: 101.903664 Map: 0',' (GUID: 931418632)'),(1112,'2016-05-16 16:13:41','Domino',5,'.server shutdown 0','X: -8618.091797 Y: 752.907715 Z: 96.711143 Map: 0',' (GUID: 931418712)'),(1113,'2016-05-16 16:15:15','Domino',5,'.mod sc 0.1','X: -8659.004883 Y: 745.492737 Z: 96.668221 Map: 0',' (GUID: 943309480)'),(1114,'2016-05-16 16:17:13','Domino',5,'.morph 30362','X: -8692.973633 Y: 718.084778 Z: 97.016853 Map: 0',' (GUID: 943309480)'),(1115,'2016-05-16 16:17:45','Domino',5,'.morph 25488','X: -8741.252930 Y: 702.371338 Z: 98.743034 Map: 0',' (GUID: 943309560)'),(1116,'2016-05-16 16:17:55','Domino',5,'.morph 27971','X: -8737.973633 Y: 696.602966 Z: 98.778130 Map: 0',' (GUID: 943309640)'),(1117,'2016-05-16 16:18:05','Domino',5,'.morph  30362','X: -8737.973633 Y: 696.602966 Z: 98.778145 Map: 0',' (GUID: 943309720)'),(1118,'2016-05-16 16:18:51','Domino',5,'.gm fly on','X: -8711.813477 Y: 653.126953 Z: 99.409538 Map: 0',' (GUID: 943309800)'),(1119,'2016-05-16 16:19:04','Domino',5,'.morph 30362','X: -8710.332031 Y: 649.859192 Z: 102.876312 Map: 0',' (GUID: 943309880)'),(1120,'2016-05-16 16:19:45','Domino',5,'.lo crea wyrm','X: -8702.485352 Y: 595.172302 Z: 126.162956 Map: 0',' (GUID: 943309880)'),(1121,'2016-05-16 16:20:19','Domino',5,'.te icecrowncitadel','X: -8734.484375 Y: 577.925476 Z: 100.183304 Map: 0',' (GUID: 922460920)'),(1122,'2016-05-16 16:20:28','Domino',5,'.demorph','X: 5864.505371 Y: 2105.995605 Z: 636.609497 Map: 571',' (GUID: 943311000)'),(1123,'2016-05-16 16:20:30','Domino',5,'.mod sc 1','X: 5864.505371 Y: 2105.995605 Z: 636.609497 Map: 571',' (GUID: 943311080)'),(1124,'2016-05-16 16:20:34','Domino',5,'.mod sp 10','X: 5848.590332 Y: 2100.237549 Z: 636.062317 Map: 571',' (GUID: 943311160)'),(1125,'2016-05-16 16:20:36','Domino',5,'.gm on','X: 5799.270996 Y: 2072.138428 Z: 636.062317 Map: 571',' (GUID: 943311240)'),(1126,'2016-05-16 16:20:57','Domino',5,'.np i','X: -469.950500 Y: 2319.476318 Z: 250.393372 Map: 631','Spire Frostwyrm (GUID: 977332728)'),(1127,'2016-05-16 16:21:13','Domino',5,'.np i','X: -344.011047 Y: 2240.928955 Z: 406.340668 Map: 631','Spire Frostwyrm (Ambient) (GUID: 884920472)'),(1128,'2016-05-16 16:21:22','Domino',5,'.morph 28982','X: -344.011047 Y: 2240.928955 Z: 406.340668 Map: 631','Domino (GUID: 893214344)'),(1129,'2016-05-16 16:21:30','Domino',5,'.morph 27982','X: -344.011047 Y: 2240.928955 Z: 406.340668 Map: 631','Domino (GUID: 893213544)'),(1130,'2016-05-16 16:21:54','Domino',5,'.np i','X: -337.809631 Y: 2098.289551 Z: 246.125824 Map: 631','Spire Frostwyrm (GUID: 884920568)'),(1131,'2016-05-16 16:22:06','Domino',5,'.gm fly off','X: -512.894043 Y: 2107.677490 Z: 199.969666 Map: 631','Domino (GUID: 943311800)'),(1132,'2016-05-16 16:22:09','Domino',5,'.gm fly on','X: -556.718506 Y: 2142.492676 Z: 201.452347 Map: 631','Domino (GUID: 943311880)'),(1133,'2016-05-16 16:22:13','Domino',5,'.gm fly off','X: -563.247192 Y: 2168.555176 Z: 199.970642 Map: 631','Domino (GUID: 943311960)'),(1134,'2016-05-16 16:22:26','Domino',5,'.morph 30902','X: -548.330139 Y: 2273.662354 Z: 199.969574 Map: 631','Domino (GUID: 943312040)'),(1135,'2016-05-16 16:22:42','Domino',5,'.np i','X: -361.126282 Y: 2318.968750 Z: 199.969528 Map: 631','Spire Gargoyle (GUID: 924652728)'),(1136,'2016-05-16 16:22:56','Domino',5,'.morph 14696','X: -361.126282 Y: 2318.968750 Z: 199.969528 Map: 631','Domino (GUID: 943312440)'),(1137,'2016-05-16 16:23:17','Domino',5,'.te gmi','X: -444.247894 Y: 2096.947021 Z: 191.244446 Map: 631',' (GUID: 943312520)'),(1138,'2016-05-16 16:23:20','Domino',5,'.demorph','X: 16226.200195 Y: 16257.000000 Z: 13.207838 Map: 1',' (GUID: 924767768)'),(1139,'2016-05-16 16:23:26','Domino',5,'.lo crea wyrm','X: 16226.200195 Y: 16257.000000 Z: 13.207838 Map: 1',' (GUID: 924767848)'),(1140,'2016-05-16 16:23:30','Domino',5,'.npc add 34225','X: 16226.200195 Y: 16257.000000 Z: 13.207838 Map: 1',' (GUID: 924768008)'),(1141,'2016-05-16 16:23:40','Domino',5,'.np del','X: 16223.250000 Y: 16254.141602 Z: 12.873738 Map: 1',' (GUID: 884925656)'),(1142,'2016-05-16 16:23:44','Domino',5,'.npc add 34425','X: 16223.250000 Y: 16254.141602 Z: 12.873738 Map: 1','Domino (GUID: 924768008)'),(1143,'2016-05-16 16:23:53','Domino',5,'.np i','X: 16227.517578 Y: 16252.109375 Z: 12.949783 Map: 1','Furious Gladiator\'s Frost Wyrm (GUID: 884925560)'),(1144,'2016-05-16 16:23:54','Domino',5,'.np i','X: 16227.517578 Y: 16252.109375 Z: 12.949783 Map: 1','Furious Gladiator\'s Frost Wyrm (GUID: 884925560)'),(1145,'2016-05-16 16:23:59','Domino',5,'.morph 25593','X: 16227.517578 Y: 16252.109375 Z: 12.949783 Map: 1','Domino (GUID: 924768888)'),(1146,'2016-05-16 16:24:10','Domino',5,'.gm fly on','X: 16214.853516 Y: 16243.365234 Z: 10.732820 Map: 1','Domino (GUID: 924768968)'),(1147,'2016-05-16 16:24:22','Domino',5,'.np del','X: 16218.316406 Y: 16250.941406 Z: 12.534504 Map: 1',' (GUID: 884924888)'),(1148,'2016-05-16 16:24:49','Domino',5,'.demorph','X: 16218.316406 Y: 16250.941406 Z: 30.104506 Map: 1',' (GUID: 929032664)'),(1149,'2016-05-16 16:24:53','Domino',5,'.gm fly off','X: 16218.316406 Y: 16250.941406 Z: 30.104506 Map: 1',' (GUID: 929033544)'),(1150,'2016-05-16 16:24:56','Domino',5,'.mod sp 1','X: 16223.595703 Y: 16262.736328 Z: 13.275723 Map: 1',' (GUID: 929033304)'),(1151,'2016-05-16 16:24:57','Domino',5,'.server shutdown 0','X: 16223.595703 Y: 16262.736328 Z: 13.275723 Map: 1',' (GUID: 929033224)'),(1152,'2016-05-16 16:26:14','Domino',5,'.te dalaran','X: 16243.645508 Y: 16254.754883 Z: 18.104698 Map: 1',' (GUID: 2200770152)'),(1153,'2016-05-17 10:46:12','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5885.609375 Y: 564.759583 Z: 640.059326 Map: 571',' (GUID: 3790613192)'),(1154,'2016-05-17 10:46:15','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5884.555176 Y: 570.404541 Z: 640.629700 Map: 571',' (GUID: 3790613192)'),(1155,'2016-05-17 10:46:19','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5876.384766 Y: 579.553223 Z: 644.684143 Map: 571',' (GUID: 3790613192)'),(1156,'2016-05-17 10:46:23','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5858.379395 Y: 599.715820 Z: 650.963684 Map: 571',' (GUID: 3790613192)'),(1157,'2016-05-17 10:46:28','Domino',5,'.aura  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 5859.172852 Y: 624.125916 Z: 648.750671 Map: 571',' (GUID: 3790613192)'),(1158,'2016-05-17 10:46:52','Domino',5,'.np mov','X: 5909.880371 Y: 691.656738 Z: 642.385132 Map: 571','Teleporter (GUID: 3789890840)'),(1159,'2016-05-17 10:47:15','Domino',5,'.np del','X: 5875.061523 Y: 716.755310 Z: 643.115173 Map: 571',' (GUID: 3789890936)'),(1160,'2016-05-17 10:47:17','Domino',5,'.te gmi','X: 5875.061523 Y: 716.755310 Z: 643.114380 Map: 571',' (GUID: 3790613912)'),(1161,'2016-05-17 10:47:32','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16226.200195 Y: 16257.000000 Z: 13.202200 Map: 1',' (GUID: 3790614232)'),(1162,'2016-05-17 10:47:39','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16227.526367 Y: 16260.202148 Z: 13.371342 Map: 1',' (GUID: 3790614232)'),(1163,'2016-05-17 10:52:17','Domino',5,'.npc add 38545','X: 16233.071289 Y: 16253.589844 Z: 13.903708 Map: 1',' (GUID: 3789866520)'),(1164,'2016-05-17 10:52:25','Domino',5,'.np del','X: 16222.794922 Y: 16246.649414 Z: 11.783497 Map: 1',' (GUID: 3764096856)'),(1165,'2016-05-17 11:03:50','Domino',5,'.unlearn  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16226.819336 Y: 16251.115234 Z: 12.773942 Map: 1',' (GUID: 3789866840)'),(1166,'2016-05-17 11:03:55','Domino',5,'.learn 71810','X: 16226.819336 Y: 16251.115234 Z: 12.773942 Map: 1',' (GUID: 3789866920)'),(1167,'2016-05-17 11:04:03','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16224.423828 Y: 16265.097656 Z: 13.214936 Map: 1',' (GUID: 3789867000)'),(1168,'2016-05-17 11:04:08','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16214.478516 Y: 16261.781250 Z: 14.044962 Map: 1',' (GUID: 3789867000)'),(1169,'2016-05-17 11:04:29','Domino',5,'.te stormwind','X: 16373.849609 Y: 15859.215820 Z: 32.248276 Map: 1',' (GUID: 3789867000)'),(1170,'2016-05-17 11:04:55','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: -8833.379883 Y: 628.627991 Z: 94.008232 Map: 0',' (GUID: 3721411672)'),(1171,'2016-05-17 11:05:06','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: -8926.091797 Y: 533.177612 Z: 116.023415 Map: 0',' (GUID: 3721411672)'),(1172,'2016-05-17 11:10:03','Domino',5,'.cast 1','X: -9402.839844 Y: 105.200897 Z: 59.214062 Map: 0',' (GUID: 3789866680)'),(1173,'2016-05-17 11:10:07','Domino',5,'.cast 1','X: -9415.484375 Y: 91.109802 Z: 57.526848 Map: 0',' (GUID: 3789866440)'),(1174,'2016-05-17 11:10:43','Domino',5,'.cast 22','X: 10317.715820 Y: 839.154480 Z: 1326.525146 Map: 1',' (GUID: 3723342824)'),(1175,'2016-05-17 11:10:57','Domino',5,'.cast 5','X: 10328.338867 Y: 849.071045 Z: 1327.113770 Map: 1',' (GUID: 3716252728)'),(1176,'2016-05-17 11:11:01','Domino',5,'.rev','X: 10328.338867 Y: 849.071045 Z: 1327.113770 Map: 1',' (GUID: 3723342824)'),(1177,'2016-05-17 11:11:26','Domino',5,'.cast 24','X: 10364.033203 Y: 837.401489 Z: 1323.776611 Map: 1',' (GUID: 3723342904)'),(1178,'2016-05-17 11:14:39','Domino',5,'.cast 71810','X: 10351.487305 Y: 853.543640 Z: 1325.206055 Map: 1',' (GUID: 3721410072)'),(1179,'2016-05-17 11:14:54','Domino',5,'.cast 71810','X: 10389.341797 Y: 856.655579 Z: 1343.899902 Map: 1',' (GUID: 3721410152)'),(1180,'2016-05-17 11:17:34','Domino',5,'.lo i x-53','X: 10389.341797 Y: 856.655579 Z: 1343.899902 Map: 1',' (GUID: 3721408712)'),(1181,'2016-05-17 11:24:07','Domino',5,'.lo i x-53 Touring Rocket','X: 10389.341797 Y: 856.655579 Z: 1343.899902 Map: 1',' (GUID: 3721408792)'),(1182,'2016-05-17 11:24:14','Domino',5,'.npc add 4276','X: 10405.477539 Y: 854.334290 Z: 1320.943604 Map: 1',' (GUID: 3721408952)'),(1183,'2016-05-17 11:24:19','Domino',5,'.np del','X: 10404.588867 Y: 851.770874 Z: 1320.603394 Map: 1',' (GUID: 3721614520)'),(1184,'2016-05-17 11:30:13','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 10395.217773 Y: 844.599426 Z: 1319.565918 Map: 1',' (GUID: 3920188552)'),(1185,'2016-05-17 11:30:23','Domino',5,'.cast 71830','X: 10334.625000 Y: 847.703674 Z: 1326.033691 Map: 1',' (GUID: 3920188552)'),(1186,'2016-05-17 11:30:34','Domino',5,'.te gmi','X: 10398.461914 Y: 867.585876 Z: 1323.515381 Map: 1','Young Thistle Boar (GUID: 3919419512)'),(1187,'2016-05-17 11:31:34','Domino',5,'.cast 80864','X: 16240.049805 Y: 16263.264648 Z: 14.965421 Map: 1',' (GUID: 3920188712)'),(1188,'2016-05-17 11:31:38','Domino',5,'.cast 80864','X: 16252.410156 Y: 16270.000000 Z: 14.903122 Map: 1',' (GUID: 3920188792)'),(1189,'2016-05-17 11:31:52','Domino',5,'.cast 76560','X: 16262.853516 Y: 16278.338867 Z: 15.221107 Map: 1',' (GUID: 3920188872)'),(1190,'2016-05-17 11:31:57','Domino',5,'.cast 76560','X: 16267.833984 Y: 16273.032227 Z: 16.779808 Map: 1',' (GUID: 3920188952)'),(1191,'2016-05-17 11:32:19','Domino',5,'.cast 76567','X: 16267.343750 Y: 16271.653320 Z: 17.119303 Map: 1','Gas Cloud (GUID: 3920193560)'),(1192,'2016-05-17 11:32:22','Domino',5,'.cast 76567','X: 16275.079102 Y: 16272.698242 Z: 18.365831 Map: 1','Gas Cloud (GUID: 3920193560)'),(1193,'2016-05-17 11:32:52','Domino',5,'.cast 75613','X: 16276.775391 Y: 16266.619141 Z: 19.495382 Map: 1',' (GUID: 3920189192)'),(1194,'2016-05-17 11:33:02','Domino',5,'.np del','X: 16263.095703 Y: 16256.714844 Z: 20.283491 Map: 1',' (GUID: 3920193656)'),(1195,'2016-05-17 11:33:19','Domino',5,'.cast 75516','X: 16255.455078 Y: 16257.231445 Z: 19.660311 Map: 1',' (GUID: 3920189272)'),(1196,'2016-05-17 11:36:22','Domino',5,'.cast 71830','X: 16248.858398 Y: 16253.964844 Z: 20.144163 Map: 1',' (GUID: 3980620696)'),(1197,'2016-05-17 11:36:45','Domino',5,'.lo spell touring','X: 16213.346680 Y: 16256.880859 Z: 14.463124 Map: 1',' (GUID: 3917770872)'),(1198,'2016-05-17 11:36:48','Domino',5,'.cast 76154','X: 16213.346680 Y: 16256.880859 Z: 14.463124 Map: 1',' (GUID: 3917770952)'),(1199,'2016-05-17 11:36:55','Domino',5,'.dismount','X: 16211.294922 Y: 16258.772461 Z: 15.414589 Map: 1',' (GUID: 3917771032)'),(1200,'2016-05-17 11:36:58','Domino',5,'.cast 76154','X: 16211.294922 Y: 16258.772461 Z: 15.414589 Map: 1',' (GUID: 3917771112)'),(1201,'2016-05-17 11:43:17','Domino',5,'.server shutdown 0','X: 16227.927734 Y: 16248.003906 Z: 12.537399 Map: 1',' (GUID: 3917771192)'),(1202,'2016-05-17 11:43:45','Domino',5,'.cast  |cff71d5ff|Hspell:71810|h[Wrathful Gladiator\'s Frost Wyrm]|h|r','X: 16228.389648 Y: 16255.163086 Z: 13.299716 Map: 1',' (GUID: 4017731096)'),(1203,'2016-05-17 11:43:54','Domino',5,'.cast 71830','X: 16240.678711 Y: 16261.627930 Z: 15.506369 Map: 1',' (GUID: 4017731096)'),(1204,'2016-05-17 11:43:57','Domino',5,'.cast 71830','X: 16252.358398 Y: 16261.855469 Z: 17.377428 Map: 1',' (GUID: 4017731176)'),(1205,'2016-05-17 11:43:58','Domino',5,'.cast 71830','X: 16252.358398 Y: 16261.855469 Z: 17.377428 Map: 1',' (GUID: 4017731256)'),(1206,'2016-05-17 11:43:59','Domino',5,'.cast 71830','X: 16255.080078 Y: 16261.909180 Z: 18.629660 Map: 1',' (GUID: 4017731336)'),(1207,'2016-05-17 11:44:33','Domino',5,'.cast 71830','X: 16285.281250 Y: 16262.500000 Z: 21.580103 Map: 1',' (GUID: 4017731416)'),(1208,'2016-05-17 11:44:37','Domino',5,'.cast 71830','X: 16293.443359 Y: 16262.660156 Z: 21.316771 Map: 1',' (GUID: 4017731496)'),(1209,'2016-05-17 11:44:42','Domino',5,'.aura 71830','X: 16313.291016 Y: 16263.047852 Z: 21.571976 Map: 1',' (GUID: 4017731576)'),(1210,'2016-05-17 11:44:59','Domino',5,'.unaura 71830','X: 16288.208984 Y: 16261.356445 Z: 22.121241 Map: 1',' (GUID: 4017731656)'),(1211,'2016-05-17 11:45:02','Domino',5,'.lo spell invincible','X: 16281.846680 Y: 16259.454102 Z: 22.307005 Map: 1',' (GUID: 4017731736)'),(1212,'2016-05-17 11:45:21','Domino',5,'.cast 72284','X: 16268.195312 Y: 16255.372070 Z: 21.440315 Map: 1',' (GUID: 4017731816)'),(1213,'2016-05-17 11:45:41','Domino',5,'.cast 72286','X: 16218.672852 Y: 16255.966797 Z: 13.165025 Map: 1',' (GUID: 4017731896)'),(1214,'2016-05-17 11:45:44','Domino',5,'.cast 72286','X: 16224.836914 Y: 16254.871094 Z: 12.960060 Map: 1',' (GUID: 4017731976)'),(1215,'2016-05-17 11:49:43','Domino',5,'.lo spell mount','X: 16232.222656 Y: 16261.939453 Z: 13.639304 Map: 1','Event Vendor (GUID: 4017746392)'),(1216,'2016-05-17 11:50:08','Domino',5,'.cast 54964','X: 16247.386719 Y: 16252.332031 Z: 20.267456 Map: 1',' (GUID: 4017732136)'),(1217,'2016-05-17 11:50:29','Domino',5,'.cast 51757','X: 16214.732422 Y: 16229.270508 Z: 6.244981 Map: 1',' (GUID: 4017732216)'),(1218,'2016-05-17 11:50:54','Domino',5,'.cast 46980','X: 16243.706055 Y: 16267.089844 Z: 14.633095 Map: 1',' (GUID: 4017732296)'),(1219,'2016-05-17 11:51:16','Domino',5,'.cast 42929','X: 16213.477539 Y: 16252.811523 Z: 13.903324 Map: 1',' (GUID: 4017732376)'),(1220,'2016-05-17 11:51:39','Domino',5,'.cast 39949','X: 16220.014648 Y: 16258.916016 Z: 13.270264 Map: 1',' (GUID: 4017732456)'),(1221,'2016-05-17 11:52:44','Domino',5,'.cast 62063','X: 16237.713867 Y: 16307.474609 Z: 43.913483 Map: 1',' (GUID: 4017732536)'),(1222,'2016-05-17 11:54:18','Domino',5,'.gm on','X: 16229.200195 Y: 16256.982422 Z: 13.440133 Map: 1',' (GUID: 4017049960)'),(1223,'2016-05-17 11:54:46','Domino',5,'.group join Cybroon','X: 16239.563477 Y: 16252.578125 Z: 16.341915 Map: 1',' (GUID: 4017050040)'),(1224,'2016-05-17 11:55:01','Domino',5,'.cast 62063','X: 16243.409180 Y: 16254.266602 Z: 18.092268 Map: 1',' (GUID: 4017732536)'),(1225,'2016-05-17 11:55:20','Domino',5,'.gm off','X: 16243.409180 Y: 16254.266602 Z: 18.092268 Map: 1',' (GUID: 4027154952)'),(1226,'2016-05-17 11:55:27','Domino',5,'.cast 62063','X: 16242.648438 Y: 16253.778320 Z: 17.855282 Map: 1',' (GUID: 4017732536)'),(1227,'2016-05-17 11:55:41','Domino',5,'.dev off','X: 16242.648438 Y: 16253.778320 Z: 17.855282 Map: 1',' (GUID: 4017732856)'),(1228,'2016-05-17 11:56:00','Domino',5,'.cast 62063','X: 16243.213867 Y: 16253.671875 Z: 18.109707 Map: 1','Domino (GUID: 4017732696)'),(1229,'2016-05-17 11:56:11','Domino',5,'.lo spell dan','X: 16243.213867 Y: 16253.671875 Z: 18.109764 Map: 1',' (GUID: 4017050040)'),(1230,'2016-05-17 11:56:49','Domino',5,'.aura 54398','X: 16243.213867 Y: 16253.671875 Z: 18.109764 Map: 1',' (GUID: 4017050120)'),(1231,'2016-05-17 11:56:54','Domino',5,'.unaura 54398','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050200)'),(1232,'2016-05-17 11:57:16','Domino',5,'.cast 51238','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050280)'),(1233,'2016-05-17 11:57:29','Domino',5,'.unaura 51238','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050360)'),(1234,'2016-05-17 11:57:39','Domino',5,'.cast 52246','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050440)'),(1235,'2016-05-17 11:57:47','Domino',5,'.unaura 52246','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050520)'),(1236,'2016-05-17 11:58:01','Domino',5,'.cast 42387','X: 16243.213867 Y: 16253.671875 Z: 18.109444 Map: 1',' (GUID: 4017050600)'),(1237,'2016-05-17 11:58:42','Domino',5,'.cast 65431','X: 16240.556641 Y: 16253.168945 Z: 16.829515 Map: 1',' (GUID: 4017050680)'),(1238,'2016-05-17 11:59:34','Domino',5,'.lo i dan','X: 16240.187500 Y: 16253.619141 Z: 16.652700 Map: 1',' (GUID: 4017051000)'),(1239,'2016-05-17 11:59:40','Domino',5,'.lo spell dan','X: 16240.187500 Y: 16253.619141 Z: 16.652700 Map: 1',' (GUID: 4017051080)'),(1240,'2016-05-17 12:00:00','Domino',5,'.cast 54398','X: 16240.187500 Y: 16253.619141 Z: 16.652700 Map: 1',' (GUID: 4017051160)'),(1241,'2016-05-17 12:00:39','Domino',5,'.lo te dev','X: 16240.631836 Y: 16253.770508 Z: 16.870289 Map: 1',' (GUID: 4017051240)'),(1242,'2016-05-17 12:00:42','Domino',5,'.lo te gm','X: 16240.631836 Y: 16253.770508 Z: 16.870289 Map: 1',' (GUID: 4017052280)'),(1243,'2016-05-17 12:01:00','Domino',5,'.cast 29004','X: 16240.631836 Y: 16253.770508 Z: 16.870289 Map: 1',' (GUID: 4017052360)'),(1244,'2016-05-17 12:01:02','Domino',5,'.cast 29004','X: 16240.631836 Y: 16253.770508 Z: 16.870289 Map: 1','Cybroon (GUID: 4017052440)'),(1245,'2016-05-17 12:01:10','Domino',5,'.cast 29004','X: 16242.181641 Y: 16254.299805 Z: 17.570969 Map: 1','Cybroon (GUID: 4017052520)'),(1246,'2016-05-17 12:02:40','Domino',5,'.gm on','X: 16242.181641 Y: 16254.299805 Z: 17.572083 Map: 1','Cybroon (GUID: 4017051240)'),(1247,'2016-05-17 12:02:41','Domino',5,'.dev on','X: 16242.181641 Y: 16254.299805 Z: 17.572083 Map: 1','Cybroon (GUID: 4017051320)'),(1248,'2016-05-17 12:03:55','Domino',5,'.lo i test mount','X: 16242.181641 Y: 16254.299805 Z: 17.572119 Map: 1',' (GUID: 4017051400)'),(1249,'2016-05-17 12:04:01','Domino',5,'.lo spell test mount','X: 16242.181641 Y: 16254.299805 Z: 17.572119 Map: 1',' (GUID: 4017051480)'),(1250,'2016-05-17 12:04:10','Domino',5,'.cast 42929','X: 16242.181641 Y: 16254.299805 Z: 17.572119 Map: 1',' (GUID: 4017051560)'),(1251,'2016-05-17 12:04:12','Domino',5,'.cast 42929','X: 16245.388672 Y: 16255.393555 Z: 18.637285 Map: 1',' (GUID: 4017051640)'),(1252,'2016-05-17 12:07:18','Domino',5,'.te dalaran','X: 16208.572266 Y: 16226.858398 Z: 3.865728 Map: 1',' (GUID: 4034045464)'),(1253,'2016-05-17 12:10:09','Domino',5,'.te gmi','X: 5830.491699 Y: 493.977325 Z: 657.710205 Map: 571',' (GUID: 1979480936)'),(1254,'2016-05-17 12:10:19','Domino',5,'.te stormwind','X: 16222.143555 Y: 16261.556641 Z: 13.270239 Map: 1',' (GUID: 1991594904)'),(1255,'2016-05-17 12:13:17','Domino',5,'.lo i dan','X: -9058.446289 Y: 434.726532 Z: 128.051102 Map: 0',' (GUID: 1900930744)'),(1256,'2016-05-17 12:13:26','Domino',5,'.lo spell test mount','X: -9058.446289 Y: 434.726532 Z: 128.051102 Map: 0',' (GUID: 1900930824)'),(1257,'2016-05-17 12:15:49','Domino',5,'.lo spell dan','X: -8834.101562 Y: 629.386719 Z: 124.161346 Map: 0',' (GUID: 4221137176)'),(1258,'2016-05-17 12:15:53','Domino',5,'.lo spell test mount','X: -8834.101562 Y: 629.386719 Z: 124.161346 Map: 0',' (GUID: 4221137256)'),(1259,'2016-05-17 12:15:56','Domino',5,'.learn 61983','X: -8834.101562 Y: 629.386719 Z: 124.161346 Map: 0',' (GUID: 4221137336)'),(1260,'2016-05-17 12:16:23','Domino',5,'.te gmi','X: -8824.712891 Y: 624.555542 Z: 110.311386 Map: 0',' (GUID: 4221137416)'),(1261,'2016-05-17 12:17:47','Domino',5,'.reload creature_template 72286','X: 16259.379883 Y: 16260.087891 Z: 18.549202 Map: 1',' (GUID: 2897393336)'),(1262,'2016-05-17 12:17:54','Domino',5,'.reload creature_template 32931','X: 16254.411133 Y: 16258.371094 Z: 19.106998 Map: 1',' (GUID: 2897393416)'),(1263,'2016-05-17 12:18:10','Domino',5,'.reload creature_template 32931','X: 16242.282227 Y: 16257.668945 Z: 17.066612 Map: 1',' (GUID: 2897393496)'),(1264,'2016-05-17 12:19:36','Domino',5,'.lo spell test mount','X: 16246.802734 Y: 16251.640625 Z: 20.228411 Map: 1',' (GUID: 2897393576)'),(1265,'2016-05-17 12:22:51','Domino',5,'.cast 62015','X: 16261.582031 Y: 16306.729492 Z: 20.867943 Map: 1',' (GUID: 2959250968)'),(1266,'2016-05-17 12:22:55','Domino',5,'.cast 62015','X: 16261.582031 Y: 16306.729492 Z: 20.868336 Map: 1',' (GUID: 2959251048)'),(1267,'2016-05-17 12:23:00','Domino',5,'.cast 62015','X: 16257.959961 Y: 16286.792969 Z: 20.931494 Map: 1',' (GUID: 2959251128)'),(1268,'2016-05-17 12:23:25','Domino',5,'.cast 62048','X: 16255.666016 Y: 16276.512695 Z: 14.753203 Map: 1',' (GUID: 2959251208)'),(1269,'2016-05-17 12:24:25','Domino',5,'.cast 62123','X: 16219.563477 Y: 16242.331055 Z: 17.812468 Map: 1',' (GUID: 2959251288)'),(1270,'2016-05-17 12:24:28','Domino',5,'.cast 62123','X: 16224.091797 Y: 16254.245117 Z: 12.900279 Map: 1',' (GUID: 2959251368)'),(1271,'2016-05-17 12:24:31','Domino',5,'.cast 62123','X: 16224.919922 Y: 16256.423828 Z: 13.066806 Map: 1',' (GUID: 2959251448)'),(1272,'2016-05-17 12:24:44','Domino',5,'.cast 62123','X: 16224.919922 Y: 16256.423828 Z: 13.066806 Map: 1',' (GUID: 2959251528)'),(1273,'2016-05-17 12:25:02','Domino',5,'.cast 62123','X: 16227.157227 Y: 16262.309570 Z: 13.345545 Map: 1',' (GUID: 2959251608)'),(1274,'2016-05-17 12:25:13','Domino',5,'.cast 62124','X: 16233.867188 Y: 16262.006836 Z: 13.851681 Map: 1',' (GUID: 2959251688)'),(1275,'2016-05-17 12:25:17','Domino',5,'.cast 62124','X: 16252.549805 Y: 16265.046875 Z: 16.151777 Map: 1','Gas Cloud (GUID: 2960222808)'),(1276,'2016-05-17 12:25:19','Domino',5,'.cast 62124','X: 16255.106445 Y: 16265.875977 Z: 16.140654 Map: 1','Gas Cloud (GUID: 2960222808)'),(1277,'2016-05-17 12:25:20','Domino',5,'.cast 62124','X: 16257.989258 Y: 16266.811523 Z: 16.285261 Map: 1','Gas Cloud (GUID: 2960222808)'),(1278,'2016-05-17 12:25:26','Domino',5,'.cast 62121','X: 16257.989258 Y: 16266.811523 Z: 16.285261 Map: 1','Gas Cloud (GUID: 2960222808)'),(1279,'2016-05-17 12:25:29','Domino',5,'.cast 62121','X: 16271.909180 Y: 16271.282227 Z: 17.642382 Map: 1','Gas Cloud (GUID: 2960222808)'),(1280,'2016-05-17 12:25:57','Domino',5,'.cast 62187','X: 16261.658203 Y: 16266.042969 Z: 17.046097 Map: 1',' (GUID: 2959252168)'),(1281,'2016-05-17 12:26:00','Domino',5,'.cast 62187','X: 16250.391602 Y: 16267.158203 Z: 15.274170 Map: 1',' (GUID: 2959252248)'),(1282,'2016-05-17 12:26:24','Domino',5,'.cast 62282','X: 16248.386719 Y: 16265.106445 Z: 15.738987 Map: 1',' (GUID: 2959252328)'),(1283,'2016-05-17 12:26:30','Domino',5,'.unaura 62282','X: 16242.601562 Y: 16259.187500 Z: 16.726532 Map: 1',' (GUID: 2959252408)'),(1284,'2016-05-17 12:27:03','Domino',5,'.cast 62500','X: 16242.601562 Y: 16259.187500 Z: 16.726532 Map: 1',' (GUID: 2959252488)'),(1285,'2016-05-17 12:27:07','Domino',5,'.cast 62500','X: 16226.642578 Y: 16252.837891 Z: 12.937414 Map: 1',' (GUID: 2959252568)'),(1286,'2016-05-17 12:27:12','Domino',5,'.unaura 62500','X: 16226.642578 Y: 16252.837891 Z: 12.937414 Map: 1',' (GUID: 2959252648)'),(1287,'2016-05-17 12:59:31','Domino',5,'.gm off','X: 16231.429688 Y: 16254.998047 Z: 13.716467 Map: 1',' (GUID: 1579174120)'),(1288,'2016-05-17 13:04:35','Domino',5,'.unaura 57724','X: 16224.122070 Y: 16254.420898 Z: 12.914630 Map: 1',' (GUID: 1644391192)'),(1289,'2016-05-17 13:04:37','Domino',5,'.unaura 57724','X: 16224.122070 Y: 16254.420898 Z: 12.914630 Map: 1','Cybroon (GUID: 1579174120)'),(1290,'2016-05-17 13:04:53','Domino',5,'.cast 32182','X: 16224.122070 Y: 16254.420898 Z: 12.914630 Map: 1',' (GUID: 1579174200)'),(1291,'2016-05-17 13:05:06','Domino',5,'.cast 2825','X: 16224.122070 Y: 16254.420898 Z: 12.914630 Map: 1',' (GUID: 1579174280)'),(1292,'2016-05-17 13:05:07','Domino',5,'.cast 2825','X: 16224.122070 Y: 16254.420898 Z: 12.915524 Map: 1',' (GUID: 1579174360)'),(1293,'2016-05-17 13:05:10','Domino',5,'.cooldown','X: 16224.122070 Y: 16254.420898 Z: 12.915524 Map: 1',' (GUID: 1579174440)'),(1294,'2016-05-17 13:05:11','Domino',5,'.cast 2825','X: 16224.122070 Y: 16254.420898 Z: 12.915524 Map: 1',' (GUID: 1579174520)'),(1295,'2016-05-17 13:05:32','Domino',5,'.unaura 57723','X: 16219.202148 Y: 16258.593750 Z: 13.346529 Map: 1','Cybroon (GUID: 1579174600)'),(1296,'2016-05-17 13:05:33','Domino',5,'.unaura 57723','X: 16219.202148 Y: 16258.593750 Z: 13.346529 Map: 1','Domino (GUID: 1579174680)'),(1297,'2016-05-17 13:06:17','Domino',5,'.lo spell test mount','X: 16222.889648 Y: 16241.144531 Z: 15.352114 Map: 1',' (GUID: 1644391032)'),(1298,'2016-06-11 21:01:19','Domino',5,'.gm i','X: 16226.702148 Y: 16236.694336 Z: 18.483189 Map: 1',' (GUID: 2302670520)'),(1299,'2016-06-11 21:01:25','Domino',5,'.gm on','X: 16226.759766 Y: 16236.690430 Z: 41.959789 Map: 1',' (GUID: 2302670600)'),(1300,'2016-06-11 21:01:27','Domino',5,'.gm i','X: 16226.759766 Y: 16236.690430 Z: 41.959789 Map: 1',' (GUID: 2302670760)');
/*!40000 ALTER TABLE `logy_gm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logy_item_get`
--

DROP TABLE IF EXISTS `logy_item_get`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logy_item_get` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `guid` int(9) NOT NULL,
  `player` varchar(12) NOT NULL,
  `account` int(9) NOT NULL,
  `item` int(6) NOT NULL,
  `item_guid` int(12) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `position` varchar(96) NOT NULL,
  `target` varchar(96) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logy_item_get`
--

LOCK TABLES `logy_item_get` WRITE;
/*!40000 ALTER TABLE `logy_item_get` DISABLE KEYS */;
INSERT INTO `logy_item_get` VALUES (1,'2016-05-09 18:27:39',1,'Mew',1,49623,74,0,'X: 16230.580078 Y: 16258.316406 Z: 13.665567 Map: 1',' (GUID: 3992410184)'),(2,'2016-05-09 20:44:21',1,'Mew',1,50398,76,0,'X: -8813.872070 Y: 613.303162 Z: 95.237091 Map: 0','Ashen Rings (GUID: 3124727320)'),(3,'2016-05-09 20:44:41',1,'Mew',1,50398,76,0,'X: -8813.046875 Y: 615.009827 Z: 95.039642 Map: 0','Ashen Rings (GUID: 3074050200)'),(4,'2016-05-10 11:03:23',2,'Domino',5,45570,102,0,'X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 502636488)'),(5,'2016-05-10 11:03:28',2,'Domino',5,45570,102,0,'X: 16232.181641 Y: 16415.300781 Z: -64.379120 Map: 1',' (GUID: 691982040)'),(6,'2016-05-10 11:09:25',2,'Domino',5,51393,105,0,'X: 16228.533203 Y: 16388.179688 Z: -64.379303 Map: 1','Trapjaw Rix (GUID: 495776760)'),(7,'2016-05-10 11:09:27',2,'Domino',5,51393,106,0,'X: 16228.533203 Y: 16388.179688 Z: -64.379303 Map: 1','Trapjaw Rix (GUID: 495776664)'),(8,'2016-05-10 11:10:32',2,'Domino',5,51393,105,0,'X: 16225.132812 Y: 16395.619141 Z: -64.380081 Map: 1','Trainer (GUID: 495777144)'),(9,'2016-05-10 11:10:34',2,'Domino',5,51393,106,0,'X: 16231.223633 Y: 16393.240234 Z: -64.377617 Map: 1','Trainer (GUID: 495777144)'),(10,'2016-05-10 11:10:45',2,'Domino',5,51391,107,0,'X: 16230.300781 Y: 16386.523438 Z: -64.379547 Map: 1','Trapjaw Rix (GUID: 495777240)'),(11,'2016-05-10 11:10:46',2,'Domino',5,51391,108,0,'X: 16230.300781 Y: 16386.523438 Z: -64.379547 Map: 1','Trapjaw Rix (GUID: 495777144)'),(12,'2016-05-10 11:14:25',2,'Domino',5,51391,108,0,'X: 16236.013672 Y: 16410.718750 Z: -64.375114 Map: 1',' (GUID: 502632568)'),(13,'2016-05-10 11:14:27',2,'Domino',5,51391,107,0,'X: 16236.013672 Y: 16410.718750 Z: -64.375114 Map: 1',' (GUID: 502632648)'),(14,'2016-05-10 11:18:33',2,'Domino',5,40848,112,0,'X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502633768)'),(15,'2016-05-10 11:18:40',2,'Domino',5,40848,112,0,'X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634088)'),(16,'2016-05-10 11:18:47',2,'Domino',5,40848,113,0,'X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634328)'),(17,'2016-05-10 11:19:00',2,'Domino',5,40848,113,0,'X: 16237.054688 Y: 16404.388672 Z: -64.377747 Map: 1',' (GUID: 502634648)'),(18,'2016-05-10 11:24:29',2,'Domino',5,51423,117,0,'X: 5754.409668 Y: 584.132690 Z: 613.735352 Map: 571','Nargle Lashcord (GUID: 532234040)'),(19,'2016-05-12 14:37:57',2,'Domino',5,54581,350,0,'X: 3170.048096 Y: 529.042480 Z: 72.889061 Map: 724','Halion (GUID: 2622278904)'),(20,'2016-05-12 14:38:02',2,'Domino',5,54581,350,0,'X: 3170.048096 Y: 529.042480 Z: 72.889061 Map: 724',' (GUID: 3540163992)'),(21,'2016-05-12 16:08:25',10,'Cybroon',5,50398,489,0,'X: 16225.269531 Y: 16389.498047 Z: -64.379364 Map: 1',' (GUID: 2312529592)'),(22,'2016-05-12 16:08:52',10,'Cybroon',5,50400,490,0,'X: 16223.926758 Y: 16403.884766 Z: -64.376938 Map: 1',' (GUID: 2312528232)'),(23,'2016-05-12 16:09:05',10,'Cybroon',5,50404,491,0,'X: 16223.926758 Y: 16403.884766 Z: -64.376938 Map: 1',' (GUID: 2312529832)'),(24,'2016-05-12 16:33:39',11,'Sanka',1,49623,497,0,'X: 16227.665039 Y: 16399.611328 Z: -64.380280 Map: 1','Sanka (GUID: 1165738504)'),(25,'2016-05-12 16:35:11',10,'Cybroon',5,50724,504,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378647 Map: 1',' (GUID: 1165738744)'),(26,'2016-05-12 16:35:56',10,'Cybroon',5,54583,505,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378593 Map: 1',' (GUID: 1165739224)'),(27,'2016-05-12 16:36:38',11,'Sanka',1,51310,506,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1164746360)'),(28,'2016-05-12 16:36:40',11,'Sanka',1,51305,507,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1162159608)'),(29,'2016-05-12 16:36:42',11,'Sanka',1,51306,508,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1111420856)'),(30,'2016-05-12 16:36:51',11,'Sanka',1,51311,509,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1111420760)'),(31,'2016-05-12 16:36:54',11,'Sanka',1,51307,510,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378712)'),(32,'2016-05-12 16:36:54',10,'Cybroon',5,50613,511,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1165741384)'),(33,'2016-05-12 16:37:03',10,'Cybroon',5,50694,512,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1165741704)'),(34,'2016-05-12 16:37:16',10,'Cybroon',5,50699,513,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110651240)'),(35,'2016-05-12 16:37:18',11,'Sanka',1,51312,514,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378808)'),(36,'2016-05-12 16:37:20',11,'Sanka',1,51308,515,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128378904)'),(37,'2016-05-12 16:37:22',11,'Sanka',1,51313,516,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128379000)'),(38,'2016-05-12 16:37:23',10,'Cybroon',5,54582,517,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110652520)'),(39,'2016-05-12 16:37:24',11,'Sanka',1,51309,518,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128379096)'),(40,'2016-05-12 16:37:26',11,'Sanka',1,51314,519,0,'X: 16212.626953 Y: 16386.796875 Z: -64.378357 Map: 1','Ormus the Penitent (GUID: 1128379192)'),(41,'2016-05-12 16:37:32',10,'Cybroon',5,50365,520,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653400)'),(42,'2016-05-12 16:37:41',10,'Cybroon',5,50714,521,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653560)'),(43,'2016-05-12 16:37:46',10,'Cybroon',5,50719,522,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378761 Map: 1',' (GUID: 1110653720)'),(44,'2016-05-12 16:38:21',10,'Cybroon',5,50457,523,0,'X: 16229.284180 Y: 16401.312500 Z: -64.378510 Map: 1',' (GUID: 1110653880)'),(45,'2016-05-12 17:23:15',10,'Cybroon',5,50658,553,0,'X: 16230.359375 Y: 16401.314453 Z: -64.379990 Map: 1',' (GUID: 1163115208)'),(46,'2016-05-12 17:23:37',10,'Cybroon',5,50705,554,0,'X: 16230.359375 Y: 16401.314453 Z: -64.377258 Map: 1',' (GUID: 1163115768)'),(47,'2016-05-12 17:24:06',10,'Cybroon',5,50726,555,0,'X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1165738184)'),(48,'2016-05-12 17:24:32',10,'Cybroon',5,54585,556,0,'X: 16230.359375 Y: 16401.314453 Z: -64.377060 Map: 1',' (GUID: 1110654280)'),(49,'2016-05-12 17:37:23',10,'Cybroon',5,51898,578,0,'X: -8718.028320 Y: 337.285614 Z: 101.019630 Map: 0',' (GUID: 1057122856)'),(50,'2016-05-12 17:44:27',11,'Sanka',1,50730,582,0,'X: 16228.955078 Y: 16261.414062 Z: 13.423162 Map: 1',' (GUID: 1057120616)'),(51,'2016-05-12 18:36:21',13,'Yara',5,50732,638,0,'X: 16220.463867 Y: 16390.130859 Z: -64.378059 Map: 1',' (GUID: 1348467720)'),(52,'2016-05-12 18:38:12',13,'Yara',5,54588,639,0,'X: 16223.525391 Y: 16403.498047 Z: -64.380081 Map: 1',' (GUID: 1348465240)'),(53,'2016-05-12 18:38:32',13,'Yara',5,50365,640,0,'X: 16219.405273 Y: 16400.542969 Z: -64.377327 Map: 1','Trainer (GUID: 1445760536)'),(54,'2016-05-12 18:39:08',13,'Yara',5,50398,641,0,'X: 16225.900391 Y: 16387.261719 Z: -64.378784 Map: 1','Ashen Rings (GUID: 1407922072)'),(55,'2016-05-12 18:39:52',13,'Yara',5,50644,642,0,'X: 16230.841797 Y: 16392.453125 Z: -64.377205 Map: 1','Ashen Rings (GUID: 1397172728)'),(56,'2016-05-12 18:40:09',13,'Yara',5,50613,643,0,'X: 16226.590820 Y: 16398.169922 Z: -64.328941 Map: 1','Ashen Rings (GUID: 1351935480)'),(57,'2016-05-12 18:40:37',13,'Yara',5,54582,644,0,'X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397172728)'),(58,'2016-05-12 18:40:49',13,'Yara',5,54583,645,0,'X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397173976)'),(59,'2016-05-12 18:40:56',13,'Yara',5,50724,646,0,'X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1397174648)'),(60,'2016-05-12 18:41:13',13,'Yara',5,50719,647,0,'X: 16226.590820 Y: 16398.169922 Z: -64.378479 Map: 1','Ashen Rings (GUID: 1348364728)'),(61,'2016-05-12 18:42:07',13,'Yara',5,50864,648,0,'X: 16226.590820 Y: 16398.169922 Z: -64.377541 Map: 1','Ashen Rings (GUID: 1397173208)'),(62,'2016-05-12 18:42:11',13,'Yara',5,50684,649,0,'X: 16226.590820 Y: 16398.169922 Z: -64.376686 Map: 1','Ashen Rings (GUID: 1397173208)'),(63,'2016-05-12 18:42:15',13,'Yara',5,50864,648,0,'X: 16226.590820 Y: 16398.169922 Z: -64.377228 Map: 1','Ashen Rings (GUID: 1397173208)'),(64,'2016-05-12 18:46:29',13,'Yara',5,50699,693,0,'X: 16236.535156 Y: 16389.822266 Z: -64.378532 Map: 1','Orange Gems (GUID: 1348364344)'),(65,'2016-05-12 19:16:44',13,'Yara',5,51465,697,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2779942520)'),(66,'2016-05-12 19:16:46',13,'Yara',5,51463,698,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789274264)'),(67,'2016-05-12 19:16:47',13,'Yara',5,51467,699,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277432)'),(68,'2016-05-12 19:16:49',13,'Yara',5,51466,700,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277144)'),(69,'2016-05-12 19:17:05',13,'Yara',5,51451,701,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277240)'),(70,'2016-05-12 19:17:55',13,'Yara',5,51464,702,0,'X: 16229.042969 Y: 16386.078125 Z: -64.377228 Map: 1','Nargle Lashcord (GUID: 2789277432)'),(71,'2016-05-12 19:18:29',13,'Yara',5,51407,703,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Nargle Lashcord (GUID: 2779942520)'),(72,'2016-05-12 19:19:05',13,'Yara',5,51398,705,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Trapjaw Rix (GUID: 2789277432)'),(73,'2016-05-12 19:19:15',13,'Yara',5,51338,706,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942520)'),(74,'2016-05-12 19:19:23',13,'Yara',5,51332,707,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942520)'),(75,'2016-05-12 19:19:34',13,'Yara',5,51335,708,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942520)'),(76,'2016-05-12 19:19:38',13,'Yara',5,51339,709,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779942520)'),(77,'2016-05-12 19:20:05',13,'Yara',5,51337,710,0,'X: 16229.042969 Y: 16386.078125 Z: -64.378464 Map: 1','Lieutenant Tristia (GUID: 2779940600)'),(78,'2016-05-12 19:21:48',13,'Yara',5,50694,718,0,'X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779941464)'),(79,'2016-05-12 19:23:47',13,'Yara',5,47059,720,0,'X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942424)'),(80,'2016-05-12 19:24:01',13,'Yara',5,54585,721,0,'X: 16239.901367 Y: 16391.271484 Z: -64.378975 Map: 1','Purple Gems (GUID: 2779942424)'),(81,'2016-05-14 03:47:07',13,'Della',11,50734,758,0,'X: 16232.485352 Y: 16411.890625 Z: -64.378731 Map: 1',' (GUID: 455490088)'),(82,'2016-05-14 04:26:52',13,'Della',11,50250,777,0,'X: -8854.767578 Y: 641.014709 Z: 96.723106 Map: 0',' (GUID: 4205560328)');
/*!40000 ALTER TABLE `logy_item_get` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `messageType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `stationery` tinyint(3) NOT NULL DEFAULT '41',
  `mailTemplateId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sender` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
  `receiver` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
  `subject` longtext,
  `body` longtext,
  `has_items` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `expire_time` int(10) unsigned NOT NULL DEFAULT '0',
  `deliver_time` int(10) unsigned NOT NULL DEFAULT '0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `cod` int(10) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_receiver` (`receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Mail System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail`
--

LOCK TABLES `mail` WRITE;
/*!40000 ALTER TABLE `mail` DISABLE KEYS */;
INSERT INTO `mail` VALUES (18,3,41,0,37941,13,'Emblem Quartermasters in Dalaran\'s Sunreaver Sanctuary','Your achievements in Northrend have not gone unnoticed, friend.$B$BThe Emblems you have earned may be used to purchase equipment from the various Emblem Quartermasters in Dalaran.$B$BYou may find us there, in the Sunreaver Sanctuary, where each variety of Emblem has its own quartermaster.$B$BWe look forward to your arrival!',0,1465931971,1463339971,0,0,0),(19,3,41,0,37941,10,'Emblem Quartermasters in Dalaran\'s Sunreaver Sanctuary','Your achievements in Northrend have not gone unnoticed, friend.$B$BThe Emblems you have earned may be used to purchase equipment from the various Emblem Quartermasters in Dalaran.$B$BYou may find us there, in the Sunreaver Sanctuary, where each variety of Emblem has its own quartermaster.$B$BWe look forward to your arrival!',0,1465932304,1463340304,0,0,1);
/*!40000 ALTER TABLE `mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_items`
--

DROP TABLE IF EXISTS `mail_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_items` (
  `mail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `item_guid` int(10) unsigned NOT NULL DEFAULT '0',
  `receiver` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
  PRIMARY KEY (`item_guid`),
  KEY `idx_receiver` (`receiver`),
  KEY `idx_mail_id` (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_items`
--

LOCK TABLES `mail_items` WRITE;
/*!40000 ALTER TABLE `mail_items` DISABLE KEYS */;
INSERT INTO `mail_items` VALUES (1,39,2),(3,73,4),(4,76,1),(5,77,1),(7,285,1),(11,336,2),(14,488,10);
/*!40000 ALTER TABLE `mail_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `odmeny`
--

DROP TABLE IF EXISTS `odmeny`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odmeny` (
  `passphrase` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` int(32) NOT NULL DEFAULT '0',
  `entry` int(32) NOT NULL DEFAULT '0',
  `count` int(32) NOT NULL DEFAULT '0',
  `redeemed` int(32) NOT NULL DEFAULT '0',
  `player_guid` int(32) DEFAULT NULL,
  `date` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`passphrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odmeny`
--

LOCK TABLES `odmeny` WRITE;
/*!40000 ALTER TABLE `odmeny` DISABLE KEYS */;
/*!40000 ALTER TABLE `odmeny` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_aura`
--

DROP TABLE IF EXISTS `pet_aura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_aura` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `casterGuid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Full Global Unique Identifier',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `effectMask` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `recalculateMask` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `stackCount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `amount0` mediumint(8) NOT NULL,
  `amount1` mediumint(8) NOT NULL,
  `amount2` mediumint(8) NOT NULL,
  `base_amount0` mediumint(8) NOT NULL,
  `base_amount1` mediumint(8) NOT NULL,
  `base_amount2` mediumint(8) NOT NULL,
  `maxDuration` int(11) NOT NULL DEFAULT '0',
  `remainTime` int(11) NOT NULL DEFAULT '0',
  `remainCharges` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`casterGuid`,`spell`,`effectMask`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pet System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_aura`
--

LOCK TABLES `pet_aura` WRITE;
/*!40000 ALTER TABLE `pet_aura` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_aura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_spell`
--

DROP TABLE IF EXISTS `pet_spell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_spell` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell Identifier',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pet System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_spell`
--

LOCK TABLES `pet_spell` WRITE;
/*!40000 ALTER TABLE `pet_spell` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_spell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_spell_cooldown`
--

DROP TABLE IF EXISTS `pet_spell_cooldown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_spell_cooldown` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier, Low part',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell Identifier',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `categoryId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell category Id',
  `categoryEnd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_spell_cooldown`
--

LOCK TABLES `pet_spell_cooldown` WRITE;
/*!40000 ALTER TABLE `pet_spell_cooldown` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_spell_cooldown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `petition`
--

DROP TABLE IF EXISTS `petition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `petition` (
  `ownerguid` int(10) unsigned NOT NULL,
  `petitionguid` int(10) unsigned DEFAULT '0',
  `name` varchar(24) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ownerguid`,`type`),
  UNIQUE KEY `index_ownerguid_petitionguid` (`ownerguid`,`petitionguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `petition`
--

LOCK TABLES `petition` WRITE;
/*!40000 ALTER TABLE `petition` DISABLE KEYS */;
/*!40000 ALTER TABLE `petition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `petition_sign`
--

DROP TABLE IF EXISTS `petition_sign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `petition_sign` (
  `ownerguid` int(10) unsigned NOT NULL,
  `petitionguid` int(10) unsigned NOT NULL DEFAULT '0',
  `playerguid` int(10) unsigned NOT NULL DEFAULT '0',
  `player_account` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`petitionguid`,`playerguid`),
  KEY `Idx_playerguid` (`playerguid`),
  KEY `Idx_ownerguid` (`ownerguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guild System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `petition_sign`
--

LOCK TABLES `petition_sign` WRITE;
/*!40000 ALTER TABLE `petition_sign` DISABLE KEYS */;
/*!40000 ALTER TABLE `petition_sign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pool_quest_save`
--

DROP TABLE IF EXISTS `pool_quest_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_quest_save` (
  `pool_id` int(10) unsigned NOT NULL DEFAULT '0',
  `quest_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pool_id`,`quest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pool_quest_save`
--

LOCK TABLES `pool_quest_save` WRITE;
/*!40000 ALTER TABLE `pool_quest_save` DISABLE KEYS */;
INSERT INTO `pool_quest_save` VALUES (348,24636),(349,14101),(350,13904),(351,13914),(352,11377),(353,11665),(354,13422),(356,11375),(357,11376),(359,12737),(360,12762),(361,12741),(362,12760),(363,14077),(364,14090),(365,14140),(366,14145),(367,14108),(370,12563),(5662,13675),(5663,13762),(5664,13769),(5665,13773),(5666,13779),(5667,13784),(5668,13666),(5669,13603),(5670,13742),(5671,13747),(5672,13758),(5673,13753),(5674,13102),(5675,13112),(5676,13833),(5677,12961),(5678,24579),(5684,24872),(5687,24875);
/*!40000 ALTER TABLE `pool_quest_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvpstats_battlegrounds`
--

DROP TABLE IF EXISTS `pvpstats_battlegrounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvpstats_battlegrounds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `winner_faction` tinyint(4) NOT NULL,
  `bracket_id` tinyint(3) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvpstats_battlegrounds`
--

LOCK TABLES `pvpstats_battlegrounds` WRITE;
/*!40000 ALTER TABLE `pvpstats_battlegrounds` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvpstats_battlegrounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pvpstats_players`
--

DROP TABLE IF EXISTS `pvpstats_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pvpstats_players` (
  `battleground_id` bigint(20) unsigned NOT NULL,
  `character_guid` int(10) unsigned NOT NULL,
  `winner` bit(1) NOT NULL,
  `score_killing_blows` mediumint(8) unsigned NOT NULL,
  `score_deaths` mediumint(8) unsigned NOT NULL,
  `score_honorable_kills` mediumint(8) unsigned NOT NULL,
  `score_bonus_honor` mediumint(8) unsigned NOT NULL,
  `score_damage_done` mediumint(8) unsigned NOT NULL,
  `score_healing_done` mediumint(8) unsigned NOT NULL,
  `attr_1` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attr_2` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attr_3` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attr_4` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attr_5` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`battleground_id`,`character_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pvpstats_players`
--

LOCK TABLES `pvpstats_players` WRITE;
/*!40000 ALTER TABLE `pvpstats_players` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvpstats_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest_tracker`
--

DROP TABLE IF EXISTS `quest_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quest_tracker` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `character_guid` int(10) unsigned NOT NULL DEFAULT '0',
  `quest_accept_time` datetime NOT NULL,
  `quest_complete_time` datetime DEFAULT NULL,
  `quest_abandon_time` datetime DEFAULT NULL,
  `completed_by_gm` tinyint(1) NOT NULL DEFAULT '0',
  `core_hash` varchar(120) NOT NULL DEFAULT '0',
  `core_revision` varchar(120) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest_tracker`
--

LOCK TABLES `quest_tracker` WRITE;
/*!40000 ALTER TABLE `quest_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserved_name`
--

DROP TABLE IF EXISTS `reserved_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserved_name` (
  `name` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player Reserved Names';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserved_name`
--

LOCK TABLES `reserved_name` WRITE;
/*!40000 ALTER TABLE `reserved_name` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserved_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submited_chars`
--

DROP TABLE IF EXISTS `submited_chars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submited_chars` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL DEFAULT '',
  `level` int(6) DEFAULT NULL,
  `armory` int(4) NOT NULL DEFAULT '0',
  `class` varchar(65) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `s_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submited_chars`
--

LOCK TABLES `submited_chars` WRITE;
/*!40000 ALTER TABLE `submited_chars` DISABLE KEYS */;
/*!40000 ALTER TABLE `submited_chars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `name` varchar(200) NOT NULL COMMENT 'filename with extension of the update.',
  `hash` char(40) DEFAULT '' COMMENT 'sha1 hash of the sql file.',
  `state` enum('RELEASED','ARCHIVED') NOT NULL DEFAULT 'RELEASED' COMMENT 'defines if an update is released or archived.',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp when the query was applied.',
  `speed` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'time the query takes to apply in ms.',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of all applied updates in this database.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
INSERT INTO `updates` VALUES ('2015_03_20_00_characters.sql','B761760804EA73BD297F296C5C1919687DF7191C','ARCHIVED','2015-03-21 21:44:15',0),('2015_03_20_01_characters.sql','894F08B70449A5481FFAF394EE5571D7FC4D8A3A','ARCHIVED','2015-03-21 21:44:15',0),('2015_03_20_02_characters.sql','97D7BE0CAADC79F3F11B9FD296B8C6CD40FE593B','ARCHIVED','2015-03-21 21:44:51',0),('2015_06_26_00_characters_335.sql','C2CC6E50AFA1ACCBEBF77CC519AAEB09F3BBAEBC','ARCHIVED','2015-07-13 23:49:22',0),('2015_09_28_00_characters_335.sql','F8682A431D50E54BDC4AC0E7DBED21AE8AAB6AD4','ARCHIVED','2015-09-28 21:00:00',0),('2015_08_26_00_characters_335.sql','C7D6A3A00FECA3EBFF1E71744CA40D3076582374','ARCHIVED','2015-08-26 21:00:00',0),('2015_10_06_00_characters.sql','16842FDD7E8547F2260D3312F53EFF8761EFAB35','ARCHIVED','2015-10-06 16:06:38',0),('2015_10_07_00_characters.sql','E15AB463CEBE321001D7BFDEA4B662FF618728FD','ARCHIVED','2015-10-07 23:32:00',0),('2015_10_12_00_characters.sql','D6F9927BDED72AD0A81D6EC2C6500CBC34A39FA2','ARCHIVED','2015-10-12 15:35:47',0),('2015_10_28_00_characters.sql','622A9CA8FCE690429EBE23BA071A37C7A007BF8B','ARCHIVED','2015-10-19 14:32:22',0),('2015_10_29_00_characters_335.sql','4555A7F35C107E54C13D74D20F141039ED42943E','ARCHIVED','2015-10-29 17:05:43',0),('2015_11_03_00_characters.sql','CC045717B8FDD9733351E52A5302560CD08AAD57','ARCHIVED','2015-10-12 15:23:33',0),('2015_11_07_00_characters.sql','0ACDD35EC9745231BCFA701B78056DEF94D0CC53','ARCHIVED','2016-04-11 00:42:36',94),('2016_02_10_00_characters.sql','F1B4DA202819CABC7319A4470A2D224A34609E97','ARCHIVED','2016-02-10 00:00:00',0),('2016_03_13_2016_01_05_00_characters.sql','0EAD24977F40DE2476B4567DA2B477867CC0DA1A','ARCHIVED','2016-03-13 20:03:56',0),('2016_04_11_00_characters.sql','0ACDD35EC9745231BCFA701B78056DEF94D0CC53','RELEASED','2016-04-11 03:18:17',0),('guild_house_db.sql','D0FF0CDD4E1D37AD55F30F7DC9EA9A27BDBB97BF','RELEASED','2016-05-10 14:41:54',16);
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates_include`
--

DROP TABLE IF EXISTS `updates_include`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates_include` (
  `path` varchar(200) NOT NULL COMMENT 'directory to include. $ means relative to the source directory.',
  `state` enum('RELEASED','ARCHIVED') NOT NULL DEFAULT 'RELEASED' COMMENT 'defines if the directory contains released or archived updates.',
  PRIMARY KEY (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of directories where we want to include sql updates.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates_include`
--

LOCK TABLES `updates_include` WRITE;
/*!40000 ALTER TABLE `updates_include` DISABLE KEYS */;
INSERT INTO `updates_include` VALUES ('$/sql/updates/characters','RELEASED'),('$/sql/custom/characters','RELEASED'),('$/sql/old/3.3.5a/characters','ARCHIVED');
/*!40000 ALTER TABLE `updates_include` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warden_action`
--

DROP TABLE IF EXISTS `warden_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warden_action` (
  `wardenId` smallint(5) unsigned NOT NULL,
  `action` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`wardenId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warden_action`
--

LOCK TABLES `warden_action` WRITE;
/*!40000 ALTER TABLE `warden_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `warden_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worldstates`
--

DROP TABLE IF EXISTS `worldstates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worldstates` (
  `entry` int(10) unsigned NOT NULL DEFAULT '0',
  `value` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` tinytext,
  PRIMARY KEY (`entry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Saves';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worldstates`
--

LOCK TABLES `worldstates` WRITE;
/*!40000 ALTER TABLE `worldstates` DISABLE KEYS */;
INSERT INTO `worldstates` VALUES (3781,9000000,NULL),(3801,0,NULL),(3802,1,NULL),(20001,0,'NextArenaPointDistributionTime'),(20002,1466268273,'NextWeeklyQuestResetTime'),(20003,1465797600,'NextBGRandomDailyResetTime'),(20004,0,'cleaning_flags'),(20006,1465797600,NULL),(20007,1467331200,NULL);
/*!40000 ALTER TABLE `worldstates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-12 18:23:13

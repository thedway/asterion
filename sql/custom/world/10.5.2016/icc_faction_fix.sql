REPLACE INTO spell_area VALUES (100001, 4812, 0, 0, 0, 0, 2, 1, 64, 11);
DELETE FROM spell_linked_spell WHERE spell_effect = 100001;
UPDATE spell_dbc SET AttributesEx3 = 1048576, Comment = 'Faction Swap Spell' WHERE Id = 100001;

REPLACE INTO `spell_dbc` (`Id`, `Dispel`, `Mechanic`, `Attributes`, `AttributesEx`, `AttributesEx2`, `AttributesEx3`, `AttributesEx4`, `AttributesEx5`, `AttributesEx6`, `AttributesEx7`, `Stances`, `StancesNot`, `Targets`, 
`CastingTimeIndex`, `AuraInterruptFlags`, `ProcFlags`, `ProcChance`, `ProcCharges`, `MaxLevel`, `BaseLevel`, `SpellLevel`, `DurationIndex`, `RangeIndex`, `StackAmount`, `EquippedItemClass`, `EquippedItemSubClassMask`, 
`EquippedItemInventoryTypeMask`, `Effect1`, `Effect2`, `Effect3`, `EffectDieSides1`, `EffectDieSides2`, `EffectDieSides3`, `EffectRealPointsPerLevel1`, `EffectRealPointsPerLevel2`, `EffectRealPointsPerLevel3`, 
`EffectBasePoints1`, `EffectBasePoints2`, `EffectBasePoints3`, `EffectMechanic1`, `EffectMechanic2`, `EffectMechanic3`, `EffectImplicitTargetA1`, `EffectImplicitTargetA2`, `EffectImplicitTargetA3`, `EffectImplicitTargetB1`, 
`EffectImplicitTargetB2`, `EffectImplicitTargetB3`, `EffectRadiusIndex1`, `EffectRadiusIndex2`, `EffectRadiusIndex3`, `EffectApplyAuraName1`, `EffectApplyAuraName2`, `EffectApplyAuraName3`, `EffectAmplitude1`, `EffectAmplitude2`, 
`EffectAmplitude3`, `EffectMultipleValue1`, `EffectMultipleValue2`, `EffectMultipleValue3`, `EffectItemType1`, `EffectItemType2`, `EffectItemType3`, `EffectMiscValue1`, `EffectMiscValue2`, `EffectMiscValue3`, `EffectMiscValueB1`, 
`EffectMiscValueB2`, `EffectMiscValueB3`, `EffectTriggerSpell1`, `EffectTriggerSpell2`, `EffectTriggerSpell3`, `EffectSpellClassMaskA1`, `EffectSpellClassMaskA2`, `EffectSpellClassMaskA3`, `EffectSpellClassMaskB1`, 
`EffectSpellClassMaskB2`, `EffectSpellClassMaskB3`, `EffectSpellClassMaskC1`, `EffectSpellClassMaskC2`, `EffectSpellClassMaskC3`, `MaxTargetLevel`, `SpellFamilyName`, `SpellFamilyFlags1`, `SpellFamilyFlags2`, `SpellFamilyFlags3`, 
`MaxAffectedTargets`, `DmgClass`, `PreventionType`, `DmgMultiplier1`, `DmgMultiplier2`, `DmgMultiplier3`, `AreaGroupId`, `SchoolMask`, `Comment`) VALUES ('100001', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 
'1', '0', '0', '0', '0', '0', '0', '0', '21', '1', '0', '-1', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4', '0', '0', '0', '0', '0', '0', 
'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Icecrown Faction');
UPDATE creature_template SET faction = 35 WHERE entry IN(36838,36839);
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES
('70121', '100001', '2', 'Horde Gunship player factions'),
('70120', '100001', '2', 'Alliance Gunship player factions');
REPLACE INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (100001, 'spell_icc_faction_change');
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=36971;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37833;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37920;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37970;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37830;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37187;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37200;
UPDATE `creature_template` SET `faction`=35 WHERE  `entry`=37182;
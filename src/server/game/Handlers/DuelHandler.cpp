/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Log.h"
#include "Player.h"

void WorldSession::HandleDuelAcceptedOpcode(WorldPacket& recvPacket)
{
    ObjectGuid guid;
    Player* player;
    Player* plTarget;

    recvPacket >> guid;

    if (!GetPlayer()->duel)                                  // ignore accept from duel-sender
        return;

    player = GetPlayer();
    plTarget = player->duel->opponent;

    if (player == player->duel->initiator || !plTarget || player == plTarget || player->duel->startTime != 0 || plTarget->duel->startTime != 0)
        return;

    //TC_LOG_DEBUG("network", "WORLD: Received CMSG_DUEL_ACCEPTED");
    TC_LOG_DEBUG("network", "Player 1 is: %u (%s)", player->GetGUID().GetCounter(), player->GetName().c_str());
    TC_LOG_DEBUG("network", "Player 2 is: %u (%s)", plTarget->GetGUID().GetCounter(), plTarget->GetName().c_str());

    time_t now = time(NULL);
    player->duel->startTimer = now;
    plTarget->duel->startTimer = now;

    player->SendDuelCountdown(3000);
    plTarget->SendDuelCountdown(3000);

    player->RemoveArenaSpellCooldowns(true);
    player->ResetAllPowers();
    player->RemoveAura(57724); // Remove Sated Debuff
    player->RemoveAura(57723); // Remove Sated Debuff
    if (player->getClass() == CLASS_MAGE) { //  --- Mage Debuffs
        player->RemoveAura(41425); // Hypothermia
    }
    if (player->getClass() == CLASS_PALADIN) {// --- Paladin Debuffs
        player->RemoveAura(61987); // Avenging Wrath Marker
        player->RemoveAura(61988); // Immune Shield Marker
        player->RemoveAura(25771); // Forbearance
    }
    if (player->getClass() == CLASS_DRUID) { // --- druids in forms
        player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
    }

    plTarget->RemoveArenaSpellCooldowns(true);
    plTarget->ResetAllPowers();
    plTarget->RemoveAura(57724); // Remove Sated Debuff
    plTarget->RemoveAura(57723); // Remove Sated Debuff
    if (plTarget->getClass() == CLASS_MAGE) { // --- Mage Debuffs
        plTarget->RemoveAura(41425); // Hypothermia
    }
    if (plTarget->getClass() == CLASS_PALADIN) { // --- Paladin Debuffs
        plTarget->RemoveAura(61987); // Avenging Wrath Marker
        plTarget->RemoveAura(61988); // Immune Shield Marker  
        plTarget->RemoveAura(25771); // Forbearance
    }
    if (plTarget->getClass() == CLASS_DRUID) { // --- druids in forms
        plTarget->SetPower(POWER_MANA, plTarget->GetMaxPower(POWER_MANA));
    }
}

void WorldSession::HandleDuelCancelledOpcode(WorldPacket& recvPacket)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_DUEL_CANCELLED");
    ObjectGuid guid;
    recvPacket >> guid;

    // no duel requested
    if (!GetPlayer()->duel)
        return;

    // player surrendered in a duel using /forfeit
    if (GetPlayer()->duel->startTime != 0)
    {
        GetPlayer()->CombatStopWithPets(true);
        if (GetPlayer()->duel->opponent)
            GetPlayer()->duel->opponent->CombatStopWithPets(true);

        GetPlayer()->CastSpell(GetPlayer(), 7267, true);    // beg
        GetPlayer()->DuelComplete(DUEL_WON);
        return;
    }

    GetPlayer()->DuelComplete(DUEL_INTERRUPTED);
}
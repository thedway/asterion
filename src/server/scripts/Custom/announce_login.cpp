class announce_login : public PlayerScript
{
public:
    announce_login() : PlayerScript("announce_login") { }

    void OnLogin(Player* player, bool /*loginFirst*/)
    {
        std::string Hodnost;
        switch (player->GetSession()->GetSecurity()) {
            case 1: { Hodnost = "V.I.P. hrac"; } break;
            case 3: { Hodnost = "Moderator"; } break;
            case 4: { Hodnost = "<ZD> Eventmaster"; } break;
            case 5: { Hodnost = "<ZD> Gamemaster"; } break;
            case 6: { Hodnost = "Eventmaster"; } break;
            case 7: { Hodnost = "Gamemaster"; } break;
            case 8: { Hodnost = "Developer"; } break;
            case 9: { Hodnost = "Head EM"; } break;
            case 10: { Hodnost = "Head GM"; } break;
            case 11: { Hodnost = "Head Developer"; } break;
            case 12: { Hodnost = "Administrator"; } break;
            case 13: { Hodnost = "Administrator"; } break;
        }
        // VIP az GM Levels
        if (player->GetSession()->GetSecurity() >= 1)
        {
            std::ostringstream ss;
            ss << "|cff3DAEFF[ Login Announcer ]|cff00e9ff : "<< Hodnost <<"|cff4CFF00 " << player->GetName() << " |cffFFD800se prihlasil.";
            sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
        }
    }
};

void AddSC_announce_login()
{
    new announce_login;
}
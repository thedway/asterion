/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:
void AddSC_Transmogrification();

void AddSC_professions_npc();
void AddSC_npc_enchantment();
void AddSC_arena_top_teams();
void AddSC_npc_casino();
void AddSC_npc_argent_ring_restorer();
void AddSC_eventmark_vendor();
void AddSC_announce_login();
void AddSC_guildmaster();


// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddCustomScripts()
{
    AddSC_Transmogrification();
    AddSC_professions_npc();
	AddSC_npc_enchantment();
	AddSC_arena_top_teams();
	AddSC_npc_casino();
	AddSC_npc_argent_ring_restorer();
	AddSC_eventmark_vendor();
	AddSC_announce_login();
	AddSC_guildmaster();
}

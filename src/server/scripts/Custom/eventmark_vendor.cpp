#include "Player.h"
#include "Creature.h"
#include "ScriptedGossip.h"
#include "eventmark_vendor.h"

class eventmark_vendor : public CreatureScript
{
public:
    eventmark_vendor() : CreatureScript("eventmark_vendor") {}

    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->PlayerTalkClass->ClearMenus();
        player->ADD_GOSSIP_ITEM(4, "|TInterface\\icons\\INV_Shirt_01:26|t|r Shirty", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
        player->ADD_GOSSIP_ITEM(4, "|TInterface\\icons\\INV_MISC_NOTE_02:26|t|r Hracky", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
        player->ADD_GOSSIP_ITEM(4, "|TInterface\\icons\\ABILITY_MOUNT_GOLDENGRYPHON:26|t|r Mounti", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
        player->SEND_GOSSIP_MENU(1, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions)
    {
        switch (actions)
        {
            player->PlayerTalkClass->ClearMenus();

            case GOSSIP_ACTION_INFO_DEF+1:
                player->PlayerTalkClass->ClearMenus();
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Tauren (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Taurenka (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Humanka (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+6);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Human (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+7);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Gnomka (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+8);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Gnom (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+9);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Blood Elfka (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+10);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Shirt promena > Blood Elf (70 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+11);
                player->SEND_GOSSIP_MENU(1, creature->GetGUID());
                return true;

            
            // @TODO pridat nejake hracky za event marky
            case GOSSIP_ACTION_INFO_DEF+2:
                player->PlayerTalkClass->ClearMenus();
                //player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nedostupne.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+12);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Sylvanas\' Music Box (50 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+14);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "D.I.S.C.O (50 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+15);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Muradin\'s Favor (50 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+16);
                player->SEND_GOSSIP_MENU(1, creature->GetGUID());
                break;

            // Hracky
            case GOSSIP_ACTION_INFO_DEF+14:
            if (player->HasItemCount(event_mark,hracky_em_count))
                {
                    if (player->HasItemCount(sylvanas_music_box,1))
                        creature->Whisper(MSG_have_item, LANG_UNIVERSAL, player);
                    else
                        player->DestroyItemCount(event_mark,hracky_em_count, true, false);
                        player->AddItem(sylvanas_music_box,hracky_count);
                        creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF+15:
            if (player->HasItemCount(event_mark,hracky_em_count))
                {
                    player->DestroyItemCount(event_mark,hracky_em_count, true, false);
                    player->AddItem(disco,hracky_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+16:
            if (player->HasItemCount(event_mark,hracky_em_count))
                {
                    if (player->HasItemCount(muradins_favor,1))
                        creature->Whisper(MSG_have_item, LANG_UNIVERSAL, player); 
                    else
                        player->DestroyItemCount(event_mark,hracky_em_count, true, false);
                        player->AddItem(muradins_favor,hracky_count);
                        creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            // @TODO pridat nejake mounty za event marky
            case GOSSIP_ACTION_INFO_DEF+3:
                player->PlayerTalkClass->ClearMenus();
                //player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Prozatim nedostupne.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+13);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Reins of the Swift Spectral Tiger (200 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+17);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Magic Rooster Egg (160 EM)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+18);
                player->SEND_GOSSIP_MENU(1, creature->GetGUID());
                break;

            case GOSSIP_ACTION_INFO_DEF+17:
            if (player->HasItemCount(event_mark,spectral_tiger_em_count))
                {
                    player->DestroyItemCount(event_mark,spectral_tiger_em_count, true, false);
                    player->AddItem(spectral_tiger,mount_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+18:
            if (player->HasItemCount(event_mark,magic_rooster_egg_em_count))
                {
                    player->DestroyItemCount(event_mark,magic_rooster_egg_em_count, true, false);
                    player->AddItem(magic_rooster_egg,mount_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            // Shirts
            case GOSSIP_ACTION_INFO_DEF+4:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(tauren,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;


            case GOSSIP_ACTION_INFO_DEF+5:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(taurenka,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+6:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(humanka,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+7:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(human,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+8:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(gnomka,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+9:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(gnom,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+10:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(blood_elfka,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;

            case GOSSIP_ACTION_INFO_DEF+11:
                if (player->HasItemCount(event_mark,shirt_em_count))
                {
                    player->DestroyItemCount(event_mark,shirt_em_count, true, false);
                    player->AddItem(blood_elf,shirt_count);
                    creature->Whisper(MSG_sucess_buy, LANG_UNIVERSAL, player);
                }
                else
                {
                    creature->Whisper(MSG_no_enough_event_marks, LANG_UNIVERSAL, player);
                }
                player->CLOSE_GOSSIP_MENU();
                break;
        }
    }
};

void AddSC_eventmark_vendor()
{
    new eventmark_vendor();
}
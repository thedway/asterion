#define event_mark 22033

enum messages
{
	#define MSG_no_enough_event_marks "Nemas dostatek Event Marek pro zakoupeni tohoto itemu."
	#define MSG_sucess_buy "Nakup byl dokoncen."
	#define MSG_have_item "Tento item jiz vlastnis."
};

enum item_count
{
	#define shirt_count 1
	#define mount_count 1
	#define hracky_count 1
};

enum counts
{
	#define event_mark_count 1
	#define shirt_em_count 70
	#define hracky_em_count 50
	#define spectral_tiger_em_count 200
	#define magic_rooster_egg_em_count 160
};

enum shirts
{
	#define tauren 100000
	#define taurenka 100001
	#define humanka 100002
	#define human 100003
	#define gnomka 100004
	#define gnom 100005
	#define blood_elfka 100006
	#define blood_elf 100007
};

enum hracky
{
	#define sylvanas_music_box 52253
	#define disco 38301
	#define muradins_favor 52201
};

enum mounti
{
	#define spectral_tiger 33225
	#define magic_rooster_egg 46778
};
#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Config.h"
#include <cstring>
#include "GuildMgr.h"
#include "ObjectMgr.h"
#include "Guild.h"

class guildmaster : public CreatureScript
{
    private: 
        #define MSG_GOSSIP_TELE          "Teleport to Guild House..."
        #define MSG_GOSSIP_BUY           "[BUY] Guild House"
        #define MSG_GOSSIP_SELL          "[SELL] Guild House"
        #define MSG_GOSSIP_NEXTPAGE      "Next page -->"
        #define MSG_GOSSIP_MENU          "<-- Back to Menu"
        #define MSG_INCOMBAT             "You are in combat and cannot be teleported to your Guild House."
        #define MSG_NOGUILDHOUSE         "Your guild currently does not own a Guild House."
        #define MSG_NOFREEGH             "Unfortunately, all Guild Houses are in use."
        #define MSG_ALREADYHAVEGH        "Sorry, but you already own a Guild House (%s)."
        #define MSG_NOTENOUGHMONEY       "You do not have the %u tokens required to purchase a Guild House."
        #define MSG_GHOCCUPIED           "This Guild House is unavailable for purchase as it is currently in use."
        #define MSG_CONGRATULATIONS      "Congratulations! You have successfully purchased a Guild House."
        #define MSG_SOLD                 "You have sold your Guild House and have received %u token."
        #define MSG_NOTINGUILD           "You need to be in a guild before you can use a Guild House."
        #define MSG_SELL_CONFIRM         "Are you sure you want to sell your Guild House for 1x [Guild House Token]?"
        #define MSG_BUY_CONFIRM_1        "Are you sure you want to buy GH in \""
        #define MSG_BUY_CONFIRM_2        "\"?"

        #define OFFSET_GH_ID_TO_ACTION 1500
        #define OFFSET_SHOWBUY_FROM 10000
    
        #define ACTION_TELE 1001
        #define ACTION_SHOW_BUYLIST 1002
        #define ACTION_SELL_GUILDHOUSE 1003
    
        #define ICON_GOSSIP_BALOON 0
        #define ICON_GOSSIP_WING 2
        #define ICON_GOSSIP_BOOK 3
        #define ICON_GOSSIP_WHEEL1 4
        #define ICON_GOSSIP_WHEEL2 5
        #define ICON_GOSSIP_GOLD 6
        #define ICON_GOSSIP_BALOONDOTS 7
        #define ICON_GOSSIP_TABARD 8
        #define ICON_GOSSIP_XSWORDS 9

        #define COST_BUY 2
        #define COST_SELL 1
        #define GUILD_TOKENN 28389 // GUILD TOKEN ID
    
        #define GOSSIP_COUNT_MAX 10

    public:
        guildmaster() : CreatureScript("guildmaster") {}

        bool isPlayerGuildLeader(Player* player)
        {
            return (player->GetRank() == 0) && (player->GetGuildId() != 0);
        }
        bool getGuildHouseCoords(uint32 guildId, float &x, float &y, float &z, uint32 &map)
        {
            if (guildId == 0)
            {
                //if player has no guild
                return false;
            }
            QueryResult result;
            result = CharacterDatabase.PQuery("SELECT `x`, `y`, `z`, `map` FROM `guild_house` WHERE `guildId` = %u", guildId);
            if (result)
            {
                Field *fields = result->Fetch();
                x = fields[0].GetFloat();
                y = fields[1].GetFloat();
                z = fields[2].GetFloat();
                map = fields[3].GetUInt32();
                return true;
            }
            return false;
        }
    
        void teleportPlayerToGuildHouse(Player* player, Creature* npc)
        {
            if (player->GetGuildId() == 0)
            {
                //if player has no guild
                npc->Whisper(MSG_NOTINGUILD, LANG_UNIVERSAL, player);
                return;
            }

            if (!player->getAttackers().empty())
            {
                //if player in combat
                npc->Say(MSG_INCOMBAT, LANG_UNIVERSAL, player);
                return;
            }
            Guild* guild = player->GetGuild();
            WorldLocation loc = guild->GetGuildHouseLoc();

            if (loc.m_positionZ == 0)
            {
                npc->Whisper(MSG_NOGUILDHOUSE, LANG_UNIVERSAL, player);
                return;
            }
            //teleport player to the specified location
            player->TeleportTo(loc.m_mapId, loc.m_positionX, loc.m_positionY, loc.m_positionZ, 0.0f);
        }
    
        bool showBuyList(Player* player, Creature* npc, uint32 showFromId = 0)
        {
            //show not used guild houses
            QueryResult result;
            result = CharacterDatabase.PQuery("SELECT `id`, `comment` FROM `guild_house` WHERE `guildId` = 0 AND `id` > %u ORDER BY `id` ASC LIMIT %u", showFromId, GOSSIP_COUNT_MAX);
            if (result)
            {
                uint32 guildhouseId = 0;
                std::string comment = "";
                do
                {
                    Field *fields = result->Fetch();
                    guildhouseId = fields[0].GetInt32();
                    comment = fields[1].GetString();
                    player->ADD_GOSSIP_ITEM_EXTENDED(ICON_GOSSIP_GOLD, comment, GOSSIP_SENDER_MAIN, guildhouseId + OFFSET_GH_ID_TO_ACTION, MSG_BUY_CONFIRM_1 + comment + MSG_BUY_CONFIRM_2, 0, false);
                }

                while (result->NextRow());
                {
                    if (result->GetRowCount() == GOSSIP_COUNT_MAX)
                    {
                        player->ADD_GOSSIP_ITEM(ICON_GOSSIP_BALOONDOTS, MSG_GOSSIP_NEXTPAGE, GOSSIP_SENDER_MAIN, guildhouseId + OFFSET_SHOWBUY_FROM);
                    }
                    player->ADD_GOSSIP_ITEM(ICON_GOSSIP_BALOON, MSG_GOSSIP_MENU, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, npc->GetGUID());
                    return true;
                }
            }

            else if (!result)
            {
                //all guild houses are used
                npc->Whisper(MSG_NOFREEGH, LANG_UNIVERSAL, player);
                player->CLOSE_GOSSIP_MENU();
            }
            else
            {
                //just show GHs from beginning
                showBuyList(player, npc, 0);
            }
            return false;
        }
    
        bool isPlayerHasGuildhouse(Player* player, Creature* npc, bool whisper = false)
        {
            if (Guild* guild = player->GetGuild())
            {
                if (guild->GetGuildHouseLoc().m_positionZ != 0)
                {
                    if (whisper)
                    {
                        //whisper to player "already have etc..."
                        npc->Whisper(MSG_ALREADYHAVEGH, LANG_UNIVERSAL, player);
                    }
                    return true;
                }
            }
            return false;
        }

        void buyGuildhouse(Player* player, Creature* npc, uint32 guildhouseId)
        {
            if (isPlayerHasGuildhouse(player, npc, true))
            {
                //player already have GH
                return;
            }

            if (player->HasItemCount(GUILD_TOKENN, COST_BUY))
            {
                player->DestroyItemCount(GUILD_TOKENN, COST_BUY, true);
                npc->Say(MSG_CONGRATULATIONS, LANG_UNIVERSAL, player);
                //update DB
                CharacterDatabase.PQuery("UPDATE `guild_house` SET `guildId` = %u WHERE `id` = %u", player->GetGuildId(), guildhouseId);

                float x,y,z;
                uint32 map;
                getGuildHouseCoords(player->GetGuildId(), x, y, z, map);
                player->GetGuild()->SetGuildHouseLoc(x,y,z,map);
            }

            else
            {
                //show how much money player need to buy GH (in tokens)
                char msg[100];
                sprintf(msg, MSG_NOTENOUGHMONEY, COST_BUY);
                npc->Whisper(msg, LANG_UNIVERSAL, player);
            }
            return;
        }
    
        void sellGuildhouse(Player* player, Creature* npc)
        {
            if (isPlayerHasGuildhouse(player, npc))
            {
                QueryResult result;
                result = CharacterDatabase.PQuery("UPDATE `guild_house` SET `guildId` = 0 WHERE `guildId` = %u", player->GetGuildId());
                player->AddItem(GUILD_TOKENN, COST_SELL);
                //display message e.g. "here your money etc."
                char msg[100];
                sprintf(msg, MSG_SOLD, COST_SELL);
                npc->Whisper(msg, LANG_UNIVERSAL, player);
                player->GetGuild()->SetGuildHouseLoc(0.0f,0.0f,0.0f,0);
            }
        }
    
        bool OnGossipSelect(Player* player, Creature* npc, uint32 sender, uint32 action)
        {
            player->PlayerTalkClass->ClearMenus();
            if (sender != GOSSIP_SENDER_MAIN)
            {
                return false;
            }
            switch (action)
            {
                case GOSSIP_ACTION_INFO_DEF: {
                    OnGossipHello(player, npc);
                    break;
                }

                case ACTION_TELE: {
                    //teleport player to GH
                    player->CLOSE_GOSSIP_MENU();
                    teleportPlayerToGuildHouse(player, npc);
                    break;
                }

                case ACTION_SHOW_BUYLIST: {
                    //show list of GHs which currently not used
                    showBuyList(player, npc);
                    break;
                }

                case ACTION_SELL_GUILDHOUSE: {
                    sellGuildhouse(player, npc);
                    player->CLOSE_GOSSIP_MENU();
                    break;
                }

            default:
                if (action > OFFSET_SHOWBUY_FROM)
                {
                    showBuyList(player, npc, action - OFFSET_SHOWBUY_FROM);
                }
                else if (action > OFFSET_GH_ID_TO_ACTION)
                {
                    //player clicked on buy list
                    player->CLOSE_GOSSIP_MENU();

                    //get guildhouseId from action
                    buyGuildhouse(player, npc, action - OFFSET_GH_ID_TO_ACTION);
                }
                break;
            }
            return true;
        }
        
        bool OnGossipHello(Player* player, Creature* npc)
        {
            player->ADD_GOSSIP_ITEM(ICON_GOSSIP_BALOON, MSG_GOSSIP_TELE, GOSSIP_SENDER_MAIN, ACTION_TELE);

            if (isPlayerGuildLeader(player))
            {
                if (isPlayerHasGuildhouse(player, npc))
                {
                    player->ADD_GOSSIP_ITEM_EXTENDED(ICON_GOSSIP_GOLD, MSG_GOSSIP_SELL, GOSSIP_SENDER_MAIN, ACTION_SELL_GUILDHOUSE, MSG_SELL_CONFIRM, 0, false);
                }
                else
                {
                    //show additional menu for guild leader
                    player->ADD_GOSSIP_ITEM(ICON_GOSSIP_GOLD, MSG_GOSSIP_BUY, GOSSIP_SENDER_MAIN, ACTION_SHOW_BUYLIST);
                }
            }
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, npc->GetGUID());
        return true;
        }
    };
void AddSC_guildmaster()
{
    new guildmaster();
}
/*##################
# npc_icc_rep_rings
###################*/

#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"

#define GMSG  537006
#define GMSNG 537008

enum AshenRings
{
    ASHEN_VERDICT = 1156,
    C_F = 50377,
    H_F = 50378,
    MS_F = 52569,
    MA_F = 50376,
    T_F = 50375,
    C_H = 50384,
    H_H = 50386,
    MS_H = 52570,
    MA_H = 50387,
    T_H = 50388,
    C_R = 50397,
    H_R = 50399,
    MS_R = 52571,
    MA_R = 50401,
    T_R = 50403,
    C_E = 50398,
    H_E = 50400,
    MS_E = 52572,
    MA_E = 50402,
    T_E = 50404,
};

const uint16 friendly[5] =
{
    C_F, H_F, MS_F, MA_F, T_F
};

const uint16 honored[5] =
{
    C_H, H_H, MS_H, MA_H, T_H
};

const uint16 revered[5] =
{
    C_R, H_R, MS_R, MA_R, T_R
};

const uint16 exalted[5] =
{
    C_E, H_E, MS_E, MA_E, T_E
};

class npc_argent_ring_restorer : public CreatureScript
{
public:
    npc_argent_ring_restorer() : CreatureScript("npc_argent_ring_restorer") { }

    void printHasItem(Player* player, Creature* creature)
    {
        player->SEND_GOSSIP_MENU(GMSNG, creature->GetGUID());
    }

    bool HasRingFitRank(Player* plr, uint8 rank)
    {
        switch(rank)
        {
        case REP_FRIENDLY:
            return HasRingFriendly(plr);
            break;
        case REP_HONORED:
            return HasRingHonored(plr);
            break;
        case REP_REVERED:
            return HasRingRevered(plr);
            break;
        case REP_EXALTED:
            return HasRingExalted(plr);
            break;
        default:
            return false;
        }
    }

    bool HasRingFriendly(Player* plr)
    {
        return HasRing(plr, friendly);
    }

    bool HasRingHonored(Player* plr)
    {
        if (HasRingFriendly(plr))
            return true;
        return HasRing(plr, honored);
    }

    bool HasRingRevered(Player* plr)
    {
        if (HasRingHonored(plr))
            return true;
        return HasRing(plr, revered);
    }

    bool HasRingExalted(Player* plr)
    {
        if (HasRingRevered(plr))
            return true;
        return HasRing(plr, exalted);
    }

    bool HasRing(Player* plr, const uint16 rings[])
    {
        for(uint8 i =0;i<5;++i)
        {
            uint16 ring = rings[i];
            if (plr->HasItemCount(ring, 1, true))
                return true;
        }
        return false;
    }

    void ShowRingMenu(Player* plr, Creature* npc, uint8 rank)
    {
        std::string rankText = " ring ";
        switch(rank)
        {
        case REP_FRIENDLY:
            rankText += "(friendly)";
            break;
        case REP_HONORED:
            rankText += "(honored)";
            break;
        case REP_REVERED:
            rankText += "(revered)";
            break;
        case REP_EXALTED:
            rankText = "(exalted)";
            break;
        default:
            return;
        }
        rank-=4;
        std::string txt = "Ring: DPS [speller] - " + rankText;
        plr->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt.c_str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + rank*5);
        txt = "Ring: HEALER - " + rankText;
        plr->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt.c_str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1 + rank*5);
        txt = "Ring: DPS [strength] - " + rankText;
        plr->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt.c_str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2 + rank*5);
        txt = "Ring: DPS [agility] - " + rankText;
        plr->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt.c_str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3 + rank*5);
        txt = "Ring: TANK - " + rankText;
        plr->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt.c_str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4 + rank*5);
        plr->SEND_GOSSIP_MENU(GMSG, npc->GetGUID());
    }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        ReputationRank rank = player->GetReputationRank(ASHEN_VERDICT);
        switch(rank)
        {
        case REP_FRIENDLY:
        case REP_HONORED:
        case REP_REVERED:
        case REP_EXALTED:
        {
            if (!HasRingExalted(player))
            {
                ShowRingMenu(player, creature, rank);
                return true;
            }
            else
                printHasItem(player, creature);
            break;
        }

        default:
            printHasItem(player, creature);
        }
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* /*creature*/, uint32 /*uiSender*/, uint32 uiAction)
    {
        if (player)
            player->PlayerTalkClass->ClearMenus();
        if (uiAction < GOSSIP_ACTION_INFO_DEF || uiAction > GOSSIP_ACTION_INFO_DEF + 20)
            return false;
        uiAction -= GOSSIP_ACTION_INFO_DEF;
        uint8 rank = (uiAction / 5) + 4;
        uint8 ring = uiAction % 5;

        switch(rank)
        {
        case REP_FRIENDLY:
            player->AddItem(friendly[ring], 1);
            break;
        case REP_HONORED:
            player->AddItem(honored[ring], 1);
            break;
        case REP_REVERED:
            player->AddItem(revered[ring], 1);
            break;
        case REP_EXALTED:
            player->AddItem(exalted[ring], 1);
            break;
        }
        player->CLOSE_GOSSIP_MENU();
        return true;
    }
};

void AddSC_npc_argent_ring_restorer()
{
    new npc_argent_ring_restorer();
}

#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"

class npc_casino : public CreatureScript
{
    uint32 transfer(const char *vstup)
    {
        uint32 i = 0;
        uint32 sum = 0;
        while (vstup[i] != 0)
        {
            if ((vstup[i] >= 48) && (vstup[i] <= 57))
            {
                sum *= 10;
                sum += vstup[i] - 48;
            }
            i += 1;
        }
        return sum;
    }


public:
    npc_casino() : CreatureScript("npc_casino") { }

    bool OnGossipHello(Player *player, Creature *_Creature) // Vstup
    {
        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "Vsadit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0, true);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Navod", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Odejit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
        player->SEND_GOSSIP_MENU(1, _Creature->GetGUID());
        return true;
    }


    bool OnGossipSelect(Player *player, Creature *_creature, uint32 /*sender*/, uint32 action)
    {
        player->PlayerTalkClass->ClearMenus();
        switch (action)
        {

        case GOSSIP_ACTION_INFO_DEF + 4: // Vstup fake
        {
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "Vsadit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0, true);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Navod", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Odejit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
            player->SEND_GOSSIP_MENU(1, _creature->GetGUID());
            break;
        }

        case GOSSIP_ACTION_INFO_DEF + 2: // Navod
        {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Zadate castku, kterou si chcete vsadit.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Minimalni sazka je 20g. Maximalni 5000g.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Cena jedne sazky je 1g.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Nasledne probehne losovani cisel od 1 do 100. Podle vylosovaneho cisla ziskate nasobek vasi sazky.", GOSSIP_SENDER_INFO, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Cisla               Vyhra", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "1-70             0x sazka", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "71-90            2x sazka", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "91-97            3x sazka", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "98-99            5x sazka", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "100             10x sazka", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Zpet", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
            player->SEND_GOSSIP_MENU(1, _creature->GetGUID());
            break;
        }

        case GOSSIP_ACTION_INFO_DEF + 3: // Leave
        {
            player->PlayerTalkClass->SendCloseGossip();
            break;
        }

        }
        return true;
    }

    bool OnGossipSelectCode(Player* player, Creature* me, uint32 /*uiSender*/, uint32 uiAction, const char* code)
    {
        if (uiAction != GOSSIP_ACTION_INFO_DEF + 1)
            return true;

        uint32 roll = 0;
        uint32 bet, curr;
        player->PlayerTalkClass->ClearMenus();
        bet = transfer(code);
        roll = urand(1, 100);
        curr = player->GetMoney();
        if (bet > 5000)
            bet = 5000;
        else if (bet < 20)
        {
            player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4, "Minimalni sazka je 20 goldu!", 0);
            player->SEND_GOSSIP_MENU(1, me->GetGUID());
            return true;
        }

        if (curr >= bet * 10000 + 10000)
        {
            if (roll < 70)
            { // 0x
                player->SetMoney(curr - 1 * 10000 * bet - 10000);
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Bohuzel, nic jste nevyhrali.", 0);
                player->SEND_GOSSIP_MENU(1, me->GetGUID());
                me->Whisper("Bohuzel, nic jsi nevyhral.", LANG_UNIVERSAL, player);
            }
            else if (roll < 90)
            { //2x
                player->SetMoney(curr + 1 * 10000 * bet - 10000);
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Vyhrali jste dvojnasobek.", 0);
                player->SEND_GOSSIP_MENU(1, me->GetGUID());
                me->Whisper("Gratuluji, vyhravas dvojnasobek sazky.", LANG_UNIVERSAL, player);
            }
            else if (roll <97)
            { // 3x
                player->SetMoney(curr + 2 * 10000 * bet - 10000);
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Vyhrali jste trojnasobek.", 0);
                player->SEND_GOSSIP_MENU(1, me->GetGUID());
                me->Whisper("Gratuluji, vyhravas trojnasobek sazky.", LANG_UNIVERSAL, player);
            }
            else if (roll < 99)
            { // 5x
                player->SetMoney(curr + 4 * 10000 * bet - 10000);
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Vyhrali jste petinasobek.", 0);
                player->SEND_GOSSIP_MENU(1, me->GetGUID());
                me->Whisper("Gratuluji, vyhravas petinasobek sazky.", LANG_UNIVERSAL, player);
            }
            else
            { // 10x
                player->SetMoney(curr + 9 * 10000 * bet - 10000);
                player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Gratulujeme, vyhrali jste desetinasobek.", 0);
                player->SEND_GOSSIP_MENU(1, me->GetGUID());
                me->Whisper("Gratuluji, vyhravas desetinasobek sazky.", LANG_UNIVERSAL, player);
            }
            player->PlayerTalkClass->SendCloseGossip();
        }
        else
        { //no money
            player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, GOSSIP_ICON_DOT, "", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4, "Nemate dostatek penez!", 0);
            player->SEND_GOSSIP_MENU(1, me->GetGUID());
        }
        return true;
    }
};

void AddSC_npc_casino()
{
    new npc_casino();
}
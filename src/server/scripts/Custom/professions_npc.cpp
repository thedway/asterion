#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Creature.h"
#include "Language.h"
#include "Chat.h"
 
class professions_npc : public CreatureScript
{
public:
    professions_npc () : CreatureScript("professions_npc") {}
           
    void CreatureWhisperBasedOnBool(const char *text, Creature *_creature, Player *pPlayer, bool value)
    {
    if (value) {
        _creature->TextEmote(text, pPlayer);
    }
}

public:
    uint32 PlayerMaxLevel() const
    {
        return sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL);
    }

    bool OnGossipHello(Player *pPlayer, Creature* _creature)
    {
        if (pPlayer->HasItemCount(22021, 1) || pPlayer->IsGameMaster()) {
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_alchemy:30|t Alchemy.", GOSSIP_SENDER_MAIN, 1);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Ingot_05:30|t Blacksmithing.", GOSSIP_SENDER_MAIN, 2);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_LeatherScrap_02:30|t Leatherworking.", GOSSIP_SENDER_MAIN, 3);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Fabric_Felcloth_Ebon:30|t Tailoring.", GOSSIP_SENDER_MAIN, 4);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_wrench_01:30|t Engineering.", GOSSIP_SENDER_MAIN, 5);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_engraving:30|t Enchanting.", GOSSIP_SENDER_MAIN, 6);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_gem_01:30|t Jewelcrafting.", GOSSIP_SENDER_MAIN, 7);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Scroll_08:30|t Inscription.", GOSSIP_SENDER_MAIN, 8);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Bandage_Frostweave_Heavy:30|t First Aid.", GOSSIP_SENDER_MAIN, 13);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Herb_07:30|t Herbalism.", GOSSIP_SENDER_MAIN, 9);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_pelt_wolf_01:30|t Skinning.", GOSSIP_SENDER_MAIN, 10);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_mining:30|t Mining.", GOSSIP_SENDER_MAIN, 11);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\Achievement_Profession_Fishing_OutlandAngler:30|t Fishing.", GOSSIP_SENDER_MAIN, 14);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Cauldron_Fire:30|t Cooking.", GOSSIP_SENDER_MAIN, 15);
                pPlayer->PlayerTalkClass->SendGossipMenu(1, _creature->GetGUID());
        }
        else {
            pPlayer->ADD_GOSSIP_ITEM(3, "I dont have enough tokens", GOSSIP_SENDER_MAIN, 12);
        }
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "[Close]", GOSSIP_SENDER_MAIN, 12);
        pPlayer->PlayerTalkClass->SendGossipMenu(907, _creature->GetGUID());
        return true;
    }
           
    bool PlayerAlreadyHasTwoProfessions(const Player *pPlayer) const
    {
        uint32 skillCount = 0;

        if (pPlayer->HasSkill(SKILL_MINING)) {
            skillCount++;
        }
        if (pPlayer->HasSkill(SKILL_SKINNING)) {
            skillCount++;
        }
        if (pPlayer->HasSkill(SKILL_HERBALISM)) {
            skillCount++;
        }
        if (skillCount >= sWorld->getIntConfig(CONFIG_MAX_PRIMARY_TRADE_SKILL)) {
            return true;
            }
        for (uint32 i = 1; i < sSkillLineStore.GetNumRows(); ++i)
        {
            SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(i);
            if (!SkillInfo) {
                continue;
                }
            if (SkillInfo->categoryId == SKILL_CATEGORY_SECONDARY) {
                continue;
                }
            if ((SkillInfo->categoryId != SKILL_CATEGORY_PROFESSION) || !SkillInfo->canLink) {
                continue;
                }
            const uint32 skillID = SkillInfo->id;
            if (pPlayer->HasSkill(skillID)) {
                skillCount++;
                }
            if (skillCount >= sWorld->getIntConfig(CONFIG_MAX_PRIMARY_TRADE_SKILL)) {
                return true;
            }
        }
        return false;
    }

    bool LearnAllRecipesInProfession(Player *pPlayer, SkillType skill)
    {
        ChatHandler handler(pPlayer->GetSession());
        char* skill_name;

        SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(skill);
        skill_name = SkillInfo->name[handler.GetSessionDbcLocale()]; 

        LearnSkillRecipesHelper(pPlayer, SkillInfo->id);

        pPlayer->SetSkill(SkillInfo->id, pPlayer->GetSkillStep(SkillInfo->id), 450, 450);
        handler.PSendSysMessage(LANG_COMMAND_LEARN_ALL_RECIPES, skill_name);
           
        return true;
    }
   
    void LearnSkillRecipesHelper(Player *player, uint32 skill_id)
    {
        uint32 classmask = player->getClassMask();
        for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j)
        {
            SkillLineAbilityEntry const *skillLine = sSkillLineAbilityStore.LookupEntry(j);
            if (!skillLine) {
                continue;
            }
            // wrong skill
            if (skillLine->skillId != skill_id) {
                continue;
            }
            // not high rank
            if (skillLine->forward_spellid) {
                continue;
            }
            // skip racial skills
            if (skillLine->racemask != 0) {
                continue;
            }
            // skip wrong class skills
            if (skillLine->classmask && (skillLine->classmask & classmask) == 0) {
                continue;
            }
            SpellInfo const * spellInfo = sSpellMgr->GetSpellInfo(skillLine->spellId);
            if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player, false)) {
                continue;
            }
            player->LearnSpell(skillLine->spellId, false);
        }
    }

    bool IsSecondarySkill(SkillType skill) const {
        return skill == SKILL_COOKING || skill == SKILL_FIRST_AID || skill == SKILL_FISHING;
    }
    void CompleteLearnProfession(Player *pPlayer, Creature *pCreature, SkillType skill)
    {
        if (PlayerAlreadyHasTwoProfessions(pPlayer) && !IsSecondarySkill(skill)) {
            pCreature->Whisper("You already know four professions!", LANG_UNIVERSAL, pPlayer);
        }
        else
        {
            if (!LearnAllRecipesInProfession(pPlayer, skill)) {
                pCreature->Whisper("Internal error occured!", LANG_UNIVERSAL, pPlayer);
            }
        }
    }
   
    bool OnGossipSelect(Player* pPlayer, Creature* _creature, uint32 uiSender, uint32 uiAction)
    {
        pPlayer->PlayerTalkClass->ClearMenus();
        if (uiSender == GOSSIP_SENDER_MAIN)
        {
            switch (uiAction)
            {
                // ALCHEMY
                case 1: {
                    if(pPlayer->HasSkill(SKILL_ALCHEMY))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_ALCHEMY);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // BLACKSMITHING
                case 2: {
                    if(pPlayer->HasSkill(SKILL_BLACKSMITHING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_BLACKSMITHING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // LEATHERWORKING
                case 3: {
                    if(pPlayer->HasSkill(SKILL_LEATHERWORKING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_LEATHERWORKING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // TAILORING
                case 4: {
                    if(pPlayer->HasSkill(SKILL_TAILORING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_TAILORING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // ENGINEERING
                case 5: {
                    if(pPlayer->HasSkill(SKILL_ENGINEERING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_ENGINEERING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // ENCHANTING
                case 6: {
                    if(pPlayer->HasSkill(SKILL_ENCHANTING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_ENCHANTING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // JEWELCRAFTING
                case 7: {
                    if(pPlayer->HasSkill(SKILL_JEWELCRAFTING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_JEWELCRAFTING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // INSCRIPTION
                case 8: {
                    if(pPlayer->HasSkill(SKILL_INSCRIPTION))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_INSCRIPTION);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // HERBALISM
                case 9: {
                    if(pPlayer->HasSkill(SKILL_HERBALISM))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }
                    CompleteLearnProfession(pPlayer, _creature, SKILL_HERBALISM);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // SKINNING
                case 10: {
                    if(pPlayer->HasSkill(SKILL_SKINNING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }           
                    CompleteLearnProfession(pPlayer, _creature, SKILL_SKINNING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // MINING
                case 11: {
                    if(pPlayer->HasSkill(SKILL_MINING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }

                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }           
                    CompleteLearnProfession(pPlayer, _creature, SKILL_MINING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // FIRST AID
                case 13: {
                    if(pPlayer->HasSkill(SKILL_FIRST_AID))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                    
                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }           
                    CompleteLearnProfession(pPlayer, _creature, SKILL_FIRST_AID);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // Fishing
                case 14: {
                    if(pPlayer->HasSkill(SKILL_FISHING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                    
                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }           
                    CompleteLearnProfession(pPlayer, _creature, SKILL_FISHING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // Cooking
                case 15: {
                    if(pPlayer->HasSkill(SKILL_COOKING))
                    {
                        pPlayer->PlayerTalkClass->SendCloseGossip();
                        break;
                    }
                    
                    else if(!pPlayer->IsGameMaster()) {
                        pPlayer->DestroyItemCount(22021, 1, true);
                    }           
                    CompleteLearnProfession(pPlayer, _creature, SKILL_COOKING);
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }

                // NEVERMIND
                case 12: {
                    pPlayer->PlayerTalkClass->SendCloseGossip();
                    break;
                }
            }
        }
        return true;
    }
};
void AddSC_professions_npc()
{
    new professions_npc();
}
